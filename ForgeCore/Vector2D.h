#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
#include <ostream>

#define TAU (2 * M_PI)

class Vector2D
{
public:
	float X, Y;

	Vector2D();
	Vector2D(const float &_x, const float &_y);
	Vector2D(const float &_v);

	~Vector2D() = default;

	void SetX(const float &x)																					{ X = x; }
	float GetX() 																								const { return X; }

	void SetY(const float &y)																					{ Y = y; }
	float GetY()																								const { return Y; }
	
	static float RadiansToDegrees(const float &rads)															{ return rads * (360.f / TAU); }

	static float DegreesToRadians(const float &deg)																{ return deg * (TAU / 360.f); }

	static float Length(const Vector2D &v);

	static float Dot(const Vector2D &v, const Vector2D &u);

	static float AngleBetweenVectors(const Vector2D &v, const Vector2D &u, const bool inDegrees = false);

	static Vector2D Normalize(const Vector2D &v); 

	static Vector2D Perpendicular(const Vector2D &v);

	void Zero();

	void ToPositives();

	void Truncate(const float &max);

	float Distance(const Vector2D &v) const;

	Vector2D Reverse() const;

	// Adds vectors
	Vector2D operator+(const Vector2D &v) const
	{
		return Vector2D(X + v.X, Y + v.Y);
	}

	Vector2D &operator+=(const Vector2D &v)
	{
		*this = *this + v;

		return *this;
	}

	// Subtract vectors
	Vector2D operator-(const Vector2D &v) const
	{
		return Vector2D(X - v.X, Y - v.Y);
	}

	Vector2D &operator-=(const Vector2D &v)
	{
		*this = *this - v;

		return *this;
	}

	// Multiply vectors
	Vector2D operator*(const Vector2D &v) const
	{
		return Vector2D(X * v.X, Y * v.Y);
	}

	Vector2D &operator*=(const Vector2D &v)
	{
		*this = *this * v;

		return *this;
	}

	// Multiply vector by a scalar
	Vector2D operator*(const float &f) const
	{
		return Vector2D(X * f, Y * f);
	}

	Vector2D &operator*=(const float &f)
	{
		*this = *this * f;

		return *this;
	}

	// Divides vectors
	Vector2D operator/(const Vector2D &v) const
	{
		return Vector2D(X / v.X, Y / v.Y);
	}

	Vector2D &operator/=(const Vector2D &v)
	{
		*this = *this / v;

		return *this;
	}

	// Divides vector by a scalar
	Vector2D operator/(const float &f) const
	{
		return Vector2D(X / f, Y / f);
	}

	Vector2D &operator/=(const float &f)
	{
		*this = *this / f;

		return *this;
	}

	// Makes vectors equal to one another
	Vector2D operator=(const Vector2D &v)
	{
		this->X = v.X;
		this->Y = v.Y;

		return *this;
	}

	// Checks if two vectors are equal
	bool operator==(const Vector2D &v) const
	{
		return this->X == v.X && this->Y == v.Y;
	}

	// Checks if two vectors are not equal
	bool operator!=(const Vector2D &v) const
	{
		return !(this->X == v.X && this->Y == v.Y);
	}

	friend std::ostream &operator<<(std::ostream &os, const Vector2D &v)
	{
		return os << "X: " << v.X << " Y: " << v.Y;
	}
};

