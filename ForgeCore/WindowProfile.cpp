#include "WindowProfile.h"

namespace forge
{
	namespace sys
	{
		WindowProfile::WindowProfile(HWND handle, UINT clientWidth, UINT clientHeight, bool fullscreen)
		{
			WindowHandle	= handle;
			ClientHeight	= clientHeight;
			ClientWidth		= clientWidth;
			Fullscreen		= fullscreen;
		}

		WindowProfile::~WindowProfile() = default;
	}
}
