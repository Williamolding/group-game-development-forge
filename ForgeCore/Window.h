#pragma once
#include <Windows.h>
#include "IRenderTarget.h"
#include "WindowProfile.h"
#include <functional>

namespace forge
{
	namespace sys
	{
		enum WindowState
		{
			UNKNOWN,
			UNINITALISED,
			ACTIVE,
			CLOSING,
			CLOSED
		};

		struct WindowSettings
		{
			unsigned short	Width;
			unsigned short	Height;
			const char*		Title;
			DWORD			Style;
		};

		class Window : graphics::IRenderTarget
		{
			unsigned short	_width;
			unsigned short	_height;
			const char*		_title;
			DWORD			_style;		
			HINSTANCE		_instance;
			RECT			_renderRect;
			HWND			_handle;

			WindowState		_windowState;

		public:
			std::function<void(int, int)> ResizeCallback;
		public:
			explicit Window(WindowSettings settings);
			~Window();

			LRESULT MsgProc(HWND window, UINT message, WPARAM w_parameters, LPARAM l_parameters);

			void ProcessEvents();
			void Clear();
			bool IsClosed() override;
			void Initalise() override;
			void Show();
			WindowProfile GetProfile();
		};
	}
}

