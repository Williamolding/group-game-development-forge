#pragma once
#include "fmod_studio.hpp"
#include "fmod.hpp"
#include <string>
#include <map>
#include <vector>
#include "../Include/fmod_studio_common.h"

class FmodSystem
{
public:

	FMOD::Studio::System* mpStudioSystem;
	FMOD::System* mpSystem;

	int mnNextChannelId;

	typedef std::map<std::string, FMOD::Sound*> SoundMap;
	typedef std::map<int, FMOD::Channel*> ChannelMap;
	typedef std::map<std::string, FMOD::Studio::EventInstance*> EventMap;
	typedef std::map<std::string, FMOD::Studio::Bank*> BankMap;
	BankMap mBanks;
	EventMap mEvents;
	SoundMap mSounds;
	ChannelMap mChannels;
	bool FmodMasterMute;
	double fmodMasterVolume;
	//static FmodSystem& getInstance()
	//{
		//static FmodSystem    instance;
		//return instance;
	//}
	FmodSystem() 
	{
		mpStudioSystem = NULL;
		FMOD::Studio::System::create(&mpStudioSystem);
		mpStudioSystem->initialize(32, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, NULL);
		mpSystem = NULL;
		mpStudioSystem->getLowLevelSystem(&mpSystem);
		FmodMasterMute = false;
		fmodMasterVolume = 1;
	}                 
	~FmodSystem() {
		mpStudioSystem->release();
		mChannels.clear();
		mSounds.clear();		
	}
	
};
