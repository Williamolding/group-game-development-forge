#pragma once

namespace forge
{
	namespace graphics
	{
		class IRenderTarget
		{
		public:
			virtual ~IRenderTarget() = default;
			virtual void Initalise() = 0;
			virtual bool IsClosed() = 0;
		};
	}
}