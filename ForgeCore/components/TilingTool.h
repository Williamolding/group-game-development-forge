#pragma once
#include "IComponent.h"
#include <vector>
#include <string>
#include "entity/IEntityManager.h"

namespace forge
{
	namespace component
	{
		class TilingTool : public IComponent
		{
			int                         _width;
			int                         _height;
			component::Transform*       _transform;
			bool overrideRectCollider = true;

		public:
			//int							Width;
			//int							Height;


		public:
			TilingTool(int w = 1, int h = 1)
			{
				_width = w;
				_height = h;
			}
			~TilingTool() override = default;

			void Initialise() override
			{
				_transform = Entity->getComponent<component::Transform>();
			}
			void Update(double deltatime) override
			{

			}

			void UpdateChildren()
			{
			}

			int GetHeight() const { return _height; }
			void SetHeight(const int &h)
			{
				for (int i = 0; i < _transform->_children.size(); i++)
				{
					_transform->_children.at(i)->Entity->Destroy();
				}
				_transform->_children.clear();
				Entity->GetManager().refresh();

				_height = h;

				CreateChildren();
			}

			int GetWidth() const { return _width; }
			void SetWidth(const int &w)
			{
				for (int i = 0; i < _transform->_children.size(); i++)
				{
					_transform->_children.at(i)->Entity->Destroy();
				}
				_transform->_children.clear();
				Entity->GetManager().refresh();

				_width = w;

				CreateChildren();
			}

			void SetOverride(bool value)
			{
				overrideRectCollider = value;
			}

			bool GetOverride()
			{
				return overrideRectCollider;
			}

			void CreateChildren()
			{
				for (int i = 0; i < _transform->_children.size(); i++)
				{
					_transform->_children.at(i)->Entity->Destroy();
				}
				_transform->_children.clear();
				Entity->GetManager().refresh();

				for (int i = 0; i < _width; i++)
				{
					for (int j = 0; j < _height; j++)
					{
						if (!(i == 0 && j == 0))
						{
							entity::Entity* child = &Entity->GetManager().addEntity(Entity->GetName() + "_TT" + std::to_string(i + (_width*j)));

							DirectX::XMFLOAT3 toolPos = Entity->getComponent<component::Transform>()->GetPosition();

							auto meshData = Entity->getComponent<SpriteRenderer>()->GetMesh();
							auto textureData = Entity->getComponent<SpriteRenderer>()->GetTexture();
							child->addComponent<SpriteRenderer>(meshData, textureData, 64);
							auto spriteSize = child->getComponent<SpriteRenderer>()->SpriteSize;

							child->addComponent<component::Transform>(DirectX::XMFLOAT3(i * spriteSize, j * spriteSize, toolPos.z));
							child->getComponent<Transform>()->SetParent(Entity);
						}
					}
				}

				if (overrideRectCollider && Entity->hasComponent<RectCollider>())
				{
					RectCollider* rectCollider = Entity->getComponent<RectCollider>();

					rectCollider->SetWidth(_width * (Entity->getComponent<SpriteRenderer>()->SpriteSize / 2.0f));
					rectCollider->SetHeight(_height * (Entity->getComponent<SpriteRenderer>()->SpriteSize / 2.0f));

					Vector2D v = Vector2D(0,0);
					v.X = (_width - 1) * (Entity->getComponent<SpriteRenderer>()->SpriteSize / 2.0f);
					v.Y = (_height - 1) * (Entity->getComponent<SpriteRenderer>()->SpriteSize / 2.0f);

					rectCollider->SetOffset(v);
				}
			}
		};
	}
}
