#pragma once

#include <map>
#include "Transform.h"
#include "Vector2D.h"
#include "physics/Forces/GravityForce.h"
#include "physics/Forces/FrictionForce.h"
#include "physics/Forces/PushForce.h"

namespace forge
{
	namespace component
	{
		class Particle : public IComponent
		{
		private:
			Transform * _transform;

			Vector2D acceleration;
			Vector2D velocity;
			Vector2D netForce;

			float mass; // kilograms
			float coefficientOfRestitution;

			bool isTerminalVelocityOn;
			bool beingPushed;

			std::map<std::string, physics::ForceGenerator*> forcesMap;

		private:
			void UpdateNetForce(const double deltatime);
			void UpdateAcceleration();
			void Move(const double deltatime) const;
			void UpdateVelocity(const double deltatime);

			float CalculateTerminalVelocity() const;

		public:
			Particle(Transform *_trans);

			~Particle() override;		

			void Enable()
			{
				Enabled = true;
			}

			void Disable()
			{
				Enabled = false;
			}

			void Initialise() override;

			Transform *GetTransform() const															{ return _transform; }

			void SetVelocity(const Vector2D &v) 													{ velocity = v; }
			void SetVelocity(const float &x, const float &y) 										{ velocity = Vector2D(x, y); }
			void SetLuaVelocity(const float &x, const float &y)										{ SetVelocity(x, y); }
			float GetSpeed() const																	{ return Vector2D::Length(velocity); }
			Vector2D GetVelocity() const															{ return velocity; }

			void SetAcceleration(const Vector2D &v) 												{ acceleration = v; }
			void SetAcceleration(const float &x, const float &y) 									{ acceleration = Vector2D(x, y); }
			void SetLuaAcceleration(const float &x, const float &y)									{ SetAcceleration(x, y); }
			float AccelerationMagnitude() const														{ return Vector2D::Length(acceleration); }
			Vector2D GetAcceleration() const														{ return acceleration; }

			void SetMass(const float &m) 															{ mass = m; }
			float GetMass() const																	{ return mass; }

			void SetCoefficientOfRestitution(const float &cor) 										{ if (cor <= 1.f && cor >= 0.f) coefficientOfRestitution = cor; }
			float GetCoefficientOfRestitution() const												{ return  coefficientOfRestitution; }

			void SetBeingPushed(const bool &bp) 													{ beingPushed = bp; }
			bool BeingPushed() const																{ return beingPushed; }

			void SetTerminalVelocity(const bool &tv)												{ isTerminalVelocityOn = tv; }
			void KillTerminalVelocity()																{ isTerminalVelocityOn = false; }
			bool IsTerminalVelocityOn() const														{ return isTerminalVelocityOn; }

			std::map<std::string, physics::ForceGenerator*> GetForcesList() const					{ return forcesMap; }
			physics::GravityForce* GetGravityForceGenerator() const									{ return dynamic_cast<physics::GravityForce*>(forcesMap.at("gravity")); }
			physics::FrictionForce* GetFrictionGenerator() const									{ return dynamic_cast<physics::FrictionForce*>(forcesMap.at("friction")); }
			physics::PushForce* GetPushForceGenerator() const										{ return forcesMap.find("push") != forcesMap.end() ? dynamic_cast<physics::PushForce*>(forcesMap.at("push")) : nullptr; }

			void AddPushForce()																		{ AddGenerator("push", new physics::PushForce()); }

			void AddForce(const Vector2D &f);
			void LuaAddForce(const float &x, const float &y);
			void AddGenerator(const std::string &id, physics::ForceGenerator *fg);

			void Update(double deltaTime) override;
		};
	}
}
