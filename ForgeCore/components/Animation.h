#pragma once
#include "IComponent.h"
#include "SpriteRenderer.h"
#include "graphics/AnimationRect.h"

namespace forge
{
	namespace component
	{
		class Animation: public IComponent
		{
			SpriteRenderer*				_spriteRenderer;
			graphics::AnimationRect*	_animationRect;
			int							_currentFrame;
			int							_previousFrame;
			int							_frameCount;
			double						_animationSpeed;
			double						_animationClock;
			double						_fps;
			double						_rawFps;
			bool						_stopped;
		public:
			Animation(float fps, float speed)
			{
				_rawFps				= fps;
				_fps				= (1000.0f / fps) / 1000.0f;
				_animationSpeed		= speed;
				_stopped			= false;
			}
			void Initialise() override
			{
				_spriteRenderer = Entity->getComponent<SpriteRenderer>();
				_frameCount = _spriteRenderer->Width / _spriteRenderer->Height;
				_animationRect	= new graphics::AnimationRect(_spriteRenderer->Width, _spriteRenderer->Height, _frameCount);
				_animationRect->BuildFrames();
				_currentFrame = 0;
				_animationClock = 0;	

				UpdateVerticies(_animationRect->GetFrame(_currentFrame));
			}
			void Update(double deltatime) override
			{
				if (_stopped)
					return;

				_animationClock += _animationSpeed * deltatime;

				if(_animationClock > _fps)
				{
					_animationClock = 0;
					_currentFrame++;
					UpdateVerticies(_animationRect->GetFrame(_currentFrame));

					if (_currentFrame >= _frameCount)
						_currentFrame = 0;		
				}
			}
			~Animation() override
			{
				
			}

			void Stop()
			{
				_currentFrame = 0;
				UpdateVerticies(_animationRect->GetFrame(_currentFrame));
				_stopped = true;
			}

			void Start()
			{
				_stopped = false;
			}

			std::vector<SpriteVertex> GetVerticies()
			{
				return  _spriteRenderer->GetMesh().Verticies;
			}

			void SetFps(float fps) 
			{
				_rawFps = fps;
				_fps = (1000.0f / fps) / 1000.0f;
			}

			void SetSpeed(float speed)
			{
				_animationSpeed = speed;
			}

			double GetFps() const
			{
				return _rawFps;
			}
			double GetSpeed() const
			{
				return _animationSpeed;
			}

		private:
			void UpdateVerticies(graphics::Rect renderRect)
			{
				auto width		= _spriteRenderer->GetMesh().Width;
				auto height		= _spriteRenderer->GetMesh().Height;
				auto layer		= _spriteRenderer->GetLayer();

				auto left = width / 2 * -1;
				auto right = left + width;
				auto top = height / 2;
				auto bottom = top - height;

				auto colour = _spriteRenderer->GetColour();
				_spriteRenderer->GetMesh().Verticies = std::vector<SpriteVertex>() = {
					SpriteVertex(left,top,layer,renderRect.Left,renderRect.Top,colour.R,colour.G,colour.B),
					SpriteVertex(right,bottom,layer,renderRect.Right,renderRect.Bottom,colour.R ,colour.G,colour.B),
					SpriteVertex(left,bottom,layer,renderRect.Left,renderRect.Bottom,colour.R,colour.G,colour.B),
					SpriteVertex(left,top,layer,renderRect.Left,renderRect.Top,colour.R,colour.G,colour.B),
					SpriteVertex(right,top,layer,renderRect.Right,renderRect.Top,colour.R,colour.G,colour.B),
					SpriteVertex(right,bottom,layer,renderRect.Right,renderRect.Bottom,colour.R,colour.G,colour.B)
				};
			}
		};
	}
}
