#pragma once
#include "IComponent.h"
#include "FmodSystem.h"
#include "asset/AssetManager.h"

namespace forge 
{
	namespace component
	{
		class SoundEmitter : public IComponent
		{
			asset::Audio _audio;
			FmodSystem* _instance;
			bool playing = false;
		public:
		
			int nChannelId = NULL;
			SoundEmitter(asset::Audio audio, FmodSystem* fmodsystem) : _audio(audio)
			{
				_instance = fmodsystem;

			}
			void Initialise() override
			{
				
			}

			void Update(double deltatime) override
			{
				if (_audio.PlayOnLoad && !playing) {
					PlaySounds();
					playing = true;
				}

				//Run every game loop
				vector<FmodSystem::ChannelMap::iterator> pStoppedChannels;

				//iterates over channesl and if it not currently playing adds it the the stopped channels vector so it can be cleand up later(line 41)
				for (auto it = _instance->mChannels.begin(), itEnd = _instance->mChannels.end(); it != itEnd; ++it)
				{
					bool bIsPlaying = false;
					it->second->isPlaying(&bIsPlaying);
					if (!bIsPlaying)
					{
						pStoppedChannels.push_back(it);

					}
				}
				FMOD::Channel* pChannel = _instance->mChannels[nChannelId];
				bool bIsPlaying = false;
				pChannel->isPlaying(&bIsPlaying);
				if (!bIsPlaying) {
					nChannelId = NULL;
				}
				//cleans up the channels in stopped channels vector so we can use it for other sounds 
				for (auto& it : pStoppedChannels)
				{					
					if (_instance->mChannels.size() >= 0) {						
						_instance->mChannels.erase(it);
					}
				}
				//Calls the update for the fmod system
				_instance->mpStudioSystem->update();
			}


			void PlaySounds()
			{
				nChannelId = _instance->mnNextChannelId++;
				FMOD::Channel* pChannel = nullptr;
				_instance->mpSystem->playSound(_audio.Sound, nullptr, true, &pChannel);
				if (pChannel)
				{
					//apply the paramenter that were set when we loaded the sound, Looping and volume
					FMOD_MODE currMode;
					_audio.Sound->getMode(&currMode);
					if (_audio.volume > _instance->fmodMasterVolume) {
						pChannel->setVolume(_instance->fmodMasterVolume);
					}
					else {
						pChannel->setVolume(_audio.volume);
					}
					pChannel->setPitch(_audio.pitch);
					//Once parameter are set we unpause the sound and it plays
					if (!_instance->FmodMasterMute) {
						pChannel->setPaused(false);
						//we add the created channel to the channel Map 
						_instance->mChannels[nChannelId] = pChannel;
					}


				}
			}
				//toggles the sound between paused and unpaused when it is playing within a channel
				void TogglePauseSound() {
					if (nChannelId) {
						FMOD::Channel* pChannel = _instance->mChannels[nChannelId];
						bool isPaused;
						pChannel->getPaused(&isPaused);
						if (isPaused) {
							_instance->mChannels[nChannelId]->setPaused(false);
						}
						else {
							_instance->mChannels[nChannelId]->setPaused(true);
						}
					}					
				};

				//returns ture or false depdning on if a sound is playing within a channel
				bool IsPlaying() {
					if (nChannelId) {
						FMOD::Channel* pChannel = _instance->mChannels[nChannelId];
						bool AudioIsPlaying;
						pChannel->isPlaying(&AudioIsPlaying);
						return AudioIsPlaying;
					} else {
						return false;
					}
				};

				//set the volume of sound playing within a channel and updates componets stored volume
				void SetChannelVolume(float fVolumedB)
				{
					if (nChannelId) {
						_audio.volume = fVolumedB;
						auto tFoundIt = _instance->mChannels.find(nChannelId);
						if (_audio.volume > _instance->fmodMasterVolume) {
							tFoundIt->second->setVolume(_instance->fmodMasterVolume);
						}
						else {
							tFoundIt->second->setVolume(_audio.volume);
						}
					}					
				}

				//Gets the current volume of a sound playing within a channel not the one stored in component 
				float GetChannelVolume()
				{
					if (nChannelId) {
						auto tFoundIt = _instance->mChannels.find(nChannelId);
						float channelVolume;
						tFoundIt->second->getVolume(&channelVolume);
						return channelVolume;
					}
					else {
						return 0.0f;
					}
				}

				//set the pitch of sound playing within a channel and updates componets stored pitch
				void SetChannelPitch(float pitch)
				{
					if (nChannelId) {
						_audio.pitch = pitch;
						auto tFoundIt = _instance->mChannels.find(nChannelId);
						tFoundIt->second->setPitch(_audio.pitch);
					}
				}

				//Gets the current pitch of a sound playing within a channel not the one stored in component 
				float GetChannelPitch()
				{
					if (nChannelId) {
						auto tFoundIt = _instance->mChannels.find(nChannelId);
						float channelPitch;
						tFoundIt->second->getPitch(&channelPitch);
						return channelPitch;
					}
					else {
						return 0.0f;
					}
				}

				asset::Audio& GetAudio()
				{
					return _audio;
				}

				~SoundEmitter() override = default;
			};
	}
}