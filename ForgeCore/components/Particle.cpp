#include "Particle.h"

namespace forge
{
	namespace component
	{
		Particle::Particle(Transform *_trans) : _transform(_trans)
		{
			Particle::Initialise();
		}

		void Particle::Initialise()
		{			
			coefficientOfRestitution = 0.50f;

			mass = 100.0f;

			velocity = Vector2D();
			acceleration = Vector2D();
			netForce = Vector2D();

			beingPushed = false;		
			isTerminalVelocityOn = true;

			forcesMap.clear();

			forcesMap.insert(std::pair<std::string, physics::ForceGenerator*>("gravity", new physics::GravityForce(-9.81)));
			forcesMap.insert(std::pair<std::string, physics::ForceGenerator*>("friction", new physics::FrictionForce()));
		}

		Particle::~Particle()
		{
			for (auto force : forcesMap)
			{
				delete force.second;
				force.second = nullptr;
			}
			forcesMap.clear();
		}

		void Particle::UpdateNetForce(const double deltatime)
		{
			for (auto fg : forcesMap)
				fg.second->Update(this);
		}

		void Particle::UpdateAcceleration()
		{
			acceleration = netForce / mass;
		}

		void Particle::Move(const double deltatime) const
		{
			Vector2D pos = _transform->GetPositionV2D();

			pos += velocity * deltatime + acceleration * deltatime * deltatime * .5f;

			_transform->SetPositionV2D(pos);
		}

		void Particle::UpdateVelocity(const double deltatime)
		{
			velocity += acceleration * deltatime;
			velocity.X *= 0.98f;

			if (GetSpeed() < 0.001f)
				velocity.Zero();

			if (isTerminalVelocityOn)
			{
				const auto tvValue = CalculateTerminalVelocity();

				if (fabsf(velocity.Y) > tvValue)
					velocity.Y = velocity.Y < 0 ? -tvValue : tvValue;
			}
		}

		void Particle::AddForce(const Vector2D &f)
		{
			netForce += f;
		}

		void Particle::LuaAddForce(const float &x,  const float &y)
		{
			AddForce(Vector2D(x, y));
		}

		void Particle::AddGenerator(const std::string &id, physics::ForceGenerator *fg)
		{
			forcesMap.insert(std::pair<std::string, physics::ForceGenerator*>(id, fg));
		}

		float Particle::CalculateTerminalVelocity() const
		{
			const float airDensity = 1.225f;

			const float doubleWeight = 2.f * fabsf(mass * dynamic_cast<physics::GravityForce*>(forcesMap.at("gravity"))->GetRealGravity().Y);

			const float airResistance = dynamic_cast<physics::FrictionForce*>(forcesMap.at("friction"))->GetCoF() * airDensity;

			// Approximate calculation for terminal velocity
			return sqrtf(doubleWeight / airResistance);
		}
		
		void Particle::Update(double deltaTime)
		{
			if (mass <= 0.f)
				return;

			deltaTime = deltaTime > 1.f / 60.f ? 1.f / 60.f : deltaTime;

			// Must be in this order
			UpdateNetForce(deltaTime);
			UpdateAcceleration();
			Move(deltaTime);
			UpdateVelocity(deltaTime);

			acceleration.Zero();
			netForce.Zero();
		}
	}
}
