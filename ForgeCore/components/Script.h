#pragma once
#include "IComponent.h"
#include <lua.hpp>
#include "Transform.h"

namespace forge
{
	namespace component
	{
		class Script : public IComponent
		{
			std::string		_filename;
			std::string		_path;
		public:
			lua_State*		_luaState;
		
			Script(std::string path, std::string filename) : _filename(filename), _path(path)
			{
				
			}
			void Initialise() override
			{

			}
			void Update(double deltatime) override
			{
				try
				{
					auto lookupName = _filename + Entity->GetName();

					lua_getfield(_luaState, LUA_REGISTRYINDEX, lookupName.c_str());
					lua_getfield(_luaState, -1, "Update");
					lua_pushnumber(_luaState, deltatime);
					lua_call(_luaState, 1, 0);
				}
				catch(...)
				{
					//Add error Code Here
				}		
			}

			void CallOnCollision()
			{
				try
				{
					auto lookupName = _filename + Entity->GetName();

					lua_getfield(_luaState, LUA_REGISTRYINDEX, lookupName.c_str());
					lua_getfield(_luaState, -1, "OnCollision");
					lua_call(_luaState, 0, 0);
				}
				catch (...)
				{
					//Add error Code Here
				}

			}

			void OnTriggerEnter()
			{
				try
				{
					auto lookupName = _filename + Entity->GetName();

					lua_getfield(_luaState, LUA_REGISTRYINDEX, lookupName.c_str());
					lua_getfield(_luaState, -1, "OnTriggerEnter");
					lua_call(_luaState, 0, 0);
				}
				catch (...)
				{
					//Add error Code Here
				}
			}

			void OnTriggerExit()
			{
				try
				{
					auto lookupName = _filename + Entity->GetName();

					lua_getfield(_luaState, LUA_REGISTRYINDEX, lookupName.c_str());
					lua_getfield(_luaState, -1, "OnTriggerExit");
					lua_call(_luaState, 0, 0);
				}
				catch (...)
				{
					//Add error Code Here
				}
			}

			void SetLuaState(lua_State* state)
			{
				_luaState = state;
			}

			std::string GetFilename()
			{
				return _filename;
			}

			std::string GetPath()
			{
				return _path;
			}
		};
	}
}
