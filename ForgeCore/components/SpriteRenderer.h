#pragma once
#include <components/IComponent.h>
#include "asset/AssetManager.h"
#include "graphics/TextureSamplerFactory.h"
#include <Colour.h>
namespace forge
{
	namespace component
	{
		class SpriteRenderer : public IComponent
		{	
			asset::SpriteMesh			_mesh;
			asset::SpriteTexture		_texture;
			graphics::TextureSampler	_samplerType;
			render::Colour				_colour;
			int							_layer;

		public:
			int							Width;
			int							Height;
			float                       SpriteSize;
			

		public:
			SpriteRenderer(asset::SpriteMesh mesh, asset::SpriteTexture texture, float spriteSize) : _mesh(mesh), _texture(texture), SpriteSize(spriteSize), _samplerType(graphics::CLAMPED), _colour(render::Colour())
			{
				Width	= texture.Width;
				Height	= texture.Height;
				_layer = 0;
			}
			~SpriteRenderer() override = default;

			void Initialise() override
			{
				
			}
			void Update(double deltatime) override
			{
				
			}

			void SetSprite(asset::SpriteTexture texture)
			{
				_texture = texture;
				Width = texture.Width;
				Height = texture.Height;
			}

			void SetColour(render::Colour colour)
			{
				_colour = colour;
			}

			void SetLayer(int layer)
			{
				_layer = layer;

				for(auto &vertex : _mesh.Verticies)
				{
					vertex.position.z = static_cast<float>(_layer);
				}
			}

			int GetLayer() const
			{
				return _layer;
			}

			asset::SpriteMesh& GetMesh()
			{
				for (auto &vertex : _mesh.Verticies)
				{
					vertex.colour.x = static_cast<float>(_colour.R);
					vertex.colour.y = static_cast<float>(_colour.G);
					vertex.colour.z = static_cast<float>(_colour.B);
				}

				return _mesh;			
			}

			asset::SpriteMesh& GetDebugMesh()
			{
				auto colour = render::Colour(0.92,0.60,0.25);
				for (auto &vertex : _mesh.Verticies)
				{
					vertex.colour.x = static_cast<float>(colour.R);
					vertex.colour.y = static_cast<float>(colour.G);
					vertex.colour.z = static_cast<float>(colour.B);
				}

				return _mesh;
			}

			asset::SpriteTexture& GetTexture()
			{
				return _texture;
			}

			render::Colour& GetColour()
			{
				return _colour;
			}
	
		};
	}
}
