#pragma once
#include "../IComponent.h"
#include "iCollider.h"
#include "asset/AssetManager.h"

namespace forge
{
	namespace component
	{
		class RectCollider final : public iCollider, public IComponent
		{
		public:		
			RectCollider(const float w, const float h, asset::QuadMesh *quadMesh, const Vector2D &offset = 0.0f) :
				iCollider(offset), width(w), height(h), _quadMesh(quadMesh)
			{
				RectCollider::Initialise();
			}

			// Inherited via IComponent
			void Initialise() override
			{}

			asset::QuadMesh& GetQuadData() const
			{
				const auto left			= offset.X - width;
				const auto right		= left + width * 2;
				const auto top			= height + offset.Y;
				const auto bottom		= top - height * 2;
				const auto layer		= -1000;

				auto defaultColour = DirectX::XMFLOAT3(0, 255, 0);

				_quadMesh->Verticies = std::vector<SpriteVertex>() = {
					SpriteVertex(left,top,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
					SpriteVertex(right,top,layer,1,0,defaultColour.x,defaultColour.y,defaultColour.z),
					SpriteVertex(right,bottom,layer,0,0,defaultColour.x,defaultColour.y,defaultColour.z),
					SpriteVertex(left,bottom,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
					SpriteVertex(left,top,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
				};

				return *_quadMesh;
			}

			~RectCollider() override = default;

			float GetHeight() const																								{ return height; }
			void SetHeight(const float h)																						{ height = h; }

			float GetWidth() const																								{ return width; }
			void SetWidth(const float w)																						{ width = w; }

			Vector2D GetDimensions() const																						{ return Vector2D(width, height); }
			void SetDimensions(const float x, const float y)																	{ width = x; height = y; }
			void SetLuaDimensions(const float x, const float y)																	{ SetDimensions(x, y); }
			void SetDimensions(const Vector2D &v)																				{ width = v.X; height = v.Y; }

			void Update(double deltatime) override
			{}
		private:
			float width;
			float height;

			asset::QuadMesh *_quadMesh;
		};
	}
}