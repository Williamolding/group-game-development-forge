#pragma once
#include "iCollider.h"
#include "components/IComponent.h"
#include "asset/AssetManager.h"

namespace forge
{
	namespace component
	{
		class CircleCollider final : public iCollider, public IComponent
		{
		public:
			CircleCollider(const float radius, asset::CircleMesh mesh, const Vector2D &offset = 0.0f) : iCollider(offset), _radius(radius), _mesh(std::move(mesh))
			{ }

			// Inherited via IComponent
			void Initialise() override
			{ }

			asset::CircleMesh GetMesh()
			{	
				_mesh.Radius = int(_radius);
				_mesh.Verticies.clear();

				const auto layer = -1001;

				const auto defaultColour = DirectX::XMFLOAT3(0, 1, 0);

				int index = 0;
				std::vector<unsigned int> indices = std::vector<unsigned int>();
				
				for (double angle = 0.0; angle <= TAU; angle += TAU / _mesh.Segments) {
					_mesh.Verticies.emplace_back(SpriteVertex((cos(angle) * _radius) + offset.X, (-sin(angle) * _radius) + offset.Y, layer, 0, 0, defaultColour.x, defaultColour.y, defaultColour.z));
					indices.push_back(index);
					index++;
				}

				return _mesh;
			}

			void Update(double deltatime) override 
			{}

			~CircleCollider() override = default;

			void SetRadius(const float &r)																					{ _radius = r; }
			float GetRadius()const																							{ return _radius; }
			float GetDiameter() const																						{ return 2 * _radius; }
		
		private:
			float _radius;

			asset::CircleMesh _mesh;
		};
	}
}
