#pragma once
#include <utility>
#include "Vector2D.h"

namespace forge
{
	namespace component
	{
		class iCollider
		{
		protected:
			bool isTrigger;

			Vector2D offset;

		public:
			bool isTriggerColliding;
			bool isColliding;

		public:
			iCollider(const Vector2D& offset) : offset(offset)
			{
				isTrigger = false;
				isTriggerColliding = false;
				isColliding = false;
			}

			Vector2D GetOffset() const																							{ return offset; }
			void SetOffset(const Vector2D &off)																					{ offset = off; }

			bool IsTrigger() const																								{ return isTrigger; }
			void SetTrigger(const bool it)																						{ isTrigger = it; }

			bool LuaIsTriggerColliding() const																					{ return isTriggerColliding; }
			bool LuaIsColliding() const																							{ return isColliding; }
		};
	}
}
