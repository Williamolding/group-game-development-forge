#pragma once
#include <DirectXMath.h>
#include <vector>
#include "IComponent.h"
#include "../Vector2D.h"
#include "../entity/Entity.h"

namespace forge
{
	namespace component
	{
		class Vector3
		{
		public:
			double X;
			double Y;
			double Z;

			Vector3() = default;
			Vector3(DirectX::XMFLOAT3 vector)
			{
				X = vector.x;
				Y = vector.y;
				Z = vector.z;
			}

			double GetX() const
			{
				return X;
			}
			void SetX(double x)
			{
				X = x;
			}

			double GetY() const
			{
				return Y;
			}
			void SetY(double x)
			{
				Y = x;
			}

			double GetZ() const
			{
				return Z;
			}
			void SetZ(double x)
			{
				Z = x;
			}

			DirectX::XMFLOAT3 Extract()
			{
				return DirectX::XMFLOAT3(X,Y,Z);
			}
		};

		class Transform : public IComponent
		{
			DirectX::XMFLOAT4X4	_worldRotation;
			DirectX::XMFLOAT4X4	_worldPosition;
			DirectX::XMFLOAT4X4	_worldScale;
			DirectX::XMFLOAT4X4 _worldMatrix;
			DirectX::XMFLOAT4X4	_returnMatrix;
			DirectX::XMFLOAT4X4	_staticRotation;
		public:
			DirectX::XMFLOAT4X4 _additionalRotation;
			DirectX::XMFLOAT3	_rotation;
			DirectX::XMFLOAT3	_Position;
			DirectX::XMFLOAT3	_scale;
			DirectX::XMFLOAT3	_Facing;
			DirectX::XMFLOAT3	_SpawnVector;

			float RotationX;
			float RotationY;
			float RotationZ;

			Transform*				_parentTransform;
			std::string				_potentialParentName;
			std::vector<Transform*> _children;
		public:
			Transform(DirectX::XMFLOAT3 position) : _Position(position)
			{
				XMStoreFloat4x4(&_worldMatrix, DirectX::XMMatrixIdentity());
				XMStoreFloat4x4(&_worldRotation, DirectX::XMMatrixIdentity());
				XMStoreFloat4x4(&_worldPosition, DirectX::XMMatrixIdentity());
				XMStoreFloat4x4(&_staticRotation, DirectX::XMMatrixIdentity());
				XMStoreFloat4x4(&_additionalRotation, DirectX::XMMatrixIdentity());
				XMStoreFloat4x4(&_worldScale, DirectX::XMMatrixIdentity());
				CreateWorldMatrix();
				_scale = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f);
				
			}

			~Transform() override = default;

			void Initialise() override
			{

			}

			void Update(double deltatime) override
			{

			}

			void SetLayer(float l)
			{
				_Position.z = l;		
			}
			float GetLayer()
			{
				return 	_Position.z;
			}

			void SetRotationX(float angle)
			{
				RotationX = angle;
				SetRotation(DirectX::XMFLOAT3(1, 0, 0), RotationX);
			}

			float getRoationX()
			{
				return RotationX;
			}

			void SetRotationY(float angle)
			{
				RotationY = angle;
				SetRotation(DirectX::XMFLOAT3(0, 1, 0), RotationY);
			}

			float getRoationY()
			{
				return RotationY;
			}

			void SetRotationZ(float angle)
			{
				RotationZ = angle;
				SetRotation(DirectX::XMFLOAT3(0, 0, 1), RotationZ);
			}

			float getRoationZ()
			{
				return RotationZ;
			}

			bool HasParent()
			{
				return _parentTransform != nullptr;
			}
			Transform* GetParent()
			{
				return _parentTransform;
			}

			void SetXmlParent(std::string name)
			{
				_potentialParentName = name;
			}
			DirectX::XMFLOAT3 GetWorldPosition()
			{
				Extract();

				DirectX::XMVECTOR pos;
				DirectX::XMVECTOR sca;
				DirectX::XMVECTOR rot;
				auto x = DirectX::XMMatrixDecompose(&sca, &rot,&pos, DirectX::XMLoadFloat4x4(&_returnMatrix));

				DirectX::XMFLOAT3 returnPos;
				DirectX::XMStoreFloat3(&returnPos, pos);
				return returnPos;
			}

			void luaSetPosition(Vector3 vector) const
			{
			 	const_cast<DirectX::XMFLOAT3&>(_Position) = vector.Extract();
			}

			DirectX::XMFLOAT3 GetRotation() const
			{
				return _rotation;
			}

			Vector3 luaGetPosition() const
			{
				return {_Position};
			}

			Vector2D GetPositionV2D() const
			{
				return Vector2D(_Position.x, _Position.y);
			}

			void SetPositionV2D(const Vector2D &v) const
			{
				const_cast<DirectX::XMFLOAT3&>(_Position) = DirectX::XMFLOAT3(v.X, v.Y, _Position.z);
			}

			Vector2D GetScaleV2D() const
			{
				return Vector2D(_scale.x, _scale.y);
			}

			void SetScaleV2D(const Vector2D &v) const
			{
				const_cast<DirectX::XMFLOAT3&>(_scale) = DirectX::XMFLOAT3(v.X, v.Y, _scale.z);
			}

			void CreateWorldMatrix()
			{
				XMStoreFloat4x4(&_worldPosition, DirectX::XMMatrixTranslation(_Position.x, _Position.y, _Position.z));
				XMStoreFloat4x4(&_worldScale, DirectX::XMMatrixScaling(_scale.x, _scale.y, _scale.z));
				XMStoreFloat4x4(&_worldRotation, DirectX::XMLoadFloat4x4(&_staticRotation));
				XMStoreFloat4x4(&_worldMatrix, DirectX::XMLoadFloat4x4(&_worldScale) * DirectX::XMLoadFloat4x4(&_worldPosition)  * DirectX::XMLoadFloat4x4(&_worldRotation));
			}

			DirectX::XMFLOAT4X4 Extract()
			{
				CreateWorldMatrix();
				std::vector<Transform*> transforms;
				recursiveOrientation(this, transforms);
			
				auto returnM = DirectX::XMMatrixMultiply(DirectX::XMMatrixScaling(_scale.x, _scale.y, _scale.z), DirectX::XMLoadFloat4x4(&_returnMatrix));
				DirectX::XMStoreFloat4x4(&_returnMatrix, returnM);

				returnM = DirectX::XMMatrixMultiply(DirectX::XMLoadFloat4x4(&_staticRotation), DirectX::XMLoadFloat4x4(&_returnMatrix));
				DirectX::XMStoreFloat4x4(&_returnMatrix, returnM);

				return _returnMatrix;
			}

			void FlipLeft()
			{
				SetRotation(DirectX::XMFLOAT3(0, 1, 0), 180);
			}

			void FlipRight()
			{
				SetRotation(DirectX::XMFLOAT3(0, 1, 0), 0);
			}

			 void SetScale(DirectX::XMFLOAT3 p_scale)
			{
				_scale = p_scale;
			}

			void SetPosition(DirectX::XMFLOAT3 p_position)
			{
				_Position = p_position;
			}

			void Translate(DirectX::XMFLOAT3 p_Translate)
			{
				_Position = DirectX::XMFLOAT3(_Position.x + p_Translate.x , _Position.y + p_Translate.y , _Position.z + p_Translate.z);
			}

			void SetRotation(DirectX::XMFLOAT3 rotation)
			{
				_rotation = rotation;
			}

			void SetRotation(DirectX::XMFLOAT3 p_axis, float value)
			{
				DirectX::XMVECTOR m_rotationAxis = XMLoadFloat3(&p_axis);
				DirectX::XMStoreFloat4x4(&_staticRotation, DirectX::XMMatrixRotationAxis(m_rotationAxis, DirectX::XMConvertToRadians(value)));
			}
			void RemoveParent()
			{
				std::vector<Transform*>::iterator i = std::find(_parentTransform->_children.begin(), _parentTransform->_children.end(), this);

				if (i != _parentTransform->_children.end())
				{
						_parentTransform->_children.erase(i);
						_parentTransform = nullptr;
				}
			}
			void SetParent(entity::Entity* p_entity)
			{
				Transform* transform = p_entity->getComponent<Transform>();

				if (this != transform)
				{
					transform->_children.push_back(this);
					_parentTransform = transform;
				}
			}

			DirectX::XMFLOAT3 GetPosition()
			{
				return _Position;
			}

			DirectX::XMFLOAT3* GetPositonP()
			{
				return &_Position;
			}

			DirectX::XMFLOAT3 GetScale()
			{
				return _scale;
			}

			private:
				void recursiveOrientation(Transform* transform, std::vector<Transform*> Components)
				{
					Components.push_back(transform);
					if (transform->_parentTransform)
					{					
						recursiveOrientation(transform->_parentTransform, Components);
					}
					if (transform->_parentTransform == nullptr)
					{
						DirectX::XMMATRIX World = DirectX::XMMatrixIdentity();

						for (size_t i = 0; i < Components.size(); i++)
						{						
							Components.at(i)->CreateWorldMatrix();
							DirectX::XMMATRIX WT = DirectX::XMLoadFloat4x4(&Components.at(i)->_worldPosition);
							World = XMMatrixMultiply(World, WT);
						}

						DirectX::XMFLOAT4X4 orientation;
						XMStoreFloat4x4(&orientation, World);
						_returnMatrix = orientation;
					}
				}
		};
	}
}
