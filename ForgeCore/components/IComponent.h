#pragma once
#include <entity/EntityConstants.h>

namespace forge
{
	namespace component
	{
		class IComponent
		{
		public:
			bool Enabled = true;
			bool Remove = false;
			entity::Entity* Entity;
		public:
			virtual void Initialise() = 0;
			virtual void Update(double deltatime) = 0;
			virtual ~IComponent() = default;
		};
	}
}
