#pragma once
#include <chrono>
namespace forge
{
	namespace time
	{
		template<class precision = std::chrono::milliseconds> class CoreClock
		{
		public:
			using TimerType = std::chrono::high_resolution_clock;
			CoreClock() = default;
			~CoreClock() = default;
			static double GetTickTime()
			{
				_lastTime = _currentTime;
				_currentTime = TimerType::now();

				const double CoreClockTickTime = std::chrono::duration_cast<std::chrono::duration<double>>(_currentTime - _lastTime).count();
				return CoreClockTickTime;
			}
			static double GetCoreTime()
			{
				const double CoreClockTime = std::chrono::duration_cast<std::chrono::duration<float>>(TimerType::now() - _startTime).count();
				return CoreClockTime;
			}
		private:
			static TimerType::time_point _lastTime;
			static TimerType::time_point _currentTime;
			static TimerType::time_point _startTime;

		};
		std::chrono::high_resolution_clock::time_point CoreClock<>::_lastTime = TimerType::now();
		std::chrono::high_resolution_clock::time_point CoreClock<>::_currentTime = TimerType::now();
		std::chrono::high_resolution_clock::time_point CoreClock<>::_startTime = TimerType::now();
	}
}
