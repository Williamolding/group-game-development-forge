#pragma once

#include <functional>
#include <graphics/Graphics.h>
#include <input/Mouse.h>
#include <input/Keyboard.h>
#include <IGameInstance.h>
#include <chrono>
#include <CoreClock.h>
#include <entity/EditorEntityManager.h>

#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>
#include "lua/LuaLoader.h"
#include "lua/LuaSystem.h"

#include "SceneManager.h"
#include "asset/VirtualFileSystem.h"
#include "input/InputManager.h"
#include "asset/AssetManager.h"
#include "FmodSystem.h"

#include "serialisation/XmlSerialiserSystem.h"

namespace forge
{
	namespace game
	{
		enum State
		{
			RUNNING,
			PAUSED
		};

		template<typename entityManagerType>
		class Game : public IGameInstance
		{		
			graphics::Graphics*							_graphics;
			SceneManager*								_sceneManager;
			entity::IEntityManager*						_entityManager;
			graphics::Camera*							_camera;
			asset::AssetManager*						_assetManager;
			graphics::Dx11Driver*						_driver;
			input::XboxController*						_controller;
			lua::LuaSystem*								_luaSystem;
			bool										_closing = false;
			bool										_paused = false;
			bool										_editor = false;
			State										_engineState = RUNNING;
			FmodSystem *								_FmodSystem;
		public:	
			std::function<bool()>		ClosingCallback;
			std::function<void()>		ProcessEventsCallback;

		public:
			Game(graphics::Dx11Driver* driver) : _driver(driver)
			{
			}

			~Game() override
			{		
				delete _sceneManager;
				delete _camera;
				delete _assetManager;
				delete _controller;
				delete _graphics;
				delete _luaSystem;
				delete _FmodSystem;

				_entityManager->Clear();
				xml::XmlSerialiserSystem::Clear();
				input::InputManager::Clear();
			}

			void Initalise(std::string &path, const bool Editor) override
			{
				_driver->Initalise();
				_editor = Editor;
				
				asset::VirtualFileSystem*	virtualFileSystem	= new asset::VirtualFileSystem(path);
				asset::MemoryFileSystem*	memoryFileSystem	= new asset::MemoryFileSystem("coreAssets.dat");

				_luaSystem = new lua::LuaSystem();
				const auto luaState = _luaSystem->GetState();
				
				_entityManager	= new entityManagerType();
				_graphics		= new graphics::Graphics(_driver, _entityManager, memoryFileSystem);
				_FmodSystem		= new FmodSystem;
				_assetManager	= new asset::AssetManager(_driver->_graphicsDevice, (float)_driver->_clientWidth, (float)_driver->_clientHeight, virtualFileSystem, luaState, _FmodSystem);
				_sceneManager	= new SceneManager(_entityManager, _assetManager);
			
				_controller		= new input::XboxController();

				_luaSystem->LoadBindings(_entityManager, _sceneManager);

				xml::XmlSerialiserSystem::RegisterAllModules();
				
				_camera = new graphics::Camera((float)_driver->_clientWidth, (float)_driver->_clientHeight);
				_graphics->SetMainCamera(_camera);

				_entityManager->SetCamera(_camera);
	
				input::InputManager::Initialise();

				const auto result = _graphics->Initalise();
				_sceneManager->Initialise();

				if (!result)
				{
					printf("Driver Failed To Initialise");
				}
			}

			bool Run() override
			{
				while(!_closing)
				{
					auto deltaTime = time::CoreClock<>::GetTickTime();

					if (ProcessEventsCallback)
						ProcessEventsCallback();

					switch(_engineState)
					{
					case RUNNING:
						_paused = false;
						Update(deltaTime);
						break;
					case PAUSED:
						_paused = true;
						break;
					}

					if (ClosingCallback)
						_closing = ClosingCallback();
				}

				return true;
			}

			void Resize(int width, int height) override
			{
				_graphics->Resize(width, height);
			}

			void Stop() override
			{
				_closing = true;
			}

			void Pause() override
			{
				_engineState = PAUSED;

				while(!_paused)
				{
					Sleep(10);
				}
			}
			void Resume() override
			{
				_engineState = RUNNING;

				while (_paused)
				{
					Sleep(10);
				}
			}

		private:
			void Update(double deltatime)
			{
				if (input::XboxController::IsConnected())
					input::XboxController::Update();

				_entityManager->Update(_luaSystem->GetState(), deltatime);
				_sceneManager->Update(_luaSystem, deltatime, !_editor);		

				input::XboxController::ClearControllerState();
				input::Keyboard::ClearKeyBuffer();
				input::Mouse::ClearMouseBuffer();

				_graphics->Render(_editor);

				_luaSystem->Collect();
			}
		public:
			WrapperPackage ExportWrapperPackage() override
			{
				auto package = WrapperPackage();
				package.EntityManager = _entityManager;
				package.SceneManager = _sceneManager;
				package.AssetManager = _assetManager;
				package.Graphics = _graphics;
				//Add other objects to expose here
				return package;
			}		
		};
	}
}
