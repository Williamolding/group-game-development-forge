#pragma once
#include <Windows.h>

namespace forge
{
	namespace sys
	{
		class WindowProfile
		{
		public:
			HWND WindowHandle;
			UINT ClientWidth;
			UINT ClientHeight;
			bool Fullscreen;
		public:
			WindowProfile() = default;
			WindowProfile(HWND p_handle, UINT p_clientWidth, UINT p_clientHeight, bool p_fullscreen);
			~WindowProfile();
		};
	}
}

