#include "Graphics.h"
#include <graphics/Dx11/Dx11Driver.h>
#include <graphics/Dx11/Dx11ShaderCompiler.h>
#include <graphics/Dx11/Dx11ShaderLoader.h>
#include <graphics/Dx11/Dx11ShaderLayoutFactory.h>
#include <graphics/Dx11/Dx11BufferUploader.h>
#include <graphics/Dx11/Dx11TextureSampler.h>
#include <graphics/Dx11/Dx11BlendState.h>
#include <graphics/image/TgaLoader.h>
#include "SpriteVertex.h"
#include "SpriteConstantBuffer.h"
#include "input/Keyboard.h"
#include "components/SpriteRenderer.h"
#include "components/Transform.h"
#include "components/Animation.h"
#include "components/Colliders/RectCollider.h"
#include "components/Colliders/CircleCollider.h"


namespace forge
{
	namespace graphics
	{
		Graphics::Graphics(Dx11Driver* driver, entity::IEntityManager* entityManager, asset::MemoryFileSystem* memoryfileSystem) : 
							_driver(driver), _entityManager(entityManager), _memoryFileSystem(memoryfileSystem)
		{

		}

		Graphics::~Graphics()
		{
			delete _shaderLoader;
			 _defaultInputLayout->Release();
			_defaultVertexShader->Release();
			_defaultPixelShader->Release();
			_blendState->Release();
			delete _textureSamplerFactory;
			delete _memoryFileSystem;
		}

		bool Graphics::Initalise()
		{
			const auto shadercompilerSettings	= new Dx11ShaderCompileSettings();
			const auto shaderCompiler			= new Dx11ShaderCompiler(shadercompilerSettings);
			const auto shaderLayoutFactory		= new Dx11ShaderLayoutFactory(_driver);
			const auto shaderDevice				= new Dx11ShaderDevice(_driver);
			_shaderLoader						= new Dx11ShaderLoader(shaderCompiler,shaderLayoutFactory, shaderDevice);
			_bufferMapper						= new Dx11BufferMapper();

			std::string vs = "VertexShader.hlsl";
			std::string ps = "PixelShader.hlsl";
			std::string dvs = "DebugVertexShader.hlsl";
			std::string dps = "DebugPixelShader.hlsl";

			const auto defaultVertexShaderFile	= _memoryFileSystem->GetFile(vs);
			const auto defaultPixelShaderFile	= _memoryFileSystem->GetFile(ps);
			const auto debugVertexShaderFile	= _memoryFileSystem->GetFile(dvs);
			const auto debugPixelShaderFile		= _memoryFileSystem->GetFile(dps);

			const auto VertexShader			= _shaderLoader->LoadShaderFromMemory<ID3D11VertexShader>(ShaderType::VERTEXSHADER,	defaultVertexShaderFile);
			const auto PixelShader			= _shaderLoader->LoadShaderFromMemory<ID3D11PixelShader>(ShaderType::PIXELSHADER,	defaultPixelShaderFile);
			const auto DebugVertexShader	= _shaderLoader->LoadShaderFromMemory<ID3D11VertexShader>(ShaderType::VERTEXSHADER, debugVertexShaderFile);
			const auto DebughPixelShader	= _shaderLoader->LoadShaderFromMemory<ID3D11PixelShader>(ShaderType::PIXELSHADER, debugPixelShaderFile);

			if (!VertexShader.ShaderState || !PixelShader.ShaderState)
				return false;

			if (!DebugVertexShader.ShaderState || !DebughPixelShader.ShaderState)
				return false;

			_defaultInputLayout		= VertexShader.InputLayout;
			_defaultVertexShader	= VertexShader.ShaderPtr;
			_defaultPixelShader		= PixelShader.ShaderPtr;
			_debugVertexShader		= DebugVertexShader.ShaderPtr;
			_debugPixelShader		= DebughPixelShader.ShaderPtr;

			_textureSamplerFactory = new TextureSamplerFactory(_driver->_graphicsDevice);
			_textureSamplerFactory->BuildSamplerIndex();

			auto blendState = Dx11BlendState();
			auto blendStateResult = blendState.CreateBlendState(_driver->_graphicsDevice);

			_blendState = blendStateResult.BlendState;
			_driver->_ImmediateContext->OMSetBlendState(_blendState, 0, 0xffffffff);

			return true;
		}

		void Graphics::Render(bool debug)
		{
			 auto context = _driver->BeginFrame();

			 for(auto const& entity : _entityManager->entities)
			 {	
				 if(!entity->hasComponent<component::SpriteRenderer>() || !entity->hasComponent<component::Transform>())
					 continue;

				 auto spriteRenderer	= entity->getComponent<component::SpriteRenderer>();
				 auto transform			= entity->getComponent<component::Transform>();

				 auto worldMatrix		= transform->Extract();

				 DirectX::XMMATRIX world	= DirectX::XMLoadFloat4x4(&worldMatrix);
				 DirectX::XMMATRIX view		= _camera->GetViewMatrix();
				 DirectX::XMMATRIX ortho	= _camera->GetOrthoMatrix();

				 unsigned int stride = sizeof(SpriteVertex);
				 unsigned int offset = 0;

				 SpriteConstantBuffer spriteConstantBuffer;
				 spriteConstantBuffer.World			= DirectX::XMMatrixTranspose(world);
				 spriteConstantBuffer.View			= DirectX::XMMatrixTranspose(view);
				 spriteConstantBuffer.Projection	= DirectX::XMMatrixTranspose(ortho);

				 auto meshData = spriteRenderer->GetMesh();
	
				 _bufferMapper->MapVertexBuffer<SpriteVertex>(meshData.VertexBuffer, meshData.Verticies, meshData.VertexCount, context);
				 
				 auto constantBuffer	= meshData.ConstantBuffer;
				 auto indexBuffer		= meshData.IndexBuffer;
				 auto vertexBuffer		= meshData.VertexBuffer;

				 auto texture			= spriteRenderer->GetTexture();
				 auto textureView		= texture.TextureView;
				 auto textureSampler	= _textureSamplerFactory->GetSampler(CLAMPED);
				 auto rasterState		= _driver->_rasterState;


				 context->RSSetState(rasterState);
				 context->GenerateMips(textureView);
				 context->IASetInputLayout(_defaultInputLayout);
				 context->UpdateSubresource(constantBuffer, 0, nullptr, &spriteConstantBuffer, 0, 0);
				 context->VSSetShader(_defaultVertexShader, nullptr, 0);
				 context->PSSetShader(_defaultPixelShader, nullptr, 0);
				 context->VSSetConstantBuffers(0, 1, &constantBuffer);
				 context->PSSetConstantBuffers(0, 1, &constantBuffer);
				 context->PSSetShaderResources(0, 1, &textureView);
				 context->PSSetSamplers(0, 1, &textureSampler);

				 context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
				 context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
				 context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
				 context->DrawIndexed(meshData.VertexCount, 0, 0);

				 if(_entityManager->GetSelectedEntity() == entity)
				 {
					 meshData = spriteRenderer->GetDebugMesh();
					 _bufferMapper->MapVertexBuffer<SpriteVertex>(meshData.VertexBuffer, meshData.Verticies, meshData.VertexCount, context);

					 context->VSSetShader(_debugVertexShader, nullptr, 0);
					 context->PSSetShader(_debugPixelShader, nullptr, 0);
					 context->RSSetState(_driver->_rasterStateDebug);
					 context->DrawIndexed(meshData.VertexCount, 0, 0);

					 if(entity->hasComponent<component::RectCollider>())
					 {
						 auto rectangleCollider = entity->getComponent<component::RectCollider>();
						 auto mesh = rectangleCollider->GetQuadData();

						 _bufferMapper->MapVertexBuffer<SpriteVertex>(mesh.VertexBuffer, mesh.Verticies, mesh.VertexCount, context);

						 indexBuffer = mesh.IndexBuffer;
						 vertexBuffer = mesh.VertexBuffer;

						 context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
						 context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
						 context->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
						 context->DrawIndexed(mesh.VertexCount, 0, 0);
					 }

					 if (entity->hasComponent<component::CircleCollider>())
					 {
						 auto circleCollider = entity->getComponent<component::CircleCollider>();
						 auto mesh = circleCollider->GetMesh();

						 _bufferMapper->MapVertexBuffer<SpriteVertex>(mesh.VertexBuffer, mesh.Verticies, mesh.VertexCount, context);

						 indexBuffer = mesh.IndexBuffer;
						 vertexBuffer = mesh.VertexBuffer;

						 context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
						 context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
						 context->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
						 context->DrawIndexed(mesh.VertexCount, 0, 0);
					 }
				 }
			 }

			_driver->EndFrame();
		}

		void Graphics::Resize(int width, int height)
		{
			_driver->ResizeViewport(width, height);

			if(_camera)
			{
				//_camera->Resize(width, height);
			}
		}

		void Graphics::SetMainCamera(Camera* camera)
		{
			_camera = camera;
		}

		Camera* Graphics::GetMainCamera()
		{
			return _camera;
		}

		DirectX::XMFLOAT2 Graphics::ScreenToWorldSpacePosition(DirectX::XMFLOAT2 mousePos)
		{
			DirectX::XMMATRIX view = _camera->GetViewMatrix();
			DirectX::XMMATRIX ortho = _camera->GetOrthoMatrix();

			DirectX::XMMATRIX invProjectionView = DirectX::XMMatrixInverse(&DirectX::XMMatrixDeterminant(view *  ortho), (view *  ortho));
			//invViewProjection = invView * invProjection;

			float x = (((2.0f * mousePos.x) / _driver->_clientWidth) - 1);
			float y = -(((2.0f *  mousePos.y) / _driver->_clientHeight) - 1);

			DirectX::XMVECTOR mousePosition = DirectX::XMVectorSet(x, y, 1.0f, 0.0f);

			auto mouseInWorldSpace = DirectX::XMVector3Transform(mousePosition, invProjectionView);

			DirectX::XMFLOAT2 worldpos;
			DirectX::XMStoreFloat2(&worldpos, mouseInWorldSpace);

			return worldpos;
		}
	}
}
