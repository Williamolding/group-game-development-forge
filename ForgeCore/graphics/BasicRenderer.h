#pragma once

namespace forge
{
	namespace graphics
	{
		class BasicRenderer
		{
		private:
			static BasicRenderer* instance;

			~BasicRenderer()
			{
				delete instance;
			}
		public:
			static BasicRenderer* Instance()
			{
				if (instance)
					return instance;

				instance = new BasicRenderer();
			}

			static void Initalise()
			{
				//LoadDrawCircle
				//LoadDrawRect
			}

			void DrawRect(float x,float y,float width, float height)
			{
				
			}
			void DrawCricle(float x, float y, float radius)
			{
				
			}
		};

		BasicRenderer* BasicRenderer::instance = nullptr;
	}
}

