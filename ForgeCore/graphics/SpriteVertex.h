#pragma once
#include <DirectXMath.h>


struct SpriteVertex
{
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT2 texCord;
	DirectX::XMFLOAT3 colour;

	SpriteVertex(float x, float y, float z, float tx, float ty, float r, float g, float b)
	{
		position = DirectX::XMFLOAT3(x, y, z);
		texCord = DirectX::XMFLOAT2(tx, ty);
		colour = DirectX::XMFLOAT3(r, g, b);
	}
};
