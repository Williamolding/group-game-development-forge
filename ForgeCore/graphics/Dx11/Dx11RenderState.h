#pragma once
#include<d3d11.h>

namespace forge
{
	namespace graphics
	{
		struct Dx11RenderStateResult
		{
			ID3D11RasterizerState* rasterState;
			ID3D11RasterizerState* rasterStateDebug
			;
		};

		class Dx11RenderState
		{
		public:
			static Dx11RenderStateResult CreateRenderState(ID3D11Device* device)
			{
				HRESULT result = S_OK;

				auto rasterStateresult = Dx11RenderStateResult();

				D3D11_RASTERIZER_DESC wfdesc;
				ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
				wfdesc.FillMode = D3D11_FILL_SOLID;
				wfdesc.CullMode = D3D11_CULL_NONE;
				wfdesc.MultisampleEnable = true;
				result = device->CreateRasterizerState(&wfdesc, &rasterStateresult.rasterState);

				if(FAILED(result))
					throw new Dx11InitialisationException("Unanble to create render device");

				result = S_OK;

				ZeroMemory(&wfdesc, sizeof(D3D11_RASTERIZER_DESC));
				wfdesc.FillMode = D3D11_FILL_WIREFRAME;
				wfdesc.CullMode = D3D11_CULL_NONE;
				wfdesc.MultisampleEnable = true;
				result = device->CreateRasterizerState(&wfdesc, &rasterStateresult.rasterStateDebug);

				if (FAILED(result))
					throw new Dx11InitialisationException("Unanble to create render device");

				return rasterStateresult;
			}
		};
	}
}
