#pragma once
#include <d3d11.h>

namespace forge
{
	namespace graphics
	{
		class Dx11Viewport
		{
		public:
			static void CreateViewport(ID3D11DeviceContext* renderContext, UINT width, UINT height)
			{
				D3D11_VIEWPORT ViewPort;
				ViewPort.Width = (FLOAT)width;
				ViewPort.Height = (FLOAT)height;
				ViewPort.MinDepth = 0.0f;
				ViewPort.MaxDepth = 1.0f;
				ViewPort.TopLeftX = 0;
				ViewPort.TopLeftY = 0;

				renderContext->RSSetViewports(1, &ViewPort);
			}
		};
	}
}
