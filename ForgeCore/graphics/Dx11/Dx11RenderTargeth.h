#pragma once
#include <d3d11.h>

namespace forge
{
	namespace graphics
	{
		struct Dx11RenderTargetResult 
		{
			ID3D11RenderTargetView* RenderTargetView;
		};

		class Dx11RenderTarget
		{
		public:
			static Dx11RenderTargetResult CreateRenderTarget(IDXGISwapChain* swapchain, ID3D11Device* device)
			{

				HRESULT result = S_OK;
				ID3D11Texture2D* pBackBuffer = nullptr;

				auto renderTargetResult = Dx11RenderTargetResult();

				result = swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);

				if (FAILED(result))
					throw new Dx11InitialisationException("Unanble to get swapchain buffer");

				result = device->CreateRenderTargetView(pBackBuffer, nullptr, &renderTargetResult.RenderTargetView);
				pBackBuffer->Release();

				if(FAILED(result))
					throw new Dx11InitialisationException("Unanble to create render target");

				return renderTargetResult;
			}
		};
	}
}
