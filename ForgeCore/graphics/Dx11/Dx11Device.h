#pragma once
#include <d3d11.h>
#include <vector>
namespace forge
{
	namespace graphics
	{
		struct Dx11DeviceResult
		{
			ID3D11Device*			DevicePtr;
			ID3D11DeviceContext*	DeviceContextPtr;
			D3D_DRIVER_TYPE			DriverType;
			D3D_FEATURE_LEVEL		FeatureLevel;
		};

		class Dx11Device
		{
			static const std::vector<D3D_DRIVER_TYPE> _driverTypes;
			static const std::vector<D3D_FEATURE_LEVEL> _featureLevels;
		public:
			static Dx11DeviceResult CreateDevice(UINT qualityLevel)
			{
				HRESULT result = S_OK;

				Dx11DeviceResult deviceResult;

				for (auto driver : _driverTypes)
				{
					deviceResult.DriverType = driver;
					result = D3D11CreateDevice(nullptr, driver, nullptr, D3D11_CREATE_DEVICE_DEBUG, &_featureLevels[0], _featureLevels.size(), D3D11_SDK_VERSION, &deviceResult.DevicePtr, &deviceResult.FeatureLevel, &deviceResult.DeviceContextPtr);

					if (SUCCEEDED(result))
						break;
				}

				result = deviceResult.DevicePtr->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &qualityLevel);

				if (FAILED(result))
					throw new Dx11InitialisationException("Unanble to create render device");

				return deviceResult;
			}
		};

		const std::vector<D3D_DRIVER_TYPE> Dx11Device::_driverTypes = {
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE
		};

		const std::vector<D3D_FEATURE_LEVEL> Dx11Device::_featureLevels = {
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
			D3D_FEATURE_LEVEL_9_3,
			D3D_FEATURE_LEVEL_9_2,
			D3D_FEATURE_LEVEL_9_1
		};
	}
}