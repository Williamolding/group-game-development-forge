#include <graphics/Dx11/Dx11Driver.h>
#include <graphics/Dx11/Dx11InitialisationException.h>
#include <graphics/Dx11/Dx11Device.h>
#include <graphics/Dx11/Dx11SwapChain.h>
#include <graphics/Dx11/Dx11RenderState.h>
#include <graphics/Dx11/Dx11RenderTargeth.h>
#include <graphics/Dx11/Dx11DepthBuffer.h>
#include <graphics/Dx11/Dx11Viewport.h>
#include <Window.h>
#include <iostream>

namespace forge
{
	namespace graphics
	{
		Dx11Driver::Dx11Driver(sys::WindowProfile profile)
		{
			_isClosing = false;
			_fullscreen = profile.Fullscreen;
			_clientWidth = profile.ClientWidth;
			_clientHeight = profile.ClientHeight;
			_windowHandle = profile.WindowHandle;
			_QualityLevel = 1;
		}

		Dx11Driver::~Dx11Driver()
		{
			if (_graphicsDevice)
				_graphicsDevice->Release();

			if (_ImmediateContext)
				_ImmediateContext->Release();

			if (_SwapChain)
				_SwapChain->Release();

			if (_RenderTargetView)
				_RenderTargetView->Release();

			if(_VertexLayout)
				_VertexLayout->Release();

			if (_depthStencilView)
				_depthStencilView->Release();

			if (_depthStencilBuffer)
				_depthStencilBuffer->Release();

			if (_rasterState)
				_rasterState->Release();

			if (_VertexShader)
				_VertexShader->Release();

			if (_PixelShader)
				_PixelShader->Release();

			if (_constantBuffer)
				_constantBuffer->Release();

			if (_vertexBuffer)
				_vertexBuffer->Release();

			if (_indexBuffer)
				_indexBuffer->Release();
		}

		void Dx11Driver::Initalise() try {	

			//CreateDevice
			auto deviceResult = Dx11Device::CreateDevice(_QualityLevel);

			_graphicsDevice		= deviceResult.DevicePtr;
			_ImmediateContext	= deviceResult.DeviceContextPtr;
			_featureLevel		= deviceResult.FeatureLevel;
			_driverType			= deviceResult.DriverType;

			//CreateSwapchains
			auto swapchainResult	= Dx11SwapChain::CreateSwapchain(_graphicsDevice,_clientWidth,_clientHeight,_windowHandle,_QualityLevel,_fullscreen);
			_SwapChain				= swapchainResult.SwapChain;

			//createRenderState
			auto renderStateResult	= Dx11RenderState::CreateRenderState(_graphicsDevice);
			_rasterState			= renderStateResult.rasterState;
			_rasterStateDebug		= renderStateResult.rasterStateDebug;

			//createRenderTarget
			auto renderTargetResult = Dx11RenderTarget::CreateRenderTarget(_SwapChain,_graphicsDevice);
			_RenderTargetView		= renderTargetResult.RenderTargetView;

			//createDepthBuffer
			auto depthBufferResult	= Dx11DepthBuffer::CreateDepthBuffer(_graphicsDevice, _ImmediateContext,_RenderTargetView, _clientWidth, _clientHeight, _QualityLevel);
			_depthStencilBuffer		= depthBufferResult.DepthStencilBuffer;
			_depthStencilView		= depthBufferResult.DepthStencilView;

			//Create viweport
			Dx11Viewport::CreateViewport(_ImmediateContext, _clientWidth, _clientHeight);

			std::cout << "DX11 Initalised" << std::endl;
		}
		catch(Dx11InitialisationException exception)
		{
			std::cout << exception.what() << std::endl;
		}
		catch (...)
		{
			std::cout << "something broke badly" << std::endl;
		}

		ID3D11DeviceContext* Dx11Driver::BeginFrame()
		{
			float ClearColor[4] = { 0.58f, 0.916f, 0.928f, 1.0f };

			_ImmediateContext->ClearRenderTargetView(_RenderTargetView, ClearColor);
			_ImmediateContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

			return _ImmediateContext;
		}

		void Dx11Driver::EndFrame()
		{
			_SwapChain->Present(1, 0);
		}

		void Dx11Driver::ResizeViewport(int width, int height)
		{
			/*_ImmediateContext->OMSetRenderTargets(0, 0, 0);

			// Release all outstanding references to the swap chain's buffers.
			_SwapChain->Release();

			HRESULT hr;
			// Preserve the existing buffer count and format.
			// Automatically choose the width and height to match the client rect for HWNDs.
			hr = _SwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);

			ID3D11Texture2D* pBuffer;
			hr = _SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),(void**)&pBuffer);

			hr = _graphicsDevice->CreateRenderTargetView(pBuffer, NULL,&_RenderTargetView);
			// Perform error handling here!
			pBuffer->Release();

			_ImmediateContext->OMSetRenderTargets(1, &_RenderTargetView, _depthStencilView);

			Dx11Viewport::CreateViewport(_ImmediateContext, width, height);*/
		}
	}
}
