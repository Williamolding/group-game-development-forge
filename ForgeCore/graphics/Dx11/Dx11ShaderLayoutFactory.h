#pragma once
#include <d3d11.h>
#include "Dx11Driver.h"
#include <map>

namespace forge
{
	namespace graphics
	{
		class Dx11ShaderLayoutFactory
		{
			std::map<std::string,ID3D11InputLayout*> _inputLayouts;
			Dx11Driver* _driver;
		public:
			Dx11ShaderLayoutFactory(Dx11Driver* driver) : _driver(driver)
			{
				_inputLayouts = std::map<std::string,ID3D11InputLayout*>();
			}
	
			ID3D11InputLayout* GetDefaultLayout(ID3DBlob* shaderBlob)
			{
				D3D11_INPUT_ELEMENT_DESC layout[] =
				{
					{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0	},
					{ "TEXCORD", 0,  DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0	},
					{ "COLOUR", 0 , DXGI_FORMAT_R32G32B32_FLOAT,0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0	}
				};

				UINT numElements = ARRAYSIZE(layout);
				HRESULT result;

				ID3D11InputLayout* shaderLayout;

				result = _driver->_graphicsDevice->CreateInputLayout(layout, numElements, shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), &shaderLayout);

				if (FAILED(result))
					return nullptr;

				return shaderLayout;
			}
		};
	}
}
