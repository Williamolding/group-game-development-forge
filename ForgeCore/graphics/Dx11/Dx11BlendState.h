#pragma once
#include <d3d11.h>
#include <iostream>

namespace forge
{
	namespace graphics
	{
		struct Dx11BlendSateResult
		{
			ID3D11BlendState*	BlendState;
		};

		class Dx11BlendState
		{
		public:
			Dx11BlendSateResult CreateBlendState(ID3D11Device* device)
			{
				HRESULT result = S_OK;

				auto blendStateResult = Dx11BlendSateResult();

				D3D11_BLEND_DESC omDesc;
				ZeroMemory(&omDesc, sizeof(D3D11_BLEND_DESC));
				omDesc.AlphaToCoverageEnable = true;
				omDesc.IndependentBlendEnable = true;
				omDesc.RenderTarget[0].BlendEnable = true;
				omDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
				omDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
				omDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
				omDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
				omDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
				omDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
				omDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

				result = device->CreateBlendState(&omDesc, &blendStateResult.BlendState);

				if (FAILED(result))
				{
					std::cout << "Failed to create Blend State " << std::endl;
				}

				return blendStateResult;
			}
		};
	}
}
