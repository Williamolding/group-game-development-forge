#pragma once
#include <d3d11.h>
#include <iostream>

namespace forge
{
	namespace graphics
	{
		struct Dx11TextureSamplerResult
		{
			ID3D11SamplerState* TextureSampler;
		};

		class Dx11TextureSampler
		{
		public:
			Dx11TextureSamplerResult CreateTextureSampler(ID3D11Device* device, D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE wrappingMode)
			{
				HRESULT result = S_OK;

				auto samplerResult = Dx11TextureSamplerResult();

				// Create a texture sampler state description.
				D3D11_SAMPLER_DESC samplerDesc;
				samplerDesc.Filter = filter;
				samplerDesc.AddressU = wrappingMode;
				samplerDesc.AddressV = wrappingMode;
				samplerDesc.AddressW = wrappingMode;
				samplerDesc.MipLODBias = 0.0f;
				samplerDesc.MaxAnisotropy = 4;
				samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
				samplerDesc.BorderColor[0] = 0;
				samplerDesc.BorderColor[1] = 0;
				samplerDesc.BorderColor[2] = 0;
				samplerDesc.BorderColor[3] = 0;
				samplerDesc.MinLOD = 0;
				samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

				// Create the texture sampler state.
				result = device->CreateSamplerState(&samplerDesc, &samplerResult.TextureSampler);

				if (FAILED(result))
				{
					std::cout << "Failed To Create Sampler" << std::endl;
				}

				return samplerResult;
			}
		};
	}
}
