#pragma once
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <dxgi1_3.h>


namespace forge {
	namespace sys {
		class WindowProfile;
	}
}
namespace forge
{
	namespace graphics
	{
		class Dx11Driver
		{
		public:
			bool _isClosing;
			bool _fullscreen;
			UINT _clientWidth;
			UINT _clientHeight;
			UINT _QualityLevel;
			HWND _windowHandle;
			
			D3D_DRIVER_TYPE			_driverType;
			D3D_FEATURE_LEVEL		_featureLevel;
			ID3D11Device*           _graphicsDevice;
			ID3D11DeviceContext*    _ImmediateContext;
			IDXGISwapChain*         _SwapChain;

			ID3D11RenderTargetView* _RenderTargetView;
			ID3D11InputLayout*      _VertexLayout;
			ID3D11DepthStencilView* _depthStencilView;
			ID3D11Texture2D*		_depthStencilBuffer;
			ID3D11RasterizerState*  _rasterState;
			ID3D11RasterizerState*  _rasterStateDebug;

			ID3D11VertexShader*     _VertexShader;
			ID3D11PixelShader*      _PixelShader;

			ID3D11Buffer*			_constantBuffer;
			ID3D11Buffer*			_vertexBuffer;
			ID3D11Buffer*			_indexBuffer;
		public:
			Dx11Driver(sys::WindowProfile profile);
			~Dx11Driver();
			void Initalise();
			ID3D11DeviceContext* BeginFrame();
			void EndFrame();
			void ResizeViewport(int width, int height);
		};
	}
}

