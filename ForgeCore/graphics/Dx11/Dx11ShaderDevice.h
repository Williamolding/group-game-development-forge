#pragma once
#include "Dx11Driver.h"
#include "Dx11ShaderLoader.h"

namespace forge
{
	namespace graphics
	{
		class Dx11ShaderDevice
		{
			Dx11Driver* _driver;
		public:
			Dx11ShaderDevice(Dx11Driver* driver): _driver(driver)
			{
				
			}

			HRESULT CreateShader(ID3DBlob* shaderBlob, ID3D11VertexShader** shaderPtr) const
			{
				return _driver->_graphicsDevice->CreateVertexShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), nullptr, shaderPtr);
			}
			HRESULT CreateShader(ID3DBlob* shaderBlob, ID3D11PixelShader** shaderPtr) const
			{
				return _driver->_graphicsDevice->CreatePixelShader(shaderBlob->GetBufferPointer(), shaderBlob->GetBufferSize(), nullptr, shaderPtr);
			}
		};
	}
}
