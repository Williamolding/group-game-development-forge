#pragma once
#include <d3d11_1.h>
#include <vector>

namespace forge
{
	namespace graphics
	{	 
		class Dx11BufferMapper
		{
		public:
			template<class VertexType> void MapVertexBuffer(ID3D11Buffer* vertexBuffer, std::vector<VertexType> vector, int vertexCount, ID3D11DeviceContext* context)
			{
				D3D11_MAPPED_SUBRESOURCE mappedResource;
	
				HRESULT result = context->Map(vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	
				if (FAILED(result)) {
					return;
				}

				VertexType* verticesPtr = (VertexType*)mappedResource.pData;
				memcpy(verticesPtr, (void*)vector.data(), sizeof(VertexType) * vertexCount);

				context->Unmap(vertexBuffer, 0);
			}
		};
	}
}
