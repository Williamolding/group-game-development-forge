#pragma once
#include <string>
#include <d3dcompiler.h>
#include <dxgi1_3.h>
#include <algorithm>

namespace forge
{
	namespace graphics
	{
		enum ShaderType
		{
			PIXELSHADER,
			VERTEXSHADER
		};

		struct Dx11VertexCompileSettings
		{
			LPCSTR entryPoint	= "main";
			LPCSTR version		= "vs_4_0";
		};

		struct Dx11PixelCompileSettings
		{
			LPCSTR entryPoint	= "main";
			LPCSTR version		= "ps_4_0";
		};

		struct Dx11ShaderCompileSettings
		{
			Dx11VertexCompileSettings VertexCompileSettings;
			Dx11PixelCompileSettings  PixelCompileSettings;
		};

		class Dx11ShaderCompiler
		{
			Dx11ShaderCompileSettings* _compileSettings;

		public:
			Dx11ShaderCompiler(Dx11ShaderCompileSettings* compileSettings) : _compileSettings(compileSettings)
			{
			}
		public:
			HRESULT CompileFromFile(ShaderType type, LPCWSTR fileName, ID3DBlob** compiledBlob)
			{		
				DWORD compileFlags	= D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined(_DEBUG)
				compileFlags |= D3DCOMPILE_DEBUG;
#endif
				ID3DBlob* errorBlob = nullptr;

				HRESULT result = S_OK;

				if(ShaderType::VERTEXSHADER == type)
				{
					result = D3DCompileFromFile(fileName, nullptr, nullptr,
						_compileSettings->VertexCompileSettings.entryPoint,
						_compileSettings->VertexCompileSettings.version,
						compileFlags, 0, compiledBlob, &errorBlob);
				}

				if(ShaderType::PIXELSHADER == type)
				{
					result = D3DCompileFromFile(fileName, nullptr, nullptr,
						_compileSettings->PixelCompileSettings.entryPoint,
						_compileSettings->PixelCompileSettings.version,
						compileFlags, 0, compiledBlob, &errorBlob);
				}

				if (FAILED(result))
				{
					if (errorBlob != nullptr)
						OutputDebugStringA((char*)errorBlob->GetBufferPointer());

					if (errorBlob) errorBlob->Release();

					return result;
				}

				if (errorBlob) errorBlob->Release();

				return result;
			}
			HRESULT CompileFromMemory(ShaderType type, std::string source, ID3DBlob** compiledBlob)
			{
				ID3DBlob* pErrorBlob = nullptr;
				HRESULT result = S_OK;

				if (ShaderType::VERTEXSHADER == type)
				{
					result = D3DCompile(source.c_str(), source.length(), nullptr, nullptr,nullptr, 
						_compileSettings->VertexCompileSettings.entryPoint,
						_compileSettings->VertexCompileSettings.version, 
						0, 0, compiledBlob, &pErrorBlob);
				}

				if (ShaderType::PIXELSHADER == type)
				{
					result = D3DCompile(source.c_str(), source.length(), nullptr, nullptr, nullptr,
						_compileSettings->PixelCompileSettings.entryPoint,
						_compileSettings->PixelCompileSettings.version,
						0, 0, compiledBlob, &pErrorBlob);
				}

				if (FAILED(result))
				{
					if (pErrorBlob != nullptr)
						OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());

					if (pErrorBlob) pErrorBlob->Release();

					return result;
				}

				if (pErrorBlob) pErrorBlob->Release();

				return result;
			}
		};
	}
}
