#pragma once
#include <WindowProfile.h>
#include <d3d11.h>
#include <vector>

namespace forge
{
	namespace graphics
	{
		struct Dx11SwapChainResult
		{
			IDXGISwapChain* SwapChain;
		};

		class Dx11SwapChain
		{
		public:
			static Dx11SwapChainResult CreateSwapchain(ID3D11Device* device, UINT width, UINT height, HWND handle, UINT qualityLevel, bool fullscreen)
			{
				HRESULT result = S_OK;

				auto swapchainResult = Dx11SwapChainResult();
				
				DXGI_SWAP_CHAIN_DESC swapChainDesc;
				memset(&swapChainDesc, 0, sizeof(swapChainDesc));

				swapChainDesc.BufferCount = 2;
				swapChainDesc.BufferDesc.Width	= width;
				swapChainDesc.BufferDesc.Height = height;
				swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
				swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
				swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
				swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
				swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
				swapChainDesc.OutputWindow = handle;
				swapChainDesc.SampleDesc.Count = 4;
				swapChainDesc.SampleDesc.Quality = qualityLevel - 1;
				swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
				swapChainDesc.Windowed = !fullscreen;
				swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

				unsigned int numModes = 0;

				IDXGIFactory*	factory = nullptr;
				IDXGIAdapter*	adapter = nullptr;
				IDXGIOutput*	adapterOutput = nullptr;
				DXGI_MODE_DESC* displayModeList = nullptr;
			
				result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
				if (FAILED(result))
					throw new Dx11InitialisationException("Unable to create IDXGIFactory");

				result = factory->EnumAdapters(0, &adapter);
				if (FAILED(result))
					throw new Dx11InitialisationException("Unable to enumarate graphics adapter");

				result = adapter->EnumOutputs(0, &adapterOutput);
				if (FAILED(result))
					throw new Dx11InitialisationException("Unable to enumarate graphics adapter output");

				result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
				if (FAILED(result))
					throw new Dx11InitialisationException("Unable to get display mode list");

				displayModeList = new DXGI_MODE_DESC[numModes];
				result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
				if (FAILED(result))
					throw new Dx11InitialisationException("Unable to get display mode list");

				for (unsigned int i = 0; i< numModes; i++)
				{
					if (displayModeList[i].Width == width)
					{
						if (displayModeList[i].Height == height)
						{
							swapChainDesc.BufferDesc.RefreshRate.Numerator = displayModeList[i].RefreshRate.Numerator;
							swapChainDesc.BufferDesc.RefreshRate.Denominator = displayModeList[i].RefreshRate.Denominator;
						}
					}
				}

				result = factory->CreateSwapChain(device, &swapChainDesc, &swapchainResult.SwapChain);
				return swapchainResult;
			}
		};
	}
}

