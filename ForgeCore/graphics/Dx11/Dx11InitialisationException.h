#pragma once
#include <string>
#include <exception>

namespace forge
{
	namespace graphics
	{
		class Dx11InitialisationException : std::exception
		{
			std::string _reason;
		public:
			Dx11InitialisationException(std::string reason) : _reason(reason)
			{

			}

			virtual const char* what() const throw()
			{
				return _reason.c_str();
			}
		};
	}
}