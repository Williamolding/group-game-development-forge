#pragma once
#include <d3d11.h>

namespace forge
{
	namespace graphics
	{
		struct Dx11DepthBufferResult
		{
			ID3D11DepthStencilView* DepthStencilView;
			ID3D11Texture2D*		DepthStencilBuffer;
		};

		class Dx11DepthBuffer
		{
		public:
			static Dx11DepthBufferResult CreateDepthBuffer(ID3D11Device* device, ID3D11DeviceContext* renderContext, ID3D11RenderTargetView* renderTarget, UINT width, UINT height, UINT qualityLevel)
			{
				auto depthBufferResult = Dx11DepthBufferResult();

				D3D11_TEXTURE2D_DESC depthStencilDesc;
				depthStencilDesc.Width = width;
				depthStencilDesc.Height = height;
				depthStencilDesc.MipLevels = 1;
				depthStencilDesc.ArraySize = 1;
				depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
				depthStencilDesc.SampleDesc.Count = 4;
				depthStencilDesc.SampleDesc.Quality = qualityLevel - 1;
				depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
				depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
				depthStencilDesc.CPUAccessFlags = 0;
				depthStencilDesc.MiscFlags = 0;

				device->CreateTexture2D(&depthStencilDesc, nullptr, &depthBufferResult.DepthStencilBuffer);
				device->CreateDepthStencilView(depthBufferResult.DepthStencilBuffer, nullptr, &depthBufferResult.DepthStencilView);

				D3D11_DEPTH_STENCIL_DESC  depthDisabledStencilDesc;			
				ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));
				depthDisabledStencilDesc.DepthEnable = true;
				depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
				depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
				depthDisabledStencilDesc.StencilEnable = true;
				depthDisabledStencilDesc.StencilReadMask = 0xFF;
				depthDisabledStencilDesc.StencilWriteMask = 0xFF;
				depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
				depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
				depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
				depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
				depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
				depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
				depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				
				ID3D11DepthStencilState* depthDisabledStencilState;
				device->CreateDepthStencilState(&depthDisabledStencilDesc, &depthDisabledStencilState);

				renderContext->OMSetRenderTargets(1, &renderTarget, depthBufferResult.DepthStencilView);
			    renderContext->OMSetDepthStencilState(depthDisabledStencilState, 0);

				return depthBufferResult;
			}
		};
	}
}
