#pragma once
#include <d3d11_1.h>
#include <string>
#include "Dx11Driver.h"
#include "Dx11ShaderCompiler.h"
#include "Dx11ShaderLayoutFactory.h"
#include "Dx11ShaderDevice.h"

#include <asset/AssetManager.h>

namespace forge
{
	namespace graphics
	{
		using VertexShader	= ID3D11VertexShader;
		using PixelShader	= ID3D11PixelShader;

		template<class ShaderComPtr> struct Dx11ShaderLoaderResult
		{
			bool				ShaderState;
			ID3D11InputLayout*	InputLayout;
			ShaderComPtr*		ShaderPtr;
		};

		class Dx11ShaderLoader
		{
			Dx11ShaderDevice*			_shaderDevice;
			Dx11ShaderCompiler*			_shaderCompiler;
			Dx11ShaderLayoutFactory*	_shaderLayoutFactory;

		public:
			Dx11ShaderLoader(Dx11ShaderCompiler* shaderCompiler, Dx11ShaderLayoutFactory* layoutFactory, Dx11ShaderDevice* device)
			{
				_shaderDevice			= device;
				_shaderLayoutFactory	= layoutFactory;
				_shaderCompiler			= shaderCompiler;
			}
		public:
			template<class Shader> Dx11ShaderLoaderResult<Shader> LoadShaderFromFile(ShaderType shaderType, LPCWSTR path)
			{
				Dx11ShaderLoaderResult<Shader>	result		= Dx11ShaderLoaderResult<Shader>();
				ID3DBlob*						shaderBlob	= nullptr;

				auto compiledStatus = _shaderCompiler->CompileFromFile(shaderType, path, &shaderBlob); 
				if (FAILED(compiledStatus)) {
					result.ShaderState = false;
				}

				auto uploadStatus = _shaderDevice->CreateShader(shaderBlob, &result.ShaderPtr);

				if (FAILED(uploadStatus)) {
					result.ShaderState = false;
				}

				if(shaderType == VERTEXSHADER)
					result.InputLayout = _shaderLayoutFactory->GetDefaultLayout(shaderBlob);

				shaderBlob->Release();

				result.ShaderState	= true;
				return result;
				
			};
			template<class Shader> Dx11ShaderLoaderResult<Shader> LoadShaderFromMemory(ShaderType shaderType, std::string shaderCode)
			{
				Dx11ShaderLoaderResult<Shader>	result		= Dx11ShaderLoaderResult<Shader>();
				ID3DBlob*						shaderBlob	= nullptr;

				auto compiledStatus = _shaderCompiler->CompileFromMemory(shaderType, shaderCode, &shaderBlob);

				if (!compiledStatus) {
					result.ShaderState = false;
				}

				auto uploadStatus	= _shaderDevice->CreateShader(shaderBlob, &result.ShaderPtr);

				if (!uploadStatus) {
					result.ShaderState = false;
				}

				if (shaderType == VERTEXSHADER)
					result.InputLayout = _shaderLayoutFactory->GetDefaultLayout(shaderBlob);

				shaderBlob->Release();

				result.ShaderState = true;
				return result;
			};
		};
	}
}
