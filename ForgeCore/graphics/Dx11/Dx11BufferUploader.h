#pragma once
#include <d3d11.h>
#include <vector>

namespace forge
{
	namespace graphics
	{
		struct Dx11BufferUploadResult
		{
			ID3D11Buffer* UploadedBuffer;
			bool		  UploadStatus;
		};

		class Dx11BufferUploader
		{
		public:
			template<class BufferContent> Dx11BufferUploadResult UploadBuffer(ID3D11Device* device, std::vector<BufferContent> buffer, D3D11_BIND_FLAG uploadFlag, D3D11_USAGE bufferUsage, bool dynamic = false)
			{
				HRESULT result = S_OK;

				auto bufferUploadResult = Dx11BufferUploadResult();

				D3D11_BUFFER_DESC bufferDescription;
				memset(&bufferDescription, 0 ,sizeof(bufferDescription));

				bufferDescription.Usage		= bufferUsage;
				bufferDescription.ByteWidth = sizeof(BufferContent) * buffer.size();
				bufferDescription.BindFlags = uploadFlag;
				bufferDescription.CPUAccessFlags = dynamic ? D3D11_CPU_ACCESS_WRITE : 0;

				D3D11_SUBRESOURCE_DATA Data;
				memset(&Data, 0, sizeof(Data));

				Data.pSysMem = buffer.data();
				result = device->CreateBuffer(&bufferDescription, &Data, &bufferUploadResult.UploadedBuffer);

				if (FAILED(result))
					bufferUploadResult.UploadStatus = false;

				bufferUploadResult.UploadStatus = true;

				return bufferUploadResult;
			}
			template<class BufferContent> Dx11BufferUploadResult UploadBuffer(ID3D11Device* device, D3D11_BIND_FLAG uploadFlag, D3D11_USAGE bufferUsage)
			{
				HRESULT result = S_OK;
				
				auto bufferUploadResult = Dx11BufferUploadResult();

				D3D11_BUFFER_DESC bufferDescription;
				memset(&bufferDescription, 0, sizeof(bufferDescription));

				bufferDescription.Usage = bufferUsage;
				bufferDescription.ByteWidth = sizeof(BufferContent);
				bufferDescription.BindFlags = uploadFlag;
				bufferDescription.CPUAccessFlags = 0;

				result = device->CreateBuffer(&bufferDescription, nullptr, &bufferUploadResult.UploadedBuffer);

				if (FAILED(result))
					bufferUploadResult.UploadStatus = false;

				bufferUploadResult.UploadStatus = true;

				return bufferUploadResult;
			}
		};
	}
}
