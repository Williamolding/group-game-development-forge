#include "TgaLoader.h"

TgaLoader::TgaLoader(void)
{
}

t_texture * TgaLoader::load(string fname) {
	t_tgaHeader header;
	t_texture *texture;
	FILE *fd;
	long int offset = 4 * sizeof(char) + 4 * sizeof(short int);

	texture = (t_texture *)malloc(sizeof(t_texture));

	fd = fopen(fname.c_str(), "rb");
	if (!fd) {
		printf("File %s not found\n", fname.c_str());
		exit(-1);
	}

	fseek(fd, offset, SEEK_SET);

	fread(&header.width, sizeof(short), 1, fd);
	fread(&header.height, sizeof(short), 1, fd);
	fread(&header.bitsperpixel, sizeof(char), 1, fd);
	fread(&header.imagedescriptor, sizeof(char), 1, fd);

	texture->w = header.width;
	texture->h = header.height;

	texture->bpp = header.bitsperpixel;
	int bytesperpixel = header.bitsperpixel / 8;

	texture->texels = (TGA_BYTE *)malloc(texture->w*texture->h*bytesperpixel * sizeof(TGA_BYTE));

	int pixnum = header.width * header.height;

	int i;
	if (header.bitsperpixel == 32) {
		for (i = 0; i<pixnum; i++) {
			texture->texels[i * 4 + 2] = fgetc(fd);
			texture->texels[i * 4 + 1] = fgetc(fd);
			texture->texels[i * 4 + 0] = fgetc(fd);
			texture->texels[i * 4 + 3] = fgetc(fd);
		}
	}
	if (header.bitsperpixel == 24) {
		for (i = 0; i<pixnum; i++) {
			texture->texels[i * 3 + 2] = fgetc(fd);
			texture->texels[i * 3 + 1] = fgetc(fd);
			texture->texels[i * 3 + 0] = fgetc(fd);
		}
	}

	fclose(fd);

	this->tex = texture;
	return texture;
}

TgaLoader::~TgaLoader(void)
{
	if (tex != NULL) {
		if (tex->texels)
			free(tex->texels);
		free(tex);
	}
}