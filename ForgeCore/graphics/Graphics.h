#pragma once
#include "graphics/Dx11/Dx11Driver.h"
#include "graphics/Dx11/Dx11ShaderLoader.h"
#include <graphics/Dx11/Dx11BufferMapper.h>
#include "Dx11/Dx11BufferUploader.h"
#include "Camera.h"
#include "entity/EntityManager.h"
#include "TextureSamplerFactory.h"

namespace forge
{
	namespace graphics
	{
		class Graphics
		{
			Dx11Driver*			_driver;
			Dx11ShaderLoader*	_shaderLoader;
			Dx11BufferMapper*	_bufferMapper;

			ID3D11InputLayout*	_defaultInputLayout;
			ID3D11VertexShader*	_defaultVertexShader;
			ID3D11PixelShader*	_defaultPixelShader;
			ID3D11VertexShader*	_debugVertexShader;
			ID3D11PixelShader*	_debugPixelShader;

			ID3D11BlendState*	_blendState;

			TextureSamplerFactory*		_textureSamplerFactory;
			entity::IEntityManager*		_entityManager;
			asset::MemoryFileSystem*	_memoryFileSystem;
			Camera*						_camera;
			entity::Entity*				_GameCamera;


		public:
			Graphics(Dx11Driver* driver, entity::IEntityManager* entityManager, asset::MemoryFileSystem* memoryfileSystem);
			~Graphics();

			void SetMainCamera(Camera* camera);
			Camera* GetMainCamera();
			DirectX::XMFLOAT2 ScreenToWorldSpacePosition(DirectX::XMFLOAT2 mousePosition);

			bool Initalise();
			void Render(bool debug);
			void Resize(int width, int height);
		};
	}
}

