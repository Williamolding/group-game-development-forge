#pragma once
#include <vector>

namespace forge
{
	namespace graphics
	{
		class Rect
		{
		public:
			float Left;
			float Right;
			float Top;
			float Bottom;
		};

		class AnimationRect
		{
			int _width;
			int _height;
			int _frameCount;
			std::vector<Rect> _frames;
		public:
			AnimationRect(int spriteSheetWidth, int spriteSheetHeight, int frameCount): _width(spriteSheetWidth), _height(spriteSheetHeight),_frameCount(frameCount)
			{
				_frames = std::vector<Rect>();
			}

			void BuildFrames()
			{
				auto frameWidth		= _width / _frameCount;
				auto frameHeight	= _height;

				auto left = 0;
				for(int i =0 ; i < _frameCount; i++)
				{
					auto frame = Rect();
					frame.Left = left / (float)_width;
					frame.Top = frameHeight / (float)_height;
					frame.Bottom = 0;
					frame.Right = (left + frameWidth) / (float)_width;

					_frames.push_back(frame);
					left += frameWidth;
				}
			}

			Rect GetFrame(int frame)
			{
				if (frame >= _frames.size())
					return _frames[0];

				return _frames[frame];
			}
		};
	}
}
