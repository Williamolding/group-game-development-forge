#pragma once
#include <DirectXMath.h>


struct SpriteConstantBuffer
{
	DirectX::XMMATRIX World;
	DirectX::XMMATRIX View;
	DirectX::XMMATRIX Projection;
};
