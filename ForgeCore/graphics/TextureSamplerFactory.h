#pragma once
#include <map>
#include <d3d11.h>
#include "Dx11/Dx11TextureSampler.h"

namespace forge
{
	namespace graphics
	{
		enum TextureSampler
		{
			CLAMPED,
			WRAPPED
		};

		class TextureSamplerFactory
		{
			std::map<TextureSampler, ID3D11SamplerState*> _samplers;
			ID3D11Device* _device;
		public:
			TextureSamplerFactory(ID3D11Device* device): _device(device) {
				_samplers = std::map<TextureSampler, ID3D11SamplerState*>();
			}

			void BuildSamplerIndex()
			{
				auto samplerCreator = Dx11TextureSampler();
				auto clampedSampler = samplerCreator.CreateTextureSampler(_device, D3D11_FILTER_MIN_MAG_MIP_POINT, D3D11_TEXTURE_ADDRESS_CLAMP);
				auto wrappedSampler = samplerCreator.CreateTextureSampler(_device, D3D11_FILTER_MIN_MAG_MIP_POINT, D3D11_TEXTURE_ADDRESS_WRAP);

				_samplers[TextureSampler::CLAMPED] = clampedSampler.TextureSampler;
				_samplers[TextureSampler::WRAPPED] = clampedSampler.TextureSampler;
			}

			ID3D11SamplerState* GetSampler(TextureSampler samplerType)
			{
				return _samplers[samplerType];
			}
		};
	}
}
