#pragma once
#include <string>

namespace forge
{
	namespace sys
	{
		static std::string Platform()
		{
			#ifdef _DEBUG
			#ifdef _WIN32
				return "32 bit ";
			#elif _WIN64
				return "64 bit";
			#endif
			#endif

			return "";
		}

		static std::string Build()
		{
			#ifdef _DEBUG
				return " DEBUG ";
			#endif

			return "";
		}

		static std::string WindowTitle()
		{
			return std::string("ForgeEngine" + Build() + Platform());
		}
	}
}
