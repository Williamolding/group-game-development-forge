#include "Entity.h"
#include <iostream>

namespace forge
{
	namespace entity
	{
		Entity::Entity(IEntityManager& manager, std::string name) : _manager(manager), _componentArray(ComponentArray()), _name(name)
		{

		}
		Entity::~Entity() = default;

		void Entity::Update(double deltatime)
		{
			for (auto const &component : _components)
			{
				if(component->Enabled)
					component->Update(deltatime);
			}

			DeleteComponents();
		}

		void Entity::DeleteComponents()
		{
			_components.erase(std::remove_if(std::begin(_components), std::end(_components), [](component::IComponent* component)
			{
				if (component->Remove)
					std::cout << "Removing Componenet" << std::endl;

				return component->Remove;
			}),
			std::end(_components));
		}
	}
}
