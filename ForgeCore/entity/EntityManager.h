#pragma once
#include "IEntityManager.h"

namespace forge
{
	namespace entity
	{
		class EntityManager : public IEntityManager
		{
		public:
			EntityManager();
			~EntityManager();
			void Update(lua_State* state, double p_elapsedtime) override;
		};
	}
}


