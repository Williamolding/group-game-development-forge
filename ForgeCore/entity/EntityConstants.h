#pragma once
#include <array>
#include <bitset>

namespace forge
{
	namespace component
	{
		class IComponent;
	}
	namespace entity
	{		
		class EntityManager;
		class Entity;
		
		using ComponentBitset	= std::bitset<64>;
		using ComponentArray	= std::array<component::IComponent*, 64>;
		using GroupBitset		= std::bitset<64>;

		inline std::size_t getUniqueComponentID()
		{
			static std::size_t lastID = 0;
			return lastID++;
		}

		template <typename Component> std::size_t getComponentTypeID()
		{
			static_assert(std::is_base_of<component::IComponent, Component>::value, "Must Inherit From Component");

			static std::size_t typeID = getUniqueComponentID();
			return typeID;
		}
	}
}
