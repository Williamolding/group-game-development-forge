#pragma once
#include <algorithm>
#include <memory>
#include <array>
#include <vector>
#include <entity/Entity.h>
#include <entity/EntityConstants.h>
#include <lua.hpp>
#include <components/Script.h>
#include <components/SpriteRenderer.h>
#include <components/Transform.h>
#include <Camera.h>

namespace forge
{
	namespace entity
	{
		class IEntityManager
		{

		public:
			std::vector<Entity*> entities;
			graphics::Camera* Camera;
		protected:
			Entity * _selectedEntity = nullptr;
		public:
			virtual void Update(lua_State* state, double p_elapsedtime) {}

			void refresh()
			{
				entities.erase(std::remove_if(std::begin(entities), std::end(entities), [](Entity* entity)
				{
					return !entity->IsAlive();
				}),
				std::end(entities));
			}

			void Clear()
			{
				for (auto const &entity : entities)
				{
					entity->Destroy();
				}
				refresh();
			}

			void Delete(std::string name)
			{
				for (auto const &entity : entities)
				{
					if (entity->GetName() == name)
						entity->Destroy();
				}
			}

			Entity* Find(std::string name) 
			{
				for (auto const &entity : entities)
				{
					if (entity->GetName() == name)
						return entity;
				}
				return nullptr;
			}

			Entity* FindByPosition(DirectX::XMFLOAT2 mousePos)
			{
				for (auto const &entity : entities)
				{
					if (!entity->hasComponent<component::SpriteRenderer>() || !entity->hasComponent<component::Transform>())
						continue;

					auto spriteComponent = entity->getComponent<component::SpriteRenderer>();
					auto transformComponent = entity->getComponent<component::Transform>();
					auto scale = transformComponent->GetScale();

					auto mesh = spriteComponent->GetMesh();
					auto center = transformComponent->GetPosition();

					if (transformComponent->HasParent())
					{
						center = transformComponent->GetWorldPosition();
					}

					if (mousePos.x < center.x - (mesh.Width * scale.x) / 2)
						continue;

					if (mousePos.x > center.x + (mesh.Width * scale.x) / 2)
						continue;

					if (mousePos.y > center.y + (mesh.Height * scale.y) / 2)
						continue;

					if (mousePos.y < center.y - (mesh.Height * scale.y) / 2)
						continue;

					return entity;
				}

				return nullptr;
			}

			Entity& addEntity(std::string name)
			{
				Entity* entity(new Entity(*this, name));
				entities.emplace_back(entity);
				return *entity;
			}

			Entity* GetSelectedEntity()
			{
				return _selectedEntity;
			}

			void SetSelectedEntity(Entity* entity)
			{
				_selectedEntity = entity;
			}

			graphics::Camera* GetCamera()
			{
				return Camera;
			}
#
			void SetCamera(graphics::Camera* camera)
			{
				Camera = camera;
			}
		};

	}
};
