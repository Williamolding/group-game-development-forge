#include "EditorEntityManager.h"
namespace forge
{
	namespace entity
	{
		EditorEntityManager::EditorEntityManager() = default;
		EditorEntityManager::~EditorEntityManager() = default;

		void EditorEntityManager::Update(lua_State * state, double p_elapsedtime)
		{
			for(auto const &object : entities)
			{
				object->DeleteComponents();
			}
		}
	}
}