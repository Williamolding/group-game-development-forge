#include "EntityManager.h"
#include "components/Script.h"
#include <input/Keyboard.h>
#include <LuaBridge/LuaBridge.h>

namespace forge
{
	namespace entity
	{
		EntityManager::EntityManager()		= default;
		EntityManager::~EntityManager()		= default;

		void EntityManager::Update(lua_State* state, double p_elapsedtime)
		{		
			for (auto entity : entities)
			{		
				std::string name = entity->GetName();
				if(entity->hasComponent<component::Script>())
				{				
					luabridge::getGlobalNamespace(state)
						.beginNamespace("Entity")
							.addVariable("Me", &name)
						.endNamespace();
				
					auto script = entity->getComponent<component::Script>();
					script->SetLuaState(state);
				}	
				entity->Update(p_elapsedtime);
			}
			refresh();							
		}
	}
}
