#pragma once
#include "IEntityManager.h"
namespace forge
{
	namespace entity
	{
		class EditorEntityManager : public IEntityManager
		{
		public:
			EditorEntityManager();
			~EditorEntityManager();
			void Update(lua_State* state, double p_elapsedtime) override;
		};
	}
}
