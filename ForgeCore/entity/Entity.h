#pragma once
#include <entity/EntityConstants.h>
#include <components/IComponent.h>
#include <vector>
#include <cassert>

namespace forge
{
	namespace entity
	{
		class IEntityManager;
		class Entity
		{
			std::string			_name;
			IEntityManager&		_manager;
			ComponentArray		_componentArray;
			ComponentBitset		_componentBitset;
			GroupBitset			_groupBitset;

			std::vector<component::IComponent*> _components;

			bool _alive = true;
		public:
			Entity(IEntityManager& manager, std::string name);
			~Entity();

			IEntityManager& GetManager() { return _manager; };
			void Update(double deltatime);
			void DeleteComponents();
			bool IsAlive()
			{
				return _alive;
			}
			void Destroy()
			{
				_alive = false;
			}
			
			std::string GetName()
			{
				return _name;
			}

			void SetName(const std::string &name)
			{
				_name = name;
			}

			template<typename Component> void DisableComponent()
			{
				auto component(_componentArray[getComponentTypeID<Component>()]);
				component->Enabled = false;
			}
			template<typename Component> void EnableComponent()
			{
				auto component(_componentArray[getComponentTypeID<Component>()]);
				component->Enabled = true;
			}

			template <typename Component> bool hasComponent()
			{
				return _componentBitset[getComponentTypeID<Component>()];
			}

			template<typename Component> void removeComponent()
			{
				assert(hasComponent<Component>());
				auto componentTypeId = getComponentTypeID<Component>();

				auto ptr(_componentArray[componentTypeId]);
				ptr->Remove = true;

				_componentArray[componentTypeId] = nullptr;
				_componentBitset[componentTypeId] = false;
			}

			template <typename Component, typename... Args> Component* addComponent(Args&&... args)
			{
				assert(!hasComponent<Component>());

				Component* component(new Component(std::forward<Args>(args)...));
				component->Entity = this;
				
				auto componentTypeId = getComponentTypeID<Component>();

				_components.emplace_back(std::move(component));
				_componentArray[componentTypeId] = component;
				_componentBitset[componentTypeId] = true;

				component->Initialise();

				return component;
			}

			template <typename Component> Component* getComponent()
			{
				assert(hasComponent<Component>());
				auto ptr(_componentArray[getComponentTypeID<Component>()]);
				return reinterpret_cast<Component*>(ptr);
			}
		};
	}
}
