#include "SceneManager.h"

#include "Scene.h"
#include "util/tinyXml2.h"
#include "lua/LuaSystem.h"
#include <serialisation/XmlSerialiserSystem.h>

namespace forge
{
	namespace game
	{
		SceneManager::SceneManager(entity::IEntityManager* manager, asset::AssetManager* assetManager) :
			_manager(manager), _assetManger(assetManager)
		{
			_collisionsManager = physics::CollisionsManager::Instance();
		}

		SceneManager::~SceneManager()
		{
			_scenes.clear();
		}

		void SceneManager::Initialise()
		{
		}

		void SceneManager::Update(lua::LuaSystem* lsystem, double deltatime, bool Enablephysics)
		{
			_luaSystem = lsystem;

			if (_scenes.empty())
				return;

			if (Enablephysics)
				_collisionsManager->QuadCheck(_manager, _luaSystem->GetState());

			_scenes[currentScene]->Update(deltatime);

			if (PendingLoad)
			{
				LoadSceneFromFile(pendingSceneLoad.sceneName, pendingSceneLoad.scenePath);

				PendingLoad = false;
			}
		}

		void SceneManager::AddScene(std::string sceneName, scene::IScene* scene)
		{
			_scenes[sceneName] = scene;
			currentScene = sceneName;
		}

		void SceneManager::AddScene(std::string sceneName)
		{
			_scenes[sceneName] = new scene::Scene(_manager, _assetManger);
			currentScene = sceneName;
		}

		void SceneManager::LoadScene()
		{
			_scenes[currentScene]->LoadContent();
		}

		void SceneManager::LuaLoadScene(std::string scene)
		{		
			auto scenePath = _assetManger->GetAssetPathRoot() + scene;

			pendingSceneLoad = PendingSceneLoad();
			pendingSceneLoad.sceneName = scenePath;
			pendingSceneLoad.scenePath = scenePath;

			PendingLoad = true;
		}

		void SceneManager::LoadSceneFromFile(std::string sceneName, const std::string& scenePath)
		{
			_luaSystem->ClearGlobals();

			_manager->Clear();

			tinyxml2::XMLDocument document;
			auto loadResult = document.LoadFile(scenePath.c_str());

			if (loadResult != tinyxml2::XML_SUCCESS)
			{
				if (loadResult == tinyxml2::XML_ERROR_EMPTY_DOCUMENT)
				{
					SaveSceneToFile(sceneName, scenePath);
					return;
				}

				//Add Default Elements
				std::cout << "Error Occured Loading Scene : " << loadResult << std::endl;
			}

			auto rootElement = document.FirstChildElement("Scene");
			auto sceName = rootElement->Attribute("SceneName");

			auto currentEntityNode = rootElement->FirstChildElement("Entity");
			while (currentEntityNode != nullptr)
			{
				auto name = currentEntityNode->Attribute("name");
				auto entity = &_manager->addEntity(name);

				xml::XmlSerialiserSystem::DeserialiseEntity(&document, currentEntityNode, entity, _assetManger);

				currentEntityNode = currentEntityNode->NextSiblingElement("Entity");
			}

			for (auto const &entity : _manager->entities)
			{
				if (entity->hasComponent<component::Transform>())
				{
					auto entityTransform = entity->getComponent<component::Transform>();
					auto parentName = entityTransform->_potentialParentName;

					if (parentName != "")
					{
						auto parentEntity = _manager->Find(parentName);

						if (parentEntity != nullptr)
						{
							entityTransform->SetParent(parentEntity);
						}
					}
				}
			}
			
			AddScene(sceName);
		}

		void SceneManager::SaveSceneToFile(std::string sceneName, std::string scenePath)
		{
			tinyxml2::XMLDocument document;
			auto loadResult = document.LoadFile(scenePath.c_str());

			if (loadResult != tinyxml2::XML_SUCCESS)
			{
				std::cout << "Error Occured Loading Scene : " << loadResult << std::endl;
			}

			document.Clear();

			auto root = document.NewElement("Scene");
			root->SetAttribute("SceneName", sceneName.c_str());
			document.InsertFirstChild(root);

			for (auto object : _manager->entities)
			{
				tinyxml2::XMLElement* element = document.NewElement("Entity");
				element->SetAttribute("name", object->GetName().c_str());

				xml::XmlSerialiserSystem::SerialiseEntity(&document, element, object);

				root->InsertEndChild(element);
			}

			auto saveResult = document.SaveFile(scenePath.c_str());
		}		
	}
}