#pragma once

namespace forge
{
	namespace scene
	{
		class IScene
		{
		public:
			virtual void Update(double deltatime) = 0;
			virtual void LoadContent()=0;
		};
	}
}
