#pragma once
#include "ForceGenerator.h"

namespace forge
{
	namespace physics
	{
		class GravityForce : public ForceGenerator
		{
		private:
			Vector2D currentGravity;
			Vector2D realGravity;

		public:
			GravityForce();
			GravityForce(const float g);
			~GravityForce();

			Vector2D GetRealGravity() const											{ return realGravity; }
			Vector2D GetGravity() const												{ return currentGravity; }
			void SetGravity(const float f)											{ currentGravity = realGravity = Vector2D(0.f, f); }

			void KillGravity()														{ currentGravity = Vector2D(0.0f); }
			void RestartGravity()													{ currentGravity = realGravity; }

			void Update(component::Particle* p) override;
		};
	}
}