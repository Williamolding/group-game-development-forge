#include "PushForce.h"
#include "components/Particle.h"

namespace forge
{
	namespace physics
	{
		PushForce::PushForce() : forceVel(Vector2D(10, 0.0f)), dragCoefficient(0.72f)
		{}

		PushForce::PushForce(const Vector2D& velocity, const float& drag) : forceVel(velocity), dragCoefficient(drag)
		{}

		PushForce::PushForce(const float& x, const float& drag) : forceVel(Vector2D(x, .0f)), dragCoefficient(drag)
		{}

		PushForce::~PushForce() = default;

		void PushForce::Update(component::Particle* p)
		{
			if (p->BeingPushed())
			{
				const float velMag = Vector2D::Length(forceVel);
				
				const Vector2D unitVel = Vector2D::Normalize(forceVel);
				
				const float dragMag = dragCoefficient * powf(velMag, 2);
				
				const Vector2D dragForce = unitVel * -dragMag;

				p->AddForce(dragForce);
			}
		}

	}
}