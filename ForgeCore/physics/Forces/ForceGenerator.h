#pragma once
#include "Vector2D.h"

namespace forge {
	namespace component {
		class Particle;
	}
}

namespace forge
{
	namespace physics
	{
		class ForceGenerator
		{
		public:
			virtual ~ForceGenerator() = default;
			virtual void Update(component::Particle *p) = 0;
		};
	}
}
