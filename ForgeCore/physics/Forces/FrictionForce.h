#pragma once
#include "ForceGenerator.h"

namespace forge {
	namespace physics
	{
		class FrictionForce : public ForceGenerator
		{
		private:
			float coefficientOfFriction;
			float normal;
			float multiplier;

		public:
			FrictionForce();
			FrictionForce(const float &cof, const float &normal, const float &multiplier);

			void SetCoF(const float &cof)																	{ coefficientOfFriction = cof; }
			float GetCoF() const																			{ return coefficientOfFriction; }

			void SetNormal(const float &n)																	{ normal = n; }
			float GetNormal() const																			{ return normal; }

			void SetMultiplier(const float &m)																{ multiplier = m; }
			float GetMultiplier() const																		{ return multiplier; }

			void Update(component::Particle* p) override;
		};
	}
}