#include "GravityForce.h"
#include "components/Particle.h"

namespace forge
{
	namespace physics
	{
		GravityForce::GravityForce() : currentGravity(Vector2D(0, -9.81f))
		{}

		GravityForce::GravityForce(const float g) : currentGravity(Vector2D(0, g))
		{}

		GravityForce::~GravityForce() = default;

		void GravityForce::Update(component::Particle * _p)
		{
			_p->AddForce(currentGravity * _p->GetMass()); // Applies weight to particle
		}
	}
}