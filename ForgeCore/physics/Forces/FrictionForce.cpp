#include "FrictionForce.h"
#include <components/Particle.h>

namespace forge
{
	namespace physics
	{
		FrictionForce::FrictionForce() : coefficientOfFriction(.1f), normal(1.f), multiplier(100.f)
		{}

		FrictionForce::FrictionForce(const float& cof, const float& normal, const float &multiplier) : coefficientOfFriction(cof), normal(normal), multiplier(multiplier)
		{}

		void FrictionForce::Update(component::Particle* p)
		{
			const float frictionMag = coefficientOfFriction * normal;

			const Vector2D friction = Vector2D::Normalize(p->GetVelocity().Reverse()) * frictionMag;

			p->AddForce(friction * multiplier);
		}
	}
}