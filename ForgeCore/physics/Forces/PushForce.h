#pragma once
#include "ForceGenerator.h"

namespace forge
{
	namespace physics
	{
		class PushForce final : public ForceGenerator
		{
		private:
			Vector2D forceVel;
			float dragCoefficient;

		public:
			PushForce();
			PushForce(const Vector2D &velocity, const float &drag);
			PushForce(const float &x, const float &drag);
			~PushForce();

			void SetDragCoefficient(const float &dg)									{ dragCoefficient = dg; }
			float GetDragCoefficient() const											{ return dragCoefficient; }

			void SetVelocity(const Vector2D &wc)										{ forceVel = wc; }
			void SetVelocity(const float &x)											{ forceVel = Vector2D(x, 0.0f); }
			Vector2D GetVelocity() const												{ return forceVel; }

			void Update(component::Particle* p) override;
		};
		
	}
}