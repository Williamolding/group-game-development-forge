#include "CollisionDetectionManager.h"

#include "components/Transform.h"
#include "components/Colliders/CircleCollider.h"
#include "components/Colliders/RectCollider.h"
#include <minwindef.h>

namespace forge {
	namespace physics
	{
		CollisionDetectionManager* CollisionDetectionManager::mInstance = nullptr;

		CollisionDetectionManager::~CollisionDetectionManager()
		{
			delete mInstance;
			mInstance = nullptr;
		}

		CollisionDetectionManager * CollisionDetectionManager::Instance()
		{
			if (!mInstance)
				mInstance = new CollisionDetectionManager;

			return mInstance;
		}

		float CollisionDetectionManager::ClampDistance(const float value, const float minV, const float maxV) const
		{
			return min(max(value, minV), maxV); // Limits value between variables min...max
		}
				
		bool CollisionDetectionManager::CircleToRectCheck(entity::Entity* circle, entity::Entity* rect) const
		{
			const auto circleCol = circle->getComponent<component::CircleCollider>();
			const auto rectCol = rect->getComponent<component::RectCollider>();

			const auto circlePos = circle->getComponent<component::Transform>()->GetPositionV2D() + circleCol->GetOffset();
			const auto rectPos = rect->getComponent<component::Transform>()->GetPositionV2D() + rectCol->GetOffset();
			
			// Nearest position to the circle centre with the rect dimensions
			const float nearestX = ClampDistance(circlePos.X, rectPos.X - rectCol->GetWidth(), rectPos.X + rectCol->GetWidth());
			const float nearestY = ClampDistance(circlePos.Y, rectPos.Y - rectCol->GetHeight(), rectPos.Y + rectCol->GetHeight());

			const Vector2D nearest = Vector2D(nearestX, nearestY);

			// Distance between circle centre and closest point
			const float distance = powf(nearest.X - circlePos.X, 2) + powf(nearest.Y - circlePos.Y, 2);

			return distance < circleCol->GetRadius() * circleCol->GetRadius();
		}

		bool CollisionDetectionManager::BoundingCircleCheck(entity::Entity* a, entity::Entity* b) const
		{			
			const float radiiSum = a->getComponent<component::CircleCollider>()->GetRadius() + b->getComponent<component::CircleCollider>()->GetRadius();

			const Vector2D aCirclePos = a->getComponent<component::Transform>()->GetPositionV2D() + a->getComponent<component::CircleCollider>()->GetOffset();
			const Vector2D bCirclePos = b->getComponent<component::Transform>()->GetPositionV2D() + b->getComponent<component::CircleCollider>()->GetOffset();

			return radiiSum > aCirclePos.Distance(bCirclePos);
		}

		bool CollisionDetectionManager::BoundingBoxCheck(entity::Entity* a, entity::Entity* b) const
		{
			const auto aRect		= *a->getComponent<component::RectCollider>();
			const auto bRect		= *b->getComponent<component::RectCollider>();

			const auto aTransform	= *a->getComponent<component::Transform>();
			const auto bTransform	= *b->getComponent<component::Transform>();

			const Vector2D aPos		= aTransform.GetPositionV2D() + aRect.GetOffset();
			const Vector2D bPos		= bTransform.GetPositionV2D() + bRect.GetOffset();

			const float aMinX		= aPos.X - aRect.GetWidth();
			const float aMaxX		= aPos.X + aRect.GetWidth();
			const float aBottomY	= aPos.Y - aRect.GetHeight();
			const float aTopY		= aPos.Y + aRect.GetHeight();
													
			const float bMinX		= bPos.X - bRect.GetWidth();
			const float bMaxX		= bPos.X + bRect.GetWidth();
			const float bBottomY	= bPos.Y - bRect.GetHeight();
			const float bTopY		= bPos.Y + bRect.GetHeight();

			if (a->hasComponent<component::CircleCollider>())
				a->removeComponent<component::RectCollider>();

			return (aMaxX > bMinX && aMinX < bMaxX) &&
				(aTopY > bBottomY && aBottomY < bTopY);
		}
	}
}
