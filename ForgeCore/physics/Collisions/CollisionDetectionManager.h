#pragma once
#include "entity/Entity.h"

namespace forge { namespace  physics
{
	class CollisionDetectionManager final
	{
	public:
		~CollisionDetectionManager();

		static CollisionDetectionManager* Instance();

		bool BoundingBoxCheck(entity::Entity* a, entity::Entity *b) const;
		bool BoundingCircleCheck(entity::Entity *a, entity::Entity *b) const;
		bool CircleToRectCheck(entity::Entity* circle, entity::Entity* rect) const;

	private:
		CollisionDetectionManager() = default;
		float ClampDistance(const float value, const float min, const float max) const;

	private:
		static CollisionDetectionManager *mInstance;
	};
} 
}
