#pragma once
#include <components/Particle.h>

namespace forge {
	namespace physics
	{
		struct Circle final
		{
			Vector2D centre;
			float radius;

			Circle(const float radius, const Vector2D &centre) : centre(centre), radius(radius)
			{}
		};

		struct BoundingBox final
		{
			float minX;
			float maxX;
			float bottomY;
			float topY;
			float width;
			float height;

			BoundingBox(const float _minX, const float _maxX, const float _minY, const float _maxY, const float width = 0.f, const float height = 0.f) : minX(_minX), maxX(_maxX), bottomY(_minY), topY(_maxY), width(width), height(height)
			{}
		};
		
		struct iResolver
		{
			virtual ~iResolver() = default;
			virtual void Separation(entity::Entity *a, entity::Entity *b) = 0;

		protected:
			// Reference for formulas - https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Coefficient_of_restitution.html 
			virtual void Resolve(component::Particle* aParticle, component::Particle* bParticle) const
			{
				const auto BoxAMass = aParticle->GetMass();
				const auto BoxBMass = bParticle->GetMass();

				const Vector2D newVelA = (aParticle->GetVelocity() * BoxAMass + bParticle->GetVelocity() * BoxBMass +
					(bParticle->GetVelocity() - aParticle->GetVelocity()) * BoxBMass * aParticle->GetCoefficientOfRestitution()) /
					(BoxAMass + BoxBMass);

				const Vector2D newVelB = (aParticle->GetVelocity() * BoxAMass + bParticle->GetVelocity() * BoxBMass +
					(aParticle->GetVelocity() - bParticle->GetVelocity()) * BoxAMass * bParticle->GetCoefficientOfRestitution()) /
					(BoxAMass + BoxBMass);

				aParticle->SetVelocity(newVelA);
				bParticle->SetVelocity(newVelB);
			}
		};


	}
}

/* -----------------------------------------------------------------------------------------
*Another possible collision Resolution Method
* -----------------------------------------------------------------------------------------
Vector2D relativeVelocities = b->getComponent<component::Particle>()->GetVelocity() - a->getComponent<component::Particle>()->GetVelocity();

Vector2D normal = Vector2D::Perpendicular(a->getComponent<component::Particle>()->GetVelocity());

float velAlongNormal = Vector2D::Dot(relativeVelocities, normal);

if (velAlongNormal > 0)
return;

float e = std::min(a->getComponent<component::Particle>()->GetCoefficientOfRestitution(), b->getComponent<component::Particle>()->GetCoefficientOfRestitution());

float scalar = -(1 + e) * velAlongNormal;
scalar /= 1 / a->getComponent<component::Particle>()->GetMass() + 1 / b->getComponent<component::Particle>()->GetMass();

Vector2D impulse = normal * scalar;

float aInverseMass;
float bInverseMass;

if (a->getComponent<component::Particle>()->GetMass() != 0)
aInverseMass = 1 / a->getComponent<component::Particle>()->GetMass();
else
aInverseMass = 0;

if (b->getComponent<component::Particle>()->GetMass() != 0)
bInverseMass = 1 / b->getComponent<component::Particle>()->GetMass();
else
bInverseMass = 0;

a->getComponent<component::Particle>()->SetVelocity(a->getComponent<component::Particle>()->GetVelocity() - impulse * aInverseMass);
b->getComponent<component::Particle>()->SetVelocity(b->getComponent<component::Particle>()->GetVelocity() + impulse * bInverseMass);

float AggregateMass = a->getComponent<component::Particle>()->GetMass() + b->getComponent<component::Particle>()->GetMass();
float ratio = a->getComponent<component::Particle>()->GetMass() / AggregateMass;
a->getComponent<component::Particle>()->SetVelocity(a->getComponent<component::Particle>()->GetVelocity() - impulse * ratio);

ratio = b->getComponent<component::Particle>()->GetMass() / AggregateMass;
b->getComponent<component::Particle>()->SetVelocity(b->getComponent<component::Particle>()->GetVelocity() + impulse * ratio);
*/