﻿#include "Circle2RectResolution.h"

#include <components/Colliders/RectCollider.h>
#include <components/Colliders/CircleCollider.h>
#include "RectResolution.h"

namespace forge {
	namespace physics
	{
		Circle2RectResolution *Circle2RectResolution::mInstance = nullptr;

		Circle2RectResolution* Circle2RectResolution::Instance()
		{
			if (!mInstance)
				mInstance = new Circle2RectResolution;

			return mInstance;
		}

		Circle2RectResolution::~Circle2RectResolution()
		{
			delete mInstance;
			mInstance = nullptr;
		}

		void Circle2RectResolution::Separation(entity::Entity* circle, entity::Entity* rect)
		{
			if (circle->getComponent<component::CircleCollider>()->IsTrigger() || rect->getComponent<component::RectCollider>()->IsTrigger())
				return;

			const auto circleTrans = circle->getComponent<component::Transform>();

			const Circle circleComponent (circle->getComponent<component::CircleCollider>()->GetRadius(), 
				circleTrans->GetPositionV2D() + circle->getComponent<component::CircleCollider>()->GetOffset());

			auto rectTrans = rect->getComponent<component::Transform>();
			const auto rectCol = rect->getComponent<component::RectCollider>();
			
			const float rectMinX	= rectTrans->GetPosition().x - rectCol->GetWidth() + rectCol->GetOffset().X;
			const float rectMaxX	= rectTrans->GetPosition().x + rectCol->GetWidth() + rectCol->GetOffset().X;
			const float rectBottomY = rectTrans->GetPosition().y - rectCol->GetHeight() + rectCol->GetOffset().Y;
			const float rectTopY	= rectTrans->GetPosition().y + rectCol->GetHeight() + rectCol->GetOffset().Y;

			float angles[4] = { Vector2D::AngleBetweenVectors(circleComponent.centre, Vector2D(rectMinX, rectTopY)) ,
				Vector2D::AngleBetweenVectors(circleComponent.centre, Vector2D(rectMaxX, rectTopY)) ,
				Vector2D::AngleBetweenVectors(circleComponent.centre, Vector2D(rectMinX, rectBottomY)) ,
				Vector2D::AngleBetweenVectors(circleComponent.centre, Vector2D(rectMaxX, rectBottomY)) };

			float angle = fabsf(angles[0]);

			for (short i = 1; i < 4; ++i)
				angle = static_cast<float>(min(angle, fabsf(angles[i])));

			const Vector2D pointOnCircumference = Vector2D(circleComponent.centre.X + circleComponent.radius * cosf(angle),
				circleComponent.centre.Y + circleComponent.radius * sinf(angle));

			const Vector2D currentPos = circleComponent.centre;
			const Vector2D currentPosB = rectTrans->GetPositionV2D();

			circleTrans->SetPositionV2D(currentPos);
			rectTrans->SetPositionV2D(currentPosB);

			if (circle->hasComponent<component::Particle>() && rect->hasComponent<component::Particle>())
				Resolve(circle->getComponent<component::Particle>(), rect->getComponent<component::Particle>());
			
			RectResolution::Instance()->Separation(circle, rect);
		}
	}
}

// if (!circle->hasComponent<component::Particle>())
// 	return;
//
// Vector2D nearest = Vector2D(std::max(rectMinX, std::min(circleComponent.centre.X, rectMaxX)),
// 	std::max(rectBottomY, std::min(circleComponent.centre.Y, rectTopY)));
//
// Vector2D distance = circleComponent.centre - nearest;
//
// auto tangentVel = Vector2D::Dot(Vector2D::Normalize(distance), circle->getComponent<component::Particle>()->GetVelocity());
//
// circle->getComponent<component::Particle>()->SetVelocity(circle->getComponent<component::Particle>()->GetVelocity() - tangentVel);