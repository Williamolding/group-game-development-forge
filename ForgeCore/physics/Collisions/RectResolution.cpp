﻿#include "RectResolution.h"

#include <components/Colliders/RectCollider.h>
#include <components/Colliders/CircleCollider.h>

namespace forge
{
	namespace physics
	{
		RectResolution* RectResolution::mInstance = nullptr;

		RectResolution::~RectResolution()
		{
			delete mInstance;
			mInstance = nullptr;
		}

		RectResolution* RectResolution::Instance()
		{
			if (!mInstance)
				mInstance = new RectResolution;

			return mInstance;
		}

		void RectResolution::Separation(entity::Entity *a, entity::Entity *b)
		{		
			bool alreadyHadRectCol = true;

			if (a->hasComponent<component::CircleCollider>() && !a->hasComponent<component::RectCollider>())
			{
				const auto aCollider = a->getComponent<component::CircleCollider>();
				a->addComponent<component::RectCollider>(aCollider->GetRadius(), aCollider->GetRadius(), nullptr, aCollider->GetOffset());
				a->getComponent<component::RectCollider>()->SetTrigger(aCollider->IsTrigger());
				alreadyHadRectCol = false;
			}

			if (a->getComponent<component::RectCollider>()->IsTrigger() || b->getComponent<component::RectCollider>()->IsTrigger())
				return;
			
			const auto aTransform = a->getComponent<component::Transform>();
			const auto bTransform = b->getComponent<component::Transform>();

			const auto aRect = a->getComponent<component::RectCollider>();
			const auto bRect = b->getComponent<component::RectCollider>();

			const auto newPosA = aTransform->GetPositionV2D() + aRect->GetOffset();
			const auto newPosB = bTransform->GetPositionV2D() + bRect->GetOffset();

			// Parameter order - minX, maxX, bottomY, topY
			const BoundingBox aBB(newPosA.X - aRect->GetWidth(), newPosA.X + aRect->GetWidth(),
				newPosA.Y - aRect->GetHeight(), newPosA.Y + aRect->GetHeight());

			// Parameter order - minX, maxX, bottomY, topY
			const BoundingBox bBB(newPosB.X - bRect->GetWidth(), newPosB.X + bRect->GetWidth(),
				newPosB.Y - bRect->GetHeight(), newPosB.Y + bRect->GetHeight());

			const float minVertical = min(aBB.topY - bBB.bottomY, bBB.topY - aBB.bottomY);
			const float minHorizontal = min(bBB.maxX - aBB.minX, aBB.maxX - bBB.minX);

			Vector2D overlap = Vector2D(minHorizontal, minVertical) + Vector2D(.0001f);
			overlap.ToPositives(); // Makes X & Y positives

			if (overlap.GetX() > overlap.GetY()) // Vertical Collisions
			{
				if (CollideWithTopOfBox(aBB, bBB)) // b collides with the top of a
					BCollideOnTopOfA(a, b, overlap);				
				else if (CollideWithBottomOfBox(aBB, bBB)) // b collides with the bottom of a				
					BCollideOnBottomOfA(a, b, overlap);				
			}
			else if (overlap.GetX() < overlap.GetY()) // Horizontal Collisions
			{
				if (CollideWithLeftSideOfBox(aBB, bBB)) // b collides with the left side of a
					BCollideWithLeftOfA(a, b, overlap);
				else if (CollideWithRightSideOfBox(aBB, bBB)) // b collides with the right side of a
					BCollideWithRightA(a, b, overlap);
			}

			if (a->hasComponent<component::Particle>() && b->hasComponent<component::Particle>())
				Resolve(a->getComponent<component::Particle>(), b->getComponent<component::Particle>());

			if (a->hasComponent<component::CircleCollider>() && !alreadyHadRectCol)
				a->removeComponent<component::RectCollider>();
		}

		// Resolution of collision
		void RectResolution::BCollideOnTopOfA(entity::Entity* a, entity::Entity* b, const Vector2D& overlap) const
		{
			if (a->hasComponent<component::Particle>())
			{
				const auto transform = a->getComponent<component::Transform>();
			
				auto aParticle = a->getComponent<component::Particle>();
				if (aParticle->GetGravityForceGenerator()->GetGravity().Y > 0)
				{
					if (!b->hasComponent<component::Particle>())
					{
						aParticle->GetGravityForceGenerator()->KillGravity();
						aParticle->SetVelocity(aParticle->GetVelocity().X, 0);
					}
					else
					{
						aParticle->SetVelocity(aParticle->GetVelocity().X, b->getComponent<component::Particle>()->GetVelocity().Y);
						aParticle->GetGravityForceGenerator()->KillGravity();
					}
				}

				if (!b->hasComponent<component::Particle>())
					aParticle->SetVelocity(aParticle->GetVelocity().X, 0);
				
				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X, transform->GetPositionV2D().Y - overlap.Y));
			}

			if (b->hasComponent<component::Particle>())
			{
				const auto transform = b->getComponent<component::Transform>();
				
				auto bParticle = b->getComponent<component::Particle>();
				if (!a->hasComponent<component::Particle>())
				{
					bParticle->GetGravityForceGenerator()->KillGravity();
					bParticle->SetVelocity(bParticle->GetVelocity().X, 0);
				}
				else
				{
					const auto aParticle = a->getComponent<component::Particle>();
					bParticle->SetVelocity(bParticle->GetVelocity().X, aParticle->GetVelocity().Y);
					bParticle->GetGravityForceGenerator()->KillGravity();
				}

				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X, transform->GetPositionV2D().Y + overlap.Y));
			}
		}

		void RectResolution::BCollideOnBottomOfA(entity::Entity* a, entity::Entity* b, const Vector2D& overlap) const
		{
			if (a->hasComponent<component::Particle>())
			{
				const auto transform = a->getComponent<component::Transform>();

				auto aParticle = a->getComponent<component::Particle>();
				if (!b->hasComponent<component::Particle>())
				{
					aParticle->GetGravityForceGenerator()->KillGravity();
					aParticle->SetVelocity(aParticle->GetVelocity().X, 0);
				}
				else
				{
					const auto bParticle = b->getComponent<component::Particle>();
					aParticle->SetVelocity(aParticle->GetVelocity().X, bParticle->GetVelocity().Y);
					aParticle->GetGravityForceGenerator()->KillGravity();
				}

				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X, transform->GetPositionV2D().Y + overlap.Y));
			}

			if (b->hasComponent<component::Particle>())
			{
				const auto transform = b->getComponent<component::Transform>();
				
				auto bParticle = b->getComponent<component::Particle>();
				if (bParticle->GetGravityForceGenerator()->GetGravity().Y > 0)
				{
					if (!a->hasComponent<component::Particle>())
					{
						bParticle->GetGravityForceGenerator()->KillGravity();
						bParticle->SetVelocity(bParticle->GetVelocity().X, 0);
					}
					else
					{
						bParticle->SetVelocity(bParticle->GetVelocity().X, a->getComponent<component::Particle>()->GetVelocity().Y);
						bParticle->GetGravityForceGenerator()->KillGravity();
					}
				}
				if (!a->hasComponent<component::Particle>())
					bParticle->SetVelocity(bParticle->GetVelocity().X, 0);

				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X, transform->GetPositionV2D().Y - overlap.Y));
			}
		}

		void RectResolution::BCollideWithLeftOfA(entity::Entity* a, entity::Entity* b, const Vector2D& overlap) const
		{
			if (a->hasComponent<component::Particle>())
			{
				const auto transform = a->getComponent<component::Transform>();
				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X + overlap.X, transform->GetPositionV2D().Y));
				a->getComponent<component::Particle>()->GetGravityForceGenerator()->RestartGravity();
			}

			if (b->hasComponent<component::Particle>())
			{
				const auto transform = b->getComponent<component::Transform>();
				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X - overlap.X, transform->GetPositionV2D().Y));
				b->getComponent<component::Particle>()->GetGravityForceGenerator()->RestartGravity();
			}
		}

		void RectResolution::BCollideWithRightA(entity::Entity* a, entity::Entity* b, const Vector2D& overlap) const
		{
			if (a->hasComponent<component::Particle>())
			{
				const auto transform = a->getComponent<component::Transform>();
				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X - overlap.X, transform->GetPositionV2D().Y));
				a->getComponent<component::Particle>()->GetGravityForceGenerator()->RestartGravity();
			}
			if (b->hasComponent<component::Particle>())
			{
				const auto transform = b->getComponent<component::Transform>();
				transform->SetPositionV2D(Vector2D(transform->GetPositionV2D().X + overlap.X, transform->GetPositionV2D().Y));
				b->getComponent<component::Particle>()->GetGravityForceGenerator()->RestartGravity();
			}
		}

		// Location of collision
		bool RectResolution::CollideWithTopOfBox(const BoundingBox &a, const BoundingBox &b) const // b collides with the top of a
		{
			return (a.minX <= b.maxX && a.maxX >= b.minX)
				&& (b.bottomY < a.topY && b.topY > a.topY);
		}

		bool RectResolution::CollideWithBottomOfBox(const BoundingBox &a, const BoundingBox &b) const // b collides with the bottom of a
		{
			return (a.minX <= b.maxX && a.maxX >= b.minX)
				&& (b.topY > a.bottomY && b.bottomY < a.bottomY);
		}

		bool RectResolution::CollideWithLeftSideOfBox(const BoundingBox &a, const BoundingBox &b) const // b collides with the left side of a
		{
			return (a.minX < b.maxX && a.minX > b.minX)
				&& (a.bottomY <= b.topY && a.topY >= b.bottomY);
		}

		bool RectResolution::CollideWithRightSideOfBox(const BoundingBox &a, const BoundingBox &b) const // b collides with the right side of a
		{
			return (a.maxX > b.minX && a.maxX < b.maxX)
				&& (a.bottomY <= b.topY && a.topY >= b.bottomY);
		}
	}
}
