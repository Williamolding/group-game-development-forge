#include "CollisionsManager.h"

#include "World/QuadTree.h"
#include "RectResolution.h"
#include "CircleResolution.h"
#include "entity/EntityManager.h"
#include "Circle2RectResolution.h"
#include "components/Colliders/CircleCollider.h"
#include "components/Colliders/RectCollider.h"

namespace forge
{
	namespace physics
	{
		#define RECT "rect"
		#define CIRCLE "circle"
		#define C2R "c2r"

		CollisionsManager *CollisionsManager::mInstance = nullptr;
		
		CollisionsManager* CollisionsManager::Instance()
		{
			if (!mInstance)
				mInstance = new CollisionsManager;

			return mInstance;
		}

		CollisionsManager::CollisionsManager() : _luaState(nullptr)
		{
			_cdManager = CollisionDetectionManager::Instance();

			resolutionsMap.insert(std::pair<std::string, iResolver*>(RECT, RectResolution::Instance()));
			resolutionsMap.insert(std::pair<std::string, iResolver*>(CIRCLE, CircleResolution::Instance()));
			resolutionsMap.insert(std::pair<std::string, iResolver*>(C2R, Circle2RectResolution::Instance()));
		}

		CollisionsManager::~CollisionsManager()
		{
			delete mInstance;
			mInstance = nullptr;

			for (const auto& resolver : resolutionsMap)
			{
				delete resolver.second;
			}
		}

		void CollisionsManager::QuadCheck(entity::IEntityManager* _manager, lua_State* luaState)
		{
			_luaState = luaState;

			game::QuadTree quad = game::QuadTree(new game::Quadrant(0, 0, 20000, 20000));

			quad.EmptyTrees();

			for (auto e : _manager->entities)
			{
				if (e->hasComponent<component::RectCollider>() || e->hasComponent<component::CircleCollider>())
					quad.AddIntoTree(e);
			}

			vector<entity::Entity*> returnList;
			
			for (auto e1 : _manager->entities)
			{
				if (!e1->hasComponent<component::RectCollider>() && !e1->hasComponent<component::CircleCollider>())
					continue;

				returnList.clear();
				quad.CollidablesList(returnList, e1);

				for (auto e2 : returnList)
				{
					CheckCollisions(e1, e2);
				}
			}
		}

		void CollisionsManager::IsColliderColliding(entity::Entity* e) const
		{
			const auto collider = e->hasComponent<component::RectCollider>() ? 
				dynamic_cast<component::iCollider*>(e->getComponent<component::RectCollider>()) 
				: dynamic_cast<component::iCollider*>(e->getComponent<component::CircleCollider>());

			if (e->hasComponent<component::Particle>() && collider->isColliding)
			{
				const auto particle = e->getComponent<component::Particle>();

				particle->GetGravityForceGenerator()->RestartGravity();
			}
		}

		void CollisionsManager::CheckCollisions(entity::Entity* entityA, entity::Entity* entityB)
		{
			IsColliderColliding(entityA);
			IsColliderColliding(entityB);

			if (entityA->hasComponent<component::CircleCollider>() && entityB->hasComponent<component::RectCollider>())
				CircleToRectCollision(entityA, entityB);

			else if (entityA->hasComponent<component::CircleCollider>()
				&& entityB->hasComponent<component::CircleCollider>())
				CircleCollision(entityA, entityB);

			else if (entityA->hasComponent<component::RectCollider>()
				&& entityB->hasComponent<component::RectCollider>())
				RectCollision(entityA, entityB);
		}

		void CollisionsManager::CircleToRectCollision(entity::Entity* circle, entity::Entity* rect)
		{
			const auto circleCol = circle->getComponent<component::CircleCollider>();
			const auto rectCol = rect->getComponent<component::RectCollider>();

			circleCol->isColliding = false;
			rectCol->isColliding = false;

			if (_cdManager->CircleToRectCheck(circle, rect))
			{
				CollisionScripts(circle, rect);

				circleCol->isColliding = true;
				rectCol->isColliding = true;

				resolutionsMap[C2R]->Separation(circle, rect);
			}
			else
			{
				NoCollisionScript(circle, rect);
			}
		}

		void CollisionsManager::CircleCollision(entity::Entity* circleA, entity::Entity* circleB)
		{
			circleA->getComponent<component::CircleCollider>()->isColliding = false;
			circleB->getComponent<component::CircleCollider>()->isColliding = false;

			if (_cdManager->BoundingCircleCheck(circleA, circleB))
			{
				circleA->getComponent<component::CircleCollider>()->isColliding = true;
				circleB->getComponent<component::CircleCollider>()->isColliding = true;

				CollisionScripts(circleA, circleB);
				resolutionsMap[CIRCLE]->Separation(circleA, circleB);
			}
			else
			{
				NoCollisionScript(circleA, circleB);
			}
		}

		void CollisionsManager::RectCollision(entity::Entity* rectA, entity::Entity* rectB)
		{
			const auto rectColliderA = rectA->getComponent<component::RectCollider>();
			const auto rectColliderB = rectB->getComponent<component::RectCollider>();

			rectColliderA->isColliding = false;
			rectColliderB->isColliding = false;

			if (_cdManager->BoundingBoxCheck(rectA, rectB))
			{
				rectColliderA->isColliding = true;
				rectColliderB->isColliding = true;

				CollisionScripts(rectA, rectB);
				resolutionsMap[RECT]->Separation(rectA, rectB);
			}
			else
			{
				NoCollisionScript(rectA, rectB);
			}
		}

		void CollisionsManager::CollisionScripts(entity::Entity* a, entity::Entity* b) const
		{
			const auto colliderA = a->hasComponent<component::RectCollider>() ? dynamic_cast<component::iCollider*>(a->getComponent<component::RectCollider>()) : dynamic_cast<component::iCollider*>(a->getComponent<component::CircleCollider>());
			const auto colliderB = b->hasComponent<component::RectCollider>() ? dynamic_cast<component::iCollider*>(b->getComponent<component::RectCollider>()) : dynamic_cast<component::iCollider*>(b->getComponent<component::CircleCollider>());

			if (a->hasComponent<component::Script>())
			{
				auto scriptA = a->getComponent<component::Script>();

				if (colliderA->IsTrigger() && !colliderA->isTriggerColliding)
				{
					std::string name = a->GetName();
					std::string other = b->GetName();

					luabridge::getGlobalNamespace(_luaState)
						.beginNamespace("Entity")
						.addVariable("Me", &name)
						.addVariable("Other", &other)
						.endNamespace();

					colliderA->isTriggerColliding = true;
					scriptA->OnTriggerEnter();
				}
				else
				{
					scriptA->CallOnCollision();
				}
			}

			if (b->hasComponent<component::Script>())
			{
				auto scriptB = b->getComponent<component::Script>();

				if (colliderB->IsTrigger() && !colliderB->isTriggerColliding)
				{
					std::string name = b->GetName();
					std::string other = a->GetName();

					luabridge::getGlobalNamespace(_luaState)
						.beginNamespace("Entity")
						.addVariable("Me", &name)
						.addVariable("Other", &other)
						.endNamespace();

					colliderB->isTriggerColliding = true;
					scriptB->OnTriggerEnter();
				}
				else
				{
					scriptB->CallOnCollision();
				}
			}
		}

		void CollisionsManager::NoCollisionScript(entity::Entity* a, entity::Entity* b) const
		{
			const auto colliderA = a->hasComponent<component::RectCollider>() ? dynamic_cast<component::iCollider*>(a->getComponent<component::RectCollider>()) : dynamic_cast<component::iCollider*>(a->getComponent<component::CircleCollider>());
			const auto colliderB = b->hasComponent<component::RectCollider>() ? dynamic_cast<component::iCollider*>(b->getComponent<component::RectCollider>()) : dynamic_cast<component::iCollider*>(b->getComponent<component::CircleCollider>());
			
			if (a->hasComponent<component::Script>())
			{
				if (colliderA->IsTrigger() && colliderA->isTriggerColliding)
				{
					std::string name = a->GetName();
					std::string other = b->GetName();

					luabridge::getGlobalNamespace(_luaState)
						.beginNamespace("Entity")
						.addVariable("Me", &name)
						.addVariable("Other", &other)
						.endNamespace();

					auto scriptA = a->getComponent<component::Script>();
					colliderA->isTriggerColliding = false;
					colliderA->isColliding = false;
					scriptA->OnTriggerExit();
				}
			}

			if (b->hasComponent<component::Script>())
			{
				std::string name = b->GetName();
				std::string other = a->GetName();

				luabridge::getGlobalNamespace(_luaState)
					.beginNamespace("Entity")
					.addVariable("Me", &name)
					.addVariable("Other", &other)
					.endNamespace();

				if (colliderB->IsTrigger() && colliderB->isTriggerColliding)
				{
					auto scriptB = b->getComponent<component::Script>();
					colliderB->isColliding = false;
					colliderB->isTriggerColliding = false;
					scriptB->OnTriggerExit();
				}
			}
		}
	}
}
