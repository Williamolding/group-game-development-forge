﻿#pragma once
#include "iResolver.h"

namespace forge {
	namespace physics
	{
		class CircleResolution final : public iResolver
		{
		public:
			~CircleResolution();

			static CircleResolution* Instance();
			void Separation(entity::Entity* a, entity::Entity* b) override;

		private:
			CircleResolution() = default;
			void Resolve(component::Particle* aParticle, component::Particle* bParticle) const override;

		private:
			static CircleResolution* mInstance;
		};
	}
}
