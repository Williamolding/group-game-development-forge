﻿#include "CircleResolution.h"
#include "components/Colliders/CircleCollider.h"

namespace forge {
	namespace physics
	{
		CircleResolution* CircleResolution::mInstance = nullptr;

		CircleResolution* CircleResolution::Instance()
		{
			if (!mInstance)
				mInstance = new CircleResolution;

			return mInstance;
		}

		CircleResolution::~CircleResolution()
		{
			delete mInstance;
			mInstance = nullptr;
		}

		void CircleResolution::Separation(forge::entity::Entity* a, forge::entity::Entity* b)
		{
			if (a->getComponent<component::CircleCollider>()->IsTrigger() || b->getComponent<component::CircleCollider>()->IsTrigger())
				return;

			const auto aTransform = a->getComponent<component::Transform>();
			const auto bTransform = b->getComponent<component::Transform>();

			const auto aCircle = a->getComponent<component::CircleCollider>();
			const auto bCircle = b->getComponent<component::CircleCollider>();

			const auto offsetPosA = aTransform->GetPositionV2D() + aCircle->GetOffset();
			const auto offsetPosB = bTransform->GetPositionV2D() + bCircle->GetOffset();

			const float radiiSum = aCircle->GetRadius() + bCircle->GetRadius();
			const float distance = Vector2D::Length(offsetPosA - offsetPosB);
			const float penetrationDepth = radiiSum - distance;
			const Vector2D direction = Vector2D::Normalize(offsetPosA - offsetPosB);

			const Vector2D newPosA = aTransform->GetPositionV2D() + (direction * penetrationDepth);
			const Vector2D newPosB = bTransform->GetPositionV2D() - (direction * penetrationDepth);

			if (a->hasComponent<component::Particle>())
				aTransform->SetPositionV2D(newPosA);

			if (b->hasComponent<component::Particle>())
				bTransform->SetPositionV2D(newPosB);

			if (a->hasComponent<component::Particle>() && b->hasComponent<component::Particle>())
				Resolve(a->getComponent<component::Particle>(), b->getComponent<component::Particle>());
		}

		void CircleResolution::Resolve(component::Particle* aParticle, component::Particle* bParticle) const
		{
			// Preferred Coefficient of Restitution equation for circles - Make shift equation and more reactive
			const float circleAMass = aParticle->GetMass();
			const float circleBMass = bParticle->GetMass();

			const float e = (aParticle->GetCoefficientOfRestitution() + bParticle->GetCoefficientOfRestitution()) / 2.f;
			
			Vector2D circleARest = aParticle->GetVelocity() * (circleAMass - circleBMass * e);
			Vector2D circleBRest = bParticle->GetVelocity() * (circleBMass * (1.f + e));
			
			Vector2D CollisionVelA = (circleARest + circleBRest) / (circleAMass + circleBMass);
			
			circleARest = aParticle->GetVelocity() * circleAMass;
			circleBRest = bParticle->GetVelocity() * circleBMass;
			
			const Vector2D aMomentum = CollisionVelA * circleAMass;
			
			Vector2D CollisionVelB = (circleARest + circleBRest - aMomentum) / circleBMass;
			
			const Vector2D normal1 = Vector2D::Normalize(aParticle->GetTransform()->GetPositionV2D() - bParticle->GetTransform()->GetPositionV2D());
			const Vector2D normal2 = normal1.Reverse();
			
			const Vector2D relativeVel1 = normal1 * Vector2D::Dot(normal1, CollisionVelA);
			const Vector2D relativeVel2 = normal2 * Vector2D::Dot(normal2, CollisionVelB);
			
			CollisionVelA += relativeVel1 - relativeVel2;
			CollisionVelB += relativeVel2 - relativeVel1;
			
			aParticle->SetVelocity(CollisionVelA);
			bParticle->SetVelocity(CollisionVelB);
		}
	}
}
