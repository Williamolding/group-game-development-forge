﻿#pragma once
#include "iResolver.h"

namespace forge {
	namespace physics
	{
		class Circle2RectResolution final : public iResolver
		{
		public:
			~Circle2RectResolution();

			static Circle2RectResolution* Instance();
			void Separation(entity::Entity *circle, entity::Entity *rect) override;

		private:
			Circle2RectResolution() = default;

		private: 
			static Circle2RectResolution* mInstance;
		};
	}
}