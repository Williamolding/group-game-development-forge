#pragma once
#include "iResolver.h"
#include "CollisionDetectionManager.h"
#include <lua.hpp>

namespace forge
{
	namespace physics
	{
		class CollisionsManager final
		{
		public:
			~CollisionsManager();
			
			static CollisionsManager* Instance();
			void QuadCheck(entity::IEntityManager* _manager, lua_State* luaState);

		private:
			void CheckCollisions(entity::Entity* entityA, entity::Entity* entityB);

			void RectCollision(entity::Entity* rectA, entity::Entity* rectB);
			void CircleCollision(entity::Entity* circleA, entity::Entity* circleB);
			void CircleToRectCollision(entity::Entity* circle, entity::Entity* rect);

			void IsColliderColliding(entity::Entity* e) const;

			void CollisionScripts(entity::Entity* a, entity::Entity* b) const;
			void NoCollisionScript(entity::Entity* a, entity::Entity* b) const;

		private:
			CollisionsManager();

		private:
			static CollisionsManager * mInstance;

			CollisionDetectionManager *_cdManager;
			
			lua_State* _luaState;

			std::map<std::string, iResolver*> resolutionsMap;
		};
	}
}
