﻿ #pragma once
#include "iResolver.h"

namespace forge {
	namespace physics
	{
		class RectResolution final : public iResolver
		{
		public:
			~RectResolution();

			static RectResolution* Instance();
			void Separation(entity::Entity *a, entity::Entity *b) override;

		private:
			RectResolution() = default;

			void BCollideOnTopOfA(entity::Entity *a, entity::Entity *b, const Vector2D& overlap) const;
			void BCollideWithRightA(entity::Entity *a, entity::Entity *b, const Vector2D& overlap) const;
			void BCollideWithLeftOfA(entity::Entity *a, entity::Entity *b, const Vector2D& overlap) const;
			void BCollideOnBottomOfA(entity::Entity *a, entity::Entity *b, const Vector2D& overlap) const;

			bool CollideWithTopOfBox(const BoundingBox &a, const BoundingBox &b) const;
			bool CollideWithBottomOfBox(const BoundingBox &a, const BoundingBox &b) const;
			bool CollideWithLeftSideOfBox(const BoundingBox &a, const BoundingBox &b) const;
			bool CollideWithRightSideOfBox(const BoundingBox &a, const BoundingBox &b) const;
		
		private: 
			static RectResolution* mInstance;
		};
	}
}
