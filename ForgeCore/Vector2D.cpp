#include "Vector2D.h"

Vector2D::Vector2D()
{
	X = Y = 0.0f;
}

Vector2D::Vector2D(const float &_x, const float &_y) : X(_x), Y(_y) {/*Empty*/}

Vector2D::Vector2D(const float &_v)
{
	X = Y = _v;
}

float Vector2D::Length(const Vector2D &v)
{
	return sqrtf(powf(v.X, 2) + powf(v.Y, 2));
}

float Vector2D::Dot(const Vector2D &v, const Vector2D &u)
{
	return (u.X * v.X) + (u.Y * v.Y);
}

float Vector2D::AngleBetweenVectors(const Vector2D &v, const Vector2D &u, const bool inDegrees)
{
	const float radians = acosf(Dot(v, u) / (Length(v) * Length(u)));

	return inDegrees ?	RadiansToDegrees(radians) :	radians;
}

Vector2D Vector2D::Normalize(const Vector2D &v)
{
	if (Length(v) == 0.0f)	
		return v;
	
	return v / Length(v);
}

Vector2D Vector2D::Perpendicular(const Vector2D &v)
{
	return Vector2D(-v.Y, v.X);
}

void Vector2D::Zero()
{
	X = Y = 0.0f;
}

void Vector2D::ToPositives()
{
	X = fabsf(X);
	Y = fabsf(Y);
}

void Vector2D::Truncate(const float &max)
{
	if (Length(*this) > max)	
		*this = Normalize(*this) * max;	
}

Vector2D Vector2D::Reverse() const
{
	return *this * -1.0f;
}

float Vector2D::Distance(const Vector2D &v) const
{
	return Length(v - *this);
}
