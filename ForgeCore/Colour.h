#pragma once

namespace forge
{
	namespace render
	{
		class Colour
		{
		public:
			float R;
			float G;
			float B;

			Colour(): R(1) , G(1) , B(1)
			{

			}

			Colour(float r, float g, float b)
			{
				R = r;
				G = g;
				B = b;
			}		
		};
	}
}
