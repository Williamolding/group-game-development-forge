#pragma once
#include "IScene.h"
#include <entity/EntityManager.h>
#include <asset/AssetManager.h>

namespace forge
{
	namespace scene
	{
		class Scene : public IScene
		{
			entity::IEntityManager*	_manager;
			asset::AssetManager*	_assetManager;
		public:
			Scene(entity::IEntityManager* manager, asset::AssetManager* assetManager): _manager(manager), _assetManager(assetManager)
			{
				
			}

			void Update(double deltatime) override
			{
				
			}
			void LoadContent() override
			{
				
			}
		};
	}
}
