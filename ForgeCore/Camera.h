#pragma once
#include <DirectXMath.h>
#include <input/Keyboard.h>
namespace forge
{
	namespace graphics
	{
		class Camera
		{
			DirectX::XMFLOAT4  _at;
			DirectX::XMFLOAT4  _up;
			DirectX::XMFLOAT4  _eye;

			DirectX::XMMATRIX _ortho;
			DirectX::XMMATRIX _view;

			const float _maxViewDistance = 100.0f;
			const float _minViewDistance = 0.01f;

			float _eyeX = 0.0f;
			float _eyeY = 0.0f;

			float _cameraWidth;
			float _cameraHeight;

			float _cameraZoomWidth;
			float _cameraZoomHeight;

			float _minCameraWidth = 10.0f;
			float _minCameraHeight = 10.0f;

		public:
			Camera(float windowWidth, float windowHeight)
			{
				_cameraWidth = windowWidth;
				_cameraHeight = windowHeight;

				_cameraZoomWidth = windowWidth / 100;
				_cameraZoomHeight = windowHeight / 100;

				_at		= DirectX::XMFLOAT4(0.0f, 0.0f, 1.0f, 0.0f);
				_up		= DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f);
				_eye	= DirectX::XMFLOAT4(_eyeX, _eyeY, 0.0f, 0.0f);

				_view	= DirectX::XMMatrixLookToLH(DirectX::XMLoadFloat4(&_eye), DirectX::XMLoadFloat4(&_at), DirectX::XMLoadFloat4(&_up));
				_ortho	= DirectX::XMMatrixOrthographicLH(windowWidth, windowHeight, _minViewDistance, _maxViewDistance);
			}

			void Update(double deltaTime)
			{
								
			}

			DirectX::XMMATRIX GetViewMatrix()
			{
				return _view;
			}
			DirectX::XMMATRIX GetOrthoMatrix()
			{
				return _ortho;
			}

			void Resize(int width, int height)
			{
				_cameraWidth = width;
				_cameraHeight = height;
				if (_cameraWidth >= 10.0f && _cameraHeight >= 10.0f)
				{
					_ortho = DirectX::XMMatrixOrthographicLH(width, height, _minViewDistance, _maxViewDistance);
				}
			}

			void ResetZoom()
			{
				_cameraWidth = _cameraZoomWidth * 100;
				_cameraHeight = _cameraZoomHeight * 100;
				_ortho = DirectX::XMMatrixOrthographicLH(_cameraWidth, _cameraHeight, _minViewDistance, _maxViewDistance);
			}

			void ZoomIn(float speed)
			{
				if (_cameraWidth >= 10.0f && _cameraHeight >= 10.0f)
				{
					_cameraWidth -= _cameraZoomWidth * speed;
					_cameraHeight -= _cameraZoomHeight * speed;
					_ortho = DirectX::XMMatrixOrthographicLH(_cameraWidth, _cameraHeight, _minViewDistance, _maxViewDistance);
				}
			}

			void ZoomOut(float speed)
			{
				_cameraWidth += _cameraZoomWidth * speed;
				_cameraHeight += _cameraZoomHeight * speed;
				_ortho = DirectX::XMMatrixOrthographicLH(_cameraWidth, _cameraHeight, _minViewDistance, _maxViewDistance);
			}

			void SetPosition(float x, float y)
			{
				_eyeX = x;
				_eyeY = y;
				_eye = DirectX::XMFLOAT4(_eyeX, _eyeY, 0.0f, 0.0f);
				_view = DirectX::XMMatrixLookToLH(DirectX::XMLoadFloat4(&_eye), DirectX::XMLoadFloat4(&_at), DirectX::XMLoadFloat4(&_up));
			}

			float GetPositionX()
			{
				return _eyeX;
			}

			float GetPositionY()
			{
				return _eyeY;
			}
		};
	}
}