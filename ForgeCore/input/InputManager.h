#pragma once
#include <string>
#include <map>
#include "Keyboard.h"
#include <vector>
#include "Mouse.h"
#include "XboxController.h"

namespace forge
{
	namespace input
	{
		enum InputType
		{
			Key,
			Button,
			GamePad
		};

		struct InputCollection
		{
			std::vector<Keys>				_keys;
			std::vector<Buttons>			_buttons;
			std::vector<GamePadButton>		_controls;
		};

		class InputManager
		{
			static std::map<std::string, InputCollection*> _input;
		public:
			static void Initialise()
			{
				
			}

			static void Clear()
			{
				for (auto x : _input)
				{
					delete x.second;
				}

				_input.clear();
			}

			static std::map<std::string, InputCollection*> GetInputCollections()
			{
				return _input;
			}

			static bool RegisterCollection(std::string collectionName)
			{
				if (_input.count(collectionName) > 0)
					return false;

				_input.insert({collectionName, new InputCollection()});
			}

			static bool RegisterKeyboardInput(std::string collectionName, Keys keyboardKey)
			{
				if (_input.count(collectionName) == 0)
					return false;

				_input[collectionName]->_keys.push_back(keyboardKey);
			}

			static bool RegisterMouseInput(std::string collectionName, Buttons keyboardKey)
			{
				if (_input.count(collectionName) == 0)
					return false;

				_input[collectionName]->_buttons.push_back(keyboardKey);
			}

			static bool RegisterControllerInput(std::string collectionName, GamePadButton keyboardKey)
			{
				if (_input.count(collectionName) == 0)
					return false;

				_input[collectionName]->_controls.push_back(keyboardKey);
			}

			static bool IsPressed(std::string collectionName)
			{
				if (_input.count(collectionName) == 0)
					return false;

				auto collection = _input[collectionName];

				for (auto keyboardKey : collection->_keys)
				{
					if(Keyboard::IsKeyPressed(keyboardKey))
					{
						return true;
					}
				}

				for (auto mouseKey : collection->_buttons)
				{
					if (Mouse::ButtonClicked(mouseKey))
					{
						return true;
					}
				}

				for (auto controllerControl : collection->_controls)
				{
					if (XboxController::GamepadKeyPressed(controllerControl))
					{
						return true;
					}
				}

				return false;
			}

			static bool IsDown(std::string collectionName)
			{
				if(_input.count(collectionName) == 0)
					return false;

				auto collection = _input[collectionName];

				for (auto keyboardKey : collection->_keys)
				{
					if (Keyboard::IsKeyDown(keyboardKey))
					{
						return true;
					}
				}

				for (auto mouseKey : collection->_buttons)
				{
					if (Mouse::ButtonDown(mouseKey))
					{
						return true;
					}
				}

				for (auto controllerControl : collection->_controls)
				{
					if (XboxController::GamepadKeyDown(controllerControl))
					{
						return true;
					}
				}

				return false;
			}

			static bool IsUp(std::string collectionName)
			{
				if (_input.count(collectionName) == 0)
					return false;

				auto collection = _input[collectionName];

				for (auto keyboardKey : collection->_keys)
				{
					if (Keyboard::IsKeyUp(keyboardKey))
					{
						return true;
					}
				}

				for (auto controllerControl : collection->_controls)
				{
					if (XboxController::GamepadKeyUp(controllerControl))
					{
						return true;
					}
				}

				return false;
			}
		};

		
	}
}
