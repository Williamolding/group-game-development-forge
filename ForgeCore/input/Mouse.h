#pragma once
#include <input/keys.h>
#include <Window.h>

namespace forge
{
	namespace input
	{
		class Mouse
		{
			static bool _MouseButtonIndex[3];
			static bool _PreviousMouseButtonIndex[3];
		public:
			static unsigned int MouseX;
			static unsigned int MouseY;

			static void SetButtonfalse(int index);
			static void SetButtontrue(int index);
			static void ClearMouseBuffer();

			static bool ButtonDown(Buttons button)		{ return _MouseButtonIndex[button]; }
			static bool ButtonUp(Buttons button)		{ return !_MouseButtonIndex[button]; }
			static bool ButtonClicked(Buttons button)	{ return _PreviousMouseButtonIndex[button]; }
		};
	}

}
