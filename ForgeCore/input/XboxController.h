#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <XInput.h>   
#include <iostream>
#include <map>

namespace forge
{
	namespace input
	{
		// GamePad Indexes
		enum GamePadIndex
		{
			GamePadIndex_One = 0,
			GamePadIndex_Two = 1,
			GamePadIndex_Three = 2,
			GamePadIndex_Four = 3,
		};

		enum GamePadButton
		{
			GamePad_Button_DPAD_UP = 0,
			GamePad_Button_DPAD_DOWN = 1,
			GamePad_Button_DPAD_LEFT = 2,
			GamePad_Button_DPAD_RIGHT = 3,
			GamePad_Button_START = 4,
			GamePad_Button_BACK = 5,
			GamePad_Button_LEFT_THUMB = 6,
			GamePad_Button_RIGHT_THUMB = 7,
			GamePad_Button_LEFT_SHOULDER = 8,
			GamePad_Button_RIGHT_SHOULDER = 9,
			GamePad_Button_A = 10,
			GamePad_Button_B = 11,
			GamePad_Button_X = 12,
			GamePad_Button_Y = 13,
			GamePadButton_Max = 14
		};

		class XboxControllerKeyState
		{
			bool _buttonState;
			bool _previousButtonState;
			bool _canSetState;
			DWORD _xinputKeyCode;
		public:
			XboxControllerKeyState() = default;
			XboxControllerKeyState(DWORD xinputKeyCode) : _buttonState(false), _previousButtonState(false),
				_canSetState(false), _xinputKeyCode(xinputKeyCode)
			{
			}

			void UpdateKey(XINPUT_STATE& state)
			{
				_buttonState = (state.Gamepad.wButtons & _xinputKeyCode) ? true : false;

				if (!_buttonState)
				{
					_canSetState = true;
				}

				if (_buttonState && !_previousButtonState && _canSetState)
				{
					_previousButtonState = true;
					_canSetState = false;
				}
			}

			bool Pressed()
			{
				return _previousButtonState;
			}
			bool Down()
			{
				return _buttonState;
			}
			bool Up()
			{
				return !_buttonState;
			}

			void Clear()
			{
				_previousButtonState = false;
			}
		};

		class XboxController
		{
			static XINPUT_STATE	_controllerState;
			static GamePadIndex	_ControlerNum;
			static std::map<GamePadButton, XboxControllerKeyState*> _controllerKeyStates;
			static bool _Connected;
		public:
			static bool IsConnected()
			{
				memset(&_controllerState, 0, sizeof(XINPUT_STATE));
				DWORD Result = XInputGetState(_ControlerNum, &_controllerState);
				if (Result != ERROR_SUCCESS && _Connected)
				{
					_Connected = false;
					for (auto &keyState : _controllerKeyStates)
					{
						delete keyState.second;
					}
					_controllerKeyStates.clear();
					return false;
				}

				_Connected = true;
				return true;
			}

			static void BuildState()
			{
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_A , new XboxControllerKeyState(XINPUT_GAMEPAD_A) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_B , new XboxControllerKeyState(XINPUT_GAMEPAD_B) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_X , new XboxControllerKeyState(XINPUT_GAMEPAD_X) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_Y , new XboxControllerKeyState(XINPUT_GAMEPAD_Y) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_RIGHT_SHOULDER , new XboxControllerKeyState(XINPUT_GAMEPAD_RIGHT_SHOULDER) });

				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_DPAD_DOWN	, new XboxControllerKeyState(XINPUT_GAMEPAD_DPAD_DOWN) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_DPAD_LEFT	, new XboxControllerKeyState(XINPUT_GAMEPAD_DPAD_LEFT) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_DPAD_RIGHT	, new XboxControllerKeyState(XINPUT_GAMEPAD_DPAD_RIGHT) });
				_controllerKeyStates.insert({ GamePadButton::GamePad_Button_DPAD_UP		, new XboxControllerKeyState(XINPUT_GAMEPAD_DPAD_UP) });
			}

			static void Update()
			{
				if (!_Connected) return;

				if (_Connected && _controllerKeyStates.empty())
					BuildState();

				for (auto &keyState : _controllerKeyStates)
				{
					keyState.second->UpdateKey(_controllerState);
				}

			}

			static bool GamepadKeyPressed(GamePadButton button)
			{
				if (!_Connected) return false;

				return _controllerKeyStates[button]->Pressed();
			}
			static bool GamepadKeyDown(GamePadButton button)
			{
				if (!_Connected) return false;

				return _controllerKeyStates[button]->Down();
			}
			static bool GamepadKeyUp(GamePadButton button)
			{
				if (!_Connected) return false;

				return _controllerKeyStates[button]->Up();
			}

			static void Vibrate()
			{
				// Create a new Vibraton 
				XINPUT_VIBRATION Vibration;

				memset(&Vibration, 0, sizeof(XINPUT_VIBRATION));

				int leftVib = (int)(1 * 65535.0f);
				int rightVib = (int)(1 * 65535.0f);

				// Set the Vibration Values
				Vibration.wLeftMotorSpeed = leftVib;
				Vibration.wRightMotorSpeed = rightVib;
				// Vibrate the controller
				XInputSetState((int)_ControlerNum, &Vibration);
			}

			static void ClearControllerState()
			{
				for (auto &keyState : _controllerKeyStates)
				{
					keyState.second->Clear();
				}
			}
		};
	}
}
