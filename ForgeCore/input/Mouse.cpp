#include "Mouse.h"

namespace forge
{
	namespace input
	{
		unsigned int Mouse::MouseX = 0;
		unsigned int Mouse::MouseY = 0;

		bool Mouse::_MouseButtonIndex[3]{ false };
		bool Mouse::_PreviousMouseButtonIndex[3]{ false };

		void Mouse::SetButtonfalse(int index) {
			_MouseButtonIndex[index] = false;
		}
		void Mouse::SetButtontrue(int index) {
			if (!_MouseButtonIndex[index])
			{
				_MouseButtonIndex[index] = true;
				_PreviousMouseButtonIndex[index] = true;
			}
		}
		void Mouse::ClearMouseBuffer() {
			memset(&_PreviousMouseButtonIndex, 0, sizeof(_PreviousMouseButtonIndex));
		}
	}
}
