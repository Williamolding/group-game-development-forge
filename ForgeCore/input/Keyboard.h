#pragma once
#include <input/keys.h>
#include <Window.h>

namespace forge
{
	namespace input
	{
		class Keyboard
		{
			static bool _KeyIndex[256];
			static bool _PreviousKeyIndex[256];
		public:
			static void SetKeyfalse(WPARAM index);
			static void SetKeytrue(WPARAM index);
			static void ClearKeyBuffer();
			static bool IsKeyDown(Keys key);
			static bool IsKeyPressed(Keys key);
			static bool IsKeyUp(Keys key);
		};
	}
}

