#include "XboxController.h"

namespace forge
{
	namespace input
	{
		XINPUT_STATE	XboxController::_controllerState;
		GamePadIndex	XboxController::_ControlerNum = GamePadIndex_One;
		std::map<GamePadButton, XboxControllerKeyState*> XboxController::_controllerKeyStates = std::map<GamePadButton, XboxControllerKeyState*>();
		bool  XboxController::_Connected = false;
	}
}
