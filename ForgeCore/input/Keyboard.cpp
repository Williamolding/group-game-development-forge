#include "Keyboard.h"
#include <iostream>
#include <sstream>


namespace forge
{
	namespace input
	{
		bool Keyboard::_KeyIndex[256]{ false };
		bool Keyboard::_PreviousKeyIndex[256]{ false };

		void Keyboard::SetKeyfalse(WPARAM index) {
			_KeyIndex[index] = false;
		}
		void Keyboard::SetKeytrue(WPARAM index) {
			if (!_KeyIndex[index])
			{
				_KeyIndex[index] = true;
				_PreviousKeyIndex[index] = true;
			}
		}
		void Keyboard::ClearKeyBuffer() {
			memset(&_PreviousKeyIndex, 0, sizeof(_PreviousKeyIndex));
		}
		bool Keyboard::IsKeyDown(Keys key) {
			return _KeyIndex[key];
		}
		bool Keyboard::IsKeyPressed(Keys key) {
			return _PreviousKeyIndex[key];
		}
		bool Keyboard::IsKeyUp(Keys key) {
			return !_KeyIndex[key];
		}
	}
}
