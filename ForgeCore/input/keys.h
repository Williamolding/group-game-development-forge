#pragma once

namespace forge
{
	namespace input
	{
		enum Keys
		{
			KEY_0 = 48,
			KEY_1,
			KEY_2,
			KEY_3,
			KEY_4,
			KEY_5,
			KEY_6,
			KEY_7,
			KEY_8,
			KEY_9,

			KEY_BACKSPACE= 8,
			KEY_ENTER	 = 27,
			KEY_ESCAPE	 = 13,
			KEY_SPACE	 = 32,

			KEY_ARROW_LEFT = 37,
			KEY_ARROW_UP,
			KEY_ARROW_RIGHT,
			KEY_ARROW_DOWN,

			KEY_PLUS = 43,
			KEY_MINUS = 45,
			
			KEY_A = 65,
			KEY_B, KEY_C, KEY_D,
			KEY_E, KEY_F, KEY_G, KEY_H,
			KEY_I, KEY_J, KEY_K, KEY_L,
			KEY_M, KEY_N, KEY_O, KEY_P,
			KEY_Q, KEY_R, KEY_S, KEY_T,
			KEY_U, KEY_V, KEY_W, KEY_X,
			KEY_Y, KEY_Z
		};

		enum Buttons
		{
			MOUSE_BUTTON_LEFT = 0,
			MOUSE_BUTTON_RIGHT,
			MOUSE_BUTTON_MIDDLE
		};
	}
}