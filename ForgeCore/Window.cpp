#include "Window.h"
#include <input/Keyboard.h>
#include <input/Mouse.h>
#include <ostream>

namespace forge
{
	namespace sys
	{
		Window* _window;

		LRESULT CALLBACK MainWndProc(HWND window, UINT message, WPARAM w_parameters, LPARAM l_parameters)
		{
			if (_window != nullptr)
				return _window->MsgProc(window, message, w_parameters, l_parameters);
			else
				return DefWindowProc(window, message, w_parameters, l_parameters);
		}

		Window::Window(WindowSettings settings) 
		{
			_window = this;

			_width	= settings.Width;
			_height	= settings.Height;
			_style	= settings.Style;
			_title	= settings.Title;
			_handle = nullptr;

			_windowState = UNINITALISED;
		}

		Window::~Window()
		{

		}

		bool Window::IsClosed()
		{
			return _windowState == CLOSED;
		}

		void Window::Initalise()
		{
			WNDCLASSEX Windowclass;
			ZeroMemory(&Windowclass, sizeof(Windowclass));
			Windowclass.cbClsExtra = 0;
			Windowclass.cbWndExtra = 0;
			Windowclass.cbSize = sizeof(Windowclass);
			Windowclass.style = CS_HREDRAW | CS_VREDRAW;
			Windowclass.hInstance = _instance;
			Windowclass.lpfnWndProc = MainWndProc;
			Windowclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			Windowclass.hCursor = LoadCursor(NULL, IDC_ARROW);
			Windowclass.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
			Windowclass.lpszMenuName = NULL;
			Windowclass.lpszClassName = _title;
			Windowclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

			if (!RegisterClassEx(&Windowclass))
				static_assert(true,"Failed to create window context");
			
			_renderRect = { 0, 0, _width, _height };

			AdjustWindowRect(&_renderRect, _style, FALSE);

			UINT width = _renderRect.right - _renderRect.left;
			UINT height = _renderRect.bottom - _renderRect.top;

			UINT x = GetSystemMetrics(SM_CXSCREEN) / 2 - width / 2;
			UINT y = GetSystemMetrics(SM_CYSCREEN) / 2 - height / 2;

			_handle = CreateWindow(_title, _title, _style, x, y, width, height, NULL, NULL, _instance, NULL);				
		}

		void Window::Show()
		{
			ShowWindow(_handle, SW_SHOW);
			_windowState = ACTIVE;
		}

		WindowProfile Window::GetProfile()
		{
			return WindowProfile(_handle,_width,_height,false);
		}

		LRESULT Window::MsgProc(HWND window, UINT message, WPARAM w_parameters, LPARAM l_parameters)
		{
			switch(message)
			{
			case WM_CLOSE:
				DestroyWindow(_handle);
				_windowState = CLOSED;
				break;
			case WM_DESTROY:
				PostQuitMessage(0);
				_windowState = CLOSED;
				return 0;
			case WM_KEYDOWN:
				if (w_parameters == 13)
					input::Keyboard::SetKeytrue(27);
				else
					input::Keyboard::SetKeytrue(w_parameters);		
				break;
			case WM_KEYUP:
				if (w_parameters == 13)
					input::Keyboard::SetKeyfalse(27);
				else
					input::Keyboard::SetKeyfalse(w_parameters);
				break;
			case WM_MOUSEMOVE:
				input::Mouse::MouseX = LOWORD(l_parameters);
				input::Mouse::MouseY = HIWORD(l_parameters);
				break;
			case WM_LBUTTONDOWN:
				input::Mouse::SetButtontrue(0);
				break;
			case WM_LBUTTONUP:
				input::Mouse::SetButtonfalse(0);
				break;
			case WM_RBUTTONDOWN:
				input::Mouse::SetButtontrue(1);
				break;
			case WM_RBUTTONUP:
				input::Mouse::SetButtonfalse(1);
				break;
			case WM_MBUTTONDOWN:
				input::Mouse::SetButtontrue(2);
				break;
			case WM_MBUTTONUP: 
				input::Mouse::SetButtonfalse(2);
				break;
			case WM_SIZE:
				if (ResizeCallback)
					ResizeCallback(LOWORD(l_parameters), HIWORD(l_parameters));
				break;
			default:
				return DefWindowProc(window, message, w_parameters, l_parameters);
			}

			return DefWindowProc(window, message, w_parameters, l_parameters);
		}

		void Window::ProcessEvents()
		{
			MSG msg = { 0 };

			if (PeekMessage(&msg, _handle, NULL, NULL, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			
			}
		}
	}
}
