#include "serialisation/XmlSerialiserSystem.h"

#include "Modules/XmlModuleTransformComponent.h"
#include "Modules/XmlModuleSpriteRendererComponent.h"
#include "Modules/XmlModuleAnimationComponent.h"
#include "Modules/XmlModuleScriptComponent.h"
#include "Modules/XmlModuleSoundEmitterComponent.h"
#include "Modules/XmlModuleCircleCollider.h"
#include "Modules/XmlModuleRectangleColliderComponent.h"
#include "Modules/XmlModuleParticleComponent.h"
#include "Modules/XmlModuleTilingTool.h"

namespace forge
{
	namespace xml
	{
		std::vector<XmlSerialisationModule*>  XmlSerialiserSystem::_XmlModules = std::vector<XmlSerialisationModule*>();

		void XmlSerialiserSystem::RegisterAllModules()
		{
			Register(new XmlModuleTransformComponent());
			Register(new XmlModuleSpriteRendererComponent());
			Register(new XmlModuleAnimationComponent());
			Register(new XmlModuleScriptComponent());
			Register(new XmlModuleSoundEmitterComponent());
			Register(new XmlModuleCircleCollider());
			Register(new XmlModuleRectangleComponent());
			Register(new XmlModuleParticleComponenet());
			Register(new XmlModuleTilingTool());
		}

	}
}
