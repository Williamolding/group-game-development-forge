#pragma once
#include <vector>
#include "entity/Entity.h"
#include "util/tinyXml2.h"
#include "asset/AssetManager.h"
#include "Modules/XmlSerialisationModule.h"

namespace forge
{
	namespace xml
	{
		class XmlSerialiserSystem
		{
			static std::vector<XmlSerialisationModule*> _XmlModules;
		public:
			template<class T>
			static void Register(T* module)
			{
				_XmlModules.push_back(module);
			}

			static void SerialiseEntity(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity)
			{
				for(auto const& xmlModule : _XmlModules)
				{
					xmlModule->Serialize(document,node, entity);
				}
			}

			static void DeserialiseEntity(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager)
			{
				for (auto const& xmlModule : _XmlModules)
				{
					xmlModule->Deserialise(document,node, entity, assetmanager);
				}
			}

			static void Clear()
			{
				for(auto const module : _XmlModules)
				{
					delete module;
				}

				_XmlModules.clear();
			}

			static void RegisterAllModules();
		};
	}
}
