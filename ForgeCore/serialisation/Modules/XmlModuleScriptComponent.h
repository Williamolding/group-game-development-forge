#pragma once
#include "components/Script.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleScriptComponent : public XmlSerialisationModule
		{
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::Script>())
					return;

				auto scriptComponent = entity->getComponent<component::Script>();
				auto scriptComponentElement = document->NewElement("ScriptComponent");

				scriptComponentElement->SetAttribute("path", scriptComponent->GetPath().c_str());
				scriptComponentElement->SetAttribute("filename", scriptComponent->GetFilename().c_str());

				node->InsertEndChild(scriptComponentElement);
			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode = node->FirstChildElement("ScriptComponent");

				if (componentNode == nullptr)
					return;

				std::string path = componentNode->Attribute("path");
				std::string filename = componentNode->Attribute("filename");

				assetmanager->LoadScript(filename, entity->GetName());

				entity->addComponent<component::Script>(path, filename);
			}
		};
	}
}
