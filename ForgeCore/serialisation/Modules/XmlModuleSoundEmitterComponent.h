#pragma once
#include "XmlSerialisationModule.h"
#include "components/SoundEmitter.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleSoundEmitterComponent : public XmlSerialisationModule
		{
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::SoundEmitter>())
					return;

				auto soundComponent = entity->getComponent<component::SoundEmitter>();
				auto audio = soundComponent->GetAudio();

				auto soundEmitterComponent = document->NewElement("SoundEmitterComponent");

				auto soundElement = document->NewElement("Sound");
				soundElement->SetAttribute("looping", audio.Looping);
				soundElement->SetAttribute("path", audio.FileName.c_str());
				soundElement->SetAttribute("playOnLoad", audio.PlayOnLoad);
				soundElement->SetAttribute("pitch", audio.pitch);
				soundElement->SetAttribute("volume", audio.volume);

				soundEmitterComponent->InsertEndChild(soundElement);

				node->InsertEndChild(soundEmitterComponent);
			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode = node->FirstChildElement("SoundEmitterComponent");

				if (componentNode == nullptr)
					return;

				const auto soundEmitterNode = componentNode->FirstChildElement("Sound");

				std::string path = soundEmitterNode->Attribute("path");

				bool looping;
				bool playOnLoad;
				float volume;
				float pitch;

				soundEmitterNode->QueryBoolAttribute("looping", &looping);
				soundEmitterNode->QueryBoolAttribute("playOnLoad", &playOnLoad);
				soundEmitterNode->QueryFloatAttribute("pitch", &pitch);
				soundEmitterNode->QueryFloatAttribute("volume", &volume);

				auto audio = assetmanager->CreateAudio(path,false, looping,false,volume,pitch, playOnLoad);

				auto spriteComponent = entity->addComponent<component::SoundEmitter>(audio, assetmanager->_Fmod);
			}
		};
	}
}
