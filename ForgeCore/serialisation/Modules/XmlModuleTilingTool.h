#pragma once
#include "XmlSerialisationModule.h"
#include <entity/Entity.h>
#include "components/TilingTool.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleTilingTool : public XmlSerialisationModule
		{
			const char* TilingToolElement     = "TilingToolComponent";
			const char* TilingToolWidth       = "width";
			const char* TilingToolHeight      = "height";
			const char* TilingToolOverride    = "override";
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (entity->hasComponent<component::TilingTool>() == false)
					return;

				auto tilingToolComponent = entity->getComponent<component::TilingTool>();
				auto tilingToolComponentElement = document->NewElement(TilingToolElement);

				tilingToolComponentElement->SetAttribute(TilingToolWidth, tilingToolComponent->GetWidth());
				tilingToolComponentElement->SetAttribute(TilingToolHeight, tilingToolComponent->GetHeight());
				tilingToolComponentElement->SetAttribute(TilingToolOverride, tilingToolComponent->GetOverride());

				node->InsertEndChild(tilingToolComponentElement);
			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode = node->FirstChildElement(TilingToolElement);

				if (componentNode == nullptr)
					return;

				float width;
				float height;
				bool overrideCol;

				componentNode->QueryFloatAttribute(TilingToolWidth, &width);
				componentNode->QueryFloatAttribute(TilingToolHeight, &height);
				componentNode->QueryBoolAttribute(TilingToolOverride, &overrideCol);

				auto tilingToolComponent = entity->addComponent<component::TilingTool>(width, height);
				tilingToolComponent->SetOverride(overrideCol);
			}
		};
	}
}

