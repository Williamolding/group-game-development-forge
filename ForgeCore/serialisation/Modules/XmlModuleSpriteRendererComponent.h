#pragma once
#include "components/SpriteRenderer.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleSpriteRendererComponent : public XmlSerialisationModule
		{
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::SpriteRenderer>())
					return;

				auto spriteComponent = entity->getComponent<component::SpriteRenderer>();

				auto colour		= spriteComponent->GetColour();
				auto width		= spriteComponent->Width;
				auto height		= spriteComponent->Height;
				auto texture	= spriteComponent->GetTexture();
				auto mesh		= spriteComponent->GetMesh();

				auto spriteComponentElement = document->NewElement("SpriteRendererComponent");

				auto colourElement = document->NewElement("Colour");
				colourElement->SetAttribute("r",colour.R);
				colourElement->SetAttribute("g",colour.G);
				colourElement->SetAttribute("b",colour.B);

				auto textureElement = document->NewElement("Texture");
				textureElement->SetAttribute("width",	width);
				textureElement->SetAttribute("height",	height);
				textureElement->SetAttribute("path", texture.Path.c_str());


				auto meshElement = document->NewElement("Mesh");

				meshElement->SetAttribute("width",	mesh.Width);
				meshElement->SetAttribute("height", mesh.Height);
		
				spriteComponentElement->InsertEndChild(colourElement);
				spriteComponentElement->InsertEndChild(textureElement);
				spriteComponentElement->InsertEndChild(meshElement);

				node->InsertEndChild(spriteComponentElement);

				_assetCounter++;
			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode	= node->FirstChildElement("SpriteRendererComponent");

				if (componentNode == nullptr)
					return;

				const auto meshNode		= componentNode->FirstChildElement("Mesh");

				float meshWidth;
				float meshHeight;

				meshNode->QueryFloatAttribute("width",	&meshWidth);
				meshNode->QueryFloatAttribute("height", &meshHeight);

				auto meshData = assetmanager->CreateSpriteMesh(meshWidth, meshHeight);

				const auto colourNode = componentNode->FirstChildElement("Colour");

				auto colour = render::Colour();

				colourNode->QueryFloatAttribute("r", &colour.R);
				colourNode->QueryFloatAttribute("g", &colour.G);
				colourNode->QueryFloatAttribute("b", &colour.B);

				const auto textureNode = componentNode->FirstChildElement("Texture");
				auto texturePath = textureNode->Attribute("path");

				auto textureData	= assetmanager->CreateTexture(texturePath);

				auto spriteComponent = entity->addComponent<component::SpriteRenderer>(meshData, textureData, 64);
				spriteComponent->SetColour(colour);
			}
		};
	}
}
