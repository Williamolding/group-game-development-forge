#pragma once
#include "XmlSerialisationModule.h"
#include <entity/Entity.h>
#include <components/Particle.h>

namespace forge
{
	namespace xml
	{
		class XmlModuleParticleComponenet : public XmlSerialisationModule
		{
			const char* ComponentName = "ParticleComponent";
			const char* MassElement = "Mass";
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::Particle>())
					return;

				const auto particleComponent = entity->getComponent<component::Particle>();
				const auto particleComponentElement = document->NewElement(ComponentName);
				 
				const auto massValue = particleComponent->GetMass();
				const auto corValue = particleComponent->GetCoefficientOfRestitution();
				const auto terminalVelocity = particleComponent->IsTerminalVelocityOn();
				const auto gravity = particleComponent->GetGravityForceGenerator()->GetGravity().GetY();
				const auto frictionNormal = particleComponent->GetFrictionGenerator()->GetNormal();
				const auto frictionCoF = particleComponent->GetFrictionGenerator()->GetCoF();
				const auto frictionMultiplier = particleComponent->GetFrictionGenerator()->GetMultiplier();
				const auto beingPushed = particleComponent->BeingPushed();

				particleComponentElement->SetAttribute(MassElement, massValue);
				particleComponentElement->SetAttribute("CoefficientOfRestitution", corValue);
				particleComponentElement->SetAttribute("Gravity", gravity);
				particleComponentElement->SetAttribute("FrictionCoF", frictionCoF);
				particleComponentElement->SetAttribute("FrictionNormal", frictionNormal);
				particleComponentElement->SetAttribute("FrictionMultiplier", frictionMultiplier);
				particleComponentElement->SetAttribute("TerminalVelocity", terminalVelocity);
				particleComponentElement->SetAttribute("BeingPushed", beingPushed);

				if (particleComponent->GetPushForceGenerator())
				{
					const float pushForceDrag = particleComponent->GetPushForceGenerator()->GetDragCoefficient();
					const float pushForceVel = particleComponent->GetPushForceGenerator()->GetVelocity().GetX();
					
					particleComponentElement->SetAttribute("PushForceDrag", pushForceDrag);
					particleComponentElement->SetAttribute("PushForceVel", pushForceVel);		
				}
				
				node->InsertEndChild(particleComponentElement);
			}

			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetManager) override
			{
				const auto componentNode = node->FirstChildElement(ComponentName);

				if (!componentNode)
					return;

				if (!entity->hasComponent<component::Transform>())
					return;

				auto transformComponent = entity->getComponent<component::Transform>();

				float mass;
				componentNode->QueryFloatAttribute(MassElement, &mass);

				float cor;
				componentNode->QueryFloatAttribute("CoefficientOfRestitution", &cor);

				float gravity;
				componentNode->QueryFloatAttribute("Gravity", &gravity);

				float frictionCoF;
				componentNode->QueryFloatAttribute("FrictionCoF", &frictionCoF);

				float frictionNormal;
				componentNode->QueryFloatAttribute("FrictionNormal", &frictionNormal);

				float frictionMultiplier;
				componentNode->QueryFloatAttribute("FrictionMultiplier", &frictionMultiplier);
				
				float pushForceDrag = 0.0f;
				componentNode->QueryFloatAttribute("PushForceDrag", &pushForceDrag);

				float pushForceVel = 0.0f;
				componentNode->QueryFloatAttribute("PushForceVel", &pushForceVel);

				bool terminalVelocity;
				componentNode->QueryBoolAttribute("TerminalVelocity", &terminalVelocity);

				bool beingPushed;
				componentNode->QueryBoolAttribute("BeingPushed", &beingPushed);

				auto particleComponent = entity->addComponent<component::Particle>(transformComponent);
				particleComponent->SetMass(mass);
				particleComponent->SetCoefficientOfRestitution(cor);
				particleComponent->GetGravityForceGenerator()->SetGravity(gravity);
				particleComponent->GetFrictionGenerator()->SetCoF(frictionCoF);
				particleComponent->GetFrictionGenerator()->SetNormal(frictionNormal);
				particleComponent->GetFrictionGenerator()->SetMultiplier(frictionMultiplier);
				particleComponent->SetTerminalVelocity(terminalVelocity);
				particleComponent->SetBeingPushed(beingPushed);

 				if (beingPushed)
					particleComponent->AddGenerator("push", new physics::PushForce(Vector2D(pushForceVel, 0), pushForceDrag));
			}
		};
	}
}
