#pragma once
#include "util/tinyXml2.h"

namespace forge {
	namespace asset {
		class AssetManager;
	}
}

namespace forge {
	namespace entity {
		class EntityManager;
		class Entity;
	}
}

namespace forge
{
	namespace xml
	{
		class XmlSerialisationModule
		{
		protected:
			static int _assetCounter;
		public:
			virtual ~XmlSerialisationModule() = default;
			virtual void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) = 0;
			virtual void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) = 0;
		};
	}
}
