#pragma once
#include "XmlSerialisationModule.h"
#include "components/Colliders/RectCollider.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleRectangleComponent : public XmlSerialisationModule
		{
			const char* ColliderElement		= "RectangleColliderComponent";
			const char* ColliderWidth		= "width";
			const char* ColliderHeight		= "height";
			const char* ColliderOffsetX		= "offsetX";
			const char* ColliderOffsetY		= "offsetY";
			const char* ColliderIsTrigger	= "isTrigger";
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::RectCollider>())
					return;

				const auto rectColliderComponent = entity->getComponent<component::RectCollider>();
				auto rectColliderComponentElement = document->NewElement(ColliderElement);

				rectColliderComponentElement->SetAttribute(ColliderWidth, rectColliderComponent->GetWidth());
				rectColliderComponentElement->SetAttribute(ColliderHeight, rectColliderComponent->GetHeight());
				rectColliderComponentElement->SetAttribute(ColliderIsTrigger, rectColliderComponent->IsTrigger());
				rectColliderComponentElement->SetAttribute(ColliderOffsetX, rectColliderComponent->GetOffset().X);
				rectColliderComponentElement->SetAttribute(ColliderOffsetY, rectColliderComponent->GetOffset().Y);

				node->InsertEndChild(rectColliderComponentElement);
			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode = node->FirstChildElement(ColliderElement);

				if (componentNode == nullptr)
					return;

				float width;
				float height;
				float offsetX;
				float offsetY;
				bool isTrigger;

				componentNode->QueryFloatAttribute(ColliderWidth, &width);
				componentNode->QueryFloatAttribute(ColliderHeight, &height);
				componentNode->QueryFloatAttribute(ColliderOffsetX, &offsetX);
				componentNode->QueryFloatAttribute(ColliderOffsetY, &offsetY);
				componentNode->QueryBoolAttribute(ColliderIsTrigger, &isTrigger);

				const auto debugRectColliderMesh = assetmanager->CreateQuadMesh(width, height);

				auto rectColliderComponent = entity->addComponent<component::RectCollider>(width, height, new asset::QuadMesh(debugRectColliderMesh));
				rectColliderComponent->SetTrigger(isTrigger);
				rectColliderComponent->SetOffset(Vector2D(offsetX, offsetY));
			}
		};
	}
}

