#pragma once
#include "components/Animation.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleAnimationComponent : public XmlSerialisationModule
		{
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::Animation>())
					return;

				auto animationComponent = entity->getComponent<component::Animation>();

				auto animationSpeed = animationComponent->GetSpeed();
				auto animationFps = animationComponent->GetFps();

				auto animationComponentElement = document->NewElement("AnimationComponent");
				animationComponentElement->SetAttribute("speed", animationSpeed);
				animationComponentElement->SetAttribute("fps", animationFps);

				node->InsertEndChild(animationComponentElement);

			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode = node->FirstChildElement("AnimationComponent");

				if (componentNode == nullptr)
					return;

				double fps;
				double speed;

				componentNode->QueryDoubleAttribute("speed", &speed);
				componentNode->QueryDoubleAttribute("fps", &fps);

				auto animationComponent = entity->addComponent<component::Animation>(fps, speed);
			}
		};
	}
}

