#pragma once
#include "XmlSerialisationModule.h"
#include <entity/Entity.h>
#include "components/Colliders/CircleCollider.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleCircleCollider : public XmlSerialisationModule
		{
			const char* ComponentName = "CircleColliderComponent";
			const char* ColliderRadius = "radius";
			const char* ColliderOffsetX = "offsetX";
			const char* ColliderOffsetY = "offsetY";
			const char* ColliderIsTrigger = "isTrigger";
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::CircleCollider>())
					return;

				const auto circleColliderComponent = entity->getComponent<component::CircleCollider>();
				auto circleColliderComponentElement = document->NewElement(ComponentName);
				
				circleColliderComponentElement->SetAttribute(ColliderRadius, circleColliderComponent->GetRadius());
				circleColliderComponentElement->SetAttribute(ColliderIsTrigger, circleColliderComponent->IsTrigger());
				circleColliderComponentElement->SetAttribute(ColliderOffsetX, circleColliderComponent->GetOffset().X);
				circleColliderComponentElement->SetAttribute(ColliderOffsetY, circleColliderComponent->GetOffset().Y);

				node->InsertEndChild(circleColliderComponentElement);
			}

			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				const auto componentNode = node->FirstChildElement("CircleColliderComponent");

				if (!componentNode)
					return;

				float radius;
				componentNode->QueryFloatAttribute(ColliderRadius, &radius);

				bool isTrigger;
				componentNode->QueryBoolAttribute(ColliderIsTrigger, &isTrigger);

				float offsetX;
				componentNode->QueryFloatAttribute(ColliderOffsetX, &offsetX);

				float offsetY;
				componentNode->QueryFloatAttribute(ColliderOffsetY, &offsetY);


				auto debugRenderMesh = assetmanager->CreateCircleMesh(radius);

				auto circleColliderComponent = entity->addComponent<component::CircleCollider>(radius, debugRenderMesh);
				circleColliderComponent->SetTrigger(isTrigger);
				circleColliderComponent->SetOffset(Vector2D(offsetX, offsetY));
			}
		};
	}
}
