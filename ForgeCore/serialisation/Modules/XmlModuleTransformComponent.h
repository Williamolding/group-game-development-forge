#pragma once
#include "components/Transform.h"

namespace forge
{
	namespace xml
	{
		class XmlModuleTransformComponent : public XmlSerialisationModule
		{
			const char* ComponentName		= "TransformComponent";
			const char* PositionElement		= "Position";
			const char* ScaleElement		= "Scale";
			const char* RotationElement		= "Rotation";
			const char* ParentElement		= "Parent";
		public:
			void Serialize(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity) override
			{
				if (!entity->hasComponent<component::Transform>())
					return;

				auto transformComponent = entity->getComponent<component::Transform>();

				auto position	= transformComponent->GetPosition();
				auto scale		= transformComponent->GetScale();
				auto rotation	= transformComponent->GetRotation();
				auto parent		= transformComponent->GetParent();

				auto transformComponentElement = document->NewElement(ComponentName);

				auto positionElement = document->NewElement(PositionElement);
				positionElement->SetAttribute("x", position.x);
				positionElement->SetAttribute("y", position.y);
				positionElement->SetAttribute("z", position.z);

				auto scaleElement = document->NewElement(ScaleElement);
				scaleElement->SetAttribute("x", scale.x);
				scaleElement->SetAttribute("y", scale.y);
				scaleElement->SetAttribute("z", scale.z);

				auto rotationElement = document->NewElement(RotationElement);
				rotationElement->SetAttribute("x", rotation.x);
				rotationElement->SetAttribute("y", rotation.y);
				rotationElement->SetAttribute("z", rotation.z);

				if (parent != nullptr)
				{
					auto parentName = parent->Entity->GetName();
					auto parentElement = document->NewElement(ParentElement);
					parentElement->SetAttribute("parent", parentName.c_str());
					transformComponentElement->InsertEndChild(parentElement);
				}

				transformComponentElement->InsertEndChild(positionElement);
				transformComponentElement->InsertEndChild(scaleElement);
				transformComponentElement->InsertEndChild(rotationElement);

				node->InsertEndChild(transformComponentElement);
			}
			void Deserialise(tinyxml2::XMLDocument* document, tinyxml2::XMLElement* node, entity::Entity* entity, asset::AssetManager* assetmanager) override
			{
				auto componentNode	= node->FirstChildElement(ComponentName);

				if (componentNode == nullptr)
					return;

				const auto positionNode	= componentNode->FirstChildElement(PositionElement);
				const auto scaleNode	= componentNode->FirstChildElement(ScaleElement);
				const auto rotationNode	= componentNode->FirstChildElement(RotationElement);
				const auto parentNode	= componentNode->FirstChildElement(ParentElement);

				auto position = DirectX::XMFLOAT3();

				positionNode->QueryFloatAttribute("x", &position.x);
				positionNode->QueryFloatAttribute("y", &position.y);
				positionNode->QueryFloatAttribute("z", &position.z);

				auto scale = DirectX::XMFLOAT3();

				scaleNode->QueryFloatAttribute("x", &scale.x);
				scaleNode->QueryFloatAttribute("y", &scale.y);
				scaleNode->QueryFloatAttribute("z", &scale.z);

				auto rotation = DirectX::XMFLOAT3();

				rotationNode->QueryFloatAttribute("x", &rotation.x);
				rotationNode->QueryFloatAttribute("y", &rotation.y);
				rotationNode->QueryFloatAttribute("z", &rotation.z);

				auto transformComponent = entity->addComponent<component::Transform>(position);
				transformComponent->SetScale(scale);
				transformComponent->SetRotation(rotation);

				if (parentNode != nullptr)
				{
					auto parentName = parentNode->Attribute("parent");
					transformComponent->SetXmlParent(parentName);
				}		
			}
		};
	}
}
