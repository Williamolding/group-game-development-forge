#pragma once
#include <d3d11.h>
#include "graphics/Dx11/Dx11BufferUploader.h"
#include "graphics/image/TgaLoader.h"
#include "FmodSystem.h"
#include "VirtualFileSystem.h"
#include "graphics/SpriteVertex.h"
#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>
#include <vector>
#include "../graphics/SpriteVertex.h"
#include "../graphics/Dx11/Dx11BufferUploader.h"
#include "../graphics/image/TgaLoader.h"
#include "../../Include/lua.h"
#include "../FmodSystem.h"

namespace forge
{
	namespace asset
	{
		struct SpriteMesh
		{
			int							Width;
			int							Height;
			int							VertexCount;
			std::vector<SpriteVertex>	Verticies;
			ID3D11Buffer* VertexBuffer;
			ID3D11Buffer* IndexBuffer;
			ID3D11Buffer* ConstantBuffer;
		};

		struct SpriteTexture
		{
			int						  Width;
			int						  Height;
			ID3D11ShaderResourceView* TextureView;
			ID3D11Texture2D*		  Texture;
			ID3D11SamplerState*		  TextureSampler;
			std::string				  Path;
		};

		struct Audio
		{
			FMOD::Sound* Sound;
			bool Looping;
			bool PlayOnLoad;
			float volume;
			float pitch;
			std::string				  FileName;
		};

		struct CircleMesh
		{
			int							Radius;
			int							VertexCount;
			int							Segments;
			std::vector<SpriteVertex>	Verticies;
			ID3D11Buffer* VertexBuffer;
			ID3D11Buffer* IndexBuffer;
			ID3D11Buffer* ConstantBuffer;
		};

		struct QuadMesh
		{
			int							Width;
			int							Height;
			int							VertexCount;
			std::vector<SpriteVertex>	Verticies;
			ID3D11Buffer* VertexBuffer;
			ID3D11Buffer* IndexBuffer;
			ID3D11Buffer* ConstantBuffer;
		};

		struct Script
		{
			std::string				  Path;
		};

		class AssetManager
		{
			ID3D11Device*							_device;
			graphics::Dx11BufferUploader*			_bufferUploader;
			TgaLoader*								_tgaLoader;
			lua_State*								_luastate;
			SpriteMesh*								_baseSpriteMesh;
			QuadMesh*								_baseQuadMesh;
			CircleMesh*								_baseCircleMesh;

			float _windowWidth;
			float _windowHeight;

			static std::string _assetPath;
		public:
			VirtualFileSystem * _fileSystem;
			FmodSystem*			_Fmod;

			std::map<std::string, SpriteTexture>	Textures;
			std::map<std::string, Audio>			Sounds;
			std::vector<std::string>				Scripts;
		public:
			AssetManager(ID3D11Device* device, float windowWidth, float windowHeight, VirtualFileSystem* fileSystem, lua_State* luastate,FmodSystem* fmodsystem);
			~AssetManager();

			SpriteMesh				CreateSpriteMesh(float width, float height);
			CircleMesh				CreateCircleMesh(float radius);
			QuadMesh				CreateQuadMesh(float width, float height);
			SpriteTexture			CreateTexture(std::string path);
			Audio					CreateAudio(const string& audioPath, bool b3d, bool Looping, bool Stream, float volume,float pitch,bool PlayOnLoad);
			void					LoadScript(std::string path, std::string entityName);

			std::string GetAssetPathRoot() { return _fileSystem->Root() + "//Assets//";}

			static void SetAssetPath(std::string path) { _assetPath = path; }
			static std::string GetAssetPath() { return _assetPath; }
		};
	}
}

