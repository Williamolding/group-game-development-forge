#include "VirtualFileSystem.h"
#include <vector>
#include <util/base64.h>
#include <algorithm>

namespace forge
{
	namespace asset
	{
		MemoryFileSystem::MemoryFileSystem(std::string coreAssets)
		{
			_fileMap = std::map<std::string, std::string>();

			std::vector<std::string> vec;
			std::string token;
			std::ifstream file(coreAssets);

			while (std::getline(file, token, '$')) {
				vec.push_back(token);
			}

			for(int i = 0; i < vec.size(); i+=2)
			{
				std::string decode = vec[i + 1];
				decode.erase(std::remove(decode.begin(), decode.end(), '\n'), decode.end());
				_fileMap[vec[i]] = base64_decode(decode);
			}
		}

		MemoryFileSystem::~MemoryFileSystem()
		{

		}

		VirtualFileSystem::VirtualFileSystem(std::string rootPath) : _root(rootPath)
		{

		}

		VirtualFileSystem::~VirtualFileSystem()
		{

		}
	}
}
