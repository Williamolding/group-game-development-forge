#pragma once
#include <string>
#include <map>
#include <iostream>
#include <fstream>

namespace forge
{
	namespace asset
	{
		class IMemoryFileSystem
		{
		public:
			virtual std::string& GetFile(std::string &filename) = 0;
		};

		class MemoryFileSystem : public  IMemoryFileSystem
		{
			std::map<std::string, std::string> _fileMap;
		public:
			MemoryFileSystem(std::string coreAssets);
			~MemoryFileSystem();

			std::string& GetFile(std::string &filename) override
			{
				if (_fileMap.find(filename) != _fileMap.end())
					return _fileMap.at(filename);

				return filename;
			}
		};

		class IVirtualFileSystem
		{
		public:
			virtual std::string Root() = 0;
		};


		class VirtualFileSystem : public IVirtualFileSystem
		{
			std::string _default;
			std::string _root;
		public:
			VirtualFileSystem(std::string rootPath);
			~VirtualFileSystem();

			std::string Root() override
			{
				return _root;
			}
		};
	}
}

