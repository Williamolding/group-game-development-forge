#include "AssetManager.h"
#include <corecrt_math_defines.h>
#include "../FmodSystem.h"
#include "../graphics/Dx11/Dx11BufferUploader.h"
#include "../graphics/image/TgaLoader.h"
#include <DirectXMath.h>
#include "../graphics/SpriteVertex.h"
#include "../graphics/SpriteConstantBuffer.h"
#include "../../Include/fmod_common.h"
#include "../../Include/lauxlib.h"

namespace forge
{
	namespace asset
	{
		AssetManager::AssetManager(ID3D11Device* device, float windowWidth, float windowHeight, VirtualFileSystem* fileSystem, lua_State* luastate,FmodSystem* fmodsystem)
		: _device(device), _windowWidth(windowWidth), _windowHeight(windowHeight), _luastate(luastate), _fileSystem(fileSystem)
		{
			_Fmod = fmodsystem;
			_bufferUploader = new graphics::Dx11BufferUploader();
			_tgaLoader = new TgaLoader();
		}

		AssetManager::~AssetManager()
		{
			delete _bufferUploader;
			delete _tgaLoader;
			delete _baseSpriteMesh;
		}

		SpriteMesh AssetManager::CreateSpriteMesh(float width, float height)
		{
			auto spriteResult = SpriteMesh();
			spriteResult.Width = width;
			spriteResult.Height = height;

			auto left = width / 2 * -1;
			auto right = left + width;
			auto top = height / 2;
			auto bottom = top - height;
			auto layer = 0;

			auto defaultColour = DirectX::XMFLOAT3(1,1,1);

			spriteResult.Verticies = std::vector<SpriteVertex>() = {
				SpriteVertex(left,top,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(right,bottom,layer,1,0,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(left,bottom,layer,0,0,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(left,top,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(right,top,layer,1,1,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(right,bottom,layer,1,0,defaultColour.x,defaultColour.y,defaultColour.z)
			};

			std::vector<unsigned int> indices = std::vector<unsigned int>() = {
				0,1,2,3,4,5
			};

			spriteResult.VertexCount = spriteResult.Verticies.size();

			if(_baseSpriteMesh == nullptr)
			{
				_baseSpriteMesh = new SpriteMesh();

				const auto vertexBuffer		= _bufferUploader->UploadBuffer<SpriteVertex>(_device, spriteResult.Verticies, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, true);
				const auto indexBuffer		= _bufferUploader->UploadBuffer<unsigned int>(_device, indices, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT);
				const auto constantBuffer	= _bufferUploader->UploadBuffer<SpriteConstantBuffer>(_device, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT);

				if (!vertexBuffer.UploadStatus || !indexBuffer.UploadStatus || !constantBuffer.UploadStatus)
					return spriteResult;

				_baseSpriteMesh->VertexBuffer	= vertexBuffer.UploadedBuffer;
				_baseSpriteMesh->IndexBuffer	= indexBuffer.UploadedBuffer;
				_baseSpriteMesh->ConstantBuffer = constantBuffer.UploadedBuffer;
			}

			spriteResult.VertexBuffer	= _baseSpriteMesh->VertexBuffer;
			spriteResult.IndexBuffer	= _baseSpriteMesh->IndexBuffer;
			spriteResult.ConstantBuffer = _baseSpriteMesh->ConstantBuffer;

			return spriteResult;
		}

		CircleMesh AssetManager::CreateCircleMesh(float radius)
		{
			auto circleMesh = CircleMesh();
			circleMesh.Radius = radius;
			circleMesh.Segments = 60;

			auto layer = 1;

			auto defaultColour = DirectX::XMFLOAT3(1, 1, 1);

			int index = 0;
			std::vector<unsigned int> indices = std::vector<unsigned int>();

			for (double angle = 0.0; angle <= (2 * 3.14159265358979323846); angle += (2 * 3.14159265358979323846) / circleMesh.Segments) {
				circleMesh.Verticies.emplace_back(SpriteVertex(cos(angle) * radius, -sin(angle) * radius, layer,0,0, defaultColour.x, defaultColour.y, defaultColour.z));
				indices.push_back(index);
				index++;
			}

			circleMesh.VertexCount = circleMesh.Verticies.size();

			if (_baseCircleMesh == nullptr)
			{
				_baseCircleMesh = new CircleMesh();

				const auto vertexBuffer = _bufferUploader->UploadBuffer<SpriteVertex>(_device, circleMesh.Verticies, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, true);
				const auto indexBuffer = _bufferUploader->UploadBuffer<unsigned int>(_device, indices, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT);
				const auto constantBuffer = _bufferUploader->UploadBuffer<SpriteConstantBuffer>(_device, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT);

				if (!vertexBuffer.UploadStatus || !indexBuffer.UploadStatus || !constantBuffer.UploadStatus)
					return circleMesh;

				_baseCircleMesh->VertexBuffer = vertexBuffer.UploadedBuffer;
				_baseCircleMesh->IndexBuffer = indexBuffer.UploadedBuffer;
				_baseCircleMesh->ConstantBuffer = constantBuffer.UploadedBuffer;
			}

			circleMesh.VertexBuffer = _baseCircleMesh->VertexBuffer;
			circleMesh.IndexBuffer = _baseCircleMesh->IndexBuffer;
			circleMesh.ConstantBuffer = _baseCircleMesh->ConstantBuffer;

			return circleMesh;
		}

		QuadMesh AssetManager::CreateQuadMesh(float width, float height)
		{
			auto quadMesh		= QuadMesh();
			quadMesh.Width		= width;
			quadMesh.Height		= height;

			auto left		= width / 2 * -1;
			auto right		= left + width;
			auto top		= height / 2;
			auto bottom		= top - height;
			auto layer		= 1;

			auto defaultColour = DirectX::XMFLOAT3(1, 1, 1);

			quadMesh.Verticies = std::vector<SpriteVertex>() = {
				SpriteVertex(left,top,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(right,top,layer,1,0,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(right,bottom,layer,0,0,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(left,bottom,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
				SpriteVertex(left,top,layer,0,1,defaultColour.x,defaultColour.y,defaultColour.z),
			};

			std::vector<unsigned int> indices = std::vector<unsigned int>() = {
				0,1,2,3,4
			};

			quadMesh.VertexCount = quadMesh.Verticies.size();

			if (_baseQuadMesh == nullptr)
			{
				_baseQuadMesh = new QuadMesh();

				const auto vertexBuffer			= _bufferUploader->UploadBuffer<SpriteVertex>(_device, quadMesh.Verticies, D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, true);
				const auto indexBuffer			= _bufferUploader->UploadBuffer<unsigned int>(_device, indices, D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT);
				const auto constantBuffer		= _bufferUploader->UploadBuffer<SpriteConstantBuffer>(_device, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT);

				if (!vertexBuffer.UploadStatus || !indexBuffer.UploadStatus || !constantBuffer.UploadStatus)
					return quadMesh;

				_baseQuadMesh->VertexBuffer = vertexBuffer.UploadedBuffer;
				_baseQuadMesh->IndexBuffer = indexBuffer.UploadedBuffer;
				_baseQuadMesh->ConstantBuffer = constantBuffer.UploadedBuffer;
			}

			quadMesh.VertexBuffer		= _baseQuadMesh->VertexBuffer;
			quadMesh.IndexBuffer		= _baseQuadMesh->IndexBuffer;
			quadMesh.ConstantBuffer		= _baseQuadMesh->ConstantBuffer;

			return quadMesh;
		}

		SpriteTexture AssetManager::CreateTexture(std::string path)
		{
			if (Textures.count(path) > 0)
				return Textures[path];

			auto root = _fileSystem->Root();
			auto textureResult = SpriteTexture();
			auto tgaloader = TgaLoader();
			auto texture = tgaloader.load(root+"//Assets//"+path);

			textureResult.Height	= texture->h;
			textureResult.Width		= texture->w;
			textureResult.Path		= path;

			D3D11_SUBRESOURCE_DATA initData = { texture->texels,  texture->w * 4, 0 };

			D3D11_TEXTURE2D_DESC desc = {};
			desc.Width = texture->w;
			desc.Height = texture->h;
			desc.MipLevels = 1;
			desc.ArraySize = 1;
			desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			desc.SampleDesc.Count = 1;
			desc.Usage = D3D11_USAGE_DEFAULT;
			desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
			desc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

			HRESULT hr = _device->CreateTexture2D(&desc, &initData, &textureResult.Texture);

			if (SUCCEEDED(hr))
			{
				D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc;
				SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				SRVDesc.Texture2D.MostDetailedMip = 0;
				SRVDesc.Texture2D.MipLevels = -1;

				_device->CreateShaderResourceView(textureResult.Texture, &SRVDesc, &textureResult.TextureView);
			}

			Textures[path] = textureResult;

			return textureResult;
		}

		Audio AssetManager::CreateAudio(const string& audioPath, bool b3d, bool Looping, bool Stream, float volume,float pitch, bool PlayOnLoad)
		{
			if (Sounds.count(audioPath) > 0)
				return Sounds[audioPath];

			auto root = _fileSystem->Root();
			auto audioRootPath = root + "//Assets//" + audioPath;

			Audio audioDetails;
			audioDetails.Looping = Looping;
			audioDetails.volume = volume;
			audioDetails.pitch = pitch;
			audioDetails.PlayOnLoad = PlayOnLoad;
			audioDetails.FileName = audioPath;
			//searchs the sound map to find sound 
			auto tFoundIt = _Fmod->mSounds.find(audioRootPath);
			if (tFoundIt != _Fmod->mSounds.end())
			{
				audioDetails.Sound = _Fmod->mSounds[audioRootPath];
				return audioDetails;
			}
			//adds the additional options if specified 
			FMOD_MODE eMode = FMOD_DEFAULT;
			eMode |= b3d ? FMOD_3D : FMOD_2D;
			eMode |= Looping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
			eMode |= Stream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;

			//Creates the new sound in FMOD and then adds it to the sound Map using name and addtional options
			_Fmod->mpSystem->createSound(audioRootPath.c_str(), eMode, nullptr, &audioDetails.Sound);
			if (audioDetails.Sound) {
				_Fmod->mSounds[audioRootPath] = audioDetails.Sound;
			}

			Sounds[audioPath] = audioDetails;

			return audioDetails;
		}

		void AssetManager::LoadScript(std::string filename,std::string entityName)
		{
			auto indexName = filename + entityName;

			auto root = _fileSystem->Root();
			auto scriptRoot = root + "//Assets//" + filename;

			std::cout << "script root: " << scriptRoot << std::endl;

			lua_settop(_luastate,0);

			auto result = luaL_loadfile(_luastate, scriptRoot.c_str());

			if (result != LUA_OK)
			{
				const char* message = lua_tostring(_luastate, -1);
				std::cout << message << std::endl;
				assert(true, message);
				lua_pop(_luastate, 1);
				return;
			}

			lua_newtable(_luastate);
			//Create metatable
			lua_newtable(_luastate);
			//Get the global table
			lua_getglobal(_luastate, "_G");
			lua_setfield(_luastate, -2, "__index");
			//Set global as the metatable
			lua_setmetatable(_luastate, -2);
			//Push to registry with a unique name.
			//I feel like these 2 steps could be merged or replaced but I'm not sure how
			lua_setfield(_luastate, LUA_REGISTRYINDEX, indexName.c_str());
			//Retrieve it. 
			lua_getfield(_luastate, LUA_REGISTRYINDEX, indexName.c_str());
			//Set the upvalue (_ENV)
			lua_setupvalue(_luastate, 1, 1);
			//Run chunks
			auto runcall = lua_pcall(_luastate, 0, 0, 0);

			if (runcall != LUA_OK)
			{
				const char* message = lua_tostring(_luastate, -1);
				std::cout << message << std::endl;
				assert(true, message);
				lua_pop(_luastate, 1);
				return;
			}

			int stackSize = lua_gettop(_luastate);
			lua_pop(_luastate, stackSize);
		}

		std::string AssetManager::_assetPath;
	}
}
