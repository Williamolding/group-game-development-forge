#pragma once
#include "entity/EntityManager.h"
#include "asset/AssetManager.h"
#include "Scene.h"
#include "physics/Collisions/CollisionsManager.h"
#include <map>

namespace forge
{
	namespace lua
	{
		class LuaSystem;
	}

	namespace game
	{
		struct PendingSceneLoad
		{
			std::string sceneName;
			std::string scenePath;
		};

		class SceneManager final
		{
		private:
			entity::IEntityManager* _manager;
			
			asset::AssetManager* _assetManger;

			lua::LuaSystem* _luaSystem;

			std::map<std::string, scene::IScene*> _scenes;

			std::string currentScene;
			PendingSceneLoad pendingSceneLoad;
			bool PendingLoad = false;

			physics::CollisionsManager *_collisionsManager;

		public:
			SceneManager(entity::IEntityManager* manager, asset::AssetManager* assetManager);
			~SceneManager();
			
			void Initialise();
			void Update(lua::LuaSystem* lsystem, double deltatime, bool Enablephysics);

			void AddScene(std::string sceneName, scene::IScene* scene);
			void AddScene(std::string sceneNamescene);
			void LoadScene();
			void LuaLoadScene(std::string scene);

			void LoadSceneFromFile(std::string sceneName, const std::string& scenePath);
			void SaveSceneToFile(std::string sceneName, std::string scenePath);

		};
	}
}
