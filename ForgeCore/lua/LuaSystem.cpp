#include "LuaSystem.h"
#include "LuaLoader.h"
#include "Modules/LuaModuleInput.h"
#include "Modules/LuaModuleSceneManager.h"
#include "Modules/LuaModuleAnimation.h"
#include "Modules/LuaModuleCamera.h"
#include "Modules/LuaModuleRectCollider.h"
#include "Modules/LuaModuleCircleCollider.h"
#include "Modules/LuaModuleVector2D.h"
#include "Modules/LuaModuleEntityManager.h"
#include "Modules/LuaModuleParticle.h"
#include "Modules/LuaModuleSoundEmitterComponent.h"
#include "Modules/LuaModuleTransformComponent.h"
#include "Modules/LuaModuleVector3.h"
#include "Modules/LuaModuleEntity.h"
#include "Modules/LuaModuleGlobals.h"
#include "Functionality/LuaGlobals.h"

namespace forge
{
	namespace lua
	{
		void LuaSystem::LoadBindings(entity::IEntityManager* entityManager, game::SceneManager* sceneManager)
		{
			_entityManager = entityManager;
			_sceneManager = sceneManager;

			LuaLoader::Register<LuaModuleInput, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleEntity, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleVector3, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleTransformComponent, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleSoundEmitterComponent, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleParticleComponent, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleEntityManager, entity::IEntityManager>(_luaState, _entityManager);
			LuaLoader::Register<LuaModuleVector2D, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleCircleCollider, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleRectCollider, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleCamera, entity::IEntityManager>(_luaState, _entityManager);
			LuaLoader::Register<LuaModuleAnimation, NoLuaRef>(_luaState, nullptr);
			LuaLoader::Register<LuaModuleSceneManager, game::SceneManager>(_luaState, _sceneManager);
			LuaLoader::Register<LuaModuleGlobals, NoLuaRef>(_luaState, nullptr);
		}

		void LuaSystem::ClearGlobals()
		{
			LuaGlobals::Clear();
		}
	}
}