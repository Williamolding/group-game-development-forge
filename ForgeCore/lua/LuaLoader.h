#pragma once
#include <lua.h>

namespace forge
{
	namespace lua
	{	
		class NoLuaRef {};

		class LuaLoader
		{
		public:
			template<class T, class R>
			static void Register(lua_State* state, R* reference)
			{
				T::Register(state, reference);
			}
		};

		template<class reference>
		class LuaModule
		{
		public:
			virtual void Register(lua_State* state, reference* ref) = 0;
		};
	}
}
