#pragma once
#include <map>
#include <string.h>


class StorageInt
{
private :
	int _value;
public:

	StorageInt()
	{
		_value = 0;
	}

	int GetValue() { return _value; }
	void SetValue(int value) { _value = value; }
	void IncrementValue() { _value++; }
	void DecrementValue() { _value--; }
};

class LuaGlobals
{
private:
	static std::map<std::string, StorageInt*> _storedGlobals;
public:
	static void AddGlobal(std::string name, int initalValue)
	{
		auto value  = new StorageInt();
		value->SetValue(initalValue);
		_storedGlobals[name] = value;		
	}

	static int GetValue(std::string name)
	{
		auto value = _storedGlobals.at(name);
		return value->GetValue();
	}

	static void SetValue(std::string name, int value)
	{
		if (_storedGlobals.count(name) == 0)
		{
			auto value = new StorageInt();
			value->SetValue(0);
			_storedGlobals[name] = value;
		}
		

		//auto inte = _storedGlobals.at(name);
		//return inte->SetValue(value);
	}

	static void Clear()
	{
		_storedGlobals.clear();
	}

	static void IncrementValue(std::string name)
	{
		auto value = _storedGlobals.at(name);
		value->IncrementValue();
	}

	static void DecrementValue(std::string name)
	{
		auto value = _storedGlobals.at(name);
		value->DecrementValue();
	}
};

