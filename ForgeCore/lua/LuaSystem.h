#pragma once
#include "SceneManager.h"
#include "entity/EntityManager.h"

namespace forge
{
	namespace lua
	{
		class LuaSystem
		{
			lua_State* _luaState;
			entity::IEntityManager* _entityManager;
			game::SceneManager* _sceneManager;
		public:
			LuaSystem()
			{
				CreateNewLuaState();
			}

			~LuaSystem()
			{
				lua_close(_luaState);
			}

			void CreateNewLuaState()
			{
				_luaState = luaL_newstate();
				luaL_openlibs(_luaState);
			}

			lua_State* GetState() const 
			{
				return _luaState;
			}

			void Collect() const
			{
				lua_gc(_luaState, LUA_GCCOLLECT, 0);
			}

			void ClearGlobals();

			void ResetLauState()
			{
				if (_luaState) 
				{
					lua_close(_luaState);

					CreateNewLuaState();
				}
			}

			void LoadBindings(entity::IEntityManager* entityManager, game::SceneManager* sceneManager);
		};
	}
}
