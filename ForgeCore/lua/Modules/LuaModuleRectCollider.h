#pragma once
#include <lua/LuaLoader.h>
#include <lua.h>
#include <LuaBridge/detail/Namespace.h>
#include "components/Colliders/iCollider.h"
#include "components/Colliders/RectCollider.h"

namespace forge {
	namespace  lua
	{
		class LuaModuleRectCollider final : public LuaModule<lua::NoLuaRef>
		{
		public:
			static void Register(lua_State* state, lua::NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<component::RectCollider>("def_RectCollider")
							.addFunction("IsColliding", &component::RectCollider::LuaIsColliding)
							.addFunction("IsTriggerColliding", &component::RectCollider::LuaIsTriggerColliding)
							.addFunction("Dimensions", &component::RectCollider::GetDimensions)
							.addProperty("Width", &component::RectCollider::GetWidth, &component::RectCollider::SetWidth)
							.addProperty("Height", &component::RectCollider::GetHeight, &component::RectCollider::SetHeight)
							.addProperty("Offset", &component::RectCollider::GetOffset, &component::RectCollider::SetOffset)
							.addProperty("IsTrigger", &component::RectCollider::IsTrigger, &component::RectCollider::SetTrigger)
						.endClass()
					.endNamespace();
			}
		};
	}
}
