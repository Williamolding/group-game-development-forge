#pragma once
#include <components/Animation.h>
#include "../Functionality/LuaGlobals.h"

namespace forge
{
	namespace lua
	{
		class LuaModuleGlobals : public LuaModule<NoLuaRef>
		{
		public:
			static void Register(lua_State* state, NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Global")
					.addFunction("Set", &LuaGlobals::SetValue)
					.addFunction("Get", &LuaGlobals::GetValue)
					.addFunction("Inc", &LuaGlobals::IncrementValue)
					.addFunction("Dec", &LuaGlobals::DecrementValue)
					.endNamespace();
			}
		};
	}
}

