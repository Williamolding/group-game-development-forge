#pragma once
#include "components/Colliders/RectCollider.h"
#include "components/Colliders/CircleCollider.h"
#include "components/SoundEmitter.h"
#include "components/Animation.h"

namespace forge
{
	namespace lua
	{
		class LuaModuleEntity : public LuaModule<NoLuaRef>
		{
		public:
			static void Register(lua_State* state, NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<entity::Entity>("def_Entity")
							.addFunction("Name", &entity::Entity::GetName)
							.addFunction("Transform", &entity::Entity::getComponent<component::Transform>)
							.addFunction("SoundEmitter", &entity::Entity::getComponent<component::SoundEmitter>)
							.addFunction("Particle", &entity::Entity::getComponent<component::Particle>)
							.addFunction("RectCollider", &entity::Entity::getComponent<component::RectCollider>)
							.addFunction("CircleCollider", &entity::Entity::getComponent<component::CircleCollider>)
							.addFunction("Animation", &entity::Entity::getComponent<component::Animation>)
						.endClass()
					.endNamespace();
			}
		};
	}
}
