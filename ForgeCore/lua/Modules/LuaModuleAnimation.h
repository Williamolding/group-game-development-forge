#pragma once
#include <components/Animation.h>
namespace forge
{
	namespace lua
	{
		class LuaModuleAnimation : public LuaModule<NoLuaRef>
		{
		public:
			static void Register(lua_State* state, NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Animation")
						.beginClass<component::Animation>("def_Animation")
							.addFunction("Stop", &component::Animation::Stop)
							.addFunction("Start", &component::Animation::Start)
					.endClass()
					.endNamespace();
			}
		};
	}
}

