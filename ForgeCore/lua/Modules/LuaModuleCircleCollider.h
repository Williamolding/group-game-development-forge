#pragma once
#include <lua/LuaLoader.h>
#include <lua.h>
#include <LuaBridge/detail/Namespace.h>
#include <components/Colliders/CircleCollider.h>

namespace forge {
	namespace  lua
	{
		class LuaModuleCircleCollider final : public LuaModule<lua::NoLuaRef>
		{
		public:
			static void Register(lua_State* state, lua::NoLuaRef* reference) 
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<component::CircleCollider>("def_CircleCollider")
							.addFunction("Diameter", &component::CircleCollider::GetDiameter)
							.addFunction("IsColliding", &component::CircleCollider::LuaIsColliding)
							.addFunction("IsTriggerColliding", &component::CircleCollider::LuaIsTriggerColliding)
							.addProperty("Radius", &component::CircleCollider::GetRadius, &component::CircleCollider::SetRadius)
							.addProperty("Offset", &component::CircleCollider::GetOffset, &component::CircleCollider::SetOffset)
							.addProperty("IsTrigger", &component::CircleCollider::IsTrigger, &component::CircleCollider::SetTrigger)
						.endClass()
					.endNamespace();
			}
		};
	}
}