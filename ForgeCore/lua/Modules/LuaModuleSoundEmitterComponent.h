#pragma once
#include "components/SoundEmitter.h"

namespace forge
{
	namespace lua
	{
		class LuaModuleSoundEmitterComponent : public LuaModule<lua::NoLuaRef>
		{
		public:
			static void Register(lua_State* state, lua::NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<component::SoundEmitter>("def_SoundEmitter")
							.addFunction("Play", &component::SoundEmitter::PlaySounds)
							.addFunction("Pause", &component::SoundEmitter::TogglePauseSound)
							.addFunction("isPlaying", &component::SoundEmitter::IsPlaying)
							.addFunction("setVolume", &component::SoundEmitter::SetChannelVolume)
							.addFunction("getVolume", &component::SoundEmitter::GetChannelVolume)
							.addFunction("setPitch", &component::SoundEmitter::SetChannelPitch)
							.addFunction("getPitch", &component::SoundEmitter::GetChannelPitch)
						.endClass()
					.endNamespace();
			}
		};
	}
}

