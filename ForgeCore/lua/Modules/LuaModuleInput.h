#pragma once
#include "input/InputManager.h"

namespace forge
{
	namespace lua
	{
		//add lua key mappings here
		const std::map<std::string, forge::input::Keys> _scanCodeMapper = {
			{ "A", forge::input::Keys::KEY_A },
			{ "B", forge::input::Keys::KEY_B },
			{ "C", forge::input::Keys::KEY_C },
			{ "D", forge::input::Keys::KEY_D },
			{ "E", forge::input::Keys::KEY_E },
			{ "F", forge::input::Keys::KEY_F },
			{ "G", forge::input::Keys::KEY_G },
			{ "H", forge::input::Keys::KEY_H },
			{ "I", forge::input::Keys::KEY_I },
			{ "J", forge::input::Keys::KEY_J },
			{ "K", forge::input::Keys::KEY_K },
			{ "L", forge::input::Keys::KEY_L },
			{ "M", forge::input::Keys::KEY_M },
			{ "N", forge::input::Keys::KEY_N },
			{ "O", forge::input::Keys::KEY_O },
			{ "P", forge::input::Keys::KEY_P },
			{ "Q", forge::input::Keys::KEY_Q },
			{ "R", forge::input::Keys::KEY_R },
			{ "S", forge::input::Keys::KEY_S },
			{ "T", forge::input::Keys::KEY_T },
			{ "U", forge::input::Keys::KEY_U },
			{ "V", forge::input::Keys::KEY_V },
			{ "W", forge::input::Keys::KEY_W },
			{ "X", forge::input::Keys::KEY_X },
			{ "Y", forge::input::Keys::KEY_Y },
			{ "Z", forge::input::Keys::KEY_Z },
			{ "ENTER", forge::input::Keys::KEY_ENTER }
		};

		class LuaModuleInput : public LuaModule<NoLuaRef>
		{
		public:
			static void Register(lua_State* state, NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Input")
						.addCFunction("KeyPressed", &IsKeyPressed)
						.addCFunction("KeyDown", &IsKeyDown)
						.addCFunction("KeyUp", &IsKeyUp)
					.beginNamespace("Collection")
						.addFunction("KeyPressed", &input::InputManager::IsPressed)
						.addFunction("KeyDown", &input::InputManager::IsDown)
						.addFunction("KeyUp", &input::InputManager::IsUp)
					.endNamespace()
					.endNamespace();
			}
		private:
			static int IsKeyDown(lua_State* state)
			{
				const std::string keyCode = luaL_checkstring(state, 1);

				if (forge::lua::_scanCodeMapper.count(keyCode) == 0)
				{
					std::cout << "Key " << keyCode << " Not Supported" << std::endl;

					lua_pushboolean(state, false);
					return 1;
				}

				const auto mappeKeyCode = forge::lua::_scanCodeMapper.at(keyCode);
				const auto keyState = input::Keyboard::IsKeyDown(mappeKeyCode);

				lua_pushboolean(state, keyState);

				return 1;
			}
			static int IsKeyPressed(lua_State* state)
			{
				const std::string keyCode = luaL_checkstring(state, 1);

				if (forge::lua::_scanCodeMapper.count(keyCode) == 0)
				{
					std::cout << "Key " << keyCode << " Not Supported" << std::endl;

					lua_pushboolean(state, false);
					return 1;
				}

				const auto mappeKeyCode = forge::lua::_scanCodeMapper.at(keyCode);
				const auto keyState = input::Keyboard::IsKeyPressed(mappeKeyCode);

				lua_pushboolean(state, keyState);

				return 1;
			}
			static int IsKeyUp(lua_State* state)
			{
				const std::string keyCode = luaL_checkstring(state, 1);

				if (forge::lua::_scanCodeMapper.count(keyCode) == 0)
				{
					std::cout << "Key " << keyCode << " Not Supported" << std::endl;

					lua_pushboolean(state, false);
					return 1;
				}

				const auto mappeKeyCode = forge::lua::_scanCodeMapper.at(keyCode);
				const auto keyState = input::Keyboard::IsKeyUp(mappeKeyCode);

				lua_pushboolean(state, keyState);

				return 1;
			}
		};
	}
}
