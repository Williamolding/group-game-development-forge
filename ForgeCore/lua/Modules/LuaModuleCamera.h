#pragma once
namespace forge
{
	namespace lua
	{
		class LuaModuleCamera: public LuaModule<entity::IEntityManager>
		{
		public:
			static void Register(lua_State* state, entity::IEntityManager* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("View")
					.beginClass<graphics::Camera>("def_Camera")
							.addFunction("setPosition", &graphics::Camera::SetPosition)
								.addFunction("getPositionX", &graphics::Camera::GetPositionX)
								.addFunction("getPositionY", &graphics::Camera::GetPositionY)
							.endClass()
					.endNamespace();
			}
		};
	}
}

