#pragma once
#include "entity/EntityManager.h"
#include <lua/LuaLoader.h>

namespace forge
{
	namespace lua
	{
		class LuaModuleEntityManager : public LuaModule<entity::IEntityManager>
		{
		public:
			static void Register(lua_State* state, entity::IEntityManager* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.addVariable("EntityManager", reference)
						.beginClass<entity::IEntityManager>("def_EntityManager")
							.addFunction("Find", &entity::IEntityManager::Find)
							.addFunction("MainCamera", &entity::IEntityManager::GetCamera)
							.addFunction("Delete", &entity::IEntityManager::Delete)
						.endClass()
					.endNamespace();

			}
		};
	}
}
