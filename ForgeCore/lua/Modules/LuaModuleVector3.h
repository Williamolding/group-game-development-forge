#pragma once

namespace forge
{
	namespace lua
	{
		class LuaModuleVector3 : public LuaModule<lua::NoLuaRef>
		{
		public:
			static void Register(lua_State* state, lua::NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<component::Vector3>("def_Vector")
							.addProperty("X", &component::Vector3::GetX, &component::Vector3::SetX)
							.addProperty("Y", &component::Vector3::GetY, &component::Vector3::SetY)
							.addProperty("Z", &component::Vector3::GetZ, &component::Vector3::SetZ)
						.endClass()
					.endNamespace();
			}
		};
	}
}

