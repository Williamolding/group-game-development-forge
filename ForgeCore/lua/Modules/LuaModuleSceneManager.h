#pragma once
#include <lua/LuaLoader.h>
#include "SceneManager.h"

namespace forge
{
	namespace lua
	{
		class LuaModuleSceneManager : public LuaModule<game::SceneManager>
		{
		public:
			static game::SceneManager* ref;
		public:
			static void Register(lua_State* state, game::SceneManager* reference)
			{
				ref = reference;

				luabridge::getGlobalNamespace(state)
					.beginNamespace("Scene")
						.addCFunction("LoadScene", &luaLoadScene)
					.endNamespace();
			}
		private:
			static int luaLoadScene(lua_State* state)
			{
				const std::string sceneName = luaL_checkstring(state, 1);
				ref->LuaLoadScene(sceneName);
				return 1;
			}
		};

		game::SceneManager* lua::LuaModuleSceneManager::ref = nullptr;
	}

}
