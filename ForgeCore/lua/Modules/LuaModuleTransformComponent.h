#pragma once
namespace forge
{
	namespace lua
	{
		class LuaModuleTransformComponent : public LuaModule<lua::NoLuaRef>
		{
		public:
			static void Register(lua_State* state, lua::NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<component::Transform>("def_Transform")
							.addFunction("GetPosition", &component::Transform::GetPositionV2D)
							.addFunction("SetPosition", &component::Transform::SetPositionV2D)
							.addFunction("GetScale", &component::Transform::GetScaleV2D)
							.addFunction("SetScale", &component::Transform::SetScaleV2D)
							.addFunction("FlipLeft", &component::Transform::FlipLeft)
							.addFunction("FlipRight", &component::Transform::FlipRight)
						.endClass()
					.endNamespace();
			}
		};
	}
}

