#pragma once
#include <lua/LuaLoader.h>
#include <lua.h>
#include <LuaBridge/detail/Namespace.h>
#include "Vector2D.h"

namespace forge
{
	namespace lua
	{
		class LuaModuleVector2D : public LuaModule<lua::NoLuaRef>
		{
		public:
			static void Register(lua_State* state, lua::NoLuaRef* reference)
			{
				luabridge::getGlobalNamespace(state)
					.beginNamespace("Entity")
						.beginClass<Vector2D>("def_Vector2D")
							.addStaticFunction("Normalize", &Vector2D::Normalize)
							.addStaticFunction("Length", &Vector2D::Length)
							.addStaticFunction("Dot", &Vector2D::Dot)
							.addStaticFunction("Perpendicular", &Vector2D::Perpendicular)
							.addStaticFunction("R2D", &Vector2D::RadiansToDegrees)
							.addStaticFunction("D2R", &Vector2D::DegreesToRadians)
							.addStaticFunction("AngleBetweenV2D", &Vector2D::AngleBetweenVectors)
							.addStaticFunction("RadiansToDegrees", &Vector2D::RadiansToDegrees)
							.addStaticFunction("DegreesToRadians", &Vector2D::DegreesToRadians)
							.addFunction("Reverse", &Vector2D::Reverse)
							.addFunction("Zero", &Vector2D::Zero)
							.addFunction("ToPositives", &Vector2D::ToPositives)
							.addFunction("Distance", &Vector2D::Distance)
							.addFunction("Truncate", &Vector2D::Truncate)
							.addProperty("X", &Vector2D::GetX, &Vector2D::SetX)
							.addProperty("Y", &Vector2D::GetY, &Vector2D::SetY)					
						.endClass()
					.endNamespace();
			}
		};
	}
}
