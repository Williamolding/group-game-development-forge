#pragma once
#include <lua/LuaLoader.h>
#include <lua.h>
#include <LuaBridge/detail/Namespace.h>
#include "entity/EntityConstants.h"
#include "components/Particle.h"

namespace forge { namespace  lua
{
	class  LuaModuleParticleComponent : public LuaModule<lua::NoLuaRef>
	{
	public:
		static void Register(lua_State *state, lua::NoLuaRef *reference)
		{
			luabridge::getGlobalNamespace(state)
				.beginNamespace("Entity")
					.beginClass<component::Particle>("def_Particle")
						.addFunction("AccelerationMagnitude", &component::Particle::AccelerationMagnitude)
						.addFunction("Speed", &component::Particle::GetSpeed)
						.addFunction("LuaAddForce", &component::Particle::LuaAddForce)
						.addFunction("Enable", &component::Particle::Enable)
						.addFunction("Disable", &component::Particle::Disable)
						.addProperty("Mass", &component::Particle::GetMass, &component::Particle::SetMass)
						.addProperty("Acceleration", &component::Particle::GetAcceleration, &component::Particle::SetAcceleration)
						.addProperty("Velocity", &component::Particle::GetVelocity, &component::Particle::SetVelocity)
						.addProperty("CoefficientOfRestitution", &component::Particle::GetCoefficientOfRestitution, &component::Particle::SetCoefficientOfRestitution)
						.addProperty("BeingPushed", &component::Particle::BeingPushed, &component::Particle::SetBeingPushed)
						.addProperty("TerminalVelocity", &component::Particle::IsTerminalVelocityOn, &component::Particle::SetTerminalVelocity)
					.endClass()
				.endNamespace();
		}
	};
}
}
