#pragma once
#include "SceneManager.h"

namespace forge
{
	namespace game
	{
		struct WrapperPackage
		{
			entity::IEntityManager* EntityManager;
			SceneManager*			SceneManager;
			asset::AssetManager*	AssetManager;
			graphics::Graphics*		Graphics;
		};

		class IGameInstance
		{
		public:
			virtual ~IGameInstance() = default;
			virtual bool Run()	= 0;
			virtual void Initalise(std::string &path, const bool Editor) = 0;
			virtual void Resize(int width, int height) = 0;
			virtual void Stop() = 0;
			virtual void Pause() = 0;
			virtual void Resume() = 0;
			virtual WrapperPackage ExportWrapperPackage() = 0;
		};
	}
}
