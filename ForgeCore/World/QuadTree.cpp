#include <World/QuadTree.h>

#include "components/Transform.h"
#include "components/SpriteRenderer.h"
#include <components/Colliders/RectCollider.h>
#include <components/Colliders/CircleCollider.h>

#define MAXDENSITY 0
#define MAXLEVELS 10

namespace forge {
	namespace game
	{
		QuadTree::QuadTree(Quadrant * b, const unsigned lvl) : level(lvl), bounds(b)
		{
			for (unsigned int i = 0; i < nodes.size(); i++)
			{
				nodes[i] = nullptr;
			}
		}

		QuadTree::~QuadTree()
		{
			delete bounds;
			bounds = nullptr;

			for (auto node : nodes)
			{
				delete node;
				node = nullptr;
			}
		}

		/* Clears quad tree */
		void QuadTree::EmptyTrees()
		{
			entityList.clear();

			for (unsigned int i = 0; i < nodes.size(); i++)
			{
				if (nodes[i])
				{
					nodes[i]->EmptyTrees();
					nodes[i] = nullptr;
				}
			}
		}

		/* Splits node into 4 new sub-nodes */
		void QuadTree::Subdivide()
		{
			const float subWidth = bounds->dimensions.X * .5f;
			const float subHeight = bounds->dimensions.Y * .5f;
			const float subX = bounds->position.X;
			const float subY = bounds->position.Y;

			CreateNE(subX, subY, subWidth, subHeight);
			CreateNW(subX, subY, subWidth, subHeight);
			CreateSW(subX, subY, subWidth, subHeight);
			CreateSE(subX, subY, subWidth, subHeight);
		}

		void QuadTree::CreateNE(const float subX, const float subY, const float subWidth, const float subHeight)
		{
			if (!nodes[NORTHEAST])
				nodes[NORTHEAST] = new QuadTree(new Quadrant(subX + subWidth * .5f, subY + subHeight * .5f, subWidth, subHeight), level + 1);
		}

		void QuadTree::CreateNW(const float subX, const float subY, const float subWidth, const float subHeight)
		{
			if (!nodes[NORTHWEST])
				nodes[NORTHWEST] = new QuadTree(new Quadrant(subX - subWidth * .5f, subY + subHeight * .5f, subWidth, subHeight), level + 1);
		}

		void QuadTree::CreateSE(const float subX, const float subY, const float subWidth, const float subHeight)
		{
			if (!nodes[SOUTHEAST])
				nodes[SOUTHEAST] = new QuadTree(new Quadrant(subX + subWidth * .5f, subY - subHeight * .5f, subWidth, subHeight), level + 1);
		}

		void QuadTree::CreateSW(const float subX, const float subY, const float subWidth, const float subHeight)
		{
			if (!nodes[SOUTHWEST])
				nodes[SOUTHWEST] = new QuadTree(new Quadrant(subX - subWidth * .5f, subY - subHeight * .5f, subWidth, subHeight), level + 1);
		}

		/*
		*Determines which node owns which object.
		*-1 means object aren't completely fit within a child node
		* of the parent node
		*/
		int QuadTree::GetIndex(entity::Entity *e) const
		{
			signed int index = -1;
			const Vector2D midpoint = bounds->position;

			const Vector2D entityDimension = e->hasComponent<component::RectCollider>() ? 
				Vector2D(e->getComponent<component::RectCollider>()->GetWidth(), e->getComponent<component::RectCollider>()->GetHeight()) 
				: Vector2D(e->getComponent<component::CircleCollider>()->GetRadius());

			const Vector2D offset = e->hasComponent<component::RectCollider>() ? e->getComponent<component::RectCollider>()->GetOffset() 
				: e->getComponent<component::CircleCollider>()->GetOffset();
			
			const Vector2D entityPos = e->getComponent<component::Transform>()->GetPositionV2D() + offset;

			// Can object fully fit inside top quadrant
			const bool topQuad = entityPos.Y - entityDimension.Y > midpoint.Y;
			// Can object fully fit inside bottom quadrant
			const bool bottomQuad = entityPos.Y - entityDimension.Y < midpoint.Y && entityPos.Y + entityDimension.Y < midpoint.Y;

			// Can object completely fit inside left quadrant
			if (entityPos.X - entityDimension.X < midpoint.X && entityPos.X + entityDimension.X < midpoint.X) // West-side
			{
				if (topQuad)
					index = NORTHWEST;
				else if (bottomQuad)
					index = SOUTHWEST;
			}
			else if (entityPos.X - entityDimension.X > midpoint.X) // East-side
			{
				if (topQuad)
					index = NORTHEAST;
				else if (bottomQuad)
					index = SOUTHEAST;
			}

			return index;
		}

		void QuadTree::AddIntoTree(entity::Entity *e)
		{
			int index = GetIndex(e);

			if (index != -1)
			{
				if (nodes[index] && level < MAXLEVELS)
				{
					nodes[index]->AddIntoTree(e);
					return;
				}
			}

			entityList.emplace_back(e);

			if (index != -1)
				if (entityList.size() > MAXDENSITY && level < MAXLEVELS)
				{
					if (!nodes[index])
						Subdivide();

					size_t i = 0;

					while (i < entityList.size())
					{
						index = GetIndex(entityList[i]);

						if (index != -1)
						{
							nodes[index]->AddIntoTree(entityList[i]);
							entityList.erase(entityList.begin() + i);
						}
						else
							i++;
					}
				}
		}

		void QuadTree::CollidablesList(std::vector<entity::Entity*> &returnList, entity::Entity *e)
		{
			const int index = GetIndex(e);

			if (index != -1 && nodes[index])
				nodes[index]->CollidablesList(returnList, e);

			for (entity::Entity *entity : entityList)
			{
				if (e != entity)				
					returnList.emplace_back(entity);				
			}
		}
	}
}
