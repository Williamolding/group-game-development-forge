#pragma once
#include <vector>
#include "entity/Entity.h"
#include "Vector2D.h"

namespace forge
{
	namespace game
	{
		enum Compass
		{
			NORTHEAST = 0,	
			NORTHWEST,		
			SOUTHWEST,		
			SOUTHEAST,		
		};

		struct Quadrant
		{
			Vector2D position;
			Vector2D dimensions;

			Quadrant()
			{
				position = dimensions = Vector2D();
			}

			Quadrant(const float xVal = 0, const float yVal = 0, const float width = 100.f, const float height = 100.f) : position(Vector2D(xVal, yVal)), dimensions(Vector2D(width, height)) {/* Empty */ }			
			Quadrant(const Vector2D &position = Vector2D(), const Vector2D &dimensions = Vector2D(100.f)) : position(position), dimensions(dimensions) {/* Empty */}
		};

		class QuadTree
		{
		private:
			unsigned int level;
			Quadrant *bounds;
			std::vector<entity::Entity*> entityList;
			std::array<QuadTree*, 4> nodes;

		private:
			void Subdivide();
			int GetIndex(entity::Entity *e) const;

			void CreateNE(const float subX, const float subY, const float subWidth, const float subHeight);
			void CreateNW(const float subX, const float subY, const float subWidth, const float subHeight);
			void CreateSE(const float subX, const float subY, const float subWidth, const float subHeight);
			void CreateSW(const float subX, const float subY, const float subWidth, const float subHeight);

		public:
			QuadTree(Quadrant *b, const unsigned lvl = 0);
			~QuadTree();

			void EmptyTrees();
			void AddIntoTree(entity::Entity *e);

			void CollidablesList(std::vector<entity::Entity*> &returnList, entity::Entity *e);
		};
	}
}
