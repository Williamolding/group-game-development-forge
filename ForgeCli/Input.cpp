#include "Input.h"
#include <input/Keyboard.h>
#include <input/Mouse.h>
#include <input/XboxController.h>
#include <input/InputManager.h>

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			void Input::SetKeyfalse(unsigned short index)
			{
				forge::input::Keyboard::SetKeyfalse(index);
			}
			void Input::SetKeytrue(unsigned short index)
			{
				forge::input::Keyboard::SetKeytrue(index);
			}
			void Input::SetButtonfalse(unsigned short index)
			{
				forge::input::Mouse::SetButtonfalse(index);
			}
			void Input::SetButtontrue(unsigned short index)
			{
				forge::input::Mouse::SetButtontrue(index);
			}
			void Input::SetMouseX(int x)
			{
				forge::input::Mouse::MouseX = x;
			}
			void Input::SetMouseY(int y)
			{
				forge::input::Mouse::MouseY = y;
			}
			int Input::GetMouseX()
			{
				return forge::input::Mouse::MouseX;
			}
			int Input::GetMouseY()
			{
				return forge::input::Mouse::MouseY;
			}
			bool Input::IsButtonDown(unsigned short index)
			{
				auto button = (forge::input::Buttons)index;
				return forge::input::Mouse::ButtonDown(button);
			}
			bool Input::IsButtonUp(unsigned short index)
			{
				auto button = (forge::input::Buttons)index;
				return forge::input::Mouse::ButtonUp(button);
			}
			bool Input::IsKeyDown(unsigned short index)
			{
				auto Key = (forge::input::Keys)index;
				return forge::input::Keyboard::IsKeyDown(Key);
			}
			bool Input::IsKeyUp(unsigned short index)
			{
				auto Key = (forge::input::Keys)index;
				return forge::input::Keyboard::IsKeyUp(Key);
			}
			bool Input::RegisterInputCollection(System::String^ collectionName)
			{
				auto CppCollectionName = msclr::interop::marshal_as<std::string>(collectionName);
				return input::InputManager::RegisterCollection(CppCollectionName);
			}
			bool Input::RegisterKeyboardInput(System::String^ collectionName, unsigned short key)
			{
				auto castKey = static_cast<input::Keys>(key);
				auto CppCollectionName = msclr::interop::marshal_as<std::string>(collectionName);

				return input::InputManager::RegisterKeyboardInput(CppCollectionName, castKey);
			}
			bool Input::RegisterMouseInput(System::String^ collectionName, unsigned short button)
			{
				auto castButton = static_cast<input::Buttons>(button);
				auto CppCollectionName = msclr::interop::marshal_as<std::string>(collectionName);

				return input::InputManager::RegisterMouseInput(CppCollectionName, castButton);
			}
			bool Input::RegisterControllerInput(System::String^ collectionName, unsigned short gamePadButton)
			{
				auto castControllerButton = static_cast<input::GamePadButton>(gamePadButton);
				auto CppCollectionName = msclr::interop::marshal_as<std::string>(collectionName);

				return input::InputManager::RegisterControllerInput(CppCollectionName, castControllerButton);
			}
			void Input::ClearCollections()
			{
				input::InputManager::Clear();
			}
			System::Collections::Generic::List<InputCollection^>^ Input::GetCollectionState()
			{
				auto collections = input::InputManager::GetInputCollections();
				auto returnCollections = gcnew System::Collections::Generic::List<InputCollection^>();

				for (auto collection : collections)
				{
					auto newName = msclr::interop::marshal_as<System::String^>(collection.first);

					auto newCollection = gcnew InputCollection();
					newCollection->collectionName = newName;
					newCollection->Keys				= gcnew System::Collections::Generic::List<int>();
					newCollection->Button			= gcnew System::Collections::Generic::List<int>();
					newCollection->GamePadButton	= gcnew System::Collections::Generic::List<int>();

					for (auto key : collection.second->_keys)
					{
						newCollection->Keys->Add(key);
					}

					for (auto button : collection.second->_buttons)
					{
						newCollection->Button->Add(button);
					}

					for (auto control : collection.second->_controls)
					{
						newCollection->GamePadButton->Add(control);
					}

					returnCollections->Add(newCollection);
				}

				return returnCollections;
			}
		}
	}
}