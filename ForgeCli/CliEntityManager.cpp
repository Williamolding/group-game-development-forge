#include "CliEntityManager.h"
#include <components/Transform.h>
#include <components/SpriteRenderer.h>
#include <components/Animation.h>
#include <components/Colliders/CircleCollider.h>
#include <components/Colliders/RectCollider.h>
#include <components/SoundEmitter.h>
#include <components/TilingTool.h>
#include <components/Particle.h>
#include <input/Mouse.h>

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			CliEntityManager::CliEntityManager(entity::IEntityManager* manager, asset::AssetManager* assetManager, graphics::Graphics* graphics)
			{
				_manager = manager;
				_assestManager = assetManager;
				_graphics = graphics;
				_gridLock = false;
			}

			entity::IEntityManager* CliEntityManager::GetManager()
			{
				return _manager;
			}

			Entity^ CliEntityManager::CreateEntity()
			{
				std::string namnBase = "Entity";
				auto name = namnBase;
				int count = 1;
				while (_manager->Find(name) != nullptr)
				{
					name = namnBase + "_" + std::to_string(count);
					count++;
				}

				auto entity = &_manager->addEntity(name);
				auto mousePos = DirectX::XMFLOAT2(input::Mouse::MouseX, input::Mouse::MouseY);
				auto spawnPos = _graphics->ScreenToWorldSpacePosition(mousePos);

				entity->addComponent<component::Transform>(DirectX::XMFLOAT3(spawnPos.x, spawnPos.y, 0));

				auto returnEntity = gcnew Entity();
				returnEntity->entity = entity;
				returnEntity->name = marshal_as<System::String^>(name);
				returnEntity->children = FindChildren(entity->getComponent<component::Transform>(), returnEntity);

				_manager->SetSelectedEntity(entity);

				return returnEntity;
			}

			System::String^ CliEntityManager::FindEntityByMousePosition()
			{
				auto mousePos = DirectX::XMFLOAT2(input::Mouse::MouseX, input::Mouse::MouseY);
				auto clickPos = _graphics->ScreenToWorldSpacePosition(mousePos);

				auto entity = _manager->FindByPosition(clickPos);

				if (entity != nullptr)
				{
					auto transform = entity->getComponent<component::Transform>();
					if (transform != nullptr)
					{
						if (transform->GetParent() != nullptr && transform->GetParent()->Entity->hasComponent<component::TilingTool>())
							entity = transform->GetParent()->Entity;
					}

					_manager->SetSelectedEntity(entity);
					return marshal_as<System::String^>(entity->GetName());
				}
				else if (_manager->GetSelectedEntity() != nullptr)
				{
					_manager->SetSelectedEntity(nullptr);
					return nullptr;
				}
			}

			void CliEntityManager::SetGravity(float g)
			{
				auto selectedEntity = _manager->GetSelectedEntity();
				
				if (selectedEntity && selectedEntity->hasComponent<component::Particle>())
				{
					const auto selectedParticle = selectedEntity->getComponent<component::Particle>();
					selectedParticle->GetGravityForceGenerator()->SetGravity(g);
				}
			}

			float CliEntityManager::GetGravity()
			{
				if (_manager->GetSelectedEntity()->hasComponent<component::Particle>())
				{
					const auto selectedParticle = _manager->GetSelectedEntity()->getComponent<component::Particle>();
					return selectedParticle->GetGravityForceGenerator()->GetGravity().GetY();
				}
			}

			void CliEntityManager::SetTerminalVelocity(bool tv)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity && selectedEntity->hasComponent<component::Particle>())
				{
					const auto selectedParticle = selectedEntity->getComponent<component::Particle>();
					selectedParticle->SetTerminalVelocity(tv);
				}
			}

			bool CliEntityManager::IsTerminalVelocityOn()
			{
				if (_manager->GetSelectedEntity()->hasComponent<component::Particle>())
				{
					const auto selectedParticle = _manager->GetSelectedEntity()->getComponent<component::Particle>();
					return selectedParticle->IsTerminalVelocityOn();
				}
			}

			void CliEntityManager::SetBeingPushed(bool bp)
			{
				auto selectedEntity = _manager->GetSelectedEntity();
				
				if (selectedEntity && selectedEntity->hasComponent<component::Particle>())
					selectedEntity->getComponent<component::Particle>()->SetBeingPushed(bp);
			}

			bool CliEntityManager::BeingPushed()
			{
				if (_manager->GetSelectedEntity()->hasComponent<component::Particle>())
				{
					const auto selectedParticle = _manager->GetSelectedEntity()->getComponent<component::Particle>();
					return selectedParticle->BeingPushed();
				}
			}
			
			void CliEntityManager::AddPushForce()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::Particle>())
					_manager->GetSelectedEntity()->getComponent<component::Particle>()->AddPushForce();
			}
			
			void CliEntityManager::RemoveLaminarForce()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::Particle>())
					_manager->GetSelectedEntity()->getComponent<component::Particle>()->GetForcesList().erase("laminar");
			}

			void CliEntityManager::RemovePushForce()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::Particle>())
					_manager->GetSelectedEntity()->getComponent<component::Particle>()->GetForcesList().erase("push");
			}

			void CliEntityManager::SetCoefficientOfRestitution(float cor)
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::Particle>())
					_manager->GetSelectedEntity()->getComponent<component::Particle>()->SetCoefficientOfRestitution(cor);
			}


			float CliEntityManager::GetCoefficientOfRestitution()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::Particle>())
					return _manager->GetSelectedEntity()->getComponent<component::Particle>()->GetCoefficientOfRestitution();
			}

						
			void CliEntityManager::SetFrictionCof(float cof)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity && selectedEntity->hasComponent<component::Particle>())
				{
					const auto selectedParticle = selectedEntity->getComponent<component::Particle>();
					selectedParticle->GetFrictionGenerator()->SetCoF(cof);
				}
			}

			float CliEntityManager::GetFrictionCof()
			{
				if (_manager->GetSelectedEntity()->hasComponent<component::Particle>())
				{
					const auto selectedParticle = _manager->GetSelectedEntity()->getComponent<component::Particle>();
					return selectedParticle->GetFrictionGenerator()->GetCoF();
				}
			}

			void CliEntityManager::SetFrictionNormal(float normal)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity && selectedEntity->hasComponent<component::Particle>())
				{
					const auto selectedParticle = selectedEntity->getComponent<component::Particle>();
					selectedParticle->GetFrictionGenerator()->SetNormal(normal);
				}
			}

			float CliEntityManager::GetFrictionNormal()
			{
				if (_manager->GetSelectedEntity()->hasComponent<component::Particle>())
				{
					const auto selectedParticle = _manager->GetSelectedEntity()->getComponent<component::Particle>();
					return selectedParticle->GetFrictionGenerator()->GetNormal();
				}
			}

			void CliEntityManager::SetFrictionMultiplier(float multiplier)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity && selectedEntity->hasComponent<component::Particle>())
				{
					const auto selectedParticle = selectedEntity->getComponent<component::Particle>();
					selectedParticle->GetFrictionGenerator()->SetMultiplier(multiplier);
				}
			}

			float CliEntityManager::GetFrictionMultiplier()
			{
				if (_manager->GetSelectedEntity()->hasComponent<component::Particle>())
				{
					const auto selectedParticle = _manager->GetSelectedEntity()->getComponent<component::Particle>();
					return selectedParticle->GetFrictionGenerator()->GetMultiplier();
				}
			}

			void CliEntityManager::SetPushDrag(float dragCoefficient)
			{
				auto selected = _manager->GetSelectedEntity();

				if (!selected)
					return;

				if (selected->hasComponent<component::Particle>())
				{
					if (selected->getComponent<component::Particle>()->GetPushForceGenerator())
					{
						const auto selectedParticle = selected->getComponent<component::Particle>();
						selectedParticle->GetPushForceGenerator()->SetDragCoefficient(dragCoefficient);
					}
				}
			}

			void CliEntityManager::SetPushingForce(float force)
			{
				auto selected = _manager->GetSelectedEntity();

				if (!selected)
					return;

				if (selected->hasComponent<component::Particle>())
				{
					if (selected->getComponent<component::Particle>()->GetPushForceGenerator())
					{
						const auto selectedParticle = selected->getComponent<component::Particle>();
						selectedParticle->GetPushForceGenerator()->SetVelocity(force);
					}
				}
			}


			float CliEntityManager::GetPushForceDrag()
			{
				auto selected = _manager->GetSelectedEntity();
				
				if (selected->hasComponent<component::Particle>())
				{
					if (selected->getComponent<component::Particle>()->GetPushForceGenerator())
					{
						const auto selectedParticle = selected->getComponent<component::Particle>();
						return selectedParticle->GetPushForceGenerator()->GetDragCoefficient();
					}
				}
			}

			float CliEntityManager::GetPushingForce()
			{
				auto selected = _manager->GetSelectedEntity();
				
				if (selected->hasComponent<component::Particle>())
				{
					if (selected->getComponent<component::Particle>()->GetPushForceGenerator())
					{
						const auto selectedParticle = selected->getComponent<component::Particle>();
						return selectedParticle->GetPushForceGenerator()->GetVelocity().GetX();
					}
				}
			}

			void CliEntityManager::FindEntityByName(System::String^ name)
			{
				auto entity = _manager->Find(marshal_as<std::string>(name));

				if (entity != nullptr)
				{
					_manager->SetSelectedEntity(entity);
				}
				else if (_manager->GetSelectedEntity() != nullptr)
				{
					_manager->SetSelectedEntity(nullptr);
				}
			}

			void CliEntityManager::SetEntityName(System::String^ name)
			{
				if (_manager->GetSelectedEntity())				
					_manager->GetSelectedEntity()->SetName(marshal_as<std::string>(name));				
			}

			System::String^ CliEntityManager::GetEntityName()
			{
				if (_manager->GetSelectedEntity())
					return marshal_as<System::String^>(_manager->GetSelectedEntity()->GetName());
			}

			void CliEntityManager::DeleteSelectedEntity(System::String^ entityName)
			{
				if (_manager->Find(marshal_as<std::string>(entityName)) != nullptr)
				{
					auto entity = _manager->Find(marshal_as<std::string>(entityName));
					entity->Destroy();

					for (int i = 0; i < entity->getComponent<component::Transform>()->_children.size(); i++)
					{
						entity->getComponent<component::Transform>()->_children.at(i)->Entity->Destroy();
					}
					_manager->refresh();
				}

			}

			//void CliEntityManager::DeleteSelectedEntity(System::String^ entityName)
			//{
			//	if (_manager->Find(marshal_as<std::string>(entityName)) != nullptr)
			//	{
			//		auto entity = _manager->Find(marshal_as<std::string>(entityName));
			//		entity->Destroy();
			//		_manager->refresh();

			//	}

			//}

			void CliEntityManager::SetSelectedEntityPosition(int x, int y) //local
			{
				if (_manager->GetSelectedEntity() != nullptr)
				{
					auto entity = _manager->GetSelectedEntity();
					auto transform = entity->getComponent<component::Transform>();
					auto currentPosition = transform->GetPosition();

					if (transform->HasParent())
					{
						auto parent = transform->GetParent();
						auto parentsPositon = parent->GetPosition();
						auto childsPositionTransform = entity->getComponent<component::Transform>();

						auto newPositionX = (x);
						auto newPositionY = (y);
						auto newPositionZ = (0 - parentsPositon.z);

						childsPositionTransform->SetPosition(DirectX::XMFLOAT3(newPositionX, newPositionY, newPositionZ));
					}
					else
					{
						transform->SetPosition(DirectX::XMFLOAT3(x, y, currentPosition.z));
					}
				}
			}

			void CliEntityManager::UpdateEntityPosition()
			{
				if (_manager->GetSelectedEntity() != nullptr)
				{
					auto mousePos = DirectX::XMFLOAT2(input::Mouse::MouseX, input::Mouse::MouseY);
					auto clickPos = _graphics->ScreenToWorldSpacePosition(mousePos);

					auto entity = _manager->GetSelectedEntity();
					auto transform = entity->getComponent<component::Transform>();

					if (transform->GetParent() == nullptr || !transform->GetParent()->Entity->hasComponent<component::TilingTool>())
					{
						auto currentPosition = transform->GetPosition();

						if (_gridLock)
						{
							clickPos.x = (((int)clickPos.x + (ROUNDMULTIPLE / 2)) / ROUNDMULTIPLE) * ROUNDMULTIPLE;
							clickPos.y = (((int)clickPos.y + (ROUNDMULTIPLE / 2)) / ROUNDMULTIPLE) * ROUNDMULTIPLE;
						}

						if (transform->HasParent())
						{
							auto parent = transform->GetParent();
							auto parentsPositon = parent->GetPosition();
							auto childsPositionTransform = entity->getComponent<component::Transform>();

							auto newPositionX = (clickPos.x - parentsPositon.x);
							auto newPositionY = (clickPos.y - parentsPositon.y);
							auto newPositionZ = (0 - parentsPositon.z);

							childsPositionTransform->SetPosition(DirectX::XMFLOAT3(newPositionX, newPositionY, newPositionZ));
						}
						else
						{
							transform->SetPosition(DirectX::XMFLOAT3(clickPos.x, clickPos.y, currentPosition.z));
						}
					}
				}
			}

			void CliEntityManager::ToggleGridLock()
			{
				_gridLock = !_gridLock;
			}

			Entity^ CliEntityManager::AddSpriteComponent(System::String^ path)
			{
				auto selectedEntity = _manager->GetSelectedEntity();
				auto cppPath = msclr::interop::marshal_as<std::string>(path);

			
				auto textureData = _assestManager->CreateTexture(cppPath);
				auto meshData = _assestManager->CreateSpriteMesh(64, 64);
				selectedEntity->addComponent<component::SpriteRenderer>(meshData, textureData, 64);

				auto returnEntity = gcnew Entity();
				returnEntity->entity = selectedEntity;

				return returnEntity;
			}

			void CliEntityManager::ChangeGameObjectSprite(System::String^ path)
			{
				auto selectedEntity = _manager->GetSelectedEntity();
				auto cppPath = msclr::interop::marshal_as<std::string>(path);

				auto textureData = _assestManager->CreateTexture(cppPath);
				selectedEntity->getComponent<component::SpriteRenderer>()->SetSprite(textureData);
			}

			List<Entity^>^ CliEntityManager::GetEntities()
			{
				List<Entity^>^ list = gcnew List<Entity^>();

				for each (entity::Entity* entity in _manager->entities)
				{
					if (entity->hasComponent<component::Transform>())
					{
						if (entity->getComponent<component::Transform>()->_parentTransform == nullptr)
						{
							Entity^ e = gcnew Entity();
							e->name = msclr::interop::marshal_as<System::String^>(entity->GetName());
							e->children = FindChildren(entity->getComponent<component::Transform>(), e);
							e->entity = entity;
							list->Add(e);
						}
					}
				}

				return list;
			}

			void CliEntityManager::SetEntities(List<Entity^>^ sceneEntities)
			{
				for each (Entity^ sceneEntity in sceneEntities)
				{
					StoreDataToEntity(sceneEntity);
					if(sceneEntity->children->Count > 0) SetEntities(sceneEntity->children);
				}
			}

			void CliEntityManager::StoreDataToEntity(Entity^ sceneEntity)
			{
				entity::Entity* entity = sceneEntity->entity;

				if (sceneEntity->parent != nullptr)
				{
					std::vector<component::Transform*> children = sceneEntity->parent->entity->getComponent<component::Transform>()->_children;
					if (!(std::find(children.begin(), children.end(), sceneEntity->entity->getComponent<component::Transform>()) != children.end()))
					{
						auto parentsPosition = sceneEntity->parent->entity->getComponent<component::Transform>()->GetPosition();
						auto childsPositionTransform = entity->getComponent<component::Transform>();

						auto newPositionX = childsPositionTransform->GetPosition().x - parentsPosition.x;
						auto newPositionY = childsPositionTransform->GetPosition().y - parentsPosition.y;
						auto newPositionZ = childsPositionTransform->GetPosition().z - parentsPosition.z;

						childsPositionTransform->SetParent(sceneEntity->parent->entity);
						childsPositionTransform->SetPosition(DirectX::XMFLOAT3(newPositionX, newPositionY, newPositionZ));
					}
				}
				else if (entity->getComponent<component::Transform>()->_parentTransform)
				{
					auto transformComponent = entity->getComponent<component::Transform>();

					transformComponent->SetPosition(transformComponent->GetWorldPosition());
					transformComponent->RemoveParent();
				}
			}

			std::vector<component::Transform*> CliEntityManager::GetChildren(List<Entity^>^ children)
			{
				std::vector<component::Transform*> convertedChildren;

				for each(Entity^ child in children)
				{
					convertedChildren.push_back(child->entity->getComponent<component::Transform>());
				}

				return convertedChildren;
			}

			List<Entity^>^ CliEntityManager::FindChildren(component::Transform* entityTransform, Entity^ sceneEntity)
			{
				List<Entity^>^ children = gcnew List<Entity^>();

				for each(component::Transform* transform in entityTransform->_children)
				{
					Entity^ e = gcnew Entity();
					e->name = marshal_as<System::String^>(transform->Entity->GetName());
					e->children = FindChildren(transform, e);
					e->parent = sceneEntity;
					e->entity = transform->Entity;
					children->Add(e);
				}

				return children;
			}

			List<System::String^>^ CliEntityManager::GetAllComponents()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return gcnew List<System::String^>();

				auto ComponentList = gcnew List<System::String^>();

				if (selectedEntity->hasComponent<component::Animation>())
					ComponentList->Add("AnimationComponent");
				if (selectedEntity->hasComponent<component::SpriteRenderer>())
					ComponentList->Add("SpriteRendererComponent");
				if (selectedEntity->hasComponent<component::Transform>())
					ComponentList->Add("TransformComponent");
				if (selectedEntity->hasComponent<component::CircleCollider>())
					ComponentList->Add("CircleColliderComponent");
				if (selectedEntity->hasComponent<component::RectCollider>())
					ComponentList->Add("RectColliderComponent");
				if (selectedEntity->hasComponent<component::Script>())
					ComponentList->Add("ScriptComponent");
				if (selectedEntity->hasComponent<component::SoundEmitter>())
					ComponentList->Add("SoundEmitterComponent");
				if (selectedEntity->hasComponent<component::Particle>())
					ComponentList->Add("ParticleComponent");
				if (selectedEntity->hasComponent<component::TilingTool>())
					ComponentList->Add("TilingToolComponent");

				return ComponentList;
			}

			//Animation Components
			void CliEntityManager::AddAnimationComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (!selectedEntity->hasComponent<component::Animation>())
					selectedEntity->addComponent<component::Animation>(15, 0.5f);
			}
			void CliEntityManager::setAniationSpeed(float speed)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Animation>())
				{
					auto animationComponent = selectedEntity->getComponent<component::Animation>();
					animationComponent->SetSpeed(speed);
				}
			}
			void CliEntityManager::setAnimationFps(float fps)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Animation>())
				{
					auto animationComponent = selectedEntity->getComponent<component::Animation>();
					animationComponent->SetFps(fps);
				}
			}
			float CliEntityManager::getAnimationSpeed()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Animation>())
				{
					auto animationComponent = selectedEntity->getComponent<component::Animation>();
					return animationComponent->GetSpeed();
				}
			}
			float CliEntityManager::getAnimationFps()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Animation>())
				{
					auto animationComponent = selectedEntity->getComponent<component::Animation>();
					return animationComponent->GetFps();
				}
			}
			void CliEntityManager::RemoveAnimationComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Animation>())
					selectedEntity->removeComponent<component::Animation>();
				
			}

			//Script Components
			void CliEntityManager::AddScriptComponent(System::String^ path, System::String^ filename)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				auto cppPath = msclr::interop::marshal_as<std::string>(path);
				auto cppFilename = msclr::interop::marshal_as<std::string>(filename);

				if (!selectedEntity->hasComponent<component::Script>())
					selectedEntity->addComponent<component::Script>(cppPath, cppFilename);
			}
			System::String^ CliEntityManager::GetScriptName()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return "";

				if (selectedEntity->hasComponent<component::Script>())
				{
					auto script = selectedEntity->getComponent<component::Script>();
					auto cSharpPath = msclr::interop::marshal_as<System::String^>(script->GetFilename());
					return cSharpPath;
				}
			}

			void CliEntityManager::RemoveScriptComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Script>())
					selectedEntity->removeComponent<component::Script>();
			}

			//Cricle Component
			void CliEntityManager::AddCircleCollider(float radius)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (!selectedEntity->hasComponent<component::CircleCollider>())
				{
					auto debugRendermesh = _assestManager->CreateCircleMesh(1.0f);
					selectedEntity->addComponent<component::CircleCollider>(1.0f, debugRendermesh);
				}

			}
			void CliEntityManager::SetCircleColliderRadius(float radius)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::CircleCollider>())
				{
					auto circleColliderComponent = selectedEntity->getComponent<component::CircleCollider>();
					circleColliderComponent->SetRadius(radius);
				}
				
			}
			float CliEntityManager::getCircleColliderRadius()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::CircleCollider>())
				{
					auto circleColliderComponent = selectedEntity->getComponent<component::CircleCollider>();
					return circleColliderComponent->GetRadius();
				}

			}

			float CliEntityManager::GetCircleColliderOffsetX()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::CircleCollider>())
					return _manager->GetSelectedEntity()->getComponent<component::CircleCollider>()->GetOffset().X;

				return .0f;
			}

			float CliEntityManager::GetCircleColliderOffsetY()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::CircleCollider>())
					return _manager->GetSelectedEntity()->getComponent<component::CircleCollider>()->GetOffset().Y;

				return .0f;
			}

			void CliEntityManager::SetCircleColliderOffsetX(float x)
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::CircleCollider>())
					_manager->GetSelectedEntity()->getComponent<component::CircleCollider>()->SetOffset(Vector2D(x, _manager->GetSelectedEntity()->getComponent<component::CircleCollider>()->GetOffset().Y));
			}

			void CliEntityManager::SetCircleColliderOffsetY(float y)
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::CircleCollider>())
					_manager->GetSelectedEntity()->getComponent<component::CircleCollider>()->SetOffset(Vector2D(_manager->GetSelectedEntity()->getComponent<component::CircleCollider>()->GetOffset().X, y));
			}


			void CliEntityManager::CircleSetIsTrigger(bool it)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (!selectedEntity)
					return;

				if (selectedEntity->hasComponent<component::CircleCollider>())
					selectedEntity->getComponent<component::CircleCollider>()->SetTrigger(it);
			}

			bool CliEntityManager::CircleIsTrigger()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (!selectedEntity)
					return false;

				if (selectedEntity->hasComponent<component::CircleCollider>())
					return selectedEntity->getComponent<component::CircleCollider>()->IsTrigger();

				return false;
			}


			void CliEntityManager::RemoveCircleCollider()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::CircleCollider>())
					selectedEntity->removeComponent<component::CircleCollider>();
			}

			//rect collider Component
			void CliEntityManager::AddRectCollider(float width, float height)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;
				if (!selectedEntity->hasComponent<component::RectCollider>())
				{
					auto drawData = _assestManager->CreateQuadMesh(width, height);

					selectedEntity->addComponent<component::RectCollider>(width, height, new asset::QuadMesh(drawData));
				}
				
			}
			void CliEntityManager::SetRectColliderWidth(float width)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::RectCollider>())
				{
					auto rectColliderComponent = selectedEntity->getComponent<component::RectCollider>();
					rectColliderComponent->SetWidth(width);
				}
			}
			void CliEntityManager::SetRectColliderHeight(float height)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;


				if (selectedEntity->hasComponent<component::RectCollider>())
				{
					auto rectColliderComponent = selectedEntity->getComponent<component::RectCollider>();
					rectColliderComponent->SetHeight(height);
				}
			}
			float CliEntityManager::GetRectColliderWidth()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::RectCollider>())
				{
					auto rectColliderComponent = selectedEntity->getComponent<component::RectCollider>();
					return rectColliderComponent->GetWidth();
				}
			}
			float CliEntityManager::GetRectColliderHeight()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::RectCollider>())
				{
					auto rectColliderComponent = selectedEntity->getComponent<component::RectCollider>();
					return rectColliderComponent->GetHeight();
				}
			}

			float CliEntityManager::GetRectColliderOffsetX()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::RectCollider>())
					return _manager->GetSelectedEntity()->getComponent<component::RectCollider>()->GetOffset().X;

				return 0.0f;
			}

			float CliEntityManager::GetRectColliderOffsetY()
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::RectCollider>())
					return _manager->GetSelectedEntity()->getComponent<component::RectCollider>()->GetOffset().Y;

				return .0f;
			}

			void CliEntityManager::SetRectColliderOffsetX(float x)
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::RectCollider>())
					_manager->GetSelectedEntity()->getComponent<component::RectCollider>()->SetOffset(Vector2D(x, _manager->GetSelectedEntity()->getComponent<component::RectCollider>()->GetOffset().Y));
			}

			void CliEntityManager::SetRectColliderOffsetY(float y)
			{
				if (_manager->GetSelectedEntity() && _manager->GetSelectedEntity()->hasComponent<component::RectCollider>())
					_manager->GetSelectedEntity()->getComponent<component::RectCollider>()->SetOffset(Vector2D(_manager->GetSelectedEntity()->getComponent<component::RectCollider>()->GetOffset().X, y));
			}


			void CliEntityManager::RectSetIsTrigger(bool it)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (!selectedEntity)
					return;

				if (selectedEntity->hasComponent<component::RectCollider>())				
					selectedEntity->getComponent<component::RectCollider>()->SetTrigger(it);				
			}

			bool CliEntityManager::RectIsTrigger()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (!selectedEntity)
					return false;

				if (selectedEntity->hasComponent<component::RectCollider>())
					return selectedEntity->getComponent<component::RectCollider>()->IsTrigger();

				return false;
			}


			void CliEntityManager::RemoveRectCollider()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::RectCollider>())
					selectedEntity->removeComponent<component::RectCollider>();
			}

			//Sound Emitter Component
			void CliEntityManager::AddSoundEmitterComponent(System::String^ filename)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (!selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto cppFilename = msclr::interop::marshal_as<std::string>(filename);

					asset::Audio audio;
					audio.FileName		= cppFilename;
					audio.PlayOnLoad	= false;
					audio.Looping		= false;
					audio.volume		= 1.0f;
					audio.pitch			= 1.0f;

					selectedEntity->addComponent<component::SoundEmitter>(audio, nullptr);
				}
			}
			void CliEntityManager::SetSoundEmitterLooping(bool isLooping)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					soundEmitterComponent->GetAudio().Looping = isLooping;
				}
			}
			void CliEntityManager::SetSoundEmitterFileName(System::String^ filename) {
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;
				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					soundEmitterComponent->GetAudio().FileName = msclr::interop::marshal_as<std::string>(filename);
				}

			}
			void CliEntityManager::SetSoundEmitterPlayOnLoad(bool playOnLoad)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					soundEmitterComponent->GetAudio().PlayOnLoad = playOnLoad;
				}
			}
			void CliEntityManager::SetSoundEmitterVolume(float volume)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					soundEmitterComponent->GetAudio().volume = volume;
				}
			}
			void CliEntityManager::SetSoundEmitterPitch(float pitch)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					soundEmitterComponent->GetAudio().pitch = pitch;
				}
			}
			bool CliEntityManager::GetSoundEmitterLooping()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return false;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					return soundEmitterComponent->GetAudio().Looping;
				}
			}
			System::String^ CliEntityManager::GetSoundEmitterFileName()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return "Not Found";

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					return marshal_as<System::String^>(soundEmitterComponent->GetAudio().FileName);
				}
			}
			bool CliEntityManager::GetSoundEmitterPlayOnLoad()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return false;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					return soundEmitterComponent->GetAudio().PlayOnLoad;
				}
			}
			float CliEntityManager::GetSoundEmitterVolume()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					return soundEmitterComponent->GetAudio().volume;
				}
			}
			float CliEntityManager::GetSoundEmitterPitch()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
				{
					auto soundEmitterComponent = selectedEntity->getComponent<component::SoundEmitter>();
					return soundEmitterComponent->GetAudio().pitch;
				}
			}
			void CliEntityManager::RemoveSoundEmitterComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::SoundEmitter>())
					selectedEntity->removeComponent<component::SoundEmitter>();
			
			}

			//particle
			void CliEntityManager::AddParticleComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (!selectedEntity->hasComponent<component::Particle>())
					selectedEntity->addComponent<component::Particle>(nullptr);
			}
			float CliEntityManager::GetParticleMass()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Particle>())
				{
					auto particleComponent = selectedEntity->getComponent<component::Particle>();
					return particleComponent->GetMass();
				}
			}
			void CliEntityManager::SetParticleMass(float mass)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;
				if (selectedEntity->hasComponent<component::Particle>())
				{
					auto particleComponent = selectedEntity->getComponent<component::Particle>();
					particleComponent->SetMass(mass);
				}
			}

			void CliEntityManager::RemoveParticleComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Particle>())
					selectedEntity->removeComponent<component::Particle>();
			}

			//Transform
			void  CliEntityManager::SetTransformLayer(int layer)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					transformComponent->SetLayer((float)layer);
				}

			}
			void  CliEntityManager::SetTransformRotationX(float x)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					transformComponent->SetRotationX(x);
				}

			}
			void  CliEntityManager::SetTransformRotationY(float y)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					transformComponent->SetRotationY(y);
				}

			}
			void  CliEntityManager::SetTransFormRotationZ(float z)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					transformComponent->SetRotationZ(z);
				}
			}
			int  CliEntityManager::GetTransformLAyer()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->GetLayer();
				}
			}
			float CliEntityManager::GetTransformRotationX()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->getRoationX();
				}
			}
			float  CliEntityManager::GetTransformRotationY()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->getRoationY();
				}
			}
			float  CliEntityManager::GetTransformRotationZ()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->getRoationZ();
				}
			}

			//Scale
			void  CliEntityManager::SetTransformScaleX(float x)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					auto scale = transformComponent->GetScale();
					transformComponent->SetScale(DirectX::XMFLOAT3( x, scale.y, scale.z));
				}

			}
			void  CliEntityManager::SetTransformScaleY(float y)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					auto scale = transformComponent->GetScale();
					transformComponent->SetScale(DirectX::XMFLOAT3(scale.x, y, scale.z));
				}

			}
			void  CliEntityManager::SetTransFormScaleZ(float z)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					auto scale = transformComponent->GetScale();
					transformComponent->SetScale(DirectX::XMFLOAT3(scale.x, scale.y, z));
				}
			}

			float CliEntityManager::GetTransformScaleX()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->GetScale().x;
				}
			}
			float  CliEntityManager::GetTransformScaleY()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->GetScale().y;
				}
			}
			float  CliEntityManager::GetTransformScaleZ()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::Transform>())
				{
					auto transformComponent = selectedEntity->getComponent<component::Transform>();
					return transformComponent->GetScale().z;
				}
			}

			//Tiling Tool
			void CliEntityManager::AddTilingToolComponent() 
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (!selectedEntity->hasComponent<component::TilingTool>())
					selectedEntity->addComponent<component::TilingTool>();
			}
			void CliEntityManager::SetTilingToolWidth(int x)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::TilingTool>())
				{
					auto tilingComponent = selectedEntity->getComponent<component::TilingTool>();
					tilingComponent->SetWidth(x);
				}
			}
			void CliEntityManager::SetTilingToolHeight(int y)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::TilingTool>())
				{
					auto tilingComponent = selectedEntity->getComponent<component::TilingTool>();
					tilingComponent->SetHeight(y);
				}
			}
			int CliEntityManager::GetTilingToolWidth()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::TilingTool>())
				{
					auto tilingComponent = selectedEntity->getComponent<component::TilingTool>();
					return tilingComponent->GetWidth();
				}
			}
			int CliEntityManager::GetTilingToolHeight()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::TilingTool>())
				{
					auto tilingComponent = selectedEntity->getComponent<component::TilingTool>();
					return tilingComponent->GetHeight();
				}
			}
			void CliEntityManager::SetTilingToolOverride(bool value)
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::TilingTool>())
				{
					auto tilingComponent = selectedEntity->getComponent<component::TilingTool>();
					tilingComponent->SetOverride(value);
				}
			}
			bool CliEntityManager::GetTilingToolOverride()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				if (selectedEntity->hasComponent<component::TilingTool>())
				{
					auto tilingComponent = selectedEntity->getComponent<component::TilingTool>();
					return tilingComponent->GetOverride();
				}
			}

			void CliEntityManager::RemoveTilingToolComponent()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return;

				if (selectedEntity->hasComponent<component::TilingTool>())
					selectedEntity->removeComponent<component::TilingTool>();
			}

			float CliEntityManager::GetSpriteWidth()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				return selectedEntity->getComponent<component::SpriteRenderer>()->Width;
			}

			float CliEntityManager::GetSpriteHeight()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				return selectedEntity->getComponent<component::SpriteRenderer>()->Height;
			}

			float CliEntityManager::GetSpriteSize()
			{
				auto selectedEntity = _manager->GetSelectedEntity();

				if (selectedEntity == nullptr)
					return 0;

				return selectedEntity->getComponent<component::SpriteRenderer>()->SpriteSize;
			}

			//Camera
			void CliEntityManager::SetCameraPosition(float x, float y)
			{
				_graphics->GetMainCamera()->SetPosition(x, y);
			}

			float CliEntityManager::GetCameraPositionX()
			{
				return _graphics->GetMainCamera()->GetPositionX();
				//return 0;
			}

			float CliEntityManager::GetCameraPositionY()
			{
				return _graphics->GetMainCamera()->GetPositionY();
				//return 0;
			}

			void CliEntityManager::CameraZoomIn(float speed)
			{
				_graphics->GetMainCamera()->ZoomIn(speed);
			}

			void CliEntityManager::CameraZoomOut(float speed)
			{
				_graphics->GetMainCamera()->ZoomOut(speed);
			}

			void CliEntityManager::CameraResetZoom()
			{
				_graphics->GetMainCamera()->ResetZoom();
			}
		}
	}
}
