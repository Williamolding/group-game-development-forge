#pragma once
#include <asset/AssetManager.h>
namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			ref class CliAssetManager
			{
				forge::asset::AssetManager* _assetManager;
			public:
				CliAssetManager(forge::asset::AssetManager* assetManager);
			};
		}
	}
}

