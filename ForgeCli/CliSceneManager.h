#pragma once
#include <SceneManager.h>
#include <system.h>
#include <msclr\marshal_cppstd.h>

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			public ref class CliSceneManager
			{
			private:
				forge::game::SceneManager* _manager;
			public:
				CliSceneManager(forge::game::SceneManager* sceneManager);
				forge::game::SceneManager* GetManager();

				void LoadScene(System::String^ sceneName, System::String^ scenePath);
				void SaveScene(System::String^ sceneName, System::String^ scenePath);
			};
		}
	}
}

