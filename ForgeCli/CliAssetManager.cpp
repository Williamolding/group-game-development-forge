#include "CliAssetManager.h"

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			CliAssetManager::CliAssetManager(forge::asset::AssetManager* assetManager)
			{
				_assetManager = assetManager;
			}
		}
	}
}
