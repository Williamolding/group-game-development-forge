#include "CliSceneManager.h"

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			CliSceneManager::CliSceneManager(forge::game::SceneManager* sceneManager) : _manager(sceneManager)
			{
			
			}

			forge::game::SceneManager* CliSceneManager::GetManager()
			{
				return _manager;
			}

			void CliSceneManager::LoadScene(System::String^ sceneName, System::String^ scenePath)
			{
				auto name = msclr::interop::marshal_as<std::string>(sceneName);
				auto path = msclr::interop::marshal_as<std::string>(scenePath);

				_manager->LoadSceneFromFile(name, path);
			}

			void CliSceneManager::SaveScene(System::String^ sceneName, System::String^ scenePath)
			{
				auto name = msclr::interop::marshal_as<std::string>(sceneName);
				auto path = msclr::interop::marshal_as<std::string>(scenePath);

				_manager->SaveSceneToFile(name, path);
			}

		}
	}
}
