#pragma once
#include <Game.h>
#include <system.h>
#include "CliEntityManager.h"
#include "CliSceneManager.h"

using namespace System::Collections::Generic;

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			public ref class GameInstance
			{
			private:
				forge::graphics::Dx11Driver* _driver;
				forge::game::IGameInstance* _reference;

				CliEntityManager^  _entityManager;
				CliSceneManager^   _sceneManager;
			public:
				GameInstance(intptr_t handle, UINT32 width, UINT32 height, bool fullscreen, System::String^ directory, bool isEditor);
				~GameInstance();
				!GameInstance();
				bool Run();
				void Destory();
				void Stop();
				void Pause();
				void Resume();
				void Resize(int x, int y);

				CliEntityManager^ GetEntityManager();
				CliSceneManager^  GetSceneManager();
			};
		}
	}
}

