#include "GameInstance.h"
#include "FmodSystem.h"
#include <entity/EditorEntityManager.h>
#include <WindowProfile.h>
#include <Windows.h>
#include <msclr\marshal_cppstd.h>

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			GameInstance::GameInstance(intptr_t handle, UINT32 width, UINT32 height, bool fullscreen, System::String^ directory, bool isEditor)
			{
				auto windowProfile	= forge::sys::WindowProfile((HWND)handle, width,height,fullscreen);
				auto path			= msclr::interop::marshal_as<std::string>(directory);

				_driver = new forge::graphics::Dx11Driver(windowProfile);
				
				if (isEditor)
				{
					_reference = new forge::game::Game<forge::entity::EditorEntityManager>(_driver);
					_reference->Initalise(path,true);
				}
				else
				{
					_reference = new forge::game::Game<forge::entity::EntityManager>(_driver);
					_reference->Initalise(path,false);
				}
		
				auto wrapperPackage = _reference->ExportWrapperPackage();

				_entityManager	 = gcnew CliEntityManager(wrapperPackage.EntityManager,wrapperPackage.AssetManager, wrapperPackage.Graphics);
				_sceneManager	 = gcnew CliSceneManager(wrapperPackage.SceneManager);
			}

			void GameInstance::Pause()
			{
				return _reference->Pause();
			}
			void GameInstance::Resume()
			{
				_reference->Resume();
			}

			bool GameInstance::Run()
			{
				return _reference->Run();
			}

			void GameInstance::Stop()
			{
				_reference->Stop();
			}

			void GameInstance::Resize(int x, int y)
			{
				_reference->Resize(x, y);
			}

			CliEntityManager^ GameInstance::GetEntityManager()
			{
				return _entityManager;
			}
			CliSceneManager^  GameInstance::GetSceneManager()
			{
				return _sceneManager;
			}

			GameInstance::~GameInstance()
			{
				Destory();
			}

			GameInstance::!GameInstance()
			{
				Destory();
			}

			void GameInstance::Destory()
			{
				if (_reference != nullptr)
				{
					delete _reference;
					_reference = nullptr;
				}

				if (_driver != nullptr)
				{
					delete _driver;
					_driver = nullptr;
				}
			}
		}
	}
}
