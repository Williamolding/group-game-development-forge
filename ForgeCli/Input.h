#pragma once
#include <input/Keyboard.h>
#include <system.h>
#include <msclr\marshal_cppstd.h>

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			public enum class CollectionTypes
			{
				Key,
				Button,
				GamePad,
			};

			public ref class InputCollection
			{
			public:
				System::String^ collectionName;
				System::Collections::Generic::List<int>^ Keys;
				System::Collections::Generic::List<int>^ Button;
				System::Collections::Generic::List<int>^ GamePadButton;
			};

			public ref class Input
			{
			public:
				static void SetKeyfalse(unsigned short index);
				static void SetKeytrue(unsigned short index);
				static void SetButtonfalse(unsigned short index);
				static void SetButtontrue(unsigned short index);
				static bool IsButtonDown(unsigned short index);
				static bool IsButtonUp(unsigned short index);
				static bool IsKeyDown(unsigned short index);
				static bool IsKeyUp(unsigned short index);
				static int GetMouseX();
				static int GetMouseY();
				static void SetMouseX(int x);
				static void SetMouseY(int y);

				static void ClearCollections();
				
				static bool RegisterInputCollection(System::String^ collectionName);
				static bool RegisterKeyboardInput(System::String^ collectionName, unsigned short key);
				static bool RegisterMouseInput(System::String^ collectionName, unsigned short button);
				static bool RegisterControllerInput(System::String^ collectionName, unsigned short gamePadButton);
				static System::Collections::Generic::List<InputCollection^>^ GetCollectionState();
			};
		}
	}
}
