#include <entity/IEntityManager.h>
#include <msclr\marshal_cppstd.h>
#include <asset/AssetManager.h>
#include <graphics/Graphics.h>


using namespace System::Collections::Generic;
using namespace msclr::interop;

namespace forge
{
	namespace exprt
	{
		namespace cli
		{
			public ref struct Entity
			{
				System::String^ name;
				List<Entity^>^ children;
				Entity^ parent;
				forge::entity::Entity* entity;
			};

			public ref class CliEntityManager
			{
			private:
				entity::IEntityManager* _manager;
				asset::AssetManager* _assestManager;
				graphics::Graphics* _graphics;

				bool _gridLock;

				const int ROUNDMULTIPLE = 16;
			public:
				CliEntityManager(entity::IEntityManager* manager, asset::AssetManager* assetManager, graphics::Graphics* graphics);
				entity::IEntityManager* GetManager();

				Entity^ CreateEntity();
				Entity^ AddSpriteComponent(System::String^ path);
				void ChangeGameObjectSprite(System::String^ path);

				void DeleteSelectedEntity(System::String^ entityName);
				void SetSelectedEntityPosition(int x, int y);
				void UpdateEntityPosition();
				void ToggleGridLock();
				System::String^ FindEntityByMousePosition();
				void FindEntityByName(System::String^ name);
				void SetEntityName(System::String ^name);
				System::String^ GetEntityName();

				List<System::String^>^ GetAllComponents();

				//AnimationComponent
				void AddAnimationComponent();
				void setAniationSpeed(float speed);
				void setAnimationFps(float fps);
				float getAnimationSpeed();
				float getAnimationFps();
				void RemoveAnimationComponent();

				//scriptComponent
				void AddScriptComponent(System::String^ path, System::String^ filename);
				System::String^ GetScriptName();
				void RemoveScriptComponent();

				//CircleColliderComponenet
				void AddCircleCollider(float radius);
				void SetCircleColliderRadius(float radius);
				float getCircleColliderRadius();
				float GetCircleColliderOffsetX();
				float GetCircleColliderOffsetY();
				void SetCircleColliderOffsetX(float x);
				void SetCircleColliderOffsetY(float y);
				void CircleSetIsTrigger(bool it);
				bool CircleIsTrigger();
				void RemoveCircleCollider();

				//RectColliderComponenet
				void AddRectCollider(float width, float height);
				void SetRectColliderWidth(float width);
				void SetRectColliderHeight(float height);
				float GetRectColliderWidth();
				float GetRectColliderHeight();
				float GetRectColliderOffsetX();
				float GetRectColliderOffsetY();
				void SetRectColliderOffsetX(float x);
				void SetRectColliderOffsetY(float y);
				void RectSetIsTrigger(bool it);
				bool RectIsTrigger();
				void RemoveRectCollider();

				//Sound 
				void AddSoundEmitterComponent(System::String^ filename);
				void SetSoundEmitterLooping(bool isLooping);
				void SetSoundEmitterPlayOnLoad(bool playOnLoad);
				void SetSoundEmitterVolume(float volume);
				void SetSoundEmitterPitch(float pitch);
				void SetSoundEmitterFileName(System::String^ filename);
				bool GetSoundEmitterLooping();
				bool GetSoundEmitterPlayOnLoad();
				float GetSoundEmitterVolume();
				System::String^ GetSoundEmitterFileName();
				float GetSoundEmitterPitch();
				void RemoveSoundEmitterComponent();

				// Particle 
				void AddParticleComponent();
				float GetParticleMass();
				void SetParticleMass(float mass);
				void RemoveParticleComponent();
				void SetTerminalVelocity(bool tv);
				bool IsTerminalVelocityOn();
				void SetBeingPushed(bool bp);
				bool BeingPushed();
				void AddPushForce();
				void RemoveLaminarForce();
				void RemovePushForce();
				void SetCoefficientOfRestitution(float cor);
				float GetCoefficientOfRestitution();

				// Particle => Forces
				void SetGravity(float g);
				float GetGravity();
				void SetFrictionCof(float cof);
				float GetFrictionCof();
				void SetFrictionNormal(float normal);
				float GetFrictionNormal();
				void SetFrictionMultiplier(float multiplier);
				float GetFrictionMultiplier();
				void SetPushDrag(float dragCoefficient);
				void SetPushingForce(float force);
				float GetPushForceDrag();
				float GetPushingForce();

				// Camera
				void SetCameraPosition(float x, float y);
				float GetCameraPositionX();
				float GetCameraPositionY();

				//Transform
				void SetTransformLayer(int layer);
				void SetTransformRotationX(float x);
				void SetTransformRotationY(float y);
				void SetTransFormRotationZ(float z);
				int GetTransformLAyer();
				float GetTransformRotationX();
				float GetTransformRotationY();
				float GetTransformRotationZ();

				void CameraZoomIn(float speed);
				void CameraZoomOut(float speed);
				void CameraResetZoom();

				void SetTransformScaleX(float x);
				void SetTransformScaleY(float y);
				void SetTransFormScaleZ(float z);
				float GetTransformScaleX();
				float GetTransformScaleY();
				float GetTransformScaleZ();

				void AddTilingToolComponent();
				void SetTilingToolWidth(int x);
				void SetTilingToolHeight(int y);
				int GetTilingToolWidth();
				int GetTilingToolHeight();
				void SetTilingToolOverride(bool value);
				bool GetTilingToolOverride();
				void RemoveTilingToolComponent();

				float GetSpriteWidth();
				float GetSpriteHeight();
				float GetSpriteSize();

	
				List<Entity^>^ GetEntities();
				void SetEntities(List<Entity^>^ sceneEntities);
				void StoreDataToEntity(Entity^ sceneEntity);
				std::vector<component::Transform*>  GetChildren(List<Entity^>^ children);
			private:
				List<Entity^>^ FindChildren(component::Transform* entityTransform, Entity^ cliEntity);
			};
		}
	}
}