function onCollision()

	entity			= Entity.EntityManager:Find(Entity.Me);
	soundEmitter 	= entity:SoundEmitter();
	
	if soundEmitter:isPlaying() == false then
		soundEmitter:Play();
	end
	
end

function update(deltatime)

	entity 			= Entity.EntityManager:Find(Entity.Me);
	transform 		= entity:Transform();
	--soundEmitter 	= entity:SoundEmitter();
	position 		= transform:GetPosition();
	speed			= 150;
	particle        = entity:Particle();
    
    if Input.Collection.KeyDown("Right") then
        particle:LuaAddForce(500.0, 0.0);
	end
		
	if Input.Collection.KeyPressed("Up") then
		if transform:GetPosition().Y < -100.0 then
			particle:LuaAddForce(0.0, 15000.0);
		end
	end
	
	if Input.Collection.KeyDown("Down") then
	end
		
	if Input.Collection.KeyDown("Left") then
        particle:LuaAddForce(-500.0,0.0); 
	end
	
	if Input.KeyPressed("P") then
		soundEmitter:Play();
	end
	
	if Input.KeyPressed("Q") then
		print("Key Pressed");
	end
	
	transform:SetPosition(position);
end


