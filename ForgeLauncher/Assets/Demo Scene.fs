<Scene SceneName="./Projects\Demo Project//Assets//Demo Scene.fs">
    <Entity name="Floor0">
        <TransformComponent>
            <Position x="-300" y="-300" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor1">
        <TransformComponent>
            <Position x="-236" y="-300" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor2">
        <TransformComponent>
            <Position x="-172" y="-300" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor3">
        <TransformComponent>
            <Position x="-108" y="-300" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor4">
        <TransformComponent>
            <Position x="-44" y="-300" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor5">
        <TransformComponent>
            <Position x="20" y="-300" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor6">
        <TransformComponent>
            <Position x="84" y="-100" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor7">
        <TransformComponent>
            <Position x="148" y="-100" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor8">
        <TransformComponent>
            <Position x="212" y="-100" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Floor9">
        <TransformComponent>
            <Position x="276" y="-100" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="16" height="16" path="FloorTile.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <RectangleColliderComponent width="32" height="32"/>
        <ParticleComponent mass="-1"/>
    </Entity>
    <Entity name="Mr Naked">
        <TransformComponent>
            <Position x="-100" y="250" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="32" height="32" path="Naked.tga"/>
            <Mesh width="32" height="32"/>
        </SpriteRendererComponent>
        <CircleColliderComponent radius="10.666667"/>
        <RectangleColliderComponent width="16" height="16"/>
        <ParticleComponent mass="0.5"/>
    </Entity>
    <Entity name="Player 1">
        <TransformComponent>
            <Position x="76.999992" y="-15.49999" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="128" height="32" path="ArcherAnim.tga"/>
            <Mesh width="32" height="32"/>
        </SpriteRendererComponent>
        <AnimationComponent speed="1.5" fps="25"/>
        <ScriptComponent path="./Projects\Demo Project//Assets//script.lua" filename="script.lua"/>
        <SoundEmitterComponent>
            <Sound looping="false" path="hurt.wav" playOnLoad="true" pitch="1" volume="0.1"/>
        </SoundEmitterComponent>
        <CircleColliderComponent radius="1"/>
        <RectangleColliderComponent width="16" height="16"/>
        <ParticleComponent mass="1"/>
    </Entity>
    <Entity name="Object 5">
        <SoundEmitterComponent>
            <Sound looping="true" path="music.mp3" playOnLoad="true" pitch="1" volume="0.1"/>
        </SoundEmitterComponent>
    </Entity>
    <Entity name="Entity">
        <TransformComponent>
            <Position x="-193.00002" y="-107.50002" z="0"/>
            <Scale x="1" y="1" z="1"/>
            <Rotation x="0" y="0" z="0"/>
        </TransformComponent>
        <SpriteRendererComponent>
            <Colour r="1" g="1" b="1"/>
            <Texture width="128" height="32" path="SnakeSpriteSheet.tga"/>
            <Mesh width="64" height="64"/>
        </SpriteRendererComponent>
        <AnimationComponent speed="0.5" fps="15"/>
        <ScriptComponent path="./Projects\Demo Project\Assets\script.lua" filename="script.lua"/>
        <RectangleColliderComponent width="16" height="16"/>
        <ParticleComponent mass="1"/>
    </Entity>
</Scene>
