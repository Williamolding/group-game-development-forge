

function update(deltatime)

	entity 			= Entity.EntityManager:Find(Entity.Me);
	soundEmitter 	= entity:SoundEmitter();
	
	
	if Input.KeyPressed("J") then
		if soundEmitter:isPlaying() == false then
				soundEmitter:Play();
		end
	end
	
	if Input.KeyPressed("K") then
			soundEmitter:Pause();
	end
	
	if soundEmitter:isPlaying() == true then
		if Input.KeyPressed("M") then
				if soundEmitter:getVolume() < 1.0 then 
					soundEmitter:setVolume(soundEmitter:getVolume() + 0.1);
				end
		end
		if Input.KeyPressed("N") then
			if soundEmitter:getVolume() > 0.2 then 
				soundEmitter:setVolume(soundEmitter:getVolume() - 0.1);
			end
		end
		
		if Input.KeyPressed("B") then
				if soundEmitter:getPitch() < 5.0 then 
					soundEmitter:setPitch(soundEmitter:getPitch() + 0.1);
				end
		end
		if Input.KeyPressed("V") then
			if soundEmitter:getPitch() > 0.2 then 
				soundEmitter:setPitch(soundEmitter:getPitch() - 0.1);
			end
		end
	
	end	
end


