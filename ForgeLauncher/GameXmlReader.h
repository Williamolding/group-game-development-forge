#pragma once
#include <string>
#include <util/tinyXml2.h>
#include <iostream>

namespace forge
{
	namespace launcher
	{
		struct GameSettings
		{
			std::string sceneName;
			std::string scenePath;
			int width;
			int height;
		};

		class GameXmlReader
		{
		public:
			static GameSettings ReadGameSettings(std::string path)
			{
				auto gameSettings = GameSettings();

				tinyxml2::XMLDocument document;
				auto loadResult = document.LoadFile(path.c_str());

				if (loadResult != tinyxml2::XML_SUCCESS)
				{
					std::cout << "Error Occured Loading Scene : " << loadResult << std::endl;
				}

				auto rootElement = document.FirstChildElement("Game");

				gameSettings.width	= rootElement->IntAttribute("Width");
				gameSettings.height = rootElement->IntAttribute("Height");

				auto sceneElement = rootElement->FirstChildElement("Scenes");

				auto currentSceneNode = sceneElement->FirstChildElement("Scene");
				while (currentSceneNode != nullptr)
				{
					int id;
					currentSceneNode->QueryIntAttribute("id", &id);

					auto name = currentSceneNode->Attribute("name");
					auto path = currentSceneNode->Attribute("path");

					gameSettings.sceneName = name;
					gameSettings.scenePath = path;

					currentSceneNode = currentSceneNode->NextSiblingElement("Scene");
				}

				return gameSettings;
			}
		};
	}
}
