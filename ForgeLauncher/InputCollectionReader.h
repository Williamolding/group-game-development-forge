#pragma once
#include <string>
#include <util/tinyXml2.h>
#include <iostream>
#include <input/InputManager.h>

namespace forge
{
	namespace launcher
	{
		class InputCollectionReader
		{
		public:
			static void ReadInputCollection(std::string path)
			{
				tinyxml2::XMLDocument document;
				auto loadResult = document.LoadFile(path.c_str());

				if (loadResult != tinyxml2::XML_SUCCESS)
				{
					std::cout << "Error Occured Loading Scene : " << loadResult << std::endl;
					return;
				}

				auto rootElement = document.FirstChildElement("InputCollection");
				auto collectionNode = rootElement->FirstChildElement("Collections");

				auto currentCollectionNode = collectionNode->FirstChildElement("Collection");
				while (currentCollectionNode != nullptr)
				{
					auto collectionNodeName = currentCollectionNode->Attribute("name");
					input::InputManager::RegisterCollection(collectionNodeName);

					auto currentinputmethodNode = currentCollectionNode->FirstChildElement("InputMethod");
					while (currentinputmethodNode != nullptr)
					{
						auto inputMethodType = std::string(currentinputmethodNode->Attribute("type"));
						auto inputmethodvalue = currentinputmethodNode->Attribute("value");

						if(inputMethodType == "key")
						{
							auto value = std::stoi(inputmethodvalue);
							input::InputManager::RegisterKeyboardInput(collectionNodeName, static_cast<input::Keys>(value));
						}
						if (inputMethodType == "button")
						{
							auto value = std::stoi(inputmethodvalue);
							input::InputManager::RegisterMouseInput(collectionNodeName, static_cast<input::Buttons>(value));
						}
						if (inputMethodType == "gamepad")
						{
							auto value = std::stoi(inputmethodvalue);
							input::InputManager::RegisterControllerInput(collectionNodeName, static_cast<input::GamePadButton>(value));
						}

						currentinputmethodNode = currentinputmethodNode->NextSiblingElement("InputMethod");
					}

					currentCollectionNode = currentCollectionNode->NextSiblingElement("Collection");
				}
			}
		};
	}
}
