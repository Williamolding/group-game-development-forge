#include "Window.h"
#include <graphics/Dx11/Dx11Driver.h>
#include "Game.h"
#include "system.h"
#include "GameXmlReader.h"
#include "InputCollectionReader.h"

void GetDesktopResolution(int& width, int& height)
{
	RECT desktop;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &desktop);

	width	= desktop.right;
	height	= desktop.bottom;
}

int main(int argcount, char* args[]) try{

	auto title = forge::sys::WindowTitle();

	int width = 0;
	int height = 0;

	GetDesktopResolution(width, height);

	auto settings	= forge::sys::WindowSettings();
	settings.Style	= WS_OVERLAPPEDWINDOW;
	settings.Title	= title.c_str();

	auto gameSettings = forge::launcher::GameXmlReader::ReadGameSettings("Game.xml");

	settings.Width = gameSettings.width;
	settings.Height = gameSettings.height;

	auto window = new forge::sys::Window(settings);
	window->Initalise();

	auto closeCallback			= std::bind(&forge::sys::Window::IsClosed, window);
	auto processEventCallback	= std::bind(&forge::sys::Window::ProcessEvents, window);

	const auto graphicsDriver = new forge::graphics::Dx11Driver(window->GetProfile());

	auto game = new forge::game::Game<forge::entity::EntityManager>(graphicsDriver);

	//Look for specific structure
	std::string assetFolderPath = ".";

	game->Initalise(assetFolderPath, false);

	auto manager = game->ExportWrapperPackage();
	manager.SceneManager->LoadSceneFromFile(gameSettings.sceneName, gameSettings.scenePath);

	forge::launcher::InputCollectionReader::ReadInputCollection("InputCollections.xml");

	auto resizeCallback = std::bind(&forge::game::Game<forge::entity::EntityManager>::Resize, game, std::placeholders::_1, std::placeholders::_2);

	game->ClosingCallback			= closeCallback;
	game->ProcessEventsCallback		= processEventCallback;

	window->ResizeCallback = resizeCallback;

	window->Show();
	
	game->Run();
}
catch (...)
{
	std::cout << "Launcher Crashed!" << std::endl;
	std::cin.get();
}
