--counter = 0;

function onCollision()

	entity			= Entity.EntityManager:Find(Entity.Me);
	soundEmitter 	= entity:SoundEmitter();
	
	if soundEmitter:isPlaying() == false then
		soundEmitter:Play();
	end
	
end

function update(deltatime)

	entity 			= Entity.EntityManager:Find(Entity.Me);
	transform 		= entity:Transform();
	--soundEmitter 	= entity:SoundEmitter();
	position 		= transform:GetPosition();
	speed			= 150;
	
	--counter = counter + 1;
	--print(counter);
	
	if Input.Collection.KeyDown("D") then
		position.X = position.X + (3 * deltatime * speed);
	end
		
	if Input.Collection.KeyDown("W") then
		position.Y = position.Y + (3 * deltatime * speed);
	end
	
	if Input.Collection.KeyDown("S") then
		position.Y = position.Y - (3 * deltatime * speed);
	end
		
	if Input.Collection.KeyDown("A") then
		position.X = position.X - (3 * deltatime * speed);
	end
	
	if Input.KeyPressed("P") then
		soundEmitter:Play();
	end
	
	if Input.KeyPressed("Q") then
		print("Key Pressed");
	end
	
	transform:SetPosition(position);
end


