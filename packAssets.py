import os
import glob
import shutil
import base64
import sys
import base64

#fileHeader
#Filename$FileData$

def getAllFiles(path):
     return glob.glob(path+"*")

def writeFiles(files,path):
	fileCount = len(files)
	
	if not os.path.exists("./Bin/AssetPacks"):
		os.mkdir("Bin/AssetPacks");
		
	packedFile = open("Bin/AssetPacks/coreAssets.dat", "w")
	print(":> Files To Write : " + str(fileCount))
	for x in files:
		absPath = os.path.abspath(x)
		with open(absPath, "rb") as f:
			data = f.read()
			packedFile.write(os.path.basename(absPath)+"$")
			encodedData = base64.b64encode(data).decode('ascii')
			encodedData = encodedData.replace('\n', '')
			packedFile.write(encodedData+"$")
			print(":> File Packed! "+os.path.basename(absPath))        
	packedFile.close()

print(":> Packing Default Assets")
print(":> Enumerating all Files In Directory:")

fileLocation = "ForgeEngineAssets/"
print(":> " + fileLocation)
print(":> -----------------------------------")
print(":> Files Found :")
files = getAllFiles(fileLocation)

for x in files:
      print(":> "+ x)

print(":> packing files")
print(":> -----------------------------------") 
writeFiles(files,fileLocation)
     


  
    
