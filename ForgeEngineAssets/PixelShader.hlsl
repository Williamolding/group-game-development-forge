Texture2D shaderTexture;
SamplerState SampleType;


struct PixelInputType
{
	float4 position : SV_POSITION;
	float2 tex		: TEXCORD;
	float3 colour 	: COLOUR;
};


float4 main(PixelInputType input) : SV_TARGET
{
	float4 textureColor;
	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
	textureColor = shaderTexture.Sample(SampleType, input.tex);
	
	textureColor.x = input.colour.x * textureColor.x;
	textureColor.y = input.colour.y * textureColor.y;
	textureColor.z = input.colour.z * textureColor.z;
	
	return textureColor;
}
