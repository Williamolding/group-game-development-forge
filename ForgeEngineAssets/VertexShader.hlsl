//vs
cbuffer ConstantBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix orthoMatrix;
};

struct VS_Out
{
	float4 position : SV_POSITION;
	float2 tex		: TEXCORD;
	float3 colour 	: COLOUR;
};

VS_Out main(float4 position : POSITION, float2 tex : TEXCORD, float3 colour : COLOUR)
{
	VS_Out output;

	position.w = 1.0f;

	output.position = mul(position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, orthoMatrix);

	output.tex = tex;
	output.colour = colour;

	return output;
}