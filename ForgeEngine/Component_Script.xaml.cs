﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_Script.xaml
    /// </summary>
    public partial class Component_Script : UserControl
    {
        EntityManagerWrapper manager;
		UIElementCollection _container;
		bool allowStore = false;

        public Component_Script(Editor editor, UIElementCollection container,string name)
        {
            InitializeComponent();
			manager = editor.GetEntityWrapper();
			_container = container;
			//Get script name
			FileBox.Text = name;

            allowStore = true;
        }

        private void FileBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            expander.Header = FileBox.Text + " (Script)";
            //Set script name
        }

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			manager.RemoveScriptComponent();
			_container.Remove(this);
		}
	}
}
