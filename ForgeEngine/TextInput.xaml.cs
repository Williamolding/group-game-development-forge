﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for TextInput.xaml
    /// </summary>
    public partial class TextInput : Window
    {
        public string outputText = "ERROR";
        public bool outputExit = false;

        public TextInput(string description)
        {
            InitializeComponent();

            Title.Text = description;
        }

        private void SetName_Click(object sender, RoutedEventArgs e)
        {
            outputExit = false;
            Finish();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            outputExit = true;
            Finish();
        }

        void Finish()
        {
            //if (InputBox.Text != string.Empty)
            {
                outputText = InputBox.Text;
                Close();
            }
        }

        private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void InputBox_KeyDown(object sender, KeyEventArgs e)
        {
            outputExit = false;

            if (e.Key == Key.Return)
            {
                Finish();
            }
        }

        private void InputBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
