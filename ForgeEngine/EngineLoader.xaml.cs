﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Threading;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for EngineLoader.xaml
    /// </summary>
    public partial class EngineLoader : Window
	{
		public EngineLoader()
		{
			InitializeComponent();
		}

        private void LoadingBar_Loaded(object sender, RoutedEventArgs e)
        {
            Task x = Task.Run(() =>
            {
                Thread.Sleep(1000);

                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    ProjectViewer window = new ProjectViewer();
                    window.Show();
                    this.Close();
                });
            });
        }
    }
}
