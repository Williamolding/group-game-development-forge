﻿using forge.exprt.cli;
using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for WindowSlider.xaml
    /// </summary>
    public partial class WindowSlider : UserControl
    {
		public Editor Editor;
        public WindowSlider()
        {
            InitializeComponent();

		}

		public IntPtr GetHandle()
		{
			return Canvas.Handle;
		}

		public int GetHeight()
		{
			return Canvas.Height;
		}

		public int GetWidth()
		{
			return Canvas.Width;
		}

		public System.Windows.Forms.Panel GetPanel()
		{
			return Canvas;
		}

		private void Canvas_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (Editor != null)
			{
				Input.SetMouseX(e.X);
				Input.SetMouseY(e.Y);

				if (Input.IsButtonDown(0))
				{
					Editor.UpdateEntityPosition();
				}
			}
		}

		private void Canvas_Click(object sender, EventArgs e)
		{
			//if (Editor != null)
			//{
			//	Editor.SelectEntity();
			//}
		}

		private void Canvas_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (Editor != null)
			{
				Editor.SelectEntity(true);
			}

			Input.SetButtontrue(0);		
		}

		private void Canvas_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			Input.SetButtonfalse(0);
		}

        private void Canvas_Resize(object sender, EventArgs e)
        {
            if (Editor != null && Editor.GetWindow()._game != null)
            {
                Editor.GetWindow()._game.Resize(Canvas.Width, Canvas.Height);
            }
        }
    }
}
