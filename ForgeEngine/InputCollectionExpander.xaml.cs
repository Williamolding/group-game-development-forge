﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for InputCollectionExpander.xaml
    /// </summary>
    public partial class InputCollectionExpander : UserControl
    {
        List<InputControlCombo> comboBoxes = new List<InputControlCombo>();

        public InputCollectionExpander()
        {
            InitializeComponent();

            foreach (InputControlCombo box in comboBoxes)
            {
                box.SetCollectionName(expander.Header.ToString());
            }
        }
        public string HeaderText
        {
            get { return expander.Header.ToString(); }
            set {
                expander.Header = value;

                foreach(InputControlCombo box in comboBoxes)
                {
                    box.SetCollectionName(value);
                }
            }
        }

        private void AddComboBoxButton_Click(object sender, RoutedEventArgs e)
        {
            AddComboBox();
        }

        public InputControlCombo AddComboBox()
        {
            InputControlCombo newBox = new InputControlCombo();
            newBox.Click += new RoutedEventHandler(RemoveComboBox_Click);
            ComboStack.Children.Add(newBox);
            newBox.Margin = new Thickness(40, 0, 40, 10);

            comboBoxes.Add(newBox);
            return newBox;
        }

        public void RegisterInputs(string collectionName)
        {
            foreach (InputControlCombo box in comboBoxes)
            {
                box.RegisterInput(collectionName);
            }
        }

        private void RemoveCollectionButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ClickEvent));
        }

        public static readonly RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(InputCollectionExpander));

        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        private void RemoveComboBox_Click(object sender, RoutedEventArgs e)
        {
            InputControlCombo combo = sender as InputControlCombo;
            comboBoxes.Remove(combo);
            ComboStack.Children.Remove(combo);
        }

        private void RenameCollectionButton_Click(object sender, RoutedEventArgs e)
        {
            CollectionRename(Window.GetWindow(this));
        }
        public bool  CollectionRename(Window MainWindow)
        {
            TextInput inputWindow = new TextInput("Input a collection name:");
            inputWindow.Owner = Window.GetWindow(this);
            inputWindow.ShowDialog();
            if (!inputWindow.outputExit && inputWindow.outputText != string.Empty)
            {
                HeaderText = inputWindow.outputText;
                return true;
            }
            else if (inputWindow.outputText == string.Empty && !inputWindow.outputExit)
            {
                (MainWindow.Owner as MainWindow).ReportError("The name entered was blank, so the collection was not updated");
                return false;
            }
            return false;
        }
    }
}
