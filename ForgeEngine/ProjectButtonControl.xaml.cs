﻿using System.Windows;
using System.Windows.Controls;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for ProjectButtonControl.xaml
    /// </summary>
    public partial class ProjectButtonControl : UserControl
    {
        public event RoutedEventHandler Click;

        public ProjectButtonControl(string name)
        {
            InitializeComponent();

            projectLabel.Text = name;
        }

        public ProjectButtonControl()
        {
            InitializeComponent();
        }

        void onButtonClick(object sender, RoutedEventArgs e)
        {
            Click?.Invoke(this, e);
        }
    }
}
