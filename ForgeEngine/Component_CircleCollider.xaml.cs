﻿using ForgeEngineCore.Editor;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_CircleCollider.xaml
    /// </summary>
    public partial class Component_CircleCollider : UserControl
    {
        bool allowStore = false;

		private Editor _editor;
		private EntityManagerWrapper manager;
		private UIElementCollection _container;
		public Component_CircleCollider(Editor editor, UIElementCollection container)
        {
            InitializeComponent();
			manager = editor.GetEntityWrapper();
			_editor = editor;
			_container = container;

            TxtOffsetX.Text = manager.GetCircleColliderOffsetX().ToString();
            TxtOffsetY.Text = manager.GetCircleColliderOffsetY().ToString();
            RadiusBox.Text = manager.GetCircleColliderRadius().ToString();
            IsTriggerCheckBox.IsChecked = manager.CircleIsTrigger();

            allowStore = true;
        }

        private void RadiusBox_TextChanged(object sender, TextChangedEventArgs e)
        {
			Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(RadiusBox.Text))
            {
                float result;
                if (float.TryParse(RadiusBox.Text, out result))
                {
                    if (allowStore) manager.SetCircleCollider(result);
                }
            }
		}

		private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			manager.RemoveCircleCollider();
			_container.Remove(this);
        }

        private void isTriggerCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            bool result;

            if (bool.TryParse(IsTriggerCheckBox.IsChecked.ToString(), out result))
                if (allowStore) manager.CircleSetTrigger(result);
        }

        private void IsTriggerCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            bool result;

            if (bool.TryParse(IsTriggerCheckBox.IsChecked.ToString(), out result))
                if (allowStore) manager.CircleSetTrigger(result);
        }

        private void TxtOffsetX_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtOffsetX.Text))
            {
                if (float.TryParse(TxtOffsetX.Text, out float result))
                {
                    if (allowStore) manager.SetCircleColliderOffsetX(result);
                }
            }
        }

        private void TxtOffsetY_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtOffsetY.Text))
            {
                if (float.TryParse(TxtOffsetY.Text, out float result))
                {
                    if (allowStore) manager.SetCircleColliderOffsetY(result);
                }
            }
        }
    }
}
