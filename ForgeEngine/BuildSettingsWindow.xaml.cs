﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ForgeEngine
{
	/// <summary>
	/// Interaction logic for BuildSettingsWindow.xaml
	/// </summary>
	public partial class BuildSettingsWindow : Window
	{
		private Editor _editor;
		public BuildSettingsWindow(Editor editor)
		{
			InitializeComponent();
			_editor = editor;
		}

		private void CreateNewScene_Click(object sender, RoutedEventArgs e)
		{
			Console.WriteLine("Building Scene");

			int width = 0;
			int height = 0;
			bool? overridecheck = false;

			try
			{
				width = int.Parse(WindowWidth.Text);
				height = int.Parse(WindowHeight.Text);
				overridecheck = OverrideCheck.IsChecked;
			}
			catch(Exception)
			{
			}
			finally
			{
				_editor.BuildGame(overridecheck.Value, width, height);
			}
		

			var absolutePath = Path.GetFullPath(_editor.GetProject().Path);
			Process.Start("explorer.exe",$"{absolutePath}\\Build");
			Close();		
		}

		private void NewProjectBack_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
