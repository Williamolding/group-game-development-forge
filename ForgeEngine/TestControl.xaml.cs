﻿using ForgeEngineCore.ProjectManagment;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for TestControl.xaml
    /// </summary>
    public partial class TestControl : UserControl
    {
        public Project project;

        public TestControl(string name)
		{
			InitializeComponent();

			projectLabel.Content = name;
		}

		public TestControl()
        {
            InitializeComponent();
        }

		private void projectLabel_Click(object sender, RoutedEventArgs e)
		{
            MainWindow window = new MainWindow(project)
                { Owner = Window.GetWindow(this)};
            window.Show();
            window.Owner = null;
			Window.GetWindow(this).Close();
		}

        private void PackIcon_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                Directory.Delete(project.Path, true);
                ((Panel)this.Parent).Children.Remove(this);
            }
        }
    }
}
