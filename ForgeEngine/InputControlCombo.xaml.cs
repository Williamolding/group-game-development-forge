﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for InputControlCombo.xaml
    /// </summary>
    public partial class InputControlCombo : UserControl
    {
        //MainWindow mainWindow;
        string collectionName = "Default";

        public Dictionary<string, ushort> keyboardInputs = new Dictionary<string, ushort>()
        {
            { "Num 0", 48 }, { "Num 1", 49 }, { "Num 2", 50 },
            { "Num 3", 51 }, { "Num 4", 52 }, { "Num 5", 53 },
            { "Num 6", 54 }, { "Num 7", 55 }, { "Num 8", 56 },
            { "Num 9", 57 },

            { "Backspace", 8 },
            { "Enter", 13 },
            { "Escape", 27 },
            { "Space", 32 },

            { "Arrow Left", 37 },
            { "Arrow Right", 38 },
            { "Arrow Up", 39 },
            { "Arrow Down", 40 },

            { "A Key", 65 },
            { "B Key", 66 },
            { "C Key", 67 },
            { "D Key", 68 },
            { "E Key", 69 },
            { "F Key", 70 },
            { "G Key", 71 },
            { "H Key", 72 },
            { "I Key", 73 },
            { "J Key", 74 },
            { "K Key", 75 },
            { "L Key", 76 },
            { "M Key", 77 },
            { "N Key", 78 },
            { "O Key", 79 },
            { "P Key", 80 },
            { "Q Key", 81 },
            { "R Key", 82 },
            { "S Key", 83 },
            { "T Key", 84 },
            { "U Key", 85 },
            { "V Key", 86 },
            { "W Key", 87 },
            { "X Key", 88 },
            { "Y Key", 89 },
            { "Z Key", 90 },
        };

        public Dictionary<string, ushort> xboxInputs = new Dictionary<string, ushort>()
        {
            { "D-Pad Up", 0 },
            { "D-Pad Down", 1 },
            { "D-Pad Left", 2 },
            { "D-Pad Right", 3 },
            { "Start Button", 4 },
            { "Back Button", 5 },
            { "L3 (Thumbstick)", 6 },
            { "R3 (Thumbstick)", 7 },
            { "LB (Bumper)", 8 },
            { "RB (Bumper)", 9 },
            { "A Button", 10 },
            { "B Button", 11 },
            { "X Button", 12 },
            { "Y Button", 13 }
        };

        public InputControlCombo()
        {
            InitializeComponent();

            InputMethodComboBox.Items.Add("Keyboard");
            InputMethodComboBox.Items.Add("Xbox Controller");
            InputMethodComboBox.SelectedIndex = 0;

            SetComboBoxItems(keyboardInputs);
        }

        void SetComboBoxItems(Dictionary<string, ushort> dictionary)
        {
            InputsComboBox.Items.Clear();
            foreach (var item in dictionary)
            {
                InputsComboBox.Items.Add(item.Key);
            }
            InputsComboBox.SelectedIndex = 0;
        }

        private void InputMethodComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string value = InputMethodComboBox.SelectedValue.ToString();

            switch(value)
            {
                case "Keyboard":
                    SetComboBoxItems(keyboardInputs);
                    break;
                case "Xbox Controller":
                    SetComboBoxItems(xboxInputs);
                    break;
                default:
                    (Window.GetWindow(this).Owner as MainWindow).ReportError("Input method not found.");
                    break;
            }
        }

        private void InputsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (InputMethodComboBox.SelectedValue != null && InputsComboBox.SelectedValue != null)
            {
                string method = InputMethodComboBox.SelectedValue.ToString();

                switch (method)
                {
                    case "Keyboard":
                        //forge.exprt.cli.Input.RegisterKeyboardInput(collectionName, keyboardInputs[InputsComboBox.SelectedValue.ToString()]);
                        break;
                    case "Xbox Controller":
                        break;
                    default:
                        break;
                }
            }
        }

        public void SetCollectionName(string name)
        {
            collectionName = name;
        }

        public void RegisterInput(string collectionName, ushort value)
        {
            forge.exprt.cli.Input.RegisterKeyboardInput(collectionName, value);
        }
        public void RegisterInput(string collectionRegName)
        {
            string method = InputMethodComboBox.SelectedValue.ToString();

            switch (method)
            {
                case "Keyboard":
                    forge.exprt.cli.Input.RegisterKeyboardInput(collectionRegName, keyboardInputs[InputsComboBox.SelectedValue.ToString()]);
                    break;
                case "Xbox Controller":
                    forge.exprt.cli.Input.RegisterControllerInput(collectionRegName, xboxInputs[InputsComboBox.SelectedValue.ToString()]);
                    break;
                default:
                    break;
            }
        }

        private void RemoveInput_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ClickEvent));
        }

        public static readonly RoutedEvent ClickEvent = EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(InputControlCombo));

        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }
    }
}
