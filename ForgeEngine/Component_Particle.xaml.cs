﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
	/// <summary>
	/// Interaction logic for Component_Particle.xaml
	/// </summary>
	public partial class Component_Particle : UserControl
	{
        bool allowStore = false;

		private Editor _editor;
		private EntityManagerWrapper manager;
		private UIElementCollection _container;

	    public Component_Particle()
	    {
	    }

		public Component_Particle(Editor editor, UIElementCollection container)
		{
			InitializeComponent();

			manager = editor.GetEntityWrapper();
			_editor = editor;
			_container = container;

            MassBox.Text = manager.GetParticleMass().ToString();
		    TxtGravityRate.Text = manager.GetGravity().ToString();
            CbTerminalVelocity.IsChecked = manager.IsTerminalVelocityOn();
            SdrCoF.Value = manager.GetFrictionCoF();
            SdrNormal.Value = manager.GetFrictionNormal();
            TxtMultiplier.Text = manager.GetFrictionMultiplier().ToString();
            CbPushForce.IsChecked = manager.BeingPushed();

            //            if (CbLaminarOn.IsChecked == true)
            //                SdrLamDragCo.Value = manager.GetLaminarDrag();
            SdrCoR.Value = manager.GetCoefficientOfRestitution();
            SdrCoR.LargeChange = 0.1f;
            SdrCoR.SmallChange = 0.05f;

            if (CbPushForce.IsChecked == true)
            {
                SdrPushDragCo.Value = manager.GetPushDrag();
                TxtPushForce.Text = manager.GetPushingForce().ToString("F2");
            }

            TxtFrictionCofValue.Text = manager.GetFrictionCoF().ToString("F2");
            TxtNormalValue.Text = manager.GetFrictionNormal().ToString("F2");

            allowStore = true;
        }
        
        // Textboxes
		private void MassBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(MassBox.Text))
            {
                float result; 
                if (float.TryParse(MassBox.Text, out result))
                {
                    if (allowStore) manager.SetParticleMass(result);
                }
            }
		}

        private void TxtGravityRate_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtGravityRate.Text))
            {
                if (float.TryParse(TxtGravityRate.Text, out var result))
                {
                    if (allowStore) manager.SetGravity(result);
                }
            }
        }

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			manager.RemoveParticleComponent();
			_container.Remove(this);
		}

        private void Component_GravityForce_Loaded(object sender, RoutedEventArgs e)
        {

        }
        
        private void TxtMultiplier_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtMultiplier.Text))
            {
                if (float.TryParse(TxtMultiplier.Text, out var result))
                {
                    if (allowStore) manager.SetFrictionMultiplier(result);
                }
            }
        }

        private void TxtPushForce_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtPushForce.Text))
                if (float.TryParse(TxtPushForce.Text, out var result))
                    if (allowStore) manager.SetPushingForce(result);
        }

        private void TxtFrictionCofValue_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void TxtNormalValue_TextChanged(object sender, TextChangedEventArgs e)
        {
        }

        private void TxtPushForceDragValue_OnTextChanged(object sender, TextChangedEventArgs e)
        {
        }


        // Sliders
        private void SdrCoR_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (float.TryParse(SdrCoR.Value.ToString(), out var cor))
                if (allowStore) manager.SetCoefficientOfRestitution(cor);

            TxtCoRValue.Text = SdrCoR.Value.ToString("F2");
        }

        private void SdrCoF_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (float.TryParse(SdrCoF.Value.ToString(), out var cof))
                if (allowStore) manager.SetFrictionCoF(cof);

            TxtFrictionCofValue.Text = SdrCoF.Value.ToString("F2");
        }

        private void SdrNormal_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (float.TryParse(SdrNormal.Value.ToString(), out var normal))
                if (allowStore) manager.SetFrictionNormal(normal);

            TxtNormalValue.Text = SdrNormal.Value.ToString("F2");
        }

//        private void SdrLamDragCo_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
//        {
//            if (float.TryParse(SdrLamDragCo.Value.ToString(), out var drag))
//                if (allowStore) manager.SetLaminarDrag(drag);
//
//            TxtLaminarForceDrag.Text = SdrLamDragCo.Value.ToString("F2");
//        }

        private void SdrPushDragCo_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (float.TryParse(SdrPushDragCo.Value.ToString(), out var drag))
                if (allowStore) manager.SetPushForceDrag(drag);

            TxtPushForceDragValue.Text = SdrPushDragCo.Value.ToString("F2");
        }

         // Check Boxes
        private void CbTerminalVelocity_OnUnchecked(object sender, RoutedEventArgs e)
        {
            if (bool.TryParse(CbTerminalVelocity.IsChecked.ToString(), out var result))
                if (allowStore) manager.SetTerminalVelocity(result);
        }

        private void CbTerminalVelocity_OnChecked(object sender, RoutedEventArgs e)
        {
            if (bool.TryParse(CbTerminalVelocity.IsChecked.ToString(), out var result))
                if (allowStore) manager.SetTerminalVelocity(result);
        }

//        private void CbLaminarOn_OnUnchecked(object sender, RoutedEventArgs e)
//        {
//            if (allowStore) manager.RemoveLamForce();
//        }
//
//        private void CbLaminarOn_OnChecked(object sender, RoutedEventArgs e)
//        {
//            if (allowStore)
//            {
//                manager.SetBeingPushed(false);
//                manager.AddLaminarForce();
//                CbPushForce.IsChecked = false;
//
//                SdrLamDragCo.Value = manager.GetLaminarDrag();
//            }
//        }

        private void CbPushForce_Checked(object sender, RoutedEventArgs e)
        {
            if (bool.TryParse(CbPushForce.IsChecked.ToString(), out var result))
                if (allowStore)
                {
                    manager.AddPushForce();
                    manager.SetBeingPushed(result);
//                    CbLaminarOn.IsChecked = false;

                    SdrPushDragCo.Value = manager.GetPushDrag();
                    TxtPushForce.Text = manager.GetPushingForce().ToString("F2");
                }
        }

        private void CbPushForce_OnUnchecked(object sender, RoutedEventArgs e)
        {
            if (bool.TryParse(CbPushForce.IsChecked.ToString(), out var result))
                if (allowStore)
                {
                    manager.SetBeingPushed(result);
                    manager.RemovePushForce();
                }
        }
    }
}
