﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for ErrorPopup.xaml
    /// </summary>
    public partial class ErrorPopup : UserControl
    {
        public ErrorPopup()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ErrorTextProperty =
            DependencyProperty.Register("ErrorText", typeof(string), typeof(ErrorPopup),
                new PropertyMetadata("", new PropertyChangedCallback(OnErrorTextChanged)));

        public string ErrorText
        {
            get { return (string)GetValue(ErrorTextProperty); }
            set { SetValue(ErrorTextProperty, value); }
        }

        private static void OnErrorTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ErrorPopup control = d as ErrorPopup;
            control.OnErrorTextChanged(e);
        }

        private void OnErrorTextChanged(DependencyPropertyChangedEventArgs e)
        {
            ErrorTextBlock.Text = e.NewValue.ToString();
        }

        //--------------------------------------------------------------------

        public static readonly DependencyProperty ColorOverrideProperty =
            DependencyProperty.Register("ColorOverride", typeof(string), typeof(ErrorPopup),
                new PropertyMetadata("", new PropertyChangedCallback(OnColorOverrideChanged)));

        public string ColorOverride
        {
            get { return (string)GetValue(ColorOverrideProperty); }
            set { SetValue(ColorOverrideProperty, value); }
        }

        private static void OnColorOverrideChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ErrorPopup control = d as ErrorPopup;
            control.OnColorOverrideChanged(e);
        }

        private void OnColorOverrideChanged(DependencyPropertyChangedEventArgs e)
        {
            string hex = e.NewValue.ToString();
            switch (hex)
            {
                case "Default":
                    hex = "#BB3333";
                    break;
                case "Red":
                    hex = "#BB3333";
                    break;
                case "Green":
                    hex = "#33AA44";
                    break;
            }

            backgroundRect.Background = (SolidColorBrush)new BrushConverter().ConvertFrom(hex);
        }

        public static readonly RoutedEvent ClickEvent =
            EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(ErrorPopup));

        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        private void ErrorButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ClickEvent));
        }
    }
}
