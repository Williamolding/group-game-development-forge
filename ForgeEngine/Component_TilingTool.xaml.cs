﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_TilingTool.xaml
    /// </summary>
    public partial class Component_TilingTool : UserControl
    {
        bool allowStore = false;

        private Editor _editor;
        private EntityManagerWrapper manager;
        private UIElementCollection _container;
        public Component_TilingTool(Editor editor, UIElementCollection container)
        {
            InitializeComponent();
            manager = editor.GetEntityWrapper();
            _editor = editor;
            _container = container;
            
            WidthBox.Text = manager.GetTilingToolWidth().ToString();
            HeightBox.Text = manager.GetTilingToolHeight().ToString();
            OverrideRectCollider.IsChecked = manager.GetTilingToolOverride();

            allowStore = true;
        }
        private void WidthBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(WidthBox.Text))
            {
                int result;
                if (int.TryParse(WidthBox.Text, out result))
                {
                    if (allowStore)
                    {
                        _editor.GetWindow()._renderCanvas.Pause();
                        manager.SetTilingToolWidth(result);
                        _editor.GetWindow()._renderCanvas.Resume();
                    }
                    int resultY = 0;
                    int.TryParse(HeightBox.Text, out resultY);
                    _editor.UpdateTileTool(result, resultY);
                }
            }
        }

        private void HeightBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(HeightBox.Text))
            {
                int result;
                if (int.TryParse(HeightBox.Text, out result))
                {
                    if (allowStore)
                    {
                        _editor.GetWindow()._renderCanvas.Pause();
                        manager.SetTilingToolHeight(result);
                        _editor.GetWindow()._renderCanvas.Resume();
                    }
                    int resultX = 0;
                    int.TryParse(WidthBox.Text, out resultX);
                    _editor.UpdateTileTool(resultX, result);
                }
            }
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            manager.RemoveTilingToolComponent();
            _container.Remove(this);
        }

        private void WidthBox_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void OverrideRectCollider_Unchecked(object sender, RoutedEventArgs e)
        {
            manager.SetTilingToolOverride(false);
        }

        private void OverrideRectCollider_Checked(object sender, RoutedEventArgs e)
        {
            manager.SetTilingToolOverride(true);
        }
    }
}
