﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ForgeEngine
{
	/// <summary>
	/// Interaction logic for NewSceneWindow.xaml
	/// </summary>
	public partial class NewSceneWindow : Window
	{
		private Editor _editor;

        public bool outputExit = false;
        public string outputText = "ERROR";

        public NewSceneWindow(Editor editor)
		{
			_editor = editor;
			InitializeComponent();
		}

		//Close Window Button
		private void NewProjectBack_Click(object sender, RoutedEventArgs e)
		{
            outputExit = true;
            Close();
        }

		//Create Scene Button
		private void CreateNewScene_Click(object sender, RoutedEventArgs e)
		{
			string newSceneName = NewSceneName.Text;
            outputText =  NewSceneName.Text;
            outputExit = false;
            if (outputText != string.Empty)
            {
                _editor.CreateSceneFile(newSceneName);

            }
            Close();
		}

        private void NewSceneName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                CreateNewScene_Click(sender, e);
            }
        }
    }
}
