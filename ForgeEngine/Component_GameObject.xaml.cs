﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_GameObject.xaml
    /// </summary>
    public partial class Component_GameObject : UserControl
    {
        bool allowStore = false;

        private Editor _editor;
        private EntityManagerWrapper manager;
        private UIElementCollection _container;
        public Component_GameObject(Editor editor, UIElementCollection container)
        {
            InitializeComponent();

            manager = editor.GetEntityWrapper();
            _editor = editor;
            _container = container;
            
            GameObjectName.Text = manager.GetEntityName().ToString();
            //            GameObjectTag.Text  = manager.GetEntityTag().ToString();

            
            allowStore = true;
        }

        private void GameObjectName_TextChanged(object sender, TextChangedEventArgs e)
        {
            //            Regex regex = new Regex("[^0-9.-]+");
            //            if (!regex.IsMatch(GameObjectName.Text))

            if (allowStore)
            {
                manager.SetEntityName(GameObjectName.Text);
            }
        }

        private void GameObjectTag_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(GameObjectName.Text))
            {
                //if (allowStore) manager.SetEntityTag(int.Parse(GameObjectTag.Text));
            }
        }

        private void GameObjectName_OnLostFocus(object sender, RoutedEventArgs e)
        {
            MainWindow window = _editor.GetWindow();

            window._renderCanvas.Pause();
            foreach (var t in window.TreeSceneHierarchy.Items)
            {
                if (window.TreeSceneHierarchy.SelectedItem == t)
                {
                    ((SceneObject) window.TreeSceneHierarchy.SelectedItem).Name = GameObjectName.Text;
                    //((TreeViewItem) window.TreeSceneHierarchy.ItemContainerGenerator.ContainerFromItem(
                       // window.TreeSceneHierarchy.SelectedItem)).IsSelected = false;
                }
            }

            window.TreeSceneHierarchy.Items.Refresh();
            window._renderCanvas.Resume();
        }

        private void AddComponentButton_Click(object sender, RoutedEventArgs e)
        {
            string path = _editor.GetProject().Path + "\\Assets\\";
            string name = "";

            string[] items = Directory.GetFiles(path, "*.tga", SearchOption.AllDirectories);

            for (int i = 0; i < items.Length; i++)
            {
                items[i] = System.IO.Path.GetFileName(items[i]);
            }
            DropDownInput dropDownInput = new DropDownInput("Pick a new sprite:", _editor.GetWindow(), items);

            if(!dropDownInput.wasCancelled)
            {
                name = dropDownInput.output;
                _editor.ChangeSprite(name);
            }
        }
    }
}
