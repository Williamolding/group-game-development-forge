﻿using MaterialDesignThemes.Wpf;
using System.Windows;
using System;
using System.Windows.Controls;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for ContainerControl.xaml
    /// </summary>
    public partial class ContainerControl : UserControl
    {
        public ContainerControl()
        {
            InitializeComponent();
        }
        public ContainerControl(string name)
        {
            InitializeComponent();

            TitleBar.Text = name;
        }

        public static readonly DependencyProperty ContainerTitleProperty =
            DependencyProperty.Register("ContainerTitle", typeof(string), typeof(ContainerControl),
                new PropertyMetadata("", new PropertyChangedCallback(OnContainerTitleChanged)));

        public string ContainerTitle
        {
            get { return (string)GetValue(ContainerTitleProperty); }
            set { SetValue(ContainerTitleProperty, value); }
        }

        private static void OnContainerTitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ContainerControl containerControl = d as ContainerControl;
            containerControl.OnContainerTitleChanged(e);
        }

        private void OnContainerTitleChanged(DependencyPropertyChangedEventArgs e)
        {
            TitleBar.Text = e.NewValue.ToString();
        }

        //-----------------------------------------------------------------------------

        public static readonly DependencyProperty ContainerIconProperty =
            DependencyProperty.Register("ContainerIcon", typeof(string), typeof(ContainerControl),
                new PropertyMetadata("", new PropertyChangedCallback(OnContainerIconChanged)));

        public string ContainerIcon
        {
            get { return (string)GetValue(ContainerIconProperty); }
            set { SetValue(ContainerIconProperty, value); }
        }

        private static void OnContainerIconChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ContainerControl containerControl = d as ContainerControl;
            containerControl.OnContainerIconChanged(e);
        }

        private void OnContainerIconChanged(DependencyPropertyChangedEventArgs e)
        {
            TitleIcon.Kind = (PackIconKind)System.Enum.Parse(typeof(PackIconKind), e.NewValue.ToString());
        }

        //-----------------------------------------------------------------------------

        public object InnerContent
        {
            get { return GetValue(InnerContentProperty); }
            set { SetValue(InnerContentProperty, value); }
        }

        public static readonly DependencyProperty InnerContentProperty =
            DependencyProperty.Register("InnerContent", typeof(object), typeof(ContainerControl), new PropertyMetadata(null));
    }
}
