﻿using ForgeEngineCore.Engine;
using ForgeEngineCore.ProjectManagment;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		public App()
		{
			var ProjectScanner = new ProjectScanner();
			var EngineSettingsLoader = new EngineSettingsLoader();

			var engineBooter = new EngineBooter(EngineSettingsLoader, ProjectScanner);

			engineBooter.Boot();
		}
    }
}
