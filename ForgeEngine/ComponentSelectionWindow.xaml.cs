﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ForgeEngine
{
	/// <summary>
	/// Interaction logic for ComponentSelectionWindow.xaml
	/// </summary>
	public partial class ComponentSelectionWindow : Window
	{
		private Editor _editor;

		public ComponentSelectionWindow(Editor editor)
		{
			InitializeComponent();
			_editor = editor;
		}

		private void NewProjectBack_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void AddAnimationComponenet_Click(object sender, RoutedEventArgs e)
		{
			_editor.AddComponent_Animation();
			Close();
		}

		private void AddScriptComponent_Click(object sender, RoutedEventArgs e)
		{

			_editor.AddAndSearchComponentScript();
			Close();
		}

		private void AddParticleComponenet_Click(object sender, RoutedEventArgs e)
		{
			_editor.AddComponent_Particle();
			Close();
		}

		private void AddCircleCollider_Click(object sender, RoutedEventArgs e)
        {
            //_editor.AddComponent_Particle();
            _editor.AddComponent_CircleCollider();
			Close();
		}

		private void AddRectCollider_Click(object sender, RoutedEventArgs e)
        {
            //_editor.AddComponent_Particle();
            _editor.AddComponent_RectCollider();
            Close();
		}

		private void AddSoundEmitter_Click(object sender, RoutedEventArgs e)
		{
			_editor.SearchAndAddComponent_SoundEmitter();
			Close();
		}


        private void AddTilingTool_Click(object sender, RoutedEventArgs e)
        {
            _editor.AddComponent_TilingTool();
            Close();
        }
    }
}
