﻿using ForgeEngineCore.ProjectManagment;
using ForgeEngineCore.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using System.Windows.Media.Animation;

namespace ForgeEngine
{
	/// <summary>
	/// Interaction logic for ProjectViewer.xaml
	/// </summary>
	public partial class ProjectViewer : Window
    {
        private ProjectCreation projectCreation = new ProjectCreation();

        string createProjectName = "";
        string createProjectLocation = "";

        public ProjectViewer()
		{
			
			InitializeComponent();
            
            if(!System.IO.Directory.Exists("Projects"))
                System.IO.Directory.CreateDirectory("Projects"); //Default Path

            
            ProjectCreation.LoadProjects(ProjectViewContent, false);
        }

		private void ExitButton_Click(object sender, RoutedEventArgs e)
		{
			System.Windows.Application.Current.Shutdown();
		}

		private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			DragMove();
		}

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "XML Project (*.xml)|*.xml";

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //Update EngineSettings
                string path = System.IO.Path.GetDirectoryName(dialog.FileName);
                EngineEnviroment.EngineSettings.projectFiles.Add(path);
                List<string> tempPaths = new List<string>();
                tempPaths.AddRange(EngineEnviroment.EngineSettings.projectFiles.Distinct());
                EngineEnviroment.EngineSettings.projectFiles = tempPaths;

                EngineSettingsLoader settingsLoader = new EngineSettingsLoader();
                settingsLoader.SaveSettingsFile();

                //Clear currently loaded projects
                ProjectCreation.ClearProjects(ProjectViewContent);

                //Reload projects with new paths
                ProjectCreation.LoadProjects(ProjectViewContent, true);
            }
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = (Storyboard)NewProjectView.FindResource("OpenNewDialog");
            sb.Begin();

            //Set location box
            createProjectLocation = System.IO.Path.GetFullPath(EngineEnviroment.EngineSettings.projectFolders[0]) + "\\";
            UpdateCreateProjectLocation();
        }

        private void NewProjectBack_Click(object sender, RoutedEventArgs e)
        {
            Storyboard sb = (Storyboard)NewProjectView.FindResource("CloseNewDialog");
            sb.Begin();
        }

        private void LocateNewProjectFolder_Click(object sender, RoutedEventArgs e)
        {

            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.ShowNewFolderButton = true;

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                createProjectLocation = dialog.SelectedPath + "\\";
                UpdateCreateProjectLocation();
            }
        }

        public static string NormalizePath(string path)
        {
            return System.IO.Path.GetFullPath(new Uri(path).LocalPath)
                       .TrimEnd(System.IO.Path.DirectorySeparatorChar, System.IO.Path.AltDirectorySeparatorChar)
                       .ToUpperInvariant();
        }

        private void CreateNewProject_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(NewProjectLocation.Text))
            {
                Directory.CreateDirectory(NewProjectLocation.Text);

                string pathParent = createProjectLocation;
                bool addToXML = true;

                foreach (string folder in EngineEnviroment.EngineSettings.projectFolders)
                {
                    string folderPath = System.IO.Path.GetFullPath(folder);
                    if (NormalizePath(pathParent) == NormalizePath(folderPath))
                    {
                        addToXML = false;
                        ProjectInformation.CreateProjectFile(folder + "\\" + createProjectName, createProjectName); //Local path
                        break;
                    }
                }

                if (addToXML)
                {
                    EngineEnviroment.EngineSettings.projectFiles.Add(createProjectLocation + createProjectName);
                    EngineSettingsLoader settingsLoader = new EngineSettingsLoader();
                    settingsLoader.SaveSettingsFile();
                    
                    ProjectInformation.CreateProjectFile(NewProjectLocation.Text, createProjectName); //Absolute path
                }
                ProjectCreation.ClearProjects(ProjectViewContent);
                ProjectCreation.LoadProjects(ProjectViewContent, true);

                Storyboard sb = (Storyboard)NewProjectView.FindResource("CloseNewDialog");
                sb.Begin();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("A project with this name already exists in this directory");
            }
        }

        private void NewProjectName_TextChanged(object sender, TextChangedEventArgs e)
        {
            createProjectName = NewProjectName.Text;
            UpdateCreateProjectLocation();
        }

        void UpdateCreateProjectLocation()
        {
            NewProjectLocation.Text = createProjectLocation + createProjectName + "\\";

            NewProjectLocation.CaretIndex = NewProjectLocation.Text.Length;
            NewProjectLocation.ScrollToEnd();
        }

        private void LoadProjectButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void NewProjectName_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                CreateNewProject_Click(sender,e);
            }
        }
     
    }
}
