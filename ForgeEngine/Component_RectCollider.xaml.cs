﻿using ForgeEngineCore.Editor;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_RectCollider.xaml
    /// </summary>
    public partial class Component_RectCollider : UserControl
    {
        bool allowStore = false;

        private Editor _editor;
		private EntityManagerWrapper manager;
		private UIElementCollection _container;
		public Component_RectCollider(Editor editor, UIElementCollection container)
        {
            InitializeComponent();
			manager = editor.GetEntityWrapper();
			_editor = editor;
			_container = container;

			WidthBox.Text = (manager.GetRectColliderWidth() * 2).ToString();
			HeightBox.Text = (manager.GetRectColliderHeight() * 2).ToString();
            TxtOffsetX.Text = manager.GetRectColliderOffsetX().ToString();
            TxtOffsetY.Text = manager.GetRectColliderOffsetY().ToString();
            isTriggerCheckBox.IsChecked = manager.RectIsTrigger();

            allowStore = true;
		}

        private void WidthBox_OnTextChangedBox_TextChanged(object sender, TextChangedEventArgs e)
        {
			Regex regex = new Regex("[^0-9.-]+");
			if (!regex.IsMatch(WidthBox.Text))
            {
                if (float.TryParse(WidthBox.Text, out float result))
                {
                    if (allowStore) manager.SetRectColliderWidth(result * .5f);
                }
			}
        }

		private void HeightBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			Regex regex = new Regex("[^0-9.-]+");
			if (!regex.IsMatch(HeightBox.Text))
            {
                float result;
                if (float.TryParse(HeightBox.Text, out result))
                {
                    if (allowStore) manager.SetRectColliderHeight(result *.5f);
                }
			}
		}

		private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			manager.RemoveRectCollider();
			_container.Remove(this);
		}

        private void isTriggerCheckBox_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            bool result;

            if (bool.TryParse(isTriggerCheckBox.IsChecked.ToString(), out result))
                if (allowStore) manager.RectSetIsTrigger(result);
        }

        private void IsTriggerCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            bool result;

            if (bool.TryParse(isTriggerCheckBox.IsChecked.ToString(), out result))
                if (allowStore) manager.RectSetIsTrigger(result);
        }

        private void TxtOffsetX_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtOffsetX.Text))
            {
                if (float.TryParse(TxtOffsetX.Text, out float result))
                {
                    if (allowStore) manager.SetRectColliderOffsetX(result);
                }
            }
        }

        private void TxtOffsetY_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TxtOffsetY.Text))
            {
                if (float.TryParse(TxtOffsetY.Text, out float result))
                {
                    if (allowStore) manager.SetRectColliderOffsetY(result);
                }
            }
        }

        private void TxtOffsetX_LostFocus(object sender, RoutedEventArgs e)
        {
        }

        private void TxtOffsetY_LostFocus(object sender, RoutedEventArgs e)
        {
        }
    }
}
