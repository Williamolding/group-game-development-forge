using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for SoundEmitter.xaml
    /// </summary>
    public partial class SoundEmitter : UserControl
    {
        bool allowStore = false;

		private Editor _editor;
		private EntityManagerWrapper manager;
		private UIElementCollection _container;
		public SoundEmitter(Editor editor, UIElementCollection container)
        {
            InitializeComponent();

			manager = editor.GetEntityWrapper();
			_editor = editor;
			_container = container;

            SoundEmitterAudioFile.Text           = manager.GetSoundEmitterFileName();
            SoundEmitterPlayOnLoad.IsChecked	= manager.GetSoundEmitterPlayOnLoad();
			SoundEmitterLoop.IsChecked			= manager.GetSoundEmitterLooping();
			SoundEmitterPitch.Value				= manager.GetSoundEmitterPitch();
			SoundEmitterVolume.Value			= manager.GetSoundEmitterVolume();

            allowStore = true;
        }

        private void SoundEmitterVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            VolumeDisplay.Text = SoundEmitterVolume.Value.ToString("0.00");

			if (allowStore) manager.SetSoundEmitterVolume((float)SoundEmitterVolume.Value);
		}

        private void SoundEmitterPitch_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            PitchDisplay.Text = SoundEmitterPitch.Value.ToString("0.00");

            if (allowStore) manager.SetSoundEmitterPitch((float)SoundEmitterPitch.Value);
		}

		private void SoundEmitterLoop_Checked(object sender, RoutedEventArgs e)
		{
            if (allowStore) manager.SetSoundEmitterLooping(true);
		}

		private void SoundEmitterLoop_Unchecked(object sender, RoutedEventArgs e)
		{
            if (allowStore) manager.SetSoundEmitterLooping(false);
		}

		private void SoundEmitterPlayOnLoad_Unchecked(object sender, RoutedEventArgs e)
		{
            if (allowStore) manager.SetSoundEmitterPlayOnLoad(false);
		}

		private void SoundEmitterPlayOnLoad_Checked(object sender, RoutedEventArgs e)
		{
            if (allowStore) manager.SetSoundEmitterPlayOnLoad(true);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
            manager.RemoveSoundEmitterComponent();
			_container.Remove(this);
		}

        private void SoundEmitterAudioFile_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (allowStore) manager.SetSoundEmitterFileName(SoundEmitterAudioFile.Text);
        }
    }
}
