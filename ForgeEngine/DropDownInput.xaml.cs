﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for DropDownInput.xaml
    /// </summary>
    public partial class DropDownInput : Window
    {
        public string output;
        public bool wasCancelled = true;

        public DropDownInput(string title, Window owner, string[] items)
        {
            InitializeComponent();

            Title.Text = title;
            Owner = owner;

            foreach(string i in items)
            {
                DropdownBox.Items.Add(i);
            }

            DropdownBox.SelectedIndex = 0;
            ShowDialog();
        }

        private void DropdownBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            output = (string)DropdownBox.SelectedItem;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            wasCancelled = false;
            Close();
        }

        private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
