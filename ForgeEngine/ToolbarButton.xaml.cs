﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for ToolbarButton.xaml
    /// </summary>
    public partial class ToolbarButton : UserControl
    {
        bool isToggle = false;
        bool toggle = false;
        Brush mainColor;
        Brush accentColor;


        public ToolbarButton()
        {
            InitializeComponent();

            mainColor = bgRect.Fill;
            accentColor = PackIcon.Foreground;

        }

        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(string), typeof(ToolbarButton),
                new PropertyMetadata("", new PropertyChangedCallback(OnIconChanged)));

        public string Icon
        {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        private static void OnIconChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ToolbarButton control = d as ToolbarButton;
            control.OnIconChanged(e);
        }

        private void OnIconChanged(DependencyPropertyChangedEventArgs e)
        {
            PackIcon.Kind = (PackIconKind)System.Enum.Parse(typeof(PackIconKind), e.NewValue.ToString());
        }

        //------------------------------------------------------------------

        public static readonly DependencyProperty IsToggleProperty =
            DependencyProperty.Register("IsToggle", typeof(string), typeof(ToolbarButton),
                new PropertyMetadata("", new PropertyChangedCallback(OnIsToggleChanged)));

        public string IsToggle
        {
            get { return (string)GetValue(IsToggleProperty); }
            set { SetValue(IsToggleProperty, value); }
        }

        private static void OnIsToggleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ToolbarButton control = d as ToolbarButton;
            control.OnIsToggleChanged(e);
        }

        private void OnIsToggleChanged(DependencyPropertyChangedEventArgs e)
        {
            isToggle = e.NewValue.ToString().Equals("True", StringComparison.CurrentCultureIgnoreCase);

            if (!isToggle)
            {
                SetColor(accentColor, mainColor);
            }
        }
        
        public static readonly RoutedEvent ClickEvent =  EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
            typeof(RoutedEventHandler), typeof(ToolbarButton));
        
        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

		public bool GetToggleValue()
		{
			return toggle;
		}

		public void SetToggleValue(bool value)
		{
			toggle = !value;

			if (toggle)
			{
				SetColor(mainColor, accentColor);
			}
			else
			{
				SetColor(accentColor, mainColor);
			}
		}

        public void SetHighlight(bool value)
        {
            if (value)
            {
                SetColor(accentColor, mainColor);
            }
            else
            {
                SetColor(mainColor, accentColor);
            }
        }

        public void SetIcon(string kind)
        {
            PackIcon.Kind = (PackIconKind)System.Enum.Parse(typeof(PackIconKind), kind);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (isToggle)
            {
                if (toggle)
                {
                    SetColor(mainColor, accentColor);
                }
                else
                {
                    SetColor(accentColor, mainColor);
                }
            }

            toggle = !toggle;
            RaiseEvent(new RoutedEventArgs(ClickEvent));
        }

        private void SetColor(Brush rectColor, Brush iconColor)
        {
            bgRect.Fill = rectColor;
            PackIcon.Foreground = iconColor;
        }
    }
}
