﻿using forge.exprt.cli;
using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_Animation.xaml
    /// </summary>
    public partial class Component_Animation : UserControl
    {
        EntityManagerWrapper manager;
		UIElementCollection _container;

		bool allowStore = false;

        public Component_Animation(Editor editor, UIElementCollection container)
        {
            InitializeComponent();
			_container = container;

			manager = editor.GetEntityWrapper();

            AnimationSpeed.Value = manager.GetAnimationSpeed();
            AnimationFps.Value = manager.GetAnimationFps();
            allowStore = true;
        }

        private void AnimationSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            AnimationSpeedDisplay.Text = AnimationSpeed.Value.ToString("0.00");

            if(allowStore)
                manager.SetAnimationSpeed(Convert.ToSingle(AnimationSpeed.Value));
            
        }

        private void AnimationFps_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            AnimationFpsDisplay.Text = AnimationFps.Value.ToString();

            if (allowStore)
                manager.SetAnimationFps(Convert.ToSingle(AnimationFps.Value));
        }

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			manager.RemoveAnimationComponent();
			_container.Remove(this);
		}
	}
}
