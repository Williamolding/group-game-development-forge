﻿using ForgeEngineCore.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for InputCollections.xaml
    /// </summary>
    public partial class InputCollections : Window
    {
         List<InputCollectionExpander> collections = new List<InputCollectionExpander>();
		private Editor _editor;
		public InputCollections(Editor editor)
        {
            InitializeComponent();
            ReadCollections();

			_editor = editor;

		}

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            _window.Close();
        }

        private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void AddCollection_Click(object sender, RoutedEventArgs e)
        {
            InputCollectionExpander col = new InputCollectionExpander();
            col.Click += new RoutedEventHandler(InputCollectionExpander_Click);
            bool renameCollection =  col.CollectionRename(this);
            if (renameCollection)
            {
                CollectionStack.Children.Add(col);
                collections.Add(col);
            }      
            
        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {
            forge.exprt.cli.Input.ClearCollections();

            foreach(InputCollectionExpander collection in collections)
            {
                forge.exprt.cli.Input.RegisterInputCollection(collection.HeaderText);
                collection.RegisterInputs(collection.HeaderText);
            }

			_editor.SaveInputCollectionToProjectFile();

            (Owner as MainWindow).ReportError("Your input collections have been saved!", "Green");
		}
        private void DiscardChanges_Click(object sender, RoutedEventArgs e)
        {
            CollectionStack.Children.Clear();
            ReadCollections();
        }

        void ReadCollections()
        {
            List<forge.exprt.cli.InputCollection> list = forge.exprt.cli.Input.GetCollectionState();

            foreach(forge.exprt.cli.InputCollection li in list)
            {
                InputCollectionExpander col = new InputCollectionExpander();
                col.Click += new RoutedEventHandler(InputCollectionExpander_Click);
                col.expander.Header = li.collectionName;
                CollectionStack.Children.Add(col);
                collections.Add(col);

                //for each collection, add combo boxes
                foreach (int i in li.Keys)
                {
                    InputControlCombo combo = col.AddComboBox();
                    combo.InputMethodComboBox.SelectedIndex = 0;

                    int j = combo.InputsComboBox.Items.IndexOf(combo.keyboardInputs.FirstOrDefault(x => x.Value == i).Key);
                    combo.InputsComboBox.SelectedIndex = j;
                }
                foreach (int i in li.GamePadButton)
                {
                    InputControlCombo combo = col.AddComboBox();
                    combo.InputMethodComboBox.SelectedIndex = 1;

                    int j = combo.InputsComboBox.Items.IndexOf(combo.xboxInputs.FirstOrDefault(x => x.Value == i).Key);
                    combo.InputsComboBox.SelectedIndex = j;
                }
                foreach (int i in li.Button)
                {
                    col.AddComboBox();
                }

            }
        }

        private void InputCollectionExpander_Click(object sender, RoutedEventArgs e)
        {
            InputCollectionExpander expander = sender as InputCollectionExpander;
            collections.Remove(expander);
            CollectionStack.Children.Remove(expander);
        }
    }
}
