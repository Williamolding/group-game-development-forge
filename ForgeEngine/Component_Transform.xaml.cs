using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using ForgeEngineCore.Editor;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for Component_Transform.xaml
    /// </summary>
    public partial class Component_Transform : UserControl
    {
        bool allowStore = false;

        private Editor _editor;
		private EntityManagerWrapper manager;
		private UIElementCollection _container;
        public Component_Transform(Editor editor, UIElementCollection container)
        {
            InitializeComponent();

			manager = editor.GetEntityWrapper();
			_editor = editor;
			_container = container;

            TransformLayer.Text   = manager.GetTransformLayer().ToString();
            TransformXRotation.Value = manager.GetTransformRotationX();
            TransformYRotation.Value = manager.GetTransformRotationY();
            TransformZRotation.Value = manager.GetTransformRotationZ();

            TransformXScaleTxt.Text = manager.GetTransformScaleX().ToString();
            TransformYScaleTxt.Text = manager.GetTransformScaleY().ToString();
            //TransformXScale.Value = manager.GetTransformScaleX();
            //TransformYScale.Value = manager.GetTransformScaleY();
            //TransformZScale.Value = manager.GetTransformScaleZ();

            allowStore = true;
        }

        private void TransformZRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ZRotationDisplay.Text = TransformZRotation.Value.ToString("0.00");
			if (allowStore) manager.SetTransformRotationZ((float)TransformZRotation.Value);
		}

        private void TransformYRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            YRotationDisplay.Text = TransformYRotation.Value.ToString("0.00");
            if (allowStore) manager.SetTransformRotationY((float)TransformYRotation.Value);
		}

        private void TransformXRotation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            XRotationDisplay.Text = TransformXRotation.Value.ToString("0.00");
            if (allowStore) manager.SetTransformRotationX((float)TransformXRotation.Value);
        }

        //private void TransformZScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    ZScaleDisplay.Text = TransformZScale.Value.ToString("0.00");
        //    if (allowStore) manager.SetTransformScaleZ((float)TransformZScale.Value);
        //}

        //private void TransformYScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    YScaleDisplay.Text = TransformYScale.Value.ToString("0.00");
        //    if (allowStore) manager.SetTransformScaleY((float)TransformYScale.Value);
        //}

        //private void TransformXScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{  
        //    if (Relative.IsChecked.HasValue && Relative.IsChecked.Value)
        //    {
        //        ChangeAllScale();
        //    }
        //    else
        //    {
        //        XScaleDisplay.Text = TransformXScale.Value.ToString("0.00");
        //        if (allowStore) manager.SetTransformScaleX((float)TransformXScale.Value);
        //    }
        //}
        private void ChangeAllScale()
        {
            if (allowStore)
            {
                manager.SetTransformScaleX(float.Parse(TransformXScaleTxt.Text));
                manager.SetTransformScaleY(float.Parse(TransformXScaleTxt.Text));
            }

            //XScaleDisplay.Text = TransformXScale.Value.ToString("0.00");
            //if (allowStore) manager.SetTransformScaleX((float)TransformXScale.Value);

            //YScaleDisplay.Text = TransformYScale.Value.ToString("0.00");
            //if (allowStore) manager.SetTransformScaleY((float)TransformXScale.Value);

            //ZScaleDisplay.Text = TransformZScale.Value.ToString("0.00");
            //if (allowStore) manager.SetTransformScaleZ((float)TransformXScale.Value);
        }

        private void TransformLayer_TextChanged(object sender, TextChangedEventArgs e)
		{
			Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TransformLayer.Text))
            {
                int result;
                if (int.TryParse(TransformLayer.Text, out result))
                {
                    if (allowStore) manager.SetTransformLayer(result);
                }
            }
		}

        private void Relative_Checked(object sender, RoutedEventArgs e)
        {
            TransformYScaleTxt.IsEnabled = false;

            //YScaleDisplay.IsEnabled = false;
            //TransformYScale.IsEnabled = false;

            //ZScaleDisplay.IsEnabled = false;
            //TransformZScale.IsEnabled = false;
        }

        private void Relative_Unchecked(object sender, RoutedEventArgs e)
        {
            TransformYScaleTxt.IsEnabled = true;
            //YScaleDisplay.IsEnabled = true;
            //TransformYScale.IsEnabled = true;

            //ZScaleDisplay.IsEnabled = true;
            //TransformZScale.IsEnabled = true;
        }

        private void TransformXScaleTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TransformXScaleTxt.Text))
            {
                float result;
                if (float.TryParse(TransformXScaleTxt.Text, out result))
                {
                    if (allowStore) manager.SetTransformScaleX(result);
                }
            }
        }

        private void TransformYScaleTxt_TextChanged(object sender, TextChangedEventArgs e)
        {
            Regex regex = new Regex("[^0-9.-]+");
            if (!regex.IsMatch(TransformYScaleTxt.Text))
            {
                float result;
                if (float.TryParse(TransformYScaleTxt.Text, out result))
                {
                    if (allowStore) manager.SetTransformScaleY(result);
                }
            }
        }
    }
}
