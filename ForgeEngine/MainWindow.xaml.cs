using ForgeEngineCore.Editor;
using ForgeEngineCore.ProjectManagment;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using forge.exprt.cli;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Media;
using System.IO;
using Microsoft.Win32;
using System.Threading;
using System.Windows.Threading;

namespace ForgeEngine
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public sealed class AssetFolder
	{
		public AssetFolder()
		{
			this.Members = new ObservableCollection<Asset>();
		}

		public string FolderName { get; set; }

		public ObservableCollection<Asset> Members { get; set; }
	}

	public sealed class Asset
	{
		public string filename { get; set; }
		public string Path { get; set; }
		public string iconpath { get; set; }
		public string colour { get; set; }
	}

    public sealed class SceneHierarchy
    {
        public ObservableCollection<SceneObject> Items { get; set; }

        public SceneHierarchy()
        {
            this.Items = new ObservableCollection<SceneObject>();
        }
    }

    public sealed class SceneObject
    {
        public SceneObject()
        {
            Members = new List<SceneObject>();
        }

        public string Name { get; set; }
        public string iconpath { get; set; }
        public string colour { get; set; }
        public Entity entity { get; set; }
        public IList<SceneObject> Members { get; set; }
    }

	public partial class MainWindow : Window
    {
		public Project _activeProject;
		public Editor _editor;
		public RenderCanvas _renderCanvas;
		public GameInstance _game;


        public SceneHierarchy sceneHierarchy;

        public bool gamePlaying = false;
        public bool fullscreenEnabled = false;
        public bool fullscreenOpen = false;
		public bool errorVisible = false;
        public bool hidingError = false;
        public bool MuteToggle = false;

		public DispatcherTimer TickTimer;

        public MainWindow(Project project) 
        {
			_activeProject = project;
            InitializeComponent();

            _editor = new Editor(project,this);
			_renderCanvas = new RenderCanvas(_editor,this);
			canvas.Editor = _editor;

			Title = $"Forge Engine - {project.Name}";
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
        {
			_editor.BuildAssetView(AssetView);

			var width = canvas.GetWidth();
			var height = canvas.GetHeight();
			var handle = canvas.GetHandle();

			_game = _renderCanvas.Create((int)handle, (uint)width, (uint)height, false, _activeProject.Path,RenderMode.Editor);

			_editor.SetSceneWrapper(new SceneManagerWrapper(_game.GetSceneManager()));
			_editor.SetEntityWrapper(new EntityManagerWrapper(_game.GetEntityManager()));

			var outputter = new DebugConsole(txt_DebugConsole);
			Console.SetOut(outputter);
			Console.WriteLine("Engine Booted");

            //_editor.BuildSceneView(TreeSceneHierarchy, out sceneHierarchy, _game.GetEntityManager().GetEntities());
            _editor.BuildSceneView(TreeSceneHierarchy, _editor.GetEntityWrapper().GetEntities());
            _editor.LoadInputCollectionFromProjectFile();

			DispatcherTimer dispatcherTimer = new DispatcherTimer();
			dispatcherTimer.Tick += (object obj, EventArgs ev) =>
			{
				if (!gamePlaying && IsActive)
				{
					CameraReposition();
				}
			};
			dispatcherTimer.Interval = new TimeSpan(0, 0, 0,0,16);
			dispatcherTimer.Start();

		}

		private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
            if (e.Key == Key.Delete)
            {
                if (TreeSceneHierarchy.SelectedValue != null)
                {
                    _renderCanvas.Pause();
                    _editor.DeleteSelectedEntity(TreeSceneHierarchy.SelectedValue as SceneObject);
                    ComponentStack.Children.Clear();
                    _editor.RemoveSceneObject(sceneHierarchy, TreeSceneHierarchy.SelectedValue as SceneObject);
                    TreeSceneHierarchy.Items.Refresh();
                    TreeViewItem treeItem =  (TreeViewItem)TreeSceneHierarchy.ItemContainerGenerator.ContainerFromItem(TreeSceneHierarchy.SelectedItem);
                    if (treeItem != null)
                    {
                        treeItem.IsSelected = false;

                    }                
                    _renderCanvas.Resume();
                }
            }

            var keyvalue = 21 + e.Key;
			forge.exprt.cli.Input.SetKeytrue((ushort)keyvalue);	
		}

        private void CameraReposition()
        {
            float x = _game.GetEntityManager().GetCameraPositionX();
            float y = _game.GetEntityManager().GetCameraPositionY();

			if (Keyboard.IsKeyDown(Key.A))
                _game.GetEntityManager().SetCameraPosition(x - 10, y);
            if (Keyboard.IsKeyDown(Key.D))
                _game.GetEntityManager().SetCameraPosition(x + 10, y);
            if (Keyboard.IsKeyDown(Key.W))
                _game.GetEntityManager().SetCameraPosition(x, y + 10);
            if (Keyboard.IsKeyDown(Key.S))
                _game.GetEntityManager().SetCameraPosition(x, y - 10);

            if (Keyboard.IsKeyDown(Key.OemPlus) || Keyboard.IsKeyDown(Key.Add))
                 _game.GetEntityManager().CameraZoomIn(1f);
            if (Keyboard.IsKeyDown(Key.OemMinus) || Keyboard.IsKeyDown(Key.Subtract))
                _game.GetEntityManager().CameraZoomOut(1f);
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
		{
			var keyvalue = 21 + e.Key;
			forge.exprt.cli.Input.SetKeyfalse((ushort)keyvalue);
        }

        private void Storyboard_CurrentTimeInvalidated(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }
        
        public SceneHierarchy GetSceneHierarchy()
        {
            return sceneHierarchy;
        }

        private void ToggleMute_Click(object sender, RoutedEventArgs e)
        {
           //if (!MuteToggle)
           //{
           //    MuteToggle = true;
           //    _game.MuteSound(MuteToggle);
		   //
           //}
           //else
           //{
           //    MuteToggle = false;
           //    _game.MuteSound(MuteToggle);
          //  }

        }
        private void VolumeSliderUpdate(object sender, RoutedEventArgs e)
        {
           // _game.SetVolume(((Slider)sender).Value);
        }

        private void LoadSoundEmitterContainer(object sender, RoutedEventArgs e)
        {
           // Storyboard sb = (Storyboard)SoundContainer.FindResource("ShowSound");
           // sb.Begin();        
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            if (_editor.ActiveScene != null)
            {
                if (!gamePlaying)
                {
                    PlayGame();
                }
                else
                {
                    StopGame();
                }

            }
            else
            {
                ReportError("Please load a scene before attempting to play");
            }

            //Thread.Sleep(200);
            //_editor.BuildSceneView(TreeSceneHierarchy, _game.GetEntityManager().GetEntities());

            ComponentStack.Children.Clear();
            TreeSceneHierarchy.Items.Refresh();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StopGame();
        }

        private void PlayGame()
        {
            //Play game code
            _renderCanvas.Pause();
            _editor.SaveSceneFile();
            _renderCanvas.Resume();

            PlayButton.SetHighlight(true);
            PlayButton.SetIcon("Stop");
            gamePlaying = true;

            var drawCanvas = canvas.GetPanel();

            if (fullscreenEnabled)
            {
                if (!fullscreenOpen)
                {
                    fullscreenOpen = true;
                    Storyboard sb = (Storyboard)FullGameWindow.FindResource("OpenFullGame");
                    sb.Begin();
                    drawCanvas = CanvasFull.GetPanel();
                    _game = _renderCanvas.ReCreate((int)drawCanvas.Handle, (uint)drawCanvas.Width, (uint)drawCanvas.Height, false, _activeProject.Path, RenderMode.Game);

                    _editor.SetSceneWrapper(new SceneManagerWrapper(_game.GetSceneManager()));
                    _editor.SetEntityWrapper(new EntityManagerWrapper(_game.GetEntityManager()));
                }
            }
            else
            {
                _game = _renderCanvas.ReCreate((int)drawCanvas.Handle, (uint)drawCanvas.Width, (uint)drawCanvas.Height, false, _activeProject.Path, RenderMode.Game);
                _editor.SetSceneWrapper(new SceneManagerWrapper(_game.GetSceneManager()));
                _editor.SetEntityWrapper(new EntityManagerWrapper(_game.GetEntityManager()));
            }

            if (toggleGridLock.GetToggleValue())
            {
                _editor.ToggleGridLock();
            }
        }

        private void StopGame()
        {
            //Stop game code
            PlayButton.SetHighlight(false);
            PlayButton.SetIcon("Play");
            gamePlaying = false;

            if (fullscreenOpen)
            {
                fullscreenOpen = false;
                Storyboard sb = (Storyboard)FullGameWindow.FindResource("CloseFullGame");
                sb.Begin();
            }

            var width = canvas.GetWidth();
            var height = canvas.GetHeight();
            var handle = canvas.GetHandle();

            _game = _renderCanvas.ReCreate((int)handle, (uint)width, (uint)height, false, _activeProject.Path, RenderMode.Editor);
            _editor.SetSceneWrapper(new SceneManagerWrapper(_game.GetSceneManager()));
            _editor.SetEntityWrapper(new EntityManagerWrapper(_game.GetEntityManager()));

            if (toggleGridLock.GetToggleValue())
            {
                _editor.ToggleGridLock();
            }
        }

        public void ReportError(string text, string hexColor = "Default")
        {
            ErrorBox.ErrorText = text;
            ErrorBox.ColorOverride = hexColor;

            Storyboard sb = (Storyboard)ErrorBox.FindResource("ShowError");
            sb.Begin();
        }


        private void ErrorBox_Click(object sender, RoutedEventArgs e)
        {
            if (!hidingError)
            {
                Storyboard sb = (Storyboard)ErrorBox.FindResource("HideError");
                sb.Begin();
                hidingError = true;
            }
        }

        private void HideError_Completed(object sender, EventArgs e)
        {
            hidingError = false;
        }

        private void Fullscreen_Click(object sender, RoutedEventArgs e)
        {
            fullscreenEnabled = !fullscreenEnabled;
        }

		private void Window_Closed(object sender, EventArgs e)
		{
			_renderCanvas.Close();
			Application.Current.Shutdown();
		}

        private void Toolbar_CreateEmptyGameObject_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(_editor.GetProject().Path + "\\Assets\\Empty.tga"))
            {
                File.Copy("Projects\\Empty.tga", _editor.GetProject().Path + "\\Assets\\Empty.tga");
            }
            var filename = "Empty.tga";

            _renderCanvas.Pause();
            var entity = _editor._entityManager.CreateEntity();
            _editor._entityManager.AddSpriteComponent(filename);            
            _editor.BuildSceneView(TreeSceneHierarchy, _editor.GetEntityWrapper().GetEntities());
            _renderCanvas.Resume();
        }

        private void Toolbar_CenterCamera_Click(object sender, RoutedEventArgs e)
        {
            _game.GetEntityManager().SetCameraPosition(0, 0);
            _game.GetEntityManager().CameraResetZoom();
        }

        private void Toolbar_InputManager_Click(object sender, RoutedEventArgs e)
        {
            InputCollections inputCollections = new InputCollections(_editor);
            inputCollections.Owner = this;
            inputCollections.ShowDialog();
        }

        private void Toolbar_SaveProject_Click(object sender, RoutedEventArgs e)
        {
			if (_editor.ActiveScene != null)
			{
				_renderCanvas.Pause();
				_editor.SaveSceneFile();
				_renderCanvas.Resume();

                ReportError("Scene Saved!", "Green");
			}
		}

        private void Toolbar_Undo_Click(object sender, RoutedEventArgs e)
        {
            ReportError("Button Not Yet Implemented!");
        }

        private void Toolbar_Redo_Click(object sender, RoutedEventArgs e)
        {
            ReportError("Button Not Yet Implemented!");
        }

        private void Toolbar_VolumeToggle_Click(object sender, RoutedEventArgs e)
        {
            ReportError("Button Not Yet Implemented!");
        }

        //Scene New 

        private void MenuItem_Click(object sender, RoutedEventArgs e)
		{
			var newSceneWindow = new NewSceneWindow(_editor);
			newSceneWindow.Owner = this;
			newSceneWindow.ShowDialog();

			if (_editor.ActiveScene != null)
			{
                if (!newSceneWindow.outputExit && newSceneWindow.outputText != string.Empty)
                {
                    _editor.SaveSceneFile();
                    _editor.BuildAssetView(AssetView);

                    var sceneName = Path.GetFileNameWithoutExtension(_editor.ActiveScene.Name);

                    Container_Render.TitleBar.Text = $"Game - {sceneName}";
                } else if (newSceneWindow.outputText == string.Empty && !newSceneWindow.outputExit)
                {
                    ReportError("Scene name is blank, Failed to add scene");
                }
               
			}
		}

        public MainWindow getthis()
        {
            return this;
        }
		//Scene Save
		private void MenuItem_Click_1(object sender, RoutedEventArgs e)
		{
			if (_editor.ActiveScene != null)
			{
				_renderCanvas.Pause();
				_editor.SaveSceneFile();
				_renderCanvas.Resume();
			}
		}
		//Scene Load 
		private void MenuItem_Click_2(object sender, RoutedEventArgs e)
		{
			_renderCanvas.Pause();
			_editor.LoadSceneFile("DebugSceneTest", "TestScene.fs");
			_renderCanvas.Resume();
		}

        private void MenuItem_BuildSettings_Click(object sender, RoutedEventArgs e)
        {

        }
        
        private void StackPanel_MouseMove(object sender, MouseEventArgs e)
		{
			var stackPanel = (StackPanel)sender;

			var itemName = stackPanel.Children.OfType<TextBlock>().First();

			DataObject data = new DataObject("text", itemName);
			DragDrop.DoDragDrop(stackPanel, data, DragDropEffects.Copy | DragDropEffects.Move);
		} 

		private void Container_Render_Drop(object sender, DragEventArgs e)
		{

			var fileName = e.Data.GetData("text") as TextBlock;
			var assetPath = _activeProject.Path;

			var pos = e.GetPosition(canvas);
			
			if (_editor.ActiveScene == null && Path.GetExtension(fileName.Text) != ".fs")
			{
				ReportError("Please Load A Scene!");
				return;
			}
	
			Input.SetMouseX((int)pos.X);
			Input.SetMouseY((int)pos.Y);

			_renderCanvas.Pause();
			_editor.HandleItemDrop($"{assetPath}//Assets//{fileName.Text}");		
			_editor.BuildSceneView(TreeSceneHierarchy, _editor.GetEntityWrapper().GetEntities());
			_renderCanvas.Resume();
		}

		private void MenuItem_Click_4(object sender, RoutedEventArgs e)
		{
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "Image (*.tga)|*.tga|All files (*.*)|*.*", Multiselect = true
            };

            if (openFileDialog.ShowDialog() == true)
			{
				foreach (string filename in openFileDialog.FileNames)
				{
					var fname = Path.GetFileName(filename);
					if(!File.Exists($"{_activeProject.Path}//Assets//{fname}"))
						File.Copy(filename, $"{_activeProject.Path}//Assets//{fname}");
				}
			}

			_editor.BuildAssetView(AssetView);
		}

		private void ToolbarButton_Click(object sender, RoutedEventArgs e)
		{
			_editor.BuildAssetView(AssetView);
		}

		private void ToolbarButton_Click_1(object sender, RoutedEventArgs e)
		{
			_editor.ToggleGridLock();
		}


        //Scene Hierarchy Drag and drop 
        //------------------------------------------------------------------------------------------------ 
        bool _isDragging = false;
        bool doubleClicked = false;


        private void TreeSceneHierarchy_MouseMove(object sender, MouseEventArgs e)
        {
            if (TreeSceneHierarchy.SelectedValue != null)
            {
                if (!_isDragging && e.LeftButton == MouseButtonState.Pressed)
                {
                    _isDragging = true;
                    DragDrop.DoDragDrop(TreeSceneHierarchy, TreeSceneHierarchy.SelectedValue, DragDropEffects.Move);
                }
                
            }
        }

        private void TreeSceneHierarchy_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (TreeSceneHierarchy.SelectedValue != null)
            {
                TreeSceneHierarchy.SelectedValue.ToString();
                
            }

        }

        private void AssetView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (TreeSceneHierarchy.SelectedValue != null)
            {
                TreeSceneHierarchy.SelectedValue.ToString();

            }
        }

        private void TreeSceneHierarchy_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(SceneObject)))
            {
                e.Effects = DragDropEffects.Move;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void TreeSceneHierarchy_Drop(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(typeof(SceneObject)))
            {
                SceneObject source = (SceneObject)e.Data.GetData(typeof(SceneObject));
                SceneObject target = GetItemAtLocation<SceneObject>(e.GetPosition((UIElement)sender));
                
                if(target == null) target = sceneHierarchy.Items[0];

                if (source != target && source != sceneHierarchy.Items[0])
                {
                    _editor.SetAsParentOfSceneObject(sceneHierarchy, target, source);

                    List<Entity> entities = new List<Entity>();
                    entities.AddRange(_editor.GetEntitiesFromSceneObjects(sceneHierarchy.Items[0]));

                    _editor.GetEntityWrapper().SetEntities(entities);

                    TreeSceneHierarchy.Items.Refresh();
                }
            }
            _isDragging = false;
            ((TreeViewItem) TreeSceneHierarchy.ItemContainerGenerator.ContainerFromItem(TreeSceneHierarchy.SelectedItem)).IsSelected = false;
        }

        T GetItemAtLocation<T>(Point location)
        {
            T foundItem = default(T);
            HitTestResult hitTestResults = VisualTreeHelper.HitTest(TreeSceneHierarchy, location);

            if(hitTestResults.VisualHit is FrameworkElement)
            {
                object dataObject = (hitTestResults.VisualHit as FrameworkElement).DataContext;

                if(dataObject is T)
                {
                    foundItem = (T)dataObject;
                }
            }

            return foundItem;
        }

        private void TestTreeView_Click(object sender, RoutedEventArgs e)
        {
            _editor.RemoveSceneObject(sceneHierarchy, sceneHierarchy.Items[0].Members[0]);
            TreeSceneHierarchy.Items.Refresh();
        }

        public void ExpandTree(TreeView treeView)
        {
            TreeViewItem item = (TreeViewItem)treeView.ItemContainerGenerator.ContainerFromIndex(0);
            item.IsExpanded = true;
        }

		private void MenuItem_Click_5(object sender, RoutedEventArgs e)
		{
			var buildWindow = new BuildSettingsWindow(_editor);
			buildWindow.Owner = this;
			buildWindow.ShowDialog();
		}

        private void TreeSceneHierarchy_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (TreeSceneHierarchy.SelectedValue != null)
            {
                string name = ((SceneObject) TreeSceneHierarchy.SelectedValue).Name;
                //_editor.FindEntityFromHierarchy(name);
                _editor.SelectEntity(true, true);
            }
        }

        private void AddComponentButton_Click(object sender, RoutedEventArgs e)
        {
            if (gamePlaying)
                return;

            var buildWindow = new ComponentSelectionWindow(_editor) {Owner = this};
            buildWindow.ShowDialog();
        }

        private void ToolbarButton_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void PlayButton_Loaded(object sender, RoutedEventArgs e)
        {

        }

		public int GetEditHeight()
		{
			return (int)canvas.GetPanel().Height;
		}

		public int GetEditWidth()
		{
			return (int)canvas.GetPanel().Width;
		}

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            //Create new script
            TextInput textInput = new TextInput("Enter a name for your script:");
            textInput.Owner = _window;
            textInput.ShowDialog();

            if (!textInput.outputExit && textInput.outputText != string.Empty)
            {
                string output = textInput.outputText;
                if(!output.EndsWith(".lua")) output += ".lua";
                string path = _editor.GetProject().Path + "\\Assets\\";
                
                string templateFileLocation = "..\\..\\..\\Assets\\TemplateScript.lua";

                if (System.IO.File.Exists(templateFileLocation))
                {
                    File.Copy(templateFileLocation, path + output, false);
                    Process.Start(path + output);
                }
                else
                {
                    ReportError("ERROR 504: TEMPLATE FILE MISSING");
                }
                
                _editor.BuildAssetView(AssetView);
            }
            else if (textInput.outputText == string.Empty && !textInput.outputExit)
            {
                ReportError("The name entered was blank, so the script was not created.");
            }
        }

        private void UIElement_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
        }

        private void OpenAssetsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            var folderPath = _editor.GetProject().Path + "\\Assets\\";

            Process.Start(folderPath);

            _editor.BuildAssetView(AssetView);
        }

        //---------------------------------------------------------------------------------
    }
}
