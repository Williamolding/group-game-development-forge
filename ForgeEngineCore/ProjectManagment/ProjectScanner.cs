﻿using ForgeEngineCore.Engine;
using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ForgeEngineCore.ProjectManagment
{
    public interface IProjectScanner
	{
		bool ScanForProjects(string projectDirectory);
        void ScanForSavedProjects();
    }

    public sealed class ProjectScanner :  IProjectScanner
    {
        EngineSettingsLoader loader = new EngineSettingsLoader();

        public ProjectScanner()
		{

		}

        public void ScanForSavedProjects()
        {
            for (int i = 0; i < EngineEnviroment.EngineSettings.projectFolders.Count; i++)
            {
                string folder = EngineEnviroment.EngineSettings.projectFolders[i];
                bool success = ScanForProjects(folder);

                if (!success && EngineEnviroment.EngineSettings.projectFolders.Count > 1) 
                {
                    EngineEnviroment.EngineSettings.projectFolders.RemoveAt(i);
                    loader.SaveSettingsFile();
                    i--;
                }
            }

            for (int i = 0; i < EngineEnviroment.EngineSettings.projectFiles.Count; i++)
            {
                string file = EngineEnviroment.EngineSettings.projectFiles[i];
                bool success = AddProject(file);

                if(!success)
                {
                    EngineEnviroment.EngineSettings.projectFiles.Remove(file);
                    i--;
                    loader.SaveSettingsFile();
                }
            }
        }

		public bool ScanForProjects(string projectDirectory)
		{
            if (!Directory.Exists(projectDirectory))
                return false;

			var directories = Directory.GetDirectories(projectDirectory);

			foreach(var directory in directories)
			{
                AddProject(directory);
			}

            return true;
		}

        public bool AddProject(string fileDirectory)
        {
            var project = ValidateProject(fileDirectory);

            if (project != null)
            {
                if (project.Path != fileDirectory)
                {
                    project.Path = fileDirectory;
                    ProjectInformation.Projects.Add(project);
                    ProjectInformation.CreateProjectFile(project.Path, project.Name);
                }
                else
                {
                    ProjectInformation.Projects.Add(project);
                }
                return true;
            }

            return false;
        }

		private Project ValidateProject(string directory)
		{
			try
			{
				XmlSerializer ser = new XmlSerializer(typeof(Project));

				using (FileStream fs = File.OpenRead(directory + "/project.xml"))
				using (XmlReader reader = XmlReader.Create(fs))
				{
					return (Project)ser.Deserialize(reader);
				}
			}
			catch (Exception exception)
			{
				return null;
			}		
		}
    }
}
