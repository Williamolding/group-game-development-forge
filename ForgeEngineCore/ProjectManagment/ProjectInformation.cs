﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Windows;

namespace ForgeEngineCore.ProjectManagment
{
    public sealed class ProjectInformation
    {
		public static List<Project> Projects = new List<Project>();

        public static void CreateProjectFile(string path, string name)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(Project));
            var subReq = new Project();
            subReq.Name = name;
            subReq.Path = path;
            var xml = "";

            using (var sww = new StringWriter())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create(sww, settings))
                {
                    xsSubmit.Serialize(writer, subReq);
                    xml = sww.ToString(); // Your XML
                }
            }
            path = Path.GetFullPath(path) + "\\";
            Directory.CreateDirectory(path + "Assets");
            File.WriteAllText(path + "project.xml", xml);
        }
    }
}
