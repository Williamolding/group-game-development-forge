﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ForgeEngineCore.ProjectManagment
{
    public sealed class Project
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsLocalPath { get; set; }
    }
}
