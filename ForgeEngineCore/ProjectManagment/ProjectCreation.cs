﻿using ForgeEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace ForgeEngineCore.ProjectManagment
{
    class ProjectCreation
    {
        public static void LoadProjects(WrapPanel panel, bool scan)
        {
            if (scan)
            {
                ProjectScanner scanner = new ProjectScanner();
                scanner.ScanForSavedProjects();
            }

            foreach (var project in ProjectInformation.Projects)
            {
                var newProjectView = new TestControl(project.Name);
                newProjectView.project = project;
                newProjectView.Margin = new Thickness(5, 5, 5, 5);
                panel.Children.Add(newProjectView);
            }
        }

        public static void ClearProjects(WrapPanel panel)
        {
            ProjectInformation.Projects.Clear();
            panel.Children.Clear();
        }
    }
}
