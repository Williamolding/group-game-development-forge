﻿
using System;

namespace ForgeEngineCore.Editor
{
	public sealed class SceneManagerWrapper
    {
		private readonly forge.exprt.cli.CliSceneManager _sceneManager;

		public SceneManagerWrapper(forge.exprt.cli.CliSceneManager sceneManager)
		{
			_sceneManager = sceneManager;
		}

		public void SaveSceneToFile(string name, string path)
		{
			_sceneManager.SaveScene(name,path);
		}

		public void LoadSceneFile(string name, string path)
		{
			_sceneManager.LoadScene(name,path);
		}

	}
}
