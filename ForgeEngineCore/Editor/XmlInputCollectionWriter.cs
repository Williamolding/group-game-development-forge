﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using forge.exprt.cli;

namespace ForgeEngineCore.Editor
{
    public static class XmlInputCollectionWriter
    { 
		public static void SeralizeInputCollections(string path , List<InputCollection> inputCollection)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			XmlWriter writer = XmlWriter.Create(path, settings);

			writer.WriteStartElement("InputCollection");
			writer.WriteStartElement("Collections");
			for (int i = 0; i < inputCollection.Count; i++)
			{
				writer.WriteStartElement("Collection");
				writer.WriteAttributeString("name", inputCollection[i].collectionName);
				foreach (var key in inputCollection[i].Keys)
				{
					writer.WriteStartElement("InputMethod");
					writer.WriteAttributeString("type", "key");
					writer.WriteAttributeString("value", key.ToString());
					writer.WriteEndElement();
				}

				foreach (var button in inputCollection[i].Button)
				{
					writer.WriteStartElement("InputMethod");
					writer.WriteAttributeString("type", "button");
					writer.WriteAttributeString("value", button.ToString());
					writer.WriteEndElement();
				}

				foreach (var gamePad in inputCollection[i].GamePadButton)
				{ 
					writer.WriteStartElement("InputMethod");
					writer.WriteAttributeString("type", "gamepad");
					writer.WriteAttributeString("value", gamePad.ToString());
					writer.WriteEndElement();
				}

				writer.WriteEndElement();
			}
			writer.WriteEndElement();
			writer.WriteEndElement();
			writer.Close();
		}
	}

	public static class xmlinputCollectionReader
	{
		public static void readInputCollections(string path)
		{
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.DtdProcessing = DtdProcessing.Parse;
			try
			{
				XmlReader reader = XmlReader.Create(path, settings);


				reader.Read();
				reader.ReadStartElement("InputCollection");
				reader.ReadStartElement("Collections");

				reader.ReadToFollowing("Collection");
				do
				{
					var collectionName = reader.GetAttribute("name");
					Input.RegisterInputCollection(collectionName);

					reader.ReadToFollowing("InputMethod");
					do
					{
						var inputType = reader.GetAttribute("type");
						int inputId = int.Parse(reader.GetAttribute("value"));
						switch (inputType)
						{
							case "button":
								Input.RegisterMouseInput(collectionName, (ushort)inputId);
								break;
							case "key":
								Input.RegisterKeyboardInput(collectionName, (ushort)inputId);
								break;
							case "gamepad":
								Input.RegisterControllerInput(collectionName, (ushort)inputId);
								break;
						}


					} while (reader.ReadToNextSibling("InputMethod"));

				} while (reader.ReadToNextSibling("Collection"));

				reader.Close();
			}
			catch (Exception)
			{
				return;
			}
		}
	}
}
