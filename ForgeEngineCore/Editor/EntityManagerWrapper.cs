using System;
using System.Collections.Generic;
using System.Text;

namespace ForgeEngineCore.Editor
{
    public sealed class EntityManagerWrapper
    {
		private readonly forge.exprt.cli.CliEntityManager entityManager;

		public EntityManagerWrapper(forge.exprt.cli.CliEntityManager entityManager)
		{
			this.entityManager = entityManager;
		}

        // Entity Components
        public void SetEntities(List<forge.exprt.cli.Entity> entities) => entityManager.SetEntities(entities);
        public List<forge.exprt.cli.Entity> GetEntities() => entityManager.GetEntities();

        public forge.exprt.cli.Entity CreateEntity() => entityManager.CreateEntity();
		public forge.exprt.cli.Entity AddSpriteComponent(string filepath) => entityManager.AddSpriteComponent(filepath);
        public void ChangeGameObjectSprite(string filepath) => entityManager.ChangeGameObjectSprite(filepath);

        public List<string> GetEntityComponents() => entityManager.GetAllComponents();
        public string GetEntityName() => entityManager.GetEntityName();
        public void SetEntityName(string name) => entityManager.SetEntityName(name);
		public string FindEntityByPosition() => entityManager.FindEntityByMousePosition();
		public void UpdateEntityPosition() => entityManager.UpdateEntityPosition();
        public void DeleteSelectedEntity(string entityName) => entityManager.DeleteSelectedEntity(entityName);
        public void ToggleGridLock() => entityManager.ToggleGridLock();
        public void FindEntityByName(string name) => entityManager.FindEntityByName(name);  
		public void AddAnimationComponent() => entityManager.AddAnimationComponent();
		public void SetAnimationSpeed(float speed) => entityManager.setAniationSpeed(speed);
		public void SetAnimationFps(float fps) => entityManager.setAnimationFps(fps);
		public void RemoveAnimationComponent() => entityManager.RemoveAnimationComponent();
		public void AddScriptComponent(string path, string filename) => entityManager.AddScriptComponent(path,filename);
		public string GetScriptName() => entityManager.GetScriptName();
		public void RemoveScriptComponent() => entityManager.RemoveScriptComponent();
        public void SetSelectedEntityPosition(int x, int y) => entityManager.SetSelectedEntityPosition(x, y);

        // Circle Collider
        public void AddCircleCollider(float radius) => entityManager.AddCircleCollider(radius);
		public void RemoveCircleCollider() => entityManager.RemoveCircleCollider();
		public void SetCircleCollider(float radius) => entityManager.SetCircleColliderRadius(radius);
		public float GetCircleColliderRadius() => entityManager.getCircleColliderRadius();
        public float GetCircleColliderOffsetX() => entityManager.GetCircleColliderOffsetX();
        public float GetCircleColliderOffsetY() => entityManager.GetCircleColliderOffsetY();
        public void SetCircleColliderOffsetX(float x) => entityManager.SetCircleColliderOffsetX(x);
        public void SetCircleColliderOffsetY(float y) => entityManager.SetCircleColliderOffsetY(y);
        public void CircleSetTrigger(bool it) => entityManager.CircleSetIsTrigger(it);
        public bool CircleIsTrigger() => entityManager.CircleIsTrigger();

        // Animations
		public float GetAnimationSpeed() => entityManager.getAnimationSpeed();
		public float GetAnimationFps() => entityManager.getAnimationFps();

        // Rect Collider
		public void AddRectCollider(float width, float height) => entityManager.AddRectCollider(width,height);
		public void RemoveRectCollider() => entityManager.RemoveRectCollider();
		public void SetRectColliderWidth(float width) => entityManager.SetRectColliderWidth(width);
		public void SetRectColliderHeight(float height) => entityManager.SetRectColliderHeight(height);
		public float GetRectColliderWidth() => entityManager.GetRectColliderWidth();
		public float GetRectColliderHeight() => entityManager.GetRectColliderHeight();
        public float GetRectColliderOffsetX() => entityManager.GetRectColliderOffsetX();
        public float GetRectColliderOffsetY() => entityManager.GetRectColliderOffsetY();
        public void SetRectColliderOffsetX(float x) => entityManager.SetRectColliderOffsetX(x);
        public void SetRectColliderOffsetY(float y) => entityManager.SetRectColliderOffsetY(y);
        public void RectSetIsTrigger(bool it) => entityManager.RectSetIsTrigger(it);
        public bool RectIsTrigger() => entityManager.RectIsTrigger();

        // Sound
		public void AddSoundEmitterComponent(string filename) => entityManager.AddSoundEmitterComponent(filename);
		public void RemoveSoundEmitterComponent() => entityManager.RemoveSoundEmitterComponent();

		public void SetSoundEmitterLooping(bool looping) => entityManager.SetSoundEmitterLooping(looping);
		public void SetSoundEmitterPlayOnLoad(bool playOnLoad) => entityManager.SetSoundEmitterPlayOnLoad(playOnLoad);
		public void SetSoundEmitterPitch(float pitch) => entityManager.SetSoundEmitterPitch(pitch);
		public void SetSoundEmitterVolume(float volume) => entityManager.SetSoundEmitterVolume(volume);
        public void SetSoundEmitterFileName(string filename) => entityManager.SetSoundEmitterFileName(filename);

        public bool GetSoundEmitterLooping() => entityManager.GetSoundEmitterLooping();
        public string GetSoundEmitterFileName() => entityManager.GetSoundEmitterFileName();
        public bool GetSoundEmitterPlayOnLoad() => entityManager.GetSoundEmitterPlayOnLoad();
		public float GetSoundEmitterVolume() => entityManager.GetSoundEmitterVolume();
		public float GetSoundEmitterPitch() => entityManager.GetSoundEmitterPitch();

        // Particle
		public void AddParticleComponent() => entityManager.AddParticleComponent();
		public void RemoveParticleComponent() => entityManager.RemoveParticleComponent();
		public float GetParticleMass() => entityManager.GetParticleMass();
		public void SetParticleMass(float mass) => entityManager.SetParticleMass(mass);
        public void SetCoefficientOfRestitution(float cor) => entityManager.SetCoefficientOfRestitution(cor);
        public float GetCoefficientOfRestitution() => entityManager.GetCoefficientOfRestitution();
        public void SetTerminalVelocity(bool tv) => entityManager.SetTerminalVelocity(tv);
        public bool IsTerminalVelocityOn() => entityManager.IsTerminalVelocityOn();
        public void SetBeingPushed(bool bp) => entityManager.SetBeingPushed(bp);
        public bool BeingPushed() => entityManager.BeingPushed();
        public void AddPushForce() => entityManager.AddPushForce();
        public void RemoveLamForce() => entityManager.RemoveLaminarForce();
        public void RemovePushForce() => entityManager.RemovePushForce();

        // Forces
        public void SetGravity(float gravity) => entityManager.SetGravity(gravity);
        public float GetGravity() => entityManager.GetGravity();
        public void SetFrictionCoF(float cof) => entityManager.SetFrictionCof(cof);
        public float GetFrictionCoF() => entityManager.GetFrictionCof();
        public void SetFrictionNormal(float normal) => entityManager.SetFrictionNormal(normal);
        public float GetFrictionNormal() => entityManager.GetFrictionNormal();
        public void SetFrictionMultiplier(float multiplier) => entityManager.SetFrictionMultiplier(multiplier);
        public float GetFrictionMultiplier() => entityManager.GetFrictionMultiplier();
        public void SetPushForceDrag(float drag) => entityManager.SetPushDrag(drag);
        public float GetPushDrag() => entityManager.GetPushForceDrag();
        public void SetPushingForce(float force) => entityManager.SetPushingForce(force);
        public float GetPushingForce() => entityManager.GetPushingForce();

        // Transform
        public void SetTransformLayer(int layer) => entityManager.SetTransformLayer(layer);
		public void SetTransformRotationX(float x) => entityManager.SetTransformRotationX(x);
		public void SetTransformRotationY(float y) => entityManager.SetTransformRotationY(y);
		public void SetTransformRotationZ(float z) => entityManager.SetTransFormRotationZ(z);

		public float GetTransformLayer() => entityManager.GetTransformLAyer();
		public float GetTransformRotationX() => entityManager.GetTransformRotationX();
		public float GetTransformRotationY() => entityManager.GetTransformRotationY();
		public float GetTransformRotationZ() => entityManager.GetTransformRotationZ();

        public void SetTransformScaleX(float x) => entityManager.SetTransformScaleX(x);
        public void SetTransformScaleY(float y) => entityManager.SetTransformScaleY(y);
        public void SetTransformScaleZ(float z) => entityManager.SetTransFormScaleZ(z);
        public float GetTransformScaleX() => entityManager.GetTransformScaleX();
        public float GetTransformScaleY() => entityManager.GetTransformScaleY();
        public float GetTransformScaleZ() => entityManager.GetTransformScaleZ();

        public void AddTilingToolComponent() => entityManager.AddTilingToolComponent();
        public void SetTilingToolWidth(int x) => entityManager.SetTilingToolWidth(x);
        public void SetTilingToolHeight(int y) => entityManager.SetTilingToolHeight(y);
        public int GetTilingToolWidth() => entityManager.GetTilingToolWidth();
        public int GetTilingToolHeight() => entityManager.GetTilingToolHeight();
        public bool GetTilingToolOverride() => entityManager.GetTilingToolOverride();
        public void SetTilingToolOverride(bool value) => entityManager.SetTilingToolOverride(value);
        public void RemoveTilingToolComponent() => entityManager.RemoveTilingToolComponent();
        public float GetSpriteWidth() => entityManager.GetSpriteWidth();
        public float GetSpriteHeight() => entityManager.GetSpriteHeight();
        public float GetSpriteSize() => entityManager.GetSpriteSize();
    }
}
