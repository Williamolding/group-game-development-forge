﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace ForgeEngineCore.Editor
{
    public static class XmlWriterGame
    {
		public static void WriteGameXml(string path, Scene activeScene, int width, int height)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "\t";
			XmlWriter writer = XmlWriter.Create(path, settings);


			writer.WriteStartElement("Game");
				writer.WriteAttributeString("Width", width.ToString());
				writer.WriteAttributeString("Height", height.ToString());
				writer.WriteStartElement("Scenes");
					for (int i = 0; i < 1; i++)
					{
						writer.WriteStartElement("Scene");
						writer.WriteAttributeString("id", i.ToString());
						writer.WriteAttributeString("name",Path.GetFileName(activeScene.Name));
						writer.WriteAttributeString("path", $"./Assets/{Path.GetFileName(activeScene.Name)}");
						writer.WriteEndElement();
					}
				writer.WriteEndElement();
			writer.WriteStartElement("InputCollection");
			writer.WriteEndElement();
			writer.WriteEndElement();

			writer.Close();
		}
	}
}
