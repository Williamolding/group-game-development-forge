﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using forge.exprt.cli;
using ForgeEngine;

namespace ForgeEngineCore.Editor
{
    public enum RenderMode
    {
            Editor,
            Game
    }

    public sealed class RenderCanvas
    {
		private Thread _gameThread;
		private Editor _editor;
        private MainWindow _window;
		private forge.exprt.cli.GameInstance _game;

        public event EventHandler<GameInstance> OnLoad;

        public RenderCanvas(Editor editor, MainWindow window)
		{
			_editor = editor;
            _window = window;

        }

		public forge.exprt.cli.GameInstance Create(int handle, uint width, uint height, bool fullscreen, string path, RenderMode mode)
		{
            if(mode == RenderMode.Editor)
			    _game = new forge.exprt.cli.GameInstance(handle, width, height, fullscreen, path,true);
            else
                _game = new forge.exprt.cli.GameInstance(handle, width, height, fullscreen, path, false);

            _gameThread = new Thread(delegate ()
			{
				var sceneManager = _game.GetSceneManager();
				var currentScene = _editor.ActiveScene;

				if(currentScene != null)
					sceneManager.LoadScene(currentScene.Name, currentScene.Path);

                _editor.LoadInputCollectionFromProjectFile();
				_game.Run();
				_game.Destory();
			});

			_gameThread.Start();

			return _game;
		}

		public forge.exprt.cli.GameInstance ReCreate(int handle, uint width, uint height, bool fullscreen, string path, RenderMode mode)
		{
			Close();
			return Create(handle, width, height, fullscreen, path, mode);
		}

		public void Close()
		{
			_game.Stop();

			while (_gameThread.ThreadState == ThreadState.Running)
			{
				Thread.Sleep(1);
			}
		}

		public void Pause()
		{
			_game.Pause();
		}

		public void Resume()
		{
			_game.Resume();
		}
	}
}
