using ForgeEngine;
using ForgeEngineCore.ProjectManagment;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using forge.exprt.cli;
using System.Collections;
using MaterialDesignThemes.Wpf;
using System.Linq;
using System.IO.Compression;
using System.Threading;

namespace ForgeEngineCore.Editor
{
	public sealed class Editor
    {
		private readonly Project _project;
		private SceneManagerWrapper _sceneManager;
		public EntityManagerWrapper _entityManager;
		private MainWindow _window;

		public Scene ActiveScene;
		public string image = "Image";
		public string audio = "MusicBox";
		public string script = "Script";
        public string sceneObject = "CropFree";
		public string scene = "PackageVariantClosed";


		public Editor(Project project, MainWindow window)
		{
			_project = project;
			_window = window;	
		}

		public Project GetProject()
		{
			return _project;
		}
        
        public MainWindow GetWindow()
        {
            return _window;
        }

		public void SetSceneWrapper(SceneManagerWrapper sceneWarpper)
		{
			_sceneManager = sceneWarpper;
		}

		public void SetEntityWrapper(EntityManagerWrapper entityManager)
		{
			_entityManager = entityManager;
		}

        public EntityManagerWrapper GetEntityWrapper()
        {
            return _entityManager;
        }

		public void BuildAssetView(TreeView treeView)
        {
            bool sorted = true;

            List<AssetFolder> assets = new List<AssetFolder>();

            var assetFolder = Directory.EnumerateFiles(_project.Path + "\\Assets");
            var folder = new AssetFolder { FolderName = "Assets" };

            var subfolder1 = new AssetFolder { FolderName = "Sprites" };
            var subfolder2 = new AssetFolder { FolderName = "Audio" };
            var subfolder3 = new AssetFolder { FolderName = "Scripts" };
			var subfolder4 = new AssetFolder { FolderName = "Scenes" };

			foreach (var asset in assetFolder)
            {
                var filename = Path.GetFileName(asset);

                string imagepath = "";
                string colour = "";

                if (Path.GetExtension(asset).ToLower() == ".tga")
                {
                    imagepath = image;
                    colour = "#FFED4040";
                    if (sorted) AddToAssetFolder(subfolder1, filename, asset, imagepath, colour);
                }

                if (Path.GetExtension(asset).ToLower() == ".wav" || Path.GetExtension(asset).ToLower() == ".mp3")
                {
                    imagepath = audio;
                    colour = "#FF5F40ED";
                    if (sorted) AddToAssetFolder(subfolder2, filename, asset, imagepath, colour);
                }

                if (Path.GetExtension(asset).ToLower() == ".lua")
                {
                    imagepath = script;
                    colour = "#FFE5ED40";
                    if (sorted) AddToAssetFolder(subfolder3, filename, asset, imagepath, colour);
                }

				if (Path.GetExtension(asset).ToLower() == ".fs")
				{
					imagepath = scene;
					colour = "#FFE5ED40";
					if (sorted) AddToAssetFolder(subfolder4, filename, asset, imagepath, colour);
				}

                if (!sorted) AddToAssetFolder(folder, filename, asset, imagepath, colour);
            }

            if (sorted)
            {
                assets.Add(subfolder1);
                assets.Add(subfolder2);
                assets.Add(subfolder3);
				assets.Add(subfolder4);
			}
            else
            {
                assets.Add(folder);
            }

            treeView.ItemsSource = assets;

			ExpandTree(treeView);
		}

        void AddToAssetFolder(AssetFolder folder, string filename, string asset, string imagepath, string colour)
        {
            folder.Members.Add(new Asset
            {
                filename = filename,
                Path = asset,
                iconpath = imagepath,
                colour = colour
            });
        }

        public void BuildSceneView(TreeView treeView, List<Entity> allEntities)
        {
            _window.sceneHierarchy = new SceneHierarchy();

            var root = new SceneObject { Name = "root" };

            string imagepath = sceneObject;
            string colour = "#FFED4040";

            foreach (Entity e in allEntities)
            {
                AddToSceneObject(root, e, imagepath, colour);

                int count = SceneObjectCount(root.Members);
                AddSceneChildren(root.Members[count - 1], e.children, imagepath, colour);
            }

            _window.sceneHierarchy.Items.Add(root);
            treeView.ItemsSource = _window.sceneHierarchy.Items[0].Members;
            //_window.ExpandTree(treeView);
        }

        int SceneObjectCount(IList<SceneObject> list)
        {
            int count = 0;
            if (list is ICollection)
            {
                count = ((ICollection)list).Count;
            }
            return count;
        }

        void AddSceneChildren(SceneObject parent, List<Entity> children, string imagepath, string colour)
        {
            foreach (Entity e in children)
            {
                AddToSceneObject(parent, e, imagepath, colour);
                int count = SceneObjectCount(parent.Members);
                AddSceneChildren(parent.Members[count - 1], e.children, imagepath, colour);
            }
        }

        void AddToSceneObject(SceneObject parent, Entity entity, string imagepath, string colour)
        {
            SceneObject sceneObject = new SceneObject();
            sceneObject.Name = entity.name;
            sceneObject.iconpath = imagepath;
            sceneObject.colour = colour;
            sceneObject.entity = entity;

            parent.Members.Add(sceneObject);
        }

		public void SaveInputCollectionToProjectFile()
		{
			var path = $"{_project.Path}/InputCollections.xml";
			var inputCollection = forge.exprt.cli.Input.GetCollectionState();
			XmlInputCollectionWriter.SeralizeInputCollections(path,inputCollection);
		}
		public void LoadInputCollectionFromProjectFile()
		{
			var path = $"{_project.Path}/InputCollections.xml";
			xmlinputCollectionReader.readInputCollections(path);
		}

        public void SetParent(SceneObject parent, SceneObject child)
        {
            if (child.entity.parent != null) //Find old parent and remove child (self) from its children
                child.entity.parent.children.Remove(child.entity);

            if (parent.entity != null)
            {
                child.entity.parent = parent.entity; //Set parent
                parent.entity.children.Add(child.entity); //Add child to new parent
            }
            else
            {
                child.entity.parent = null;
            }
        }

        public void SaveEntities(SceneHierarchy sceneHierarchy)
        {

        }

        public void SetAsParentOfSceneObject(SceneHierarchy sceneHierarchy, SceneObject toParent, SceneObject toChild)
        {
            bool success = RemoveSceneObject(sceneHierarchy, toChild);

            if (!success)
            {
                _window.ReportError("Could not move gameObject");
                return;
            }
            toParent.Members.Add(toChild);

            SetParent(toParent, toChild);
        }

        public bool RemoveSceneObject(SceneHierarchy sceneHierarchy, SceneObject target)
        {
            bool success = false;

            int i = sceneHierarchy.Items.IndexOf(target);
            if (i != -1)
            {
                sceneHierarchy.Items.RemoveAt(i);
                success = true;
            }
            else
            {
                foreach (SceneObject child in sceneHierarchy.Items)
                {
                    success = RemoveSceneObject(child, target);
                    if (success) break;
                }
            }
            return success;
        }

        private bool RemoveSceneObject(SceneObject parent, SceneObject target)
        {
            bool success = false;

            int i = parent.Members.IndexOf(target);
            if (i != -1)
            {
                parent.Members.RemoveAt(i);
                success = true;
            }
            else
            {
                foreach (SceneObject child in parent.Members)
                {
                    success = RemoveSceneObject(child, target);
                    if (success) break;
                }
            }
            return success;
        }

		public void ExpandTree(TreeView treeView)
		{
			for (int i = 0; i < treeView.ItemContainerGenerator.Items.Count; i++)
			{
				TreeViewItem item = (TreeViewItem)treeView.ItemContainerGenerator.ContainerFromIndex(i);
				item.IsExpanded = true;
			}
		}

		//move this at some point
		public void CreateSceneFile(string name)
		{
			var path = $"{_project.Path}/Assets/{name}.fs";
			File.Create(path);

			ActiveScene = new Scene
			{
				Name = name,
				Path = path
			};
		}

		public void LoadSceneFile(string name, string path)
		{
			_sceneManager.LoadSceneFile(name,path);

			ActiveScene = new Scene
			{
				Name = name,
				Path = path
			};
		}

        public void SaveSceneFile(string name = "", string path = "")
        {
            if (ActiveScene == null)
            {
                _window.ReportError("There is no scene loaded, so the current scene could not be saved");
            }
            else
            {
                if (name != "" && path != "")
                    _sceneManager.SaveSceneToFile(name, path);
                else
                    _sceneManager.SaveSceneToFile(ActiveScene.Name, ActiveScene.Path);
            }
        }

		public void SelectEntity(bool clearComponentStack, bool fromHierarchy = false)
        {
            string oldEntity = "a";
            string newEntity = "b";

            if (!fromHierarchy)
            {
                if (_window.TreeSceneHierarchy.SelectedValue != null)
                {
                    oldEntity = (_window.TreeSceneHierarchy.SelectedValue as SceneObject).Name;
                }
                newEntity = _entityManager.FindEntityByPosition();

                if (newEntity == null)
                {
                    SceneObject s = _window.TreeSceneHierarchy.SelectedValue as SceneObject;
                    TreeViewItem item = (_window.TreeSceneHierarchy.ItemContainerGenerator.ContainerFromItem(s) as TreeViewItem);
                    _window.TreeSceneHierarchy.Focus();
                    if (item != null) item.IsSelected = false;
                }
            }
            else
            {
                newEntity = (_window.TreeSceneHierarchy.SelectedValue as SceneObject).Name;
                GetEntityWrapper().FindEntityByName(newEntity);
            }

            if (clearComponentStack && oldEntity != newEntity) _window.ComponentStack.Children.Clear();



            if (ActiveScene != null && newEntity != null)
            {
                
                FindEntityInHierarchy(_window.GetSceneHierarchy().Items[0], newEntity);
                
                List<string> components = GetEntityWrapper().GetEntityComponents();

                if (clearComponentStack && oldEntity != newEntity)
                {
                    if (components.Count > 0) AddComponentGameObjectUI();

                    foreach (string component in components)
                    {
                        switch (component)
                        {
                            case "TransformComponent":
                                AddComponentTransformUI();
                                break;
                            case "SpriteRendererComponent":
                                AddComponenetSpriteRendererUI();
                                break;
                            case "AnimationComponent":
                                AddAnimationUI();
                                break;
                            case "ParticleComponent":
                                AddComponent_Particle();
                                break;
                            case "CircleColliderComponent":
                                AddCircleColliderUI();
                                break;
                            case "RectColliderComponent":
                                AddRectColliderUI();
                                break;
                            case "ScriptComponent":
                                var scriptName = GetEntityWrapper().GetScriptName();
                                AddComponentScriptUI(scriptName);
                                break;
                            case "SoundEmitterComponent":
                                AddComponent_SoundEmitterUI();
                                break;
                            case "TilingToolComponent":
                                AddComponent_TilingTool();
                                break;
                        }
                    }
                }
            }
        }

        public void UpdateTileTool(int width, int height)
        {
            ////Find selected entity
            //SceneObject selectedEntity = (_window.TreeSceneHierarchy.SelectedValue as SceneObject);
            //GetEntityWrapper().FindEntityByName(selectedEntity.Name);

            ////delete all children in c# and c++
            //while(selectedEntity.Members.Count > 0)
            //{
            //    SceneObject child = selectedEntity.Members[0];
            //    _entityManager.DeleteSelectedEntity(child.Name);
            //    DeleteSelectedEntity(child);
            //    RemoveSceneObject(_window.sceneHierarchy, child);
            //}

            ////Create new children based on WxH
            //for(int w = 0; w < width; w++)
            //{
            //    for (int h = 0; h < height; h++)
            //    {
            //        if (!(w == 0 && h == 0))
            //        {
            //            Entity entity = _entityManager.CreateEntity();
            //            _entityManager.AddSpriteComponent("ground.tga"); //Get selectedEntity sprite

            //            SceneObject newSceneObject = new SceneObject();
            //            newSceneObject.entity = entity;
            //            newSceneObject.Name = entity.name;
            //            newSceneObject.iconpath = sceneObject;
            //            newSceneObject.colour = "#FFED4040";
            //            _window.sceneHierarchy.Items[0].Members.Add(newSceneObject);
            //            SetAsParentOfSceneObject(_window.sceneHierarchy, selectedEntity, newSceneObject);

            //            //Update C++ Entities
            //            List<Entity> entities = new List<Entity>();
            //            entities.AddRange(GetEntitiesFromSceneObjects(_window.sceneHierarchy.Items[0]));
            //            _entityManager.SetEntities(entities);

            //            _entityManager.FindEntityByName(entity.name);
            //            _entityManager.SetSelectedEntityPosition(64 * w, 64 * h); //Get selectedEntity mesh size
            //        }
            //    }
            //}

            //_entityManager.FindEntityByName(selectedEntity.Name);
            //SelectEntity(false, true);
            //_window.TreeSceneHierarchy.Items.Refresh();
        }

        public List<Entity> GetEntitiesFromSceneObjects(SceneObject sceneObject)
        {
            List<Entity> list = new List<Entity>();

            foreach (SceneObject o in sceneObject.Members)
            {
                if (o.entity != null) list.Add(o.entity);
                list.AddRange(GetEntitiesFromSceneObjects(o));
            }

            return list;
        }

        public void DeleteSelectedEntity(SceneObject target)
        {   
            //foreach (SceneObject child in target.Members)
            //{
            //    _entityManager.DeleteSelectedEntity(child.Name);
            //    //plz forgive us 
            //    DeleteSelectedEntity(child);
            //}
            _entityManager.DeleteSelectedEntity(target.Name);
        }

        public void AddComponent_Animation()
        {
			_entityManager.AddAnimationComponent();
			AddAnimationUI();
		}

		public void AddAnimationUI()
		{
			Component_Animation component_Animation = new Component_Animation(this, _window.ComponentStack.Children);
			_window.ComponentStack.Children.Add(component_Animation);
		}

		public void AddComponent_RectCollider()
		{
            _entityManager.AddRectCollider(_entityManager.GetSpriteSize() / 2, _entityManager.GetSpriteSize() / 2);
			AddRectColliderUI();
		}
		public void AddRectColliderUI()
		{
			Component_RectCollider component_rectCollider = new Component_RectCollider(this, _window.ComponentStack.Children);
			_window.ComponentStack.Children.Add(component_rectCollider);
		}

		public void AddComponent_CircleCollider()
		{
            float smallest = _entityManager.GetSpriteWidth() * 2;

            if (smallest > _entityManager.GetSpriteHeight() * 2)
                smallest = _entityManager.GetSpriteHeight() * 2;

            _entityManager.AddCircleCollider(smallest);
			AddCircleColliderUI();
		}
		public void AddCircleColliderUI()
		{
			Component_CircleCollider component_CircleCollider = new Component_CircleCollider(this, _window.ComponentStack.Children);
			_window.ComponentStack.Children.Add(component_CircleCollider);
		}

        public void AddComponent_TilingTool()
        {
            _entityManager.AddTilingToolComponent();
            AddTilingToolUI();
        }
        public void AddTilingToolUI()
        {
            Component_TilingTool component_TilingTool = new Component_TilingTool(this, _window.ComponentStack.Children);
            _window.ComponentStack.Children.Add(component_TilingTool);
        }
        
        public void AddComponent_SoundEmitter()
		{
			string name = "";

			TextInput textInput = new TextInput("Find a Sound by name:");
			textInput.Owner = _window;
			textInput.ShowDialog();

            if (!textInput.outputExit && textInput.outputText != string.Empty)

            {
                name = textInput.outputText;
                string path = _project.Path + "\\Assets\\";
                var fileCheck = $"{path}{name}";
                var finalPath = $"{name}";
                if (File.Exists(fileCheck))
                {
                    _entityManager.AddSoundEmitterComponent(finalPath);
                    AddComponent_SoundEmitterUI();
                }
                else
                {
                    (_window as MainWindow).ReportError("The file entered does not exist, so the collection was not updated");

                }
            }
            else if (textInput.outputText == string.Empty && !textInput.outputExit)
            { 
                (_window as MainWindow).ReportError("The name entered was blank, so the collection was not updated");
            }

        }

		public void AddComponent_SoundEmitterUI()
		{
			SoundEmitter soundEmitterComponent = new SoundEmitter(this, _window.ComponentStack.Children);
			_window.ComponentStack.Children.Add(soundEmitterComponent);
		}

		public void AddComponent_Particle()
		{
			_entityManager.AddParticleComponent();
			AddComponent_ParticleUI();
		}

		public void AddComponent_ParticleUI()
		{
			Component_Particle particleComponenet = new Component_Particle(this, _window.ComponentStack.Children);
			_window.ComponentStack.Children.Add(particleComponenet);
		}

		public void AddComponenetSpriteRendererUI()
		{

        }

        public void AddComponentGameObjectUI()
        {
            Component_GameObject gameObjectComponent = new Component_GameObject(this, _window.ComponentStack.Children);
            _window.ComponentStack.Children.Add(gameObjectComponent);
        }

        public void AddComponentTransformUI()
        {
            Component_Transform transformComponent = new Component_Transform(this, _window.ComponentStack.Children);
            _window.ComponentStack.Children.Add(transformComponent);
        }


        public void AddAndSearchComponentScript()
        {
            string path = _project.Path + "\\Assets\\";
            string name = "";
            
            string[] scripts = Directory.GetFiles(path, "*.lua", SearchOption.AllDirectories);

            for(int i = 0; i < scripts.Length; i++)
            {
                scripts[i] = Path.GetFileName(scripts[i]);
            }
            
            DropDownInput dropDownInput = new DropDownInput("Find a script by name:", _window, scripts);

            if (!dropDownInput.wasCancelled)
            {
                name = dropDownInput.output;
                if (!name.Contains(".lua")) name += ".lua";

                var finalPath = $"{path}{name}";
                if (File.Exists(finalPath))
                {
                    _entityManager.AddScriptComponent(path, name);
                    AddComponentScriptUI(name);
                    _window.ReportError("Script added", "Green");
                }
                else
                {

                    (_window as MainWindow).ReportError("The file chosen does not exist, so the component was not added");
                }
            }
        }

        //public void AddAndSearchComponentScript()
        //{
        //    string path = _project.Path + "\\Assets\\";
        //    string name = "";

        //    TextInput textInput = new TextInput("Find a script by name:");
        //    textInput.Owner = _window;
        //    textInput.ShowDialog();

        //    if (!textInput.outputExit && textInput.outputText != string.Empty)
        //    {
        //        name = textInput.outputText;

        //        if (!name.Contains(".lua"))
        //            name = name + ".lua";

        //        var finalPath = $"{path}{name}";

        //        if (File.Exists(finalPath))
        //        {
        //            _entityManager.AddScriptComponent(path, name);

        //            AddComponentScriptUI(name.Substring(0, name.Length - 4));
        //        }
        //        else
        //        {
        //            (_window as MainWindow).ReportError("The file entered does not exist, so the collection was not updated");
        //        }
        //    }
        //    else if (textInput.outputText == string.Empty && !textInput.outputExit)
        //    {
        //        (_window as MainWindow).ReportError("The name entered was blank, so the collection was not updated");
        //    }
        //}

        public void AddComponentScriptUI(string name)
        {
            Component_Script component_Script = new Component_Script(this, _window.ComponentStack.Children,  name);
            _window.ComponentStack.Children.Add(component_Script);
        }

        public void AddComponent_SoundEmitter( string Filename)
        {
            GetEntityWrapper().AddSoundEmitterComponent(Filename);
            SoundEmitter component_SoundEmitter = new SoundEmitter(this,_window.ComponentStack.Children);
            _window.ComponentStack.Children.Add(component_SoundEmitter);
        }

        public void SearchAndAddComponent_SoundEmitter()
        {
            string path = _project.Path + "\\Assets\\";
            string name = "";

            var files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".mp3") || s.EndsWith(".wav"));
            string[] soundFiles = files.ToArray();
            for (int i = 0; i < soundFiles.Length; i++)
            {
                soundFiles[i] = Path.GetFileName(soundFiles[i]);
            }

            DropDownInput dropDownInput = new DropDownInput("Find a Sound by name:", _window, soundFiles);

            if (!dropDownInput.wasCancelled)
            {
                name = dropDownInput.output;

                var finalPath = $"{path}{name}";
                if (File.Exists(finalPath))
                {
                    //_entityManager.AddScriptComponent(path, name);
                    AddComponent_SoundEmitter(name);
                    _window.ReportError("Sound added", "Green");
                }
                else
                {
                    (_window as MainWindow).ReportError("The file chosen does not exist, so the component was not added");
                }
            }
        }

        public void UpdateEntityPosition()
		{
			_entityManager.UpdateEntityPosition();
		}

		public void ToggleGridLock()
		{
			_entityManager.ToggleGridLock();
		}
	

		public void FindEntityInHierarchy(SceneObject parent, string name)
		{
			foreach (SceneObject child in parent.Members)
			{
				if (child.Name == name)
				{
					(_window.TreeSceneHierarchy.ItemContainerGenerator.ContainerFromItem(child) as TreeViewItem).IsSelected = true;
					FindEntityInHierarchy(child, name);
					return;
				}
			}
		}

		public void FindEntityFromHierarchy(string name)
        {
            _entityManager.FindEntityByName(name);
		}

		public void BuildGame(bool overrideSize, int width, int height)
		{
			if (ActiveScene == null)
				return;

			var buildDirectory = $"{_project.Path}/Build";
			var assetDirectory = $"{_project.Path}/Assets";
			var buildAssetDirectory = $"{buildDirectory}/Assets";

			if (!Directory.Exists(buildDirectory))
			{
				Directory.CreateDirectory(buildDirectory);
			}

			if (!Directory.Exists(buildAssetDirectory))
			{
				Directory.CreateDirectory(buildAssetDirectory);
			}

			foreach (string newPath in Directory.GetFiles(assetDirectory, "*.*", SearchOption.AllDirectories))
			{
				File.Copy(newPath, newPath.Replace(assetDirectory, buildAssetDirectory), true);
				Console.WriteLine($"Copied File : {Path.GetFileName(newPath)}");
			}

			var assembly = System.Reflection.Assembly.GetExecutingAssembly();
			using (Stream stream = assembly.GetManifestResourceStream("ForgeEngine.ForgeLauncher.zip"))
			{
				using (FileStream bw = new FileStream("launcher.temp", FileMode.Create))
				{
					while (stream.Position<stream.Length)
					{
						byte[] bits = new byte[stream.Length];
						stream.Read(bits, 0, (int) stream.Length);
						bw.Write(bits, 0, (int) stream.Length);
					}
				}
				stream.Close();
			}
		
			foreach (var file in Directory.EnumerateFiles(buildDirectory))
			{
				File.Delete(file);
			}

			//Put all writes after this

			if (File.Exists($"{_project.Path}/InputCollections.xml"))
			{
				File.Copy($"{_project.Path}/InputCollections.xml", $"{buildDirectory}/InputCollections.xml", true);
			}


			if(overrideSize)
				XmlWriterGame.WriteGameXml($"{buildDirectory}/Game.xml", ActiveScene, width, height);
			else
				XmlWriterGame.WriteGameXml($"{buildDirectory}/Game.xml", ActiveScene, _window.GetEditWidth(), _window.GetEditHeight());

			ZipFile.ExtractToDirectory("launcher.temp", buildDirectory);

			if (File.Exists($"{buildDirectory}/ForgeLauncher.exe"))	
			{
				var exectuableName = _project.Name.Replace(" ", "");
				File.Move($"{buildDirectory}/ForgeLauncher.exe", $"{buildDirectory}/{exectuableName}.exe");
			}

			File.Delete("launcher.temp");
		}

		public void HandleItemDrop(string filepath)
		{
			var extension = Path.GetExtension(filepath);
			var filename = Path.GetFileName(filepath);

			switch (extension)
			{
				case ".fs":
					LoadSceneFile(filepath, filepath);
					var sceneName = Path.GetFileNameWithoutExtension(ActiveScene.Name);
					_window.Container_Render.TitleBar.Text = $"Game - {sceneName}";
					break;
				case ".tga":
					var entity = _entityManager.CreateEntity();
					_entityManager.AddSpriteComponent(filename);

                    //Component
					break;
				default:
					_window.ReportError("File Not Supported");
					break;		
			}
		}


        //_entityManager.AddSpriteComponent(filename);
        public void ChangeSprite(string filename)
        {
            _entityManager.ChangeGameObjectSprite(filename);
        }
	}
}

                    