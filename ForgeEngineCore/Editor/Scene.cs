﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ForgeEngineCore.Editor
{
    public sealed class Scene
    {
		public string Name { get; set; }
		public string Path { get; set; }
	}
}
