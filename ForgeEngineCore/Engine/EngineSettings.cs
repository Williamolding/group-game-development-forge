﻿using System.Collections.Generic;

namespace ForgeEngineCore.Engine
{
    public sealed class EngineSettings
    {
        //public string ProjectDirectory { get; set; } = "./Projects";
        public List<string> projectFolders { get; set; } = new List<string>();
        public List<string> projectFiles { get; set; } = new List<string>();
    }
}
