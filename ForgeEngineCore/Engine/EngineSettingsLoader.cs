﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ForgeEngineCore.Engine
{
	public interface IEngineSettingsLoader
	{
		bool LoadEngineSettings();
	}

    public sealed class EngineSettingsLoader : IEngineSettingsLoader
    {
		public EngineSettingsLoader()
		{

		}

		public bool LoadEngineSettings()
		{
            bool existsAtBoot = File.Exists("./ForgeSettings.xml");
            if (!existsAtBoot)
			{
				CreateSettingsFile();
            }

			ReadSettingsFile();
            return true;
		}

		private void CreateSettingsFile()
		{
			XmlSerializer xsSubmit = new XmlSerializer(typeof(EngineSettings));
			var subReq = new EngineSettings();
            subReq.projectFolders.Add("./Projects");
            var xml = "";

			using (var sww = new StringWriter())
			{
				XmlWriterSettings settings = new XmlWriterSettings();
				settings.OmitXmlDeclaration = true;
				settings.Indent = true;

				using (XmlWriter writer = XmlWriter.Create(sww, settings))
				{
					xsSubmit.Serialize(writer, subReq);
					xml = sww.ToString(); // Your XML
				}
			}

			File.WriteAllText("./ForgeSettings.xml", xml);
		}

        public void SaveSettingsFile()
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(EngineSettings));
            var subReq = EngineEnviroment.EngineSettings;
            var xml = "";

            using (var sww = new StringWriter())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create(sww, settings))
                {
                    xsSubmit.Serialize(writer, subReq);
                    xml = sww.ToString(); // Your XML
                }
            }

            File.WriteAllText("./ForgeSettings.xml", xml);
        }

        private void ReadSettingsFile()
		{
            XmlSerializer ser = new XmlSerializer(typeof(EngineSettings));

			using (FileStream fs = File.OpenRead("./ForgeSettings.xml"))
			using (XmlReader reader = XmlReader.Create(fs))
			{
                EngineEnviroment.EngineSettings = (EngineSettings)ser.Deserialize(reader);
            }
        }
	}
}
