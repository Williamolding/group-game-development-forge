﻿using ForgeEngineCore.ProjectManagment;
using System;

namespace ForgeEngineCore.Engine
{
	public interface IEngineBooter
	{
		void Boot();
	}

	public sealed class EngineBooter
    {
		private readonly IProjectScanner _projectScanner;
		private readonly IEngineSettingsLoader _engineSettingsLoader;

		public EngineBooter(IEngineSettingsLoader engineSettingsLoader, IProjectScanner projectScanner)
		{
			_projectScanner = projectScanner;
			_engineSettingsLoader = engineSettingsLoader;
		}

		public void Boot()
		{
			try
			{
				_engineSettingsLoader.LoadEngineSettings();
                _projectScanner.ScanForSavedProjects();
            }
			catch(Exception)
			{

			}
		}
    }
}
